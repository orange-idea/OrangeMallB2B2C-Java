package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysArea;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysAreaService extends IService<SysArea> {

}
