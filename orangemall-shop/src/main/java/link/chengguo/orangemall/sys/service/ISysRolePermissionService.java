package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysRolePermission;

/**
 * <p>
 * 后台用户角色和权限关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
