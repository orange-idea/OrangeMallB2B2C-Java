package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysEmailConfig;
import link.chengguo.orangemall.sys.vo.EmailVo;
import org.springframework.scheduling.annotation.Async;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-11-30
 */
public interface ISysEmailConfigService extends IService<SysEmailConfig> {

    /**
     * 发送邮件
     *
     * @param emailVo     邮件发送的内容
     * @param emailConfig 邮件配置
     * @throws Exception /
     */
    @Async
    void send(EmailVo emailVo, SysEmailConfig emailConfig) throws Exception;
}
