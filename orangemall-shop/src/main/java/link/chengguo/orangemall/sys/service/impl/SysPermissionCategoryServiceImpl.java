package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysPermissionCategory;
import link.chengguo.orangemall.sys.mapper.SysPermissionCategoryMapper;
import link.chengguo.orangemall.sys.service.ISysPermissionCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Service
public class SysPermissionCategoryServiceImpl extends ServiceImpl<SysPermissionCategoryMapper, SysPermissionCategory> implements ISysPermissionCategoryService {
}
