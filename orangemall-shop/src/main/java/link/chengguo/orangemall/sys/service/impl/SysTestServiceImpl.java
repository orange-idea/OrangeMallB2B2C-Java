package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysTest;
import link.chengguo.orangemall.sys.mapper.SysTestMapper;
import link.chengguo.orangemall.sys.service.ISysTestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author orangemall
 * @date 2019-12-02
 */
@Service
public class SysTestServiceImpl extends ServiceImpl<SysTestMapper, SysTest> implements ISysTestService {


    @Resource
    private SysTestMapper sysTestMapper;


}
