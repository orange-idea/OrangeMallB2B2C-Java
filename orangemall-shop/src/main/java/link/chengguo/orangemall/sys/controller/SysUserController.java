package link.chengguo.orangemall.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.sys.entity.SysPermission;
import link.chengguo.orangemall.sys.entity.SysRole;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.sys.entity.SysUserRole;
import link.chengguo.orangemall.sys.mapper.SysPermissionMapper;
import link.chengguo.orangemall.sys.service.ISysPermissionService;
import link.chengguo.orangemall.sys.service.ISysRoleService;
import link.chengguo.orangemall.sys.service.ISysUserRoleService;
import link.chengguo.orangemall.sys.service.ISysUserService;
import link.chengguo.orangemall.service.ums.RedisService;
import link.chengguo.orangemall.util.JsonUtil;
import link.chengguo.orangemall.util.LoginThirdUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ImagUtil;
import link.chengguo.orangemall.utils.ValidatorUtils;
import link.chengguo.orangemall.vo.Rediskey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;

/**
 * <p>
 * 后台用户表 前端控制器
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Slf4j
@Api(value = "用户管理", description = "", tags = {"用户管理"})
@RestController
@RequestMapping("/sys/sysUser")
public class SysUserController extends ApiController {

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private ISysUserRoleService sysUserRoleService;
    @Resource
    private ISysRoleService roleService;
    @Resource
    private ISysPermissionService permissionService;
    @Resource
    private SysPermissionMapper permissionMapper;
    @Resource
    private RedisService redisService;

    @SysLog(MODULE = "sys", REMARK = "根据条件查询所有用户列表")
    @ApiOperation("根据条件查询所有用户列表")
    @GetMapping(value = "/list")
    public Object getUserByPage(SysUser entity,
                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setCityCode(user.getCityCode());//同一个区域下的用户
            entity.setParentId(user.getId());
            entity.setShopId(user.getShopId());
            entity.setPlatform(AllEnum.PlatformType.Shop.code());//筛选商户平台
            IPage<SysUser> page=sysUserService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id"));
            List<SysUser> list=page.getRecords();
            Iterator<SysUser> iterator = list.iterator();
            while(iterator.hasNext()){
                SysUser next = iterator.next();
                if(next.getSupplyId()!=null&&next.getSupplyId().equals(1L)){
                    //隐藏管理员
                    iterator.remove();
                    continue;
                }
                SysUserRole userRole=sysUserRoleService.getOne(new QueryWrapper<SysUserRole>().eq("admin_id",next.getId()));
                if(userRole != null){
                    SysRole role=roleService.getById(userRole.getRoleId());
                    next.setRoleName(role.getName());
                }
            }
            page.setRecords(list);
            return new CommonResult().success(page);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有用户列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "保存用户")
    @ApiOperation("保存用户")
    @PostMapping(value = "/register")
    public Object saveUser(@RequestBody SysUser entity) {
        try {
           /* if (ValidatorUtils.empty(entity.getStoreId())){
                entity.setStoreId(UserUtils.getCurrentMember().getStoreId());
            }*/
            SysUser user=UserUtils.getCurrentMember();
            entity.setCityCode(user.getCityCode());//同一个区域下的用户
            entity.setParentId(user.getId());
            entity.setShopId(user.getShopId());
            entity.setPlatform(AllEnum.PlatformType.Shop.code());//筛选商户平台
            if (sysUserService.saves(entity)) {
                return new CommonResult().success();
            }else{
                return new CommonResult().failed("该账号已存在");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存用户：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
    }

    @SysLog(MODULE = "sys", REMARK = "更新用户")
    @ApiOperation("更新用户")
    @PostMapping(value = "/update/{id}")
    public Object updateUser(@RequestBody SysUser entity) {
        try {
            entity.setPlatform(AllEnum.PlatformType.Shop.code());//筛选租户平台
            if (sysUserService.updates(entity.getId(), entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新用户：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "删除用户")
    @ApiOperation("删除用户")
    @GetMapping(value = "/delete/{id}")
    public Object deleteUser(@ApiParam("用户id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("用户id");
            }
            SysUser user = sysUserService.getById(id);
            if (user.getSupplyId() != null && user.getSupplyId() == 1) {
                return new CommonResult().paramFailed("管理员账号不能删除");
            }
            if (sysUserService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除用户：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "给用户分配角色")
    @ApiOperation("查询用户明细")
    @GetMapping(value = "/{id}")
    public Object getUserById(@ApiParam("用户id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("用户id");
            }
            SysUser coupon = sysUserService.getById(id);
            coupon.setPassword(null);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询用户明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @SysLog(MODULE = "sys", REMARK = "刷新token")
    @ApiOperation(value = "刷新token")
    @RequestMapping(value = "/token/refresh", method = RequestMethod.GET)
    @ResponseBody
    public Object refreshToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String refreshToken = sysUserService.refreshToken(token);
        if (refreshToken == null) {
            return new CommonResult().failed();
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return new CommonResult().success(tokenMap);
    }

    @SysLog(MODULE = "sys", REMARK = "登录以后返回token")
    @ApiOperation(value = "登录以后返回token")
    @IgnoreAuth
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Object login(@RequestBody SysUser umsAdminLoginParam,@RequestParam("code") String code, HttpSession session) {
        try {
            String imageCode = (String) session.getAttribute("imageCode");
            if(code == null || !code.equals(imageCode)){
                return new CommonResult().failed("验证码错误");
            }
            String token = sysUserService.login(umsAdminLoginParam.getUsername(), umsAdminLoginParam.getPassword());
            if (token == null) {
                return new CommonResult().failed("用户名或密码错误");
            }
            Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", token);
            tokenMap.put("tokenHead", tokenHead);
            // tokenMap.put("userId", UserUtils.getCurrentMember().getId());
            return new CommonResult().success(tokenMap);
        } catch (Exception e) {
            return new CommonResult().failed(e.getMessage());
        }

    }

    /**
     * 登录图片验证码
     * @param response
     * @param session
     */
    @IgnoreAuth
    @GetMapping("/imgCode")
    public void getImage(HttpServletResponse response, HttpSession session){
        //利用图片工具生成图片
        //第一个参数是生成的验证码，第二个参数是生成的图片
        Object[] objs = ImagUtil.createImage();
        //将验证码存入Session
        session.setAttribute("imageCode",objs[0]);
        //将图片输出给浏览器
        BufferedImage image = (BufferedImage) objs[1];
        response.setContentType("image/png");
        try {
            OutputStream os = response.getOutputStream();
            ImageIO.write(image, "png", os);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 代理后台直接登录
     * @param entity
     * @return
     */
    @SysLog(MODULE = "sys", REMARK = "代理后台直接登录")
    @ApiOperation(value = "代理后台直接登录")
    @RequestMapping(value = "/loginByAgent", method = RequestMethod.POST)
    @IgnoreAuth
    @ResponseBody
    public Object loginByAgent(@RequestBody SysUser entity,HttpServletRequest request) {
        try {
            StringBuffer url= request.getRequestURL();
            String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
            String baseUrl=tempContextUrl.substring(0,tempContextUrl.length()-6);
            baseUrl+=":8085/shms/shmsShopInfo/hasAuthorityAgentLogin";
            boolean authority = LoginThirdUtils.hasAuthority(entity.getPassword(), baseUrl);
            if(!authority){
                return new CommonResult().failed("无权限");
            }
            String token = sysUserService.loginByAgent(entity.getUsername());
            if (token == null) {
                return new CommonResult().failed("用户名或密码错误");
            }
            Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", token);
            tokenMap.put("tokenHead", tokenHead);
            return new CommonResult().success(tokenMap);
        } catch (Exception e) {
            return new CommonResult().failed(e.getMessage());
        }
    }


    @SysLog(MODULE = "sys", REMARK = "获取当前登录用户信息")
    @ApiOperation(value = "获取当前登录用户信息")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public Object getAdminInfo(Principal principal) {
        String username = principal.getName();
        SysUser queryU = new SysUser();
        queryU.setUsername(username);
        SysUser umsAdmin = sysUserService.getOne(new QueryWrapper<>(queryU));
        Map<String, Object> data = new HashMap<>();
        data.put("username", umsAdmin.getUsername());
        data.put("roles", new String[]{"TEST"});
        data.put("platform", umsAdmin.getPlatform());
        data.put("icon", umsAdmin.getIcon());
        return new CommonResult().success(data);
    }

    @SysLog(MODULE = "sys", REMARK = "登出功能")
    @ApiOperation(value = "登出功能")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public Object logout() {
        return new CommonResult().success(null);
    }

    @SysLog(MODULE = "sys", REMARK = "给用户分配角色")
    @ApiOperation("给用户分配角色")
    @RequestMapping(value = "/role/update", method = RequestMethod.POST)
    @ResponseBody
    public Object updateRole(@RequestParam("adminId") Long adminId,
                             @RequestParam("roleIds") List<Long> roleIds) {
        int count = sysUserService.updateUserRole(adminId, roleIds);
        if (count >= 0) {
            //更新，删除时候，如果redis里有权限列表，重置
            if (!redisService.exists(String.format(Rediskey.menuList3, adminId))) {
                List<SysPermission> list = permissionMapper.listUserPerms(adminId);
                String key = String.format(Rediskey.menuList3, adminId);
                redisService.set(key, JsonUtil.objectToJson(list));
                return list;
            }
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "获取指定用户的角色")
    @ApiOperation("获取指定用户的角色")
    @RequestMapping(value = "/role/{adminId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getRoleList(@PathVariable Long adminId) {
        List<SysRole> roleList = sysUserService.getRoleListByUserId(adminId);
        return new CommonResult().success(roleList);
    }

    @SysLog(MODULE = "sys", REMARK = "获取指定用户的角色")
    @ApiOperation("获取指定用户的角色")
    @RequestMapping(value = "/userRoleCheck", method = RequestMethod.GET)
    @ResponseBody
    public Object userRoleCheck(@RequestParam("adminId") Long adminId) {
        List<SysRole> roleList = sysUserService.getRoleListByUserId(adminId);
        List<SysRole> allroleList = roleService.list(new QueryWrapper<>());
        if (roleList != null && roleList.size() > 0) {
            for (SysRole a : allroleList) {
                for (SysRole u : roleList) {
                    if (u != null && u.getId() != null) {
                        if (a.getId().equals(u.getId())) {
                            a.setChecked(true);
                        }
                    }
                }
            }
            return new CommonResult().success(allroleList);
        }
        return new CommonResult().success(allroleList);
    }

    @SysLog(MODULE = "sys", REMARK = "给用户分配+-权限")
    @ApiOperation("给用户分配+-权限")
    @RequestMapping(value = "/permission/update", method = RequestMethod.POST)
    @ResponseBody
    public Object updatePermission(@RequestParam Long adminId,
                                   @RequestParam("permissionIds") List<Long> permissionIds) {
        int count = sysUserService.updatePermissionByUserId(adminId, permissionIds);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "获取用户所有权限（包括+-权限）")
    @ApiOperation("获取用户所有权限（包括+-权限）")
    @RequestMapping(value = "/permission/{adminId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getPermissionList(@PathVariable Long adminId) {
        List<SysPermission> permissionList = sysUserService.getPermissionListByUserId(adminId);
        return new CommonResult().success(permissionList);
    }

    @ApiOperation("修改展示状态")
    @RequestMapping(value = "/update/updateShowStatus")
    @ResponseBody
    @SysLog(MODULE = "sys", REMARK = "修改展示状态")
    public Object updateShowStatus(@RequestParam("ids") Long ids,
                                   @RequestParam("showStatus") Integer showStatus) {
        SysUser role = new SysUser();
        role.setId(ids);
        role.setStatus(showStatus);
        sysUserService.updateById(role);

        return new CommonResult().success();

    }

    @ApiOperation("修改密码")
    @RequestMapping(value = "/updatePassword")
    @ResponseBody
    @SysLog(MODULE = "sys", REMARK = "修改密码")
    public Object updatePassword(@RequestParam("password") String password,
                                 @RequestParam("renewPassword") String renewPassword,
                                 @RequestParam("newPassword") String newPassword) {
        if (ValidatorUtils.empty(password)) {
            return new CommonResult().failed("参数为空");
        }
        if (ValidatorUtils.empty(renewPassword)) {
            return new CommonResult().failed("参数为空");
        }
        if (ValidatorUtils.empty(newPassword)) {
            return new CommonResult().failed("参数为空");
        }
        if (!renewPassword.equals(newPassword)) {
            return new CommonResult().failed("新密码不一致!");
        }
        try {
            sysUserService.updatePassword(password, newPassword);
        } catch (Exception e) {
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().success();

    }
}

