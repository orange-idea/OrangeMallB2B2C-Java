package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysWebLog;
import link.chengguo.orangemall.vo.LogParam;
import link.chengguo.orangemall.vo.LogStatisc;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysWebLogService extends IService<SysWebLog> {

    List<LogStatisc> selectPageExt(LogParam entity);
}
