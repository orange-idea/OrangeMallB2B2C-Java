package link.chengguo.orangemall.config;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import link.chengguo.orangemall.ApiContext;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//Spring boot方式
@EnableTransactionManagement
@Configuration
@MapperScan("link.chengguo.orangemall.*.mapper*")
public class MybatisPlusConfig {
    @Autowired
    private ApiContext apiContext;

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {

        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();

        paginationInterceptor.setDialectType("mysql");
        /*
         * 【测试多租户】 SQL 解析处理拦截器<br>
         * 这里固定写成住户 1 实际情况你可以从cookie读取，因此数据看不到 【 麻花藤 】 这条记录（ 注意观察 SQL ）<br>
         */
//        List<ISqlParser> sqlParserList = new ArrayList<>();
//        TenantSqlParser tenantSqlParser = new TenantSqlParser();
//        tenantSqlParser.setTenantHandler(new TenantHandler() {
//
//            @Override
//            public Expression getTenantId() {
//                // 从当前系统上下文中取出当前请求的服务商ID，通过解析器注入到SQL中。
//                Long currentProviderId = apiContext.getCurrentProviderId();
//                if (null == currentProviderId) {
//                    currentProviderId = 1L;
//                    System.out.println("#1129 getCurrentProviderId error.");
//                    //  throw new RuntimeException("#1129 getCurrentProviderId error.");
//                }
//                return new LongValue(currentProviderId);
//            }
//
//            @Override
//            public String getTenantIdColumn() {
//                return "store_id";
//            }
//
//            @Override
//            public boolean doTableFilter(String tableName) {
//                if (tableName.startsWith("admin_") || tableName.startsWith("QRTZ_")) {
//                    return true;
//                }
//                return false;
//
//            }
//        });
//
//        sqlParserList.add(tenantSqlParser);
//        paginationInterceptor.setSqlParserList(sqlParserList);
//        paginationInterceptor.setSqlParserFilter(new ISqlParserFilter() {
//            @Override
//            public boolean doFilter(MetaObject metaObject) {
//                MappedStatement ms = SqlParserHelper.getMappedStatement(metaObject);
//                // 过滤自定义查询此时无租户信息约束【 麻花藤 】出现
//                if ("link.chengguo.orangemall.sys.mapper.SysUserMapper.selectByUserName".equals(ms.getId())) {
//                    return true;
//                }
//                return false;
//            }
//        });
        return paginationInterceptor;
    }


    /**
     * 性能分析拦截器，不建议生产使用
     * 用来观察 SQL 执行情况及执行时长
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor() {
        return new PerformanceInterceptor();
    }
}
