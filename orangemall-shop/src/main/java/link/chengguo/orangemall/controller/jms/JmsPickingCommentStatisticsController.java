package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingCommentStatistics;
import link.chengguo.orangemall.service.jms.IJmsPickingCommentStatisticsService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*  拣货员评价统计
*/
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingCommentStatistics")
public class JmsPickingCommentStatisticsController {

@Resource
private IJmsPickingCommentStatisticsService IJmsPickingCommentStatisticsService;

@SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货员评价统计列表")
@ApiOperation("根据条件查询所有拣货员评价统计列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:read')")
public Object getJmsPickingCommentStatisticsByPage(JmsPickingCommentStatistics entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IJmsPickingCommentStatisticsService.page(new Page<JmsPickingCommentStatistics>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有拣货员评价统计列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "保存拣货员评价统计")
@ApiOperation("保存拣货员评价统计")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:create')")
public Object saveJmsPickingCommentStatistics(@RequestBody JmsPickingCommentStatistics entity) {
try {

if (IJmsPickingCommentStatisticsService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存拣货员评价统计：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "更新拣货员评价统计")
@ApiOperation("更新拣货员评价统计")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:update')")
public Object updateJmsPickingCommentStatistics(@RequestBody JmsPickingCommentStatistics entity) {
try {
if (IJmsPickingCommentStatisticsService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新拣货员评价统计：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "删除拣货员评价统计")
@ApiOperation("删除拣货员评价统计")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:delete')")
public Object deleteJmsPickingCommentStatistics(@ApiParam("拣货员评价统计id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员评价统计id");
}
if (IJmsPickingCommentStatisticsService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除拣货员评价统计：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "给拣货员评价统计分配拣货员评价统计")
@ApiOperation("查询拣货员评价统计明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:read')")
public Object getJmsPickingCommentStatisticsById(@ApiParam("拣货员评价统计id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员评价统计id");
}
JmsPickingCommentStatistics coupon = IJmsPickingCommentStatisticsService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询拣货员评价统计明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除拣货员评价统计")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "jms", REMARK = "批量删除拣货员评价统计")
@PreAuthorize("hasAuthority('jms:jmsPickingCommentStatistics:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IJmsPickingCommentStatisticsService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingCommentStatistics entity) {
    // 模拟从数据库获取需要导出的数据
    List<JmsPickingCommentStatistics> personList = IJmsPickingCommentStatisticsService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingCommentStatistics.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<JmsPickingCommentStatistics> personList = EasyPoiUtils.importExcel(file, JmsPickingCommentStatistics.class);
    IJmsPickingCommentStatisticsService.saveBatch(personList);
    }
    }


