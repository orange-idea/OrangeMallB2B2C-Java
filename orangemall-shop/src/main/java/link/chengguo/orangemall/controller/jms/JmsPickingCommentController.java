package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingComment;
import link.chengguo.orangemall.service.jms.IJmsPickingCommentService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*  拣货员评价信息
*/
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingComment")
public class JmsPickingCommentController {

@Resource
private IJmsPickingCommentService IJmsPickingCommentService;

@SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货员评价信息列表")
@ApiOperation("根据条件查询所有拣货员评价信息列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:read')")
public Object getJmsPickingCommentByPage(JmsPickingComment entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IJmsPickingCommentService.page(new Page<JmsPickingComment>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有拣货员评价信息列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "保存拣货员评价信息")
@ApiOperation("保存拣货员评价信息")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:create')")
public Object saveJmsPickingComment(@RequestBody JmsPickingComment entity) {
try {

if (IJmsPickingCommentService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存拣货员评价信息：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "更新拣货员评价信息")
@ApiOperation("更新拣货员评价信息")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:update')")
public Object updateJmsPickingComment(@RequestBody JmsPickingComment entity) {
try {
if (IJmsPickingCommentService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新拣货员评价信息：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "删除拣货员评价信息")
@ApiOperation("删除拣货员评价信息")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:delete')")
public Object deleteJmsPickingComment(@ApiParam("拣货员评价信息id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员评价信息id");
}
if (IJmsPickingCommentService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除拣货员评价信息：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "给拣货员评价信息分配拣货员评价信息")
@ApiOperation("查询拣货员评价信息明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:read')")
public Object getJmsPickingCommentById(@ApiParam("拣货员评价信息id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员评价信息id");
}
JmsPickingComment coupon = IJmsPickingCommentService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询拣货员评价信息明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除拣货员评价信息")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "jms", REMARK = "批量删除拣货员评价信息")
@PreAuthorize("hasAuthority('jms:jmsPickingComment:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IJmsPickingCommentService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingComment entity) {
    // 模拟从数据库获取需要导出的数据
    List<JmsPickingComment> personList = IJmsPickingCommentService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingComment.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<JmsPickingComment> personList = EasyPoiUtils.importExcel(file, JmsPickingComment.class);
    IJmsPickingCommentService.saveBatch(personList);
    }
    }


