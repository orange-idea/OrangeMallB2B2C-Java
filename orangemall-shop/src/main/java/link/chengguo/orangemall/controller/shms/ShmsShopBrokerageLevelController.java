package link.chengguo.orangemall.controller.shms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopBrokerageLevel;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺抽佣等级
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopBrokerageLevelController ", description = "店铺抽佣等级")
@RequestMapping("/shms/shmsShopBrokerageLevel")
public class ShmsShopBrokerageLevelController {

    @Resource
    private link.chengguo.orangemall.service.shms.IShmsShopBrokerageLevelService IShmsShopBrokerageLevelService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺抽佣等级列表")
    @ApiOperation("根据条件查询所有店铺抽佣等级列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:read')")
    public Object getShmsShopBrokerageLevelByPage(ShmsShopBrokerageLevel entity,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopBrokerageLevelService.page(new Page<ShmsShopBrokerageLevel>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺抽佣等级列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺抽佣等级")
    @ApiOperation("保存店铺抽佣等级")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:create')")
    public Object saveShmsShopBrokerageLevel(@RequestBody ShmsShopBrokerageLevel entity) {
        try {

            if (IShmsShopBrokerageLevelService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺抽佣等级：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺抽佣等级")
    @ApiOperation("更新店铺抽佣等级")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:update')")
    public Object updateShmsShopBrokerageLevel(@RequestBody ShmsShopBrokerageLevel entity) {
        try {
            if (IShmsShopBrokerageLevelService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺抽佣等级：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺抽佣等级")
    @ApiOperation("删除店铺抽佣等级")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:delete')")
    public Object deleteShmsShopBrokerageLevel(@ApiParam("店铺抽佣等级id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺抽佣等级id");
            }
            if (IShmsShopBrokerageLevelService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺抽佣等级：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺抽佣等级分配店铺抽佣等级")
    @ApiOperation("查询店铺抽佣等级明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:read')")
    public Object getShmsShopBrokerageLevelById(@ApiParam("店铺抽佣等级id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺抽佣等级id");
            }
            ShmsShopBrokerageLevel coupon = IShmsShopBrokerageLevelService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺抽佣等级明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺抽佣等级")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺抽佣等级")
    @PreAuthorize("hasAuthority('shms:shmsShopBrokerageLevel:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopBrokerageLevelService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopBrokerageLevel entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopBrokerageLevel> personList = IShmsShopBrokerageLevelService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopBrokerageLevel.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopBrokerageLevel> personList = EasyPoiUtils.importExcel(file, ShmsShopBrokerageLevel.class);
        IShmsShopBrokerageLevelService.saveBatch(personList);
    }
}


