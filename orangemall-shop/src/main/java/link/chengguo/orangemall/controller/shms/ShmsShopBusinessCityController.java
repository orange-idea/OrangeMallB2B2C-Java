package link.chengguo.orangemall.controller.shms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopBusinessCity;
import link.chengguo.orangemall.service.shms.IShmsShopBusinessCityService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-19
 * 店铺经营城市
 */
@Slf4j
@RestController
@RequestMapping("/shms/shmsShopBusinessCity")
public class ShmsShopBusinessCityController {

    @Resource
    private IShmsShopBusinessCityService IShmsShopBusinessCityService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺经营城市列表")
    @ApiOperation("根据条件查询所有店铺经营城市列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:read')")
    public Object getShmsShopBusinessCityByPage(ShmsShopBusinessCity entity,
                                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopBusinessCityService.page(new Page<ShmsShopBusinessCity>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺经营城市列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺经营城市")
    @ApiOperation("保存店铺经营城市")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:create')")
    public Object saveShmsShopBusinessCity(@RequestBody ShmsShopBusinessCity entity) {
        try {

            if (IShmsShopBusinessCityService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺经营城市")
    @ApiOperation("更新店铺经营城市")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:update')")
    public Object updateShmsShopBusinessCity(@RequestBody ShmsShopBusinessCity entity) {
        try {
            if (IShmsShopBusinessCityService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺经营城市")
    @ApiOperation("删除店铺经营城市")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:delete')")
    public Object deleteShmsShopBusinessCity(@ApiParam("店铺经营城市id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺经营城市id");
            }
            if (IShmsShopBusinessCityService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺经营城市分配店铺经营城市")
    @ApiOperation("查询店铺经营城市明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:read')")
    public Object getShmsShopBusinessCityById(@ApiParam("店铺经营城市id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺经营城市id");
            }
            ShmsShopBusinessCity coupon = IShmsShopBusinessCityService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺经营城市明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺经营城市")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺经营城市")
    @PreAuthorize("hasAuthority('shms:shmsShopBusinessCity:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopBusinessCityService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopBusinessCity entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopBusinessCity> personList = IShmsShopBusinessCityService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopBusinessCity.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopBusinessCity> personList = EasyPoiUtils.importExcel(file, ShmsShopBusinessCity.class);
        IShmsShopBusinessCityService.saveBatch(personList);
    }
}


