package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.JmsPickingOrder;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货订单
 */
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingOrder")
public class JmsPickingOrderController {

    @Resource
    private link.chengguo.orangemall.service.jms.IJmsPickingOrderService IJmsPickingOrderService;

    @SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货订单列表")
    @ApiOperation("根据条件查询所有拣货订单列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:read')")
    public Object getJmsPickingOrderByPage(JmsPickingOrder entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IJmsPickingOrderService.page(new Page<JmsPickingOrder>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有拣货订单列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "保存拣货订单")
    @ApiOperation("保存拣货订单")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:create')")
    public Object saveJmsPickingOrder(@RequestBody JmsPickingOrder entity) {
        try {

            if (IJmsPickingOrderService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "更新拣货订单")
    @ApiOperation("更新拣货订单")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:update')")
    public Object updateJmsPickingOrder(@RequestBody JmsPickingOrder entity) {
        try {
            if (IJmsPickingOrderService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "删除拣货订单")
    @ApiOperation("删除拣货订单")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:delete')")
    public Object deleteJmsPickingOrder(@ApiParam("拣货订单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货订单id");
            }
            if (IJmsPickingOrderService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "给拣货订单分配拣货订单")
    @ApiOperation("查询拣货订单明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:read')")
    public Object getJmsPickingOrderById(@ApiParam("拣货订单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货订单id");
            }
            JmsPickingOrder coupon = IJmsPickingOrderService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询拣货订单明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除拣货订单")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "jms", REMARK = "批量删除拣货订单")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrder:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IJmsPickingOrderService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "jms", REMARK = "查询拣货订单统计数字")
    @ApiOperation("查询拣货订单统计数字")
    @PostMapping(value = "/getPickingOrderCountNumber")
    public Object getCountNumber() {
        try {
            Map<String,Object> map=new HashMap<>();
            SysUser user= UserUtils.getCurrentMember();
            int allCount= IJmsPickingOrderService.count(new QueryWrapper<JmsPickingOrder>().eq("shop_id",user.getShopId()));
            int waitGrabCount=IJmsPickingOrderService.count(new QueryWrapper<JmsPickingOrder>().eq("shop_id",user.getShopId()).eq("status", PickingStatus.WaitGrab.getValue()));
            int waitPickingCount=IJmsPickingOrderService.count(new QueryWrapper<JmsPickingOrder>().eq("shop_id",user.getShopId()).eq("status",PickingStatus.WaitPicking.getValue()));
            int pickingCount=IJmsPickingOrderService.count(new QueryWrapper<JmsPickingOrder>().eq("shop_id",user.getShopId()).eq("status",PickingStatus.Picking.getValue()));
            int finishCount=IJmsPickingOrderService.count(new QueryWrapper<JmsPickingOrder>().eq("shop_id",user.getShopId()).eq("status",PickingStatus.Picked.getValue()));
            map.put("allCount",allCount);
            map.put("waitGrabCount",waitGrabCount);
            map.put("waitPickingCount",waitPickingCount);
            map.put("pickingCount",pickingCount);
            map.put("finishCount",finishCount);
            return new CommonResult().success(map);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单跟踪统计数字：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }



    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingOrder entity) {
        // 模拟从数据库获取需要导出的数据
        List<JmsPickingOrder> personList = IJmsPickingOrderService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingOrder.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<JmsPickingOrder> personList = EasyPoiUtils.importExcel(file, JmsPickingOrder.class);
        IJmsPickingOrderService.saveBatch(personList);
    }
}


