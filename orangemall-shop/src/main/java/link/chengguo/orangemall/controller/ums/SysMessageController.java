package link.chengguo.orangemall.controller.ums;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 站内信 前端控制器
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@RestController
@RequestMapping("/ums/sysMessage")
public class SysMessageController {

}

