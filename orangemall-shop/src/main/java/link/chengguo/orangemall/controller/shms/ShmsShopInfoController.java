package link.chengguo.orangemall.controller.shms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.service.shms.IShmsShopInfoService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.sys.entity.SysOrderType;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺信息
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopInfoController ", description = "店铺信息")
@RequestMapping("/shms/shmsShopInfo")
public class ShmsShopInfoController {

    @Resource
    private IShmsShopInfoService shmsShopInfoService;



    @SysLog(MODULE = "shms", REMARK = "更新店铺信息")
    @ApiOperation("更新店铺信息")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopInfo:update')")
    public Object updateShmsShopInfo(@RequestBody ShmsShopInfo entity) {
        try {
            if (shmsShopInfoService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺信息分配店铺信息")
    @ApiOperation("查询店铺信息明细")
    @GetMapping(value = "/getShopInfo")
    public Object getShmsShopInfoById() {
        try {
            Long shopId= UserUtils.getCurrentMember().getShopId();
            ShmsShopInfo coupon = shmsShopInfoService.getById(shopId);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺信息明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }
    @SysLog(MODULE = "sys", REMARK = "商家登录密码重置")
    @ApiOperation("获取平台允许的配送模式")
    @GetMapping(value = "/getSendTypes")
    public Object getSendTypes(){
        List<SysOrderType> list = shmsShopInfoService.getSendTypes(new QueryWrapper<SysOrderType>().eq("status", 1));
        if(list.size()>0) {
            return new CommonResult().success(list);
        }
        return new CommonResult().failed("平台未开通配送模式");
    }

}


