package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingRole;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.service.jms.IJmsPickingRoleService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货员信息
 */
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingUser")
public class JmsPickingUserController {

    @Resource
    private link.chengguo.orangemall.service.jms.IJmsPickingUserService IJmsPickingUserService;
    @Resource
    private IJmsPickingRoleService roleService;

    @SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货员信息列表")
    @ApiOperation("根据条件查询所有拣货员信息列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:read')")
    public Object getJmsPickingUserByPage(JmsPickingUser entity,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            IPage<JmsPickingUser> pageInfo=IJmsPickingUserService.page(new Page<JmsPickingUser>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id").orderByDesc("id"));
            List<JmsPickingUser> list=pageInfo.getRecords();
            for (int i=0;i<list.size();i++){
               JmsPickingRole role= roleService.getById(list.get(i).getRoleId());
               if (role!=null){
                   list.get(i).setRoleName(role.getName());
               }
            }
            pageInfo.setRecords(list);
            return new CommonResult().success(pageInfo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有拣货员信息列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "保存拣货员信息")
    @ApiOperation("保存拣货员信息")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:create')")
    public Object saveJmsPickingUser(@RequestBody JmsPickingUser entity) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setShopId(user.getShopId());
            entity.setCityCode(user.getCityCode());
            if (IJmsPickingUserService.addPickingUser(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存拣货员信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "修改登录密码")
    @ApiOperation("修改登录密码")
    @PostMapping(value = "/updatePwd")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:updatePwd')")
    public Object updatePwd(@RequestBody JmsPickingUser entity) {
        try {
            if (IJmsPickingUserService.updatePwd(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改登录密码：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "更新拣货员信息")
    @ApiOperation("更新拣货员信息")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:update')")
    public Object updateJmsPickingUser(@RequestBody JmsPickingUser entity) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setShopId(user.getShopId());
            entity.setCityCode(user.getCityCode());
            if (IJmsPickingUserService.updatePickingUser(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新拣货员信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "删除拣货员信息")
    @ApiOperation("删除拣货员信息")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:delete')")
    public Object deleteJmsPickingUser(@ApiParam("拣货员信息id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货员信息id");
            }
            if (IJmsPickingUserService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除拣货员信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "给拣货员信息分配拣货员信息")
    @ApiOperation("查询拣货员信息明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:read')")
    public Object getJmsPickingUserById(@ApiParam("拣货员信息id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货员信息id");
            }
            JmsPickingUser coupon = IJmsPickingUserService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询拣货员信息明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除拣货员信息")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "jms", REMARK = "批量删除拣货员信息")
    @PreAuthorize("hasAuthority('jms:jmsPickingUser:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IJmsPickingUserService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingUser entity) {
        // 模拟从数据库获取需要导出的数据
        List<JmsPickingUser> personList = IJmsPickingUserService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingUser.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<JmsPickingUser> personList = EasyPoiUtils.importExcel(file, JmsPickingUser.class);
        IJmsPickingUserService.saveBatch(personList);
    }
}


