package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillPayments;
import link.chengguo.orangemall.bill.mapper.BillPaymentsMapper;
import link.chengguo.orangemall.service.bill.IBillPaymentsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付单表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillPaymentsServiceImpl extends ServiceImpl<BillPaymentsMapper, BillPayments> implements IBillPaymentsService {

}
