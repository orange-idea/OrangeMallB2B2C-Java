package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsGrowthChangeHistory;
import link.chengguo.orangemall.ums.mapper.UmsGrowthChangeHistoryMapper;
import link.chengguo.orangemall.service.ums.IUmsGrowthChangeHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成长值变化历史记录表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsGrowthChangeHistoryServiceImpl extends ServiceImpl<UmsGrowthChangeHistoryMapper, UmsGrowthChangeHistory> implements IUmsGrowthChangeHistoryService {

}
