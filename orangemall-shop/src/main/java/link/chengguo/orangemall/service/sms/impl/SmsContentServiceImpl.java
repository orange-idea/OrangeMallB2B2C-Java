package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsContent;
import link.chengguo.orangemall.sms.mapper.SmsContentMapper;
import link.chengguo.orangemall.service.sms.ISmsContentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author orangemall
 * @date 2019-12-07
 */
@Service
public class SmsContentServiceImpl extends ServiceImpl<SmsContentMapper, SmsContent> implements ISmsContentService {

    @Resource
    private SmsContentMapper smsContentMapper;


}
