package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;
import link.chengguo.orangemall.jms.mapper.JmsGoodsTypeTrackMapper;
import link.chengguo.orangemall.service.jms.IJmsGoodsTypeTrackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-27
 */
@Service
public class JmsGoodsTypeTrackServiceImpl extends ServiceImpl
        <JmsGoodsTypeTrackMapper, JmsGoodsTypeTrack> implements IJmsGoodsTypeTrackService {

    @Resource
    private JmsGoodsTypeTrackMapper jmsGoodsTypeTrackMapper;

    @Override
    public boolean addGoodsTypeTrack(JmsGoodsTypeTrack goodsTypeTrack) {
        goodsTypeTrack.setCreateTime(new Date());
        return jmsGoodsTypeTrackMapper.insert(goodsTypeTrack)>0?true:false;
    }
}
