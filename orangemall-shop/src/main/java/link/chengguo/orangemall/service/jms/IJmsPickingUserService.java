package link.chengguo.orangemall.service.jms;

import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingUserService extends IService<JmsPickingUser> {

    /**
     * 添加拣货员
     * @param user
     * @return
     */
    boolean addPickingUser(JmsPickingUser user);

    /**
     * 修改登录密码
     * @param user
     * @return
     */
    boolean updatePwd(JmsPickingUser user);


    /**
     * 修改拣货员信息
     * @param user
     * @return
     */
    boolean updatePickingUser(JmsPickingUser user);

}
