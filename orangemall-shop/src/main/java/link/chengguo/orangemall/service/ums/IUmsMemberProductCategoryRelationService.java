package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberProductCategoryRelation;

/**
 * <p>
 * 会员与产品分类关系表（用户喜欢的分类） 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberProductCategoryRelationService extends IService<UmsMemberProductCategoryRelation> {

}
