package link.chengguo.orangemall.service.bill;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillDeliveryItems;

/**
 * <p>
 * 发货单详情表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillDeliveryItemsService extends IService<BillDeliveryItems> {

}
