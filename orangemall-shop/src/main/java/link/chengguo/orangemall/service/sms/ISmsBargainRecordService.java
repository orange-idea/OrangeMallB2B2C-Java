package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsBargainRecord;

/**
 * <p>
 * 砍价免单记录表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsBargainRecordService extends IService<SmsBargainRecord> {

}
