package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopCommentLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopCommentLogMapper;
import link.chengguo.orangemall.service.shms.IShmsShopCommentLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopCommentLogServiceImpl extends ServiceImpl
<ShmsShopCommentLogMapper, ShmsShopCommentLog> implements IShmsShopCommentLogService {

@Resource
private  ShmsShopCommentLogMapper shmsShopCommentLogMapper;


}
