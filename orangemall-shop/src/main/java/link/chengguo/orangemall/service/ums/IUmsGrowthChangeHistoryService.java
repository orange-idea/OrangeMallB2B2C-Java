package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsGrowthChangeHistory;

/**
 * <p>
 * 成长值变化历史记录表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsGrowthChangeHistoryService extends IService<UmsGrowthChangeHistory> {

}
