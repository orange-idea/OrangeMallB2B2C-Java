package link.chengguo.orangemall.service.pms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsGiftsCategory;

/**
 * <p>
 * 帮助分类表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface IPmsGiftsCategoryService extends IService<PmsGiftsCategory> {

}
