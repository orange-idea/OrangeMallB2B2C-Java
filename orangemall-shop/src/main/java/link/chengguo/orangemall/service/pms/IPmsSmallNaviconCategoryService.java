package link.chengguo.orangemall.service.pms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;

/**
 * 小程序首页nav管理
 *
 * @author chengguo
 * @email 1264395832@qq.com
 * @date 2019-05-08 00:09:37
 */

public interface IPmsSmallNaviconCategoryService extends IService<PmsSmallNaviconCategory> {

}
