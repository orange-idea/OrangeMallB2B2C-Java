package link.chengguo.orangemall.service.bill;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillDelivery;

/**
 * <p>
 * 发货单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillDeliveryService extends IService<BillDelivery> {

}
