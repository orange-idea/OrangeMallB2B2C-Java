package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.service.cms.ICmsSubjectProductRelationService;
import link.chengguo.orangemall.pms.entity.CmsSubjectProductRelation;
import link.chengguo.orangemall.pms.mapper.CmsSubjectProductRelationMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专题商品关系表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsSubjectProductRelationServiceImpl extends ServiceImpl<CmsSubjectProductRelationMapper, CmsSubjectProductRelation> implements ICmsSubjectProductRelationService {

}
