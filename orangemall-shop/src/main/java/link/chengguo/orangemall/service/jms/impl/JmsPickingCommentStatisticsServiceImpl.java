package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingCommentStatistics;
import link.chengguo.orangemall.jms.mapper.JmsPickingCommentStatisticsMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingCommentStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingCommentStatisticsServiceImpl extends ServiceImpl
<JmsPickingCommentStatisticsMapper, JmsPickingCommentStatistics> implements IJmsPickingCommentStatisticsService {

@Resource
private  JmsPickingCommentStatisticsMapper jmsPickingCommentStatisticsMapper;


}
