package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsPaimai;

/**
 * <p>
 * 一分钱抽奖 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-19
 */
public interface ISmsPaimaiService extends IService<SmsPaimai> {

}
