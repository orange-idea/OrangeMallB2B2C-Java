package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.service.oms.IOmsOrderSettingService;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillDayMapper;
import link.chengguo.orangemall.service.shms.IShmsShopBillDayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.service.shms.IShmsShopBillLogService;
import link.chengguo.orangemall.util.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillDayServiceImpl extends ServiceImpl
        <ShmsShopBillDayMapper, ShmsShopBillDay> implements IShmsShopBillDayService {

    @Resource
    private ShmsShopBillDayMapper shmsShopBillDayMapper;
    @Resource
    private IOmsOrderSettingService omsOrderSettingService;
    @Resource
    private IShmsShopBillLogService shopBillLogService;

    @Transactional
    @Override
    public boolean addShopBillDayForTask() {
        //获取结算日期
        Date day=omsOrderSettingService.getBillDayCreateDate();
        this.addShopBillDay(day);
        return true;
    }

    @Transactional
    @Override
    public boolean addShopBillDay(Date date) {
        //查询结算的账单，并进行结算操作
        ShmsShopBillLog billLog=new ShmsShopBillLog();
        String time= DateUtils.format(date,DateUtils.DATE_PATTERN);
        billLog.setStartTime(time+" 00:00:00");
        billLog.setEndTime(time+" 23:59:59");
        List<ShmsShopBillDay> shopBillDayList=shopBillLogService.statisticsDayBill(billLog);
        for (int i=0;i<shopBillDayList.size();i++){
            shopBillDayList.get(i).setDay(DateUtils.format(date,DateUtils.DATE_PATTERN));
            shopBillDayList.get(i).setCreateTime(new Date());
            addOrUpdateShopBillDay(shopBillDayList.get(i));
        }
        return true;
    }

    @Override
    public boolean addOrUpdateShopBillDay(ShmsShopBillDay shmsShopBillDay) {
        ShmsShopBillDay tmp=getShmsShopBillDay(shmsShopBillDay.getShopId(),shmsShopBillDay.getDay());
        if (tmp!=null){
            shmsShopBillDay.setId(tmp.getId());
            shmsShopBillDayMapper.updateById(shmsShopBillDay);
        }else{
            shmsShopBillDayMapper.insert(shmsShopBillDay);
        }
        return true;
    }

    @Override
    public ShmsShopBillDay getShmsShopBillDay(Long shopId, String date) {
        ShmsShopBillDay shopBillDay=new ShmsShopBillDay();
        shopBillDay.setShopId(shopId);
        shopBillDay.setDay(date);
        return shmsShopBillDayMapper.selectOne(new QueryWrapper<>(shopBillDay));
    }

    /**
     * 月账单统计
     * @param shmsShopBillDay
     * @return
     */
    @Override
    public List<ShmsShopBillMonth> statisticsMonthBill(ShmsShopBillDay shmsShopBillDay) {
        List<ShmsShopBillMonth>  shopBillMonths= shmsShopBillDayMapper.statisticsMonthBill(shmsShopBillDay);
        return shopBillMonths;
    }
}
