package link.chengguo.orangemall.service.bill;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillRefund;

/**
 * <p>
 * 退款单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillRefundService extends IService<BillRefund> {

}
