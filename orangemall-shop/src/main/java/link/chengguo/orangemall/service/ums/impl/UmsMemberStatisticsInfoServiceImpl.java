package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberStatisticsInfo;
import link.chengguo.orangemall.ums.mapper.UmsMemberStatisticsInfoMapper;
import link.chengguo.orangemall.service.ums.IUmsMemberStatisticsInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员统计信息 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberStatisticsInfoServiceImpl extends ServiceImpl<UmsMemberStatisticsInfoMapper, UmsMemberStatisticsInfo> implements IUmsMemberStatisticsInfoService {

}
