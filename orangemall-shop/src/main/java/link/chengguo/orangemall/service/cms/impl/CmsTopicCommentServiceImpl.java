package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsTopicComment;
import link.chengguo.orangemall.cms.mapper.CmsTopicCommentMapper;
import link.chengguo.orangemall.service.cms.ICmsTopicCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专题评论表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsTopicCommentServiceImpl extends ServiceImpl<CmsTopicCommentMapper, CmsTopicComment> implements ICmsTopicCommentService {

}
