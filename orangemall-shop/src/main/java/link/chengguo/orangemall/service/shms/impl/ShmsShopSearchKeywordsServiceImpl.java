package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopSearchKeywords;
import link.chengguo.orangemall.shms.mapper.ShmsShopSearchKeywordsMapper;
import link.chengguo.orangemall.service.shms.IShmsShopSearchKeywordsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopSearchKeywordsServiceImpl extends ServiceImpl
<ShmsShopSearchKeywordsMapper, ShmsShopSearchKeywords> implements IShmsShopSearchKeywordsService {

@Resource
private  ShmsShopSearchKeywordsMapper shmsShopSearchKeywordsMapper;


}
