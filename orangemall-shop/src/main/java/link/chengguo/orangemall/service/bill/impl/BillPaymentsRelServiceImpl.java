package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillPaymentsRel;
import link.chengguo.orangemall.bill.mapper.BillPaymentsRelMapper;
import link.chengguo.orangemall.service.bill.IBillPaymentsRelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付单明细表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillPaymentsRelServiceImpl extends ServiceImpl<BillPaymentsRelMapper, BillPaymentsRel> implements IBillPaymentsRelService {

}
