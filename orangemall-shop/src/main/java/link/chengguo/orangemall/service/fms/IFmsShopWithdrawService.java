package link.chengguo.orangemall.service.fms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.fms.entity.FmsShopWithdraw;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsShopWithdrawService extends IService<FmsShopWithdraw> {

}
