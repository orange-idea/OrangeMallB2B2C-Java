package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsOrderOperateHistory;
import link.chengguo.orangemall.oms.mapper.OmsOrderOperateHistoryMapper;
import link.chengguo.orangemall.service.oms.IOmsOrderOperateHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单操作历史记录 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsOrderOperateHistoryServiceImpl extends ServiceImpl<OmsOrderOperateHistoryMapper, OmsOrderOperateHistory> implements IOmsOrderOperateHistoryService {

}
