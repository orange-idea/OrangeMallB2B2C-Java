package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsShare;
import link.chengguo.orangemall.sms.mapper.SmsShareMapper;
import link.chengguo.orangemall.service.sms.ISmsShareService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享列表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsShareServiceImpl extends ServiceImpl<SmsShareMapper, SmsShare> implements ISmsShareService {

}
