package link.chengguo.orangemall.service.bill;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BakCategory;

/**
 * <p>
 * 类目表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-17
 */
public interface IBakCategoryService extends IService<BakCategory> {

}
