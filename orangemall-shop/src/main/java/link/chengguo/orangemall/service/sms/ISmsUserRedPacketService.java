package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsUserRedPacket;

/**
 * <p>
 * 用户红包 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsUserRedPacketService extends IService<SmsUserRedPacket> {

}
