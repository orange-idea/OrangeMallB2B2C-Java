package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsPaimaiLog;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-15
 */
public interface ISmsPaimaiLogService extends IService<SmsPaimaiLog> {

}
