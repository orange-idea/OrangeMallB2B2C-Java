package link.chengguo.orangemall.service.oms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrderSetting;

import java.util.Date;

/**
 * <p>
 * 订单设置表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderSettingService extends IService<OmsOrderSetting> {


    /**
     * 获取当前生成的日账单日期
     * @return  yyyy-mm-dd
     */
    Date getBillDayCreateDate();


    /**
     * 获取当前结算的最新日期，小于该日期的进行结算。
     * @return  yyyy-mm-dd HH:mm:ss
     */
    Date getSettlementDate();

    /**
     * 获取平台订单配置信息
     * @return
     */
    OmsOrderSetting getOrderSettings();

}
