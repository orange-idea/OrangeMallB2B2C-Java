package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingOrder;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingOrderServiceImpl extends ServiceImpl
<JmsPickingOrderMapper, JmsPickingOrder> implements IJmsPickingOrderService {

@Resource
private  JmsPickingOrderMapper jmsPickingOrderMapper;


}
