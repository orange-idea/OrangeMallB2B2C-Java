package link.chengguo.orangemall.service.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopCategoryService extends IService<ShmsShopCategory> {

}
