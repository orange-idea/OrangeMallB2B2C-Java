package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsBargainConfig;

/**
 * <p>
 * 砍价免单设置表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsBargainConfigService extends IService<SmsBargainConfig> {

}
