package link.chengguo.orangemall.service.jms;

import link.chengguo.orangemall.jms.entity.JmsPickingRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingRoleService extends IService<JmsPickingRole> {

}
