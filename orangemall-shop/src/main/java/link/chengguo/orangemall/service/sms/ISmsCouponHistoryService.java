package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsCouponHistory;

/**
 * <p>
 * 优惠券使用、领取历史表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsCouponHistoryService extends IService<SmsCouponHistory> {

}
