package link.chengguo.orangemall.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.CmsSubjectProductRelation;

/**
 * <p>
 * 专题商品关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsSubjectProductRelationService extends IService<CmsSubjectProductRelation> {

}
