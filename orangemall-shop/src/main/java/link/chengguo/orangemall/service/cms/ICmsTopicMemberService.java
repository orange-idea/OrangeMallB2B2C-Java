package link.chengguo.orangemall.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsTopicMember;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsTopicMemberService extends IService<CmsTopicMember> {

}
