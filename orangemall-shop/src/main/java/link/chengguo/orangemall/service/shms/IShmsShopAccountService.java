package link.chengguo.orangemall.service.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopAccount;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopAccountService extends IService<ShmsShopAccount> {


    /**
     * 增减冻结余额
     * @param type 1-减少，其他增加
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @param  desc  操作说明
     * @return
     */
    boolean updateShopAccountFrozenBalance(int type, int accountChangeType, Long shopId, BigDecimal money, String desc);

    /**
     * 增减可用余额
     * @param type 1-减少，其他增加
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @param  desc  操作说明
     * @return
     */
    boolean updateShopAccountBalance(int type,int accountChangeType, Long shopId, BigDecimal money,String desc);

    /**
     * 冻结余额/可用余额互转
     * @param type   1-冻结转可提-结算、拒绝提现等，其他-可提转冻结-提现申请
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @param  desc  操作说明
     * @return
     *
     */
    boolean updateAccountWithdrawFrozen(int type,int accountChangeType,Long shopId, BigDecimal money,String desc);


    /**
     *  更新商家账户信息
     */
    boolean updateShopAccount(ShmsShopAccount account);

    /**
     * 根据店铺ID查询店铺账户
     * @param shopId
     * @return
     */
    ShmsShopAccount getShopAccountByShopId(Long shopId);

}
