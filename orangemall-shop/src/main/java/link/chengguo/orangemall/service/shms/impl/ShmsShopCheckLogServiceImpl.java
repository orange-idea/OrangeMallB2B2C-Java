package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopCheckLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopCheckLogMapper;
import link.chengguo.orangemall.service.shms.IShmsShopCheckLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopCheckLogServiceImpl extends ServiceImpl
<ShmsShopCheckLogMapper, ShmsShopCheckLog> implements IShmsShopCheckLogService {

@Resource
private  ShmsShopCheckLogMapper shmsShopCheckLogMapper;


}
