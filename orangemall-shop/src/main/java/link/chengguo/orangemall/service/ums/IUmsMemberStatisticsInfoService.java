package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberStatisticsInfo;

/**
 * <p>
 * 会员统计信息 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberStatisticsInfoService extends IService<UmsMemberStatisticsInfo> {

}
