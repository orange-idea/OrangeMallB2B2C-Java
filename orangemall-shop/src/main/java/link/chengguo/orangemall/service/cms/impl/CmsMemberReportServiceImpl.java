package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsMemberReport;
import link.chengguo.orangemall.cms.mapper.CmsMemberReportMapper;
import link.chengguo.orangemall.service.cms.ICmsMemberReportService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户举报表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsMemberReportServiceImpl extends ServiceImpl<CmsMemberReportMapper, CmsMemberReport> implements ICmsMemberReportService {

}
