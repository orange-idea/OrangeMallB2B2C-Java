package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.mapper.ShmsShopInfoMapper;
import link.chengguo.orangemall.service.shms.IShmsShopInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysOrderType;
import link.chengguo.orangemall.sys.mapper.SysOrderTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopInfoServiceImpl extends ServiceImpl
<ShmsShopInfoMapper, ShmsShopInfo> implements IShmsShopInfoService {

    @Resource
    private ShmsShopInfoMapper shmsShopInfoMapper;
    @Resource
    private SysOrderTypeMapper sysOrderTypeMapper;


    @Override
    public boolean updateShopInfo(ShmsShopInfo shmsShopInfo) throws Exception {
        //更新代理信息
        shmsShopInfo.setPassword(null);
        shmsShopInfoMapper.updateById(shmsShopInfo);
        return true;
    }

    @Override
    public List<SysOrderType> getSendTypes(QueryWrapper<SysOrderType> queryWrapper) {
        return sysOrderTypeMapper.selectList(queryWrapper);
    }
}
