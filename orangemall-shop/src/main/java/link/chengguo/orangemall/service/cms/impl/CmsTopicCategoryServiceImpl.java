package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsTopicCategory;
import link.chengguo.orangemall.cms.mapper.CmsTopicCategoryMapper;
import link.chengguo.orangemall.service.cms.ICmsTopicCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 话题分类表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsTopicCategoryServiceImpl extends ServiceImpl<CmsTopicCategoryMapper, CmsTopicCategory> implements ICmsTopicCategoryService {

}
