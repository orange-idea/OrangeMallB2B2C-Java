package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.shms.mapper.ShmsShopCategoryMapper;
import link.chengguo.orangemall.service.shms.IShmsShopCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopCategoryServiceImpl extends ServiceImpl
<ShmsShopCategoryMapper, ShmsShopCategory> implements IShmsShopCategoryService {

@Resource
private  ShmsShopCategoryMapper shmsShopCategoryMapper;


}
