package link.chengguo.orangemall.service.jms;

import link.chengguo.orangemall.jms.entity.JmsOrderTrack;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsOrderTrackService extends IService<JmsOrderTrack> {

    /**
     * 添加抢单池订单
     * @return
     */
    boolean addOrderTrack(JmsOrderTrack orderTrack);

}
