package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsSignRecord;

/**
 * <p>
 * 签到记录 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsSignRecordService extends IService<SmsSignRecord> {

}
