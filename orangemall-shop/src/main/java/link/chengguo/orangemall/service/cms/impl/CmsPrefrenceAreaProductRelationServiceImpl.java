package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.service.cms.ICmsPrefrenceAreaProductRelationService;
import link.chengguo.orangemall.pms.entity.CmsPrefrenceAreaProductRelation;
import link.chengguo.orangemall.pms.mapper.CmsPrefrenceAreaProductRelationMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优选专区和产品关系表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsPrefrenceAreaProductRelationServiceImpl extends
        ServiceImpl<CmsPrefrenceAreaProductRelationMapper, CmsPrefrenceAreaProductRelation> implements ICmsPrefrenceAreaProductRelationService {

}
