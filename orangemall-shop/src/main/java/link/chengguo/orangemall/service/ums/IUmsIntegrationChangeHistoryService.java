package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsIntegrationChangeHistory;

/**
 * <p>
 * 积分变化历史记录表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsIntegrationChangeHistoryService extends IService<UmsIntegrationChangeHistory> {

}
