package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotionLog;
import link.chengguo.orangemall.sms.mapper.SmsFlashPromotionLogMapper;
import link.chengguo.orangemall.service.sms.ISmsFlashPromotionLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 限时购通知记录 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsFlashPromotionLogServiceImpl extends ServiceImpl<SmsFlashPromotionLogMapper, SmsFlashPromotionLog> implements ISmsFlashPromotionLogService {

}
