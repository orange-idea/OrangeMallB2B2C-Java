package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingAccount;
import link.chengguo.orangemall.jms.mapper.JmsPickingAccountMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingAccountServiceImpl extends ServiceImpl
<JmsPickingAccountMapper, JmsPickingAccount> implements IJmsPickingAccountService {

@Resource
private  JmsPickingAccountMapper jmsPickingAccountMapper;


}
