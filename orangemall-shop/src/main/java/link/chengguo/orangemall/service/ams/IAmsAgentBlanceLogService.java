package link.chengguo.orangemall.service.ams;

import link.chengguo.orangemall.ams.entity.AmsAgentBlanceLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-20
*/

public interface IAmsAgentBlanceLogService extends IService<AmsAgentBlanceLog> {

}
