package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsGifts;
import link.chengguo.orangemall.pms.mapper.PmsGiftsMapper;
import link.chengguo.orangemall.service.pms.IPmsGiftsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
@Service
public class PmsGiftsServiceImpl extends ServiceImpl<PmsGiftsMapper, PmsGifts> implements IPmsGiftsService {

}
