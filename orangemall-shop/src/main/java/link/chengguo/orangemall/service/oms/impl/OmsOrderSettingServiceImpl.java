package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsOrderSetting;
import link.chengguo.orangemall.oms.mapper.OmsOrderSettingMapper;
import link.chengguo.orangemall.service.oms.IOmsOrderSettingService;
import link.chengguo.orangemall.util.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 * 订单设置表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsOrderSettingServiceImpl extends ServiceImpl<OmsOrderSettingMapper, OmsOrderSetting> implements IOmsOrderSettingService {

    @Resource
    private OmsOrderSettingMapper orderSettingMapper;

    @Override
    public Date getBillDayCreateDate() {
        //获取结算周期
        OmsOrderSetting omsOrderSetting = orderSettingMapper.selectById(1);
        int day = omsOrderSetting.getFinishOvertime();//天
        //获取N+1天前的时间,结算的下单时间
        Date settlementDate = DateUtils.addDateDays(new Date(), -(day+1));
//        String sDate = DateUtils.format(settlementDate, DateUtils.DATE_PATTERN);
        return settlementDate;
    }

    @Override
    public Date getSettlementDate() {
        //获取结算周期
        OmsOrderSetting omsOrderSetting = orderSettingMapper.selectById(1);
        int day = omsOrderSetting.getFinishOvertime();//天
        //获取N天前的时间,结算的下单时间
        Date settlementDate = DateUtils.addDateDays(new Date(), -(day));
        return settlementDate;
    }

    @Override
    public OmsOrderSetting getOrderSettings() {
        return this.orderSettingMapper.selectById(1);
    }
}
