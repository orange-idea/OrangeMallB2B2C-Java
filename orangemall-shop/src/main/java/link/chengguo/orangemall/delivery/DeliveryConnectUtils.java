package link.chengguo.orangemall.delivery;

import link.chengguo.orangemall.delivery.entity.CancelReason;
import link.chengguo.orangemall.delivery.entity.CommonDeliveryRequest;
import link.chengguo.orangemall.delivery.entity.DOrder;
import link.chengguo.orangemall.delivery.entity.DRiderEvaluate;
import link.chengguo.orangemall.util.HttpUtils;
import link.chengguo.orangemall.util.JsonUtil;
import link.chengguo.orangemall.utils.CommonResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 与配送系统通信工具类
 */
public class DeliveryConnectUtils {

    private static final Logger logger = LogManager.getLogger(DeliveryConnectUtils.class);
    //回调地址
    private static String URL_Callback="http://localhost:8087/dms/order/status";
    /**token平台对应的域名*/
    private static String BASE_URL="http://localhost:8076";
    /**token名称*/
    private static String tokenKey="authorization1";
    /**token值，与商家关联*/
    private static String token="db2a8140b8df417ba9aecf08991aa1dc";
    /**配送系统对应的接口编码*/
    private static String interfaceCode="hsps_mall";

    /** 添加配送订单*/
    private static String api_addOrder="/third/order/addOrder";
    /** 取消订单*/
    private static String api_cancelOrder="/third/order/cancel";
    /** 查询订单状态*/
    private static String api_queryOrder="/third/order/query";
    /** 查询骑手位置*/
    private static String api_riderLocation="/third/rider/location";
    /** 评价骑手*/
    private static String api_riderEvaluate="/third/rider/evaluate";

    /**
     * 添加订单
     * @param token 商家在配送系统注册的token
     * @param order
     * @return
     */
    public static CommonResult addOrder(String token, DOrder order){
        order.setCallback(URL_Callback);
        return post(api_addOrder,token, JsonUtil.toJsonStr(order));
    }

    /**
     * 取消订单
     * @param token 商家在配送系统注册的token
     * @param
     * @return
     */
    public static CommonResult cancelOrder(String token, CancelReason reason){
        return post(api_cancelOrder,token,JsonUtil.toJsonStr(reason));
    }

    /**
     * 查询订单状态
     * @param token
     * @param
     * @return
     */
    public static CommonResult queryOrderStatus(String token, String thirdOrderId){
        CommonDeliveryRequest params=new CommonDeliveryRequest();
        params.setThird_order_id(thirdOrderId);
        return post(api_queryOrder,token,JsonUtil.toJsonStr(params));
    }

    /**
     * 查询骑手位置
     * @param token
     * @param
     * @return
     */
    public static CommonResult queryRiderLocation(String token, String thirdOrderId){
        CommonDeliveryRequest params=new CommonDeliveryRequest();
        params.setThird_order_id(thirdOrderId);
        return post(api_riderLocation,token,JsonUtil.toJsonStr(params));
    }

    /**
     * 评价骑手
     * @param token
     * @param
     * @return
     */
    public static CommonResult riderEvaluate(String token, DRiderEvaluate riderEvaluate){
        return post(api_riderEvaluate,token,JsonUtil.toJsonStr(riderEvaluate));
    }

    /**
     * 发送到配送系统
     * @param api
     * @param params Json 字符串
     * @return
     */
    public static CommonResult post(String api,String token,String params){
        logger.info("请求参数为:" + params);
        try {
            String result= HttpUtils.post(BASE_URL+api,tokenKey,token,interfaceCode, params);
            logger.info("返回参数为:" + result);
//            JSONObject jsonObject =  JSON.parseObject(result);
//            String data = jsonObject.get("data").toString();
//            JSONObject dataObject =  JSON.parseObject(data);
//            String token=dataObject.get("token").toString();
//            String tokenHead=dataObject.get("tokenHead").toString();
            CommonResult commonResult=JsonUtil.fromJson(result,CommonResult.class);
            System.out.println(commonResult.toString());
            System.out.println(commonResult.getData());
            //发送成功
           return commonResult;
        } catch (Exception e) {
            // TODO: handle exception
            logger.error("请求异常：" + e);
            return  new CommonResult().failed("请求异常"+e.getMessage());
        }
    }

    public static void main(String[] args) throws Exception {
        DOrder order=new DOrder();
        order.setInterface_code(interfaceCode);
        order.setThird_order_id("xxxxxxxxxxxxxxxxxxxx");
        queryOrderStatus(token,"1111111111111111");
    }

}
