package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yzb
 * @date 2020-03-10
 * 第三方接入订单实体
 */
@Data
public class DGoodsDetail implements Serializable {

    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;
    /**
     *商品数量
     */
    private Integer goods_count;
    /**
     *商品图片
     */
    private String goodsImage;
    /**
     *商品规格
     */
    private String goodsAttr;
    /**
     *商品条码
     */
    private String goodsBarcode;
    /**
     *商品单位
     */
    private String goodUnit;
}
