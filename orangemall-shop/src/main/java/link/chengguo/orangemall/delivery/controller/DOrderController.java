package link.chengguo.orangemall.delivery.controller;

import link.chengguo.orangemall.delivery.entity.DOrderStatus;
import link.chengguo.orangemall.delivery.service.IDeliveryService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 *
 * 配送系统对接控制模块
 * @author yzb
 * @since 2020-03-11
 */
@Slf4j
@RestController
@Api(tags = "配送系统回调接口", description = "DOrderController")
@RequestMapping("/dms/order")
public class DOrderController extends BaseAction {
    @Resource
    private IDeliveryService deliveryService;


    @ApiOperation("订单状态回调接口")
    @PostMapping(value = "/status")
    public Object statucCallback(@RequestBody DOrderStatus entity) {
        try {
            ShmsShopInfo shopInfo=this.getShopInfo();
            if (deliveryService.updateOrderStatus(entity,shopInfo)){
                return new CommonResult().success();
            }else{
                return new CommonResult().failed("同步失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("订单状态回调接口：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }
}
