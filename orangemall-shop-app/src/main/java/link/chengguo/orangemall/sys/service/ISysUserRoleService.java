package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysUserRole;

/**
 * <p>
 * 后台用户和角色关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
