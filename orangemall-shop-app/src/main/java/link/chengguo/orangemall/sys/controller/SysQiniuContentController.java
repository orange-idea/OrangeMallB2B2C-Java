package link.chengguo.orangemall.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author chengguo
 * @since 2019-11-30
 */
@RestController
@RequestMapping("/sys/sysQiniuContent")
public class SysQiniuContentController {

}

