package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysQiniuContent;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-11-30
 */
public interface ISysQiniuContentService extends IService<SysQiniuContent> {

}
