package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysTest;

/**
 * @author orangemall
 * @date 2019-12-02
 */

public interface ISysTestService extends IService<SysTest> {

}
