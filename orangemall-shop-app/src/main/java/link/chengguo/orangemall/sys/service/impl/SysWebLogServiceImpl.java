package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysWebLog;
import link.chengguo.orangemall.sys.mapper.SysWebLogMapper;
import link.chengguo.orangemall.sys.service.ISysWebLogService;
import link.chengguo.orangemall.vo.LogParam;
import link.chengguo.orangemall.vo.LogStatisc;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Service
public class SysWebLogServiceImpl extends ServiceImpl<SysWebLogMapper, SysWebLog> implements ISysWebLogService {

    @Resource
    private SysWebLogMapper logMapper;

    @Override
    public List<LogStatisc> selectPageExt(LogParam entity) {
        return logMapper.getLogStatisc(entity);
    }
}
