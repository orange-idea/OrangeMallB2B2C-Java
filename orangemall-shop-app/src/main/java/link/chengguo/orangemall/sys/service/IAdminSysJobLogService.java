package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.AdminSysJobLog;

/**
 * <p>
 * 定时任务调度日志表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-11-26
 */
public interface IAdminSysJobLogService extends IService<AdminSysJobLog> {

    void addJobLog(AdminSysJobLog jobLog);
}
