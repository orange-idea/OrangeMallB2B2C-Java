package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bo.Tree;
import link.chengguo.orangemall.sys.entity.SysPermission;
import link.chengguo.orangemall.sys.entity.SysPermissionNode;

import java.util.List;

/**
 * <p>
 * 后台用户权限表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysPermissionService extends IService<SysPermission> {

    List<Tree<SysPermission>> getPermissionsByUserId(Long id);

    List<SysPermissionNode> treeList();

    List<Tree<SysPermission>> getAllPermission();
}
