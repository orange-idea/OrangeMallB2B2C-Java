package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;
import link.chengguo.orangemall.oms.mapper.OmsOrderItemMapper;
import link.chengguo.orangemall.service.oms.IOmsOrderItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 订单中所包含的商品 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsOrderItemServiceImpl extends ServiceImpl<OmsOrderItemMapper, OmsOrderItem> implements IOmsOrderItemService {

    @Resource
    OmsOrderItemMapper orderItemMapper;

    @Override
    public List<OmsOrderItem> getGoodsListByOrderSn(String orderSn) {
        return orderItemMapper.selectList(new QueryWrapper<OmsOrderItem>().eq("order_sn",orderSn));
    }
}
