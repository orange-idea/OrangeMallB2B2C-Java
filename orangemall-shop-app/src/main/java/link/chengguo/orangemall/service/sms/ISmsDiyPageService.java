package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsDiyPage;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-11-19
 */
public interface ISmsDiyPageService extends IService<SmsDiyPage> {

    Integer selDiyPageTypeId(Integer typeId, Long id);

    Object selDiyPageDetail(SmsDiyPage entity);

    Integer selectCounts(Long id, String name);
}
