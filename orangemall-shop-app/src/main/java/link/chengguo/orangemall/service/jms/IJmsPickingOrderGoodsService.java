package link.chengguo.orangemall.service.jms;

import link.chengguo.orangemall.jms.entity.JmsPickingOrderGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingOrderGoodsService extends IService<JmsPickingOrderGoods> {

    /**
     * 添加拣货商品信息
     * @param goods
     * @return
     */
    boolean addPickingOrderGoods(JmsPickingOrderGoods goods);

}
