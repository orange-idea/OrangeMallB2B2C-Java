package link.chengguo.orangemall.service.oms;

import link.chengguo.orangemall.oms.entity.OmsVerifyLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-01-07
*/

public interface IOmsVerifyLogService extends IService<OmsVerifyLog> {

}
