package link.chengguo.orangemall.service.oms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.vo.OmsMoneyInfoParam;
import link.chengguo.orangemall.oms.vo.OmsOrderDeliveryParam;
import link.chengguo.orangemall.oms.vo.OmsReceiverInfoParam;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderService extends IService<OmsOrder> {

    /**
     * 定时处理超时未接单订单,退款
     */
    boolean manageOvertimeReceiptOrder();

    /**
     * 订单超时关闭处理,未支付订单
     */
    boolean manageOvertimeOrderClose();

    /**
     * 查询超时订单
     * @param  type  0-超时未支付，1-超时未接单，2-超时未抢单，3-超时未确认,4-超时未评价
     */
    List<OmsOrder> getOvertimeOrderList(int type);

    /**
     * 商家设置待取货状态（自提）
     */
    @Transactional
    int updateWaitGet(Long orderId,Integer sendType);

    /**
     * 商家拒绝接单
     */
    @Transactional
    int refuseOrder(Long orderId,Integer sendType);

    /**
     * 商家接单
     */
    @Transactional
    int ensureOrder(Long orderId,Integer sendType);

    /**
     * 添加拣货订单
     * @param order
     * @return
     */
    boolean addPickingOrder(OmsOrder order);

    /**
     * 修改订单收货人信息
     */
    @Transactional
    int updateReceiverInfo(OmsReceiverInfoParam receiverInfoParam);

    /**
     * 修改订单费用信息
     */
    @Transactional
    int updateMoneyInfo(OmsMoneyInfoParam moneyInfoParam);

    /**
     * 修改订单备注
     */
    @Transactional
    int updateNote(Long id, String note, Integer status);


    /**
     * 批量发货
     */
    @Transactional
    int delivery(List<OmsOrderDeliveryParam> deliveryParamList);

    /**
     * 批量关闭订单
     */
    @Transactional
    int close(List<Long> ids, String note);

    @Transactional
    int singleDelivery(OmsOrderDeliveryParam deliveryParamList);

    /**
     * 订单日统计
     *
     * @param date
     * @return
     */
    Map orderDayStatic(String date);

    /**
     * 订单月统计
     *
     * @param date
     * @return
     */
    Map orderMonthStatic(String date);

    Object dayStatic(String date, Integer type);

    /**
     * 订单核销
     * @param orderId 订单编号
     * @return
     */
    int orderVerify(Long orderId);


}
