package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.SysAppletSet;
import link.chengguo.orangemall.ums.mapper.SysAppletSetMapper;
import link.chengguo.orangemall.service.ums.ISysAppletSetService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
@Service
public class SysAppletSetServiceImpl extends ServiceImpl<SysAppletSetMapper, SysAppletSet> implements ISysAppletSetService {

}
