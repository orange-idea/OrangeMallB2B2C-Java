package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingOrderGoods;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderGoodsMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingOrderGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingOrderGoodsServiceImpl extends ServiceImpl
<JmsPickingOrderGoodsMapper, JmsPickingOrderGoods> implements IJmsPickingOrderGoodsService {

@Resource
private  JmsPickingOrderGoodsMapper jmsPickingOrderGoodsMapper;


    @Transactional
    @Override
    public boolean addPickingOrderGoods(JmsPickingOrderGoods goods) {
        goods.setCreateTime(new Date());
        jmsPickingOrderGoodsMapper.insert(goods);
        return true;
    }
}
