package link.chengguo.orangemall.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.CmsPrefrenceAreaProductRelation;

/**
 * <p>
 * 优选专区和产品关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsPrefrenceAreaProductRelationService extends IService<CmsPrefrenceAreaProductRelation> {

}
