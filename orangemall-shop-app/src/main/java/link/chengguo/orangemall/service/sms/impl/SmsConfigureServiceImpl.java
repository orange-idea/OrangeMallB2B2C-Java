package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsConfigure;
import link.chengguo.orangemall.sms.mapper.SmsConfigureMapper;
import link.chengguo.orangemall.service.sms.ISmsConfigureService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品配置表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-18
 */
@Service
public class SmsConfigureServiceImpl extends ServiceImpl<SmsConfigureMapper, SmsConfigure> implements ISmsConfigureService {

}
