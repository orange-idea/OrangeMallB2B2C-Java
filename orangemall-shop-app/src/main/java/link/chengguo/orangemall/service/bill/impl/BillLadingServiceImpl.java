package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillLading;
import link.chengguo.orangemall.bill.mapper.BillLadingMapper;
import link.chengguo.orangemall.service.bill.IBillLadingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提货单表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillLadingServiceImpl extends ServiceImpl<BillLadingMapper, BillLading> implements IBillLadingService {

}
