package link.chengguo.orangemall.service.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopBillMonthService extends IService<ShmsShopBillMonth> {

    /**
     * 商户账单生成日统计
     * @return
     */
    boolean  addShopBillMonthForTask();

    /**
     * 商户账单生成日统计
     * @param date
     * @return
     */
    boolean  addShopBillMonth(Date date);

    /**
     * 添加或更新月账单
     * @param shmsShopBillMonth
     * @return
     */
    boolean addOrUpdateShopBillDay(ShmsShopBillMonth shmsShopBillMonth);

    /**
     * 根据店铺ID和账单时间查询日账单
     * @param shopId
     * @param  month
     * @return
     */
    ShmsShopBillMonth getShmsShopBillMonth(Long shopId,String month);

}
