package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingOrderCombine;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderCombineMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingOrderCombineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingOrderCombineServiceImpl extends ServiceImpl
<JmsPickingOrderCombineMapper, JmsPickingOrderCombine> implements IJmsPickingOrderCombineService {

@Resource
private  JmsPickingOrderCombineMapper jmsPickingOrderCombineMapper;


}
