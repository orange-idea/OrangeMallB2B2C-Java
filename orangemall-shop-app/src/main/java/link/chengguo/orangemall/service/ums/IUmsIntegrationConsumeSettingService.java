package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsIntegrationConsumeSetting;

/**
 * <p>
 * 积分消费设置 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsIntegrationConsumeSettingService extends IService<UmsIntegrationConsumeSetting> {

}
