package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopConfig;
import link.chengguo.orangemall.shms.mapper.ShmsShopConfigMapper;
import link.chengguo.orangemall.service.shms.IShmsShopConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopConfigServiceImpl extends ServiceImpl
<ShmsShopConfigMapper, ShmsShopConfig> implements IShmsShopConfigService {

@Resource
private  ShmsShopConfigMapper shmsShopConfigMapper;


}
