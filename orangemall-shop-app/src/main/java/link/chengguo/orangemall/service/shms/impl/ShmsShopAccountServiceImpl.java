package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.enums.AccountChangeType;
import link.chengguo.orangemall.shms.entity.ShmsShopAccount;
import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopAccountMapper;
import link.chengguo.orangemall.service.shms.IShmsShopAccountLogService;
import link.chengguo.orangemall.service.shms.IShmsShopAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopAccountServiceImpl extends ServiceImpl
<ShmsShopAccountMapper, ShmsShopAccount> implements IShmsShopAccountService {

    @Resource
    private ShmsShopAccountMapper shmsShopAccountMapper;
    @Resource
    private IShmsShopAccountLogService shmsShopAccountLogService;

    /**
     * 增减冻结余额
     * @param type 1-减少，其他增加
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @return
     */
    @Transactional
    @Override
    public boolean updateShopAccountFrozenBalance(int type, int accountChangeType, Long shopId, BigDecimal money, String desc) {
        //查询账户
        ShmsShopAccount account=shmsShopAccountMapper.selectById(shopId);
        if (account==null){
            System.err.println("账户不存在！！");
            return false;
        }
        BigDecimal lastBalance=account.getBalance();
        BigDecimal lastBalanceFrozen=account.getBalanceFrozen();
        if (type==1){//减少
            account.setBalanceFrozen(lastBalanceFrozen.subtract(money));
            account.setBalance(lastBalance.subtract(money));
            account.setTotalIncome(account.getTotalIncome().subtract(money));
        }else {//增加
            account.setBalanceFrozen(lastBalanceFrozen.add(money));
            account.setBalance(lastBalance.add(money));
            account.setTotalIncome(account.getTotalIncome().add(money));
        }
        this.updateShopAccount(account);
        //添加余额操作记录
        ShmsShopAccountLog accountLog=new ShmsShopAccountLog();
        accountLog.setShopId(shopId);
        accountLog.setShopAccountId(account.getId());
        accountLog.setBalance(account.getBalance());
        accountLog.setBalanceFrozen(account.getBalanceFrozen());
        accountLog.setBalanceWithdraw(account.getBalanceWithdraw());
        //记录修改前的数据
        accountLog.setLastBalance(lastBalance);
        accountLog.setLastBalanceFrozen(lastBalanceFrozen);
        accountLog.setLastBalanceWithdraw(account.getBalanceWithdraw());
        accountLog.setChangeType(accountChangeType);
        accountLog.setMoney(money);
        accountLog.setChangeDesc(desc);
        if (type==1){
            accountLog.setSymbol("-");
        }else{
            accountLog.setSymbol("+");
        }
        shmsShopAccountLogService.addShopAccountLog(accountLog);
        return true;
    }


    /**
     * 增减可用余额
     * @param type 1-减少，其他增加
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @return
     */
    @Transactional
    @Override
    public boolean updateShopAccountBalance(int type,int accountChangeType, Long shopId, BigDecimal money,String desc) {
        //查询账户
        ShmsShopAccount account=shmsShopAccountMapper.selectById(shopId);
        if (account==null){
            System.err.println("账户不存在！！");
            return false;
        }
        BigDecimal lastBalance=account.getBalance();
        BigDecimal lastBalanceWithdraw=account.getBalanceWithdraw();
        if (type==1){//减少
            account.setBalanceWithdraw(lastBalanceWithdraw.subtract(money));
            account.setBalance(lastBalance.subtract(money));
            account.setTotalIncome(account.getTotalIncome().subtract(money));
            //订单结算才涉及到佣金计算
            if (accountChangeType== AccountChangeType.OrderSettlement.getValue()){
                account.setTotalBrokerage(account.getTotalBrokerage().subtract(money));
            }
        }else {//增加
            account.setBalanceWithdraw(lastBalanceWithdraw.add(money));
            account.setBalance(lastBalance.add(money));
            account.setTotalIncome(account.getTotalIncome().add(money));
            if (accountChangeType== AccountChangeType.OrderSettlement.getValue()){
                account.setTotalBrokerage(account.getTotalBrokerage().add(money));
            }
        }
        updateShopAccount(account);
        //添加余额操作记录
        ShmsShopAccountLog accountLog=new ShmsShopAccountLog();
        accountLog.setShopId(shopId);
        accountLog.setShopAccountId(account.getId());
        accountLog.setBalance(account.getBalance());
        accountLog.setBalanceFrozen(account.getBalanceFrozen());
        accountLog.setBalanceWithdraw(account.getBalanceWithdraw());
        //记录修改前的数据
        accountLog.setLastBalance(lastBalance);
        accountLog.setLastBalanceFrozen(account.getBalanceFrozen());
        accountLog.setLastBalanceWithdraw(lastBalanceWithdraw);
        accountLog.setChangeType(accountChangeType);
        accountLog.setMoney(money);
        accountLog.setChangeDesc(desc);
        if (type==1){
            accountLog.setSymbol("-");
        }else{
            accountLog.setSymbol("+");
        }
        shmsShopAccountLogService.addShopAccountLog(accountLog);
        return true;
    }


    /**
     * 冻结余额/可用余额互转
     * @param type   1-冻结转可提-结算、拒绝提现等，其他-可提转冻结-提现申请
     * @param  accountChangeType 查看AccountChangeType常量 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     * @param shopId 店铺ID
     * @param money  操作金额
     * @param  desc  操作说明
     * @return
     */
    @Override
    public boolean updateAccountWithdrawFrozen(int type,int accountChangeType,Long shopId, BigDecimal money,String desc) {
        //查询账户
        ShmsShopAccount account=shmsShopAccountMapper.selectById(shopId);
        if (account==null){
            System.err.println("账户不存在！！");
            return false;
        }
        BigDecimal lastBalanceFrozen=account.getBalanceFrozen();
        BigDecimal lastBalanceWithdraw=account.getBalanceWithdraw();
        if(type==1){//冻结转可提
            account.setBalanceWithdraw(lastBalanceWithdraw.add(money));
            account.setBalanceFrozen(lastBalanceFrozen.subtract(money));
        }else{
            account.setBalanceWithdraw(lastBalanceWithdraw.subtract(money));
            account.setBalanceFrozen(lastBalanceFrozen.add(money));
        }
        //添加余额操作记录
        ShmsShopAccountLog accountLog=new ShmsShopAccountLog();
        accountLog.setShopId(shopId);
        accountLog.setShopAccountId(account.getId());
        accountLog.setBalance(account.getBalance());
        accountLog.setBalanceFrozen(account.getBalanceFrozen());
        accountLog.setBalanceWithdraw(account.getBalanceWithdraw());
        //记录修改前的数据
        accountLog.setLastBalance(account.getBalance());
        accountLog.setLastBalanceFrozen(lastBalanceFrozen);
        accountLog.setLastBalanceWithdraw(lastBalanceWithdraw);
        accountLog.setChangeType(accountChangeType);
        accountLog.setMoney(money);
        accountLog.setChangeDesc(desc);
        accountLog.setSymbol("");//操作总额不变，所以总金额不变。
        shmsShopAccountLogService.addShopAccountLog(accountLog);
        return true;
    }

    @Override
    public boolean updateShopAccount(ShmsShopAccount account) {
        return shmsShopAccountMapper.updateById(account) > 0 ? true:false;
    }

    @Override
    public ShmsShopAccount getShopAccountByShopId(Long shopId) {
        ShmsShopAccount account=new ShmsShopAccount();
        account.setShopId(shopId);
        ShmsShopAccount account1=shmsShopAccountMapper.selectOne(new QueryWrapper<>(account));
        return account1;
    }
}
