package link.chengguo.orangemall.service.bill;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillReshipItems;

/**
 * <p>
 * 退货单明细表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillReshipItemsService extends IService<BillReshipItems> {

}
