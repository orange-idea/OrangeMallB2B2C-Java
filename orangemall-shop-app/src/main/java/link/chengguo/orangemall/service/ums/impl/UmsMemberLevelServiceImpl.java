package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberLevel;
import link.chengguo.orangemall.ums.mapper.UmsMemberLevelMapper;
import link.chengguo.orangemall.service.ums.IUmsMemberLevelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员等级表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberLevelServiceImpl extends ServiceImpl<UmsMemberLevelMapper, UmsMemberLevel> implements IUmsMemberLevelService {

}
