package link.chengguo.orangemall.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsSubjectCategory;

/**
 * <p>
 * 专题分类表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsSubjectCategoryService extends IService<CmsSubjectCategory> {

}
