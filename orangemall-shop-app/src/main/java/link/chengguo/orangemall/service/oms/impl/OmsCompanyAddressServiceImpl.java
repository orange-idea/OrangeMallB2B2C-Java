package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsCompanyAddress;
import link.chengguo.orangemall.oms.mapper.OmsCompanyAddressMapper;
import link.chengguo.orangemall.service.oms.IOmsCompanyAddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author orangemall
 * @date 2019-12-07
 */
@Service
public class OmsCompanyAddressServiceImpl extends ServiceImpl<OmsCompanyAddressMapper, OmsCompanyAddress> implements IOmsCompanyAddressService {

    @Resource
    private OmsCompanyAddressMapper omsCompanyAddressMapper;


}
