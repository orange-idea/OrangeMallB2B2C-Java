package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsPaimaiLog;
import link.chengguo.orangemall.sms.mapper.SmsPaimaiLogMapper;
import link.chengguo.orangemall.service.sms.ISmsPaimaiLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-15
 */
@Service
public class SmsPaimaiLogServiceImpl extends ServiceImpl<SmsPaimaiLogMapper, SmsPaimaiLog> implements ISmsPaimaiLogService {

}
