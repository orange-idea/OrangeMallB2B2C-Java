package link.chengguo.orangemall.service.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopBillLogService extends IService<ShmsShopBillLog> {

    /**
     * 账单结算，每分钟执行一次账单结算。
     * @return
     */
    boolean shopBillSettement();

    /**
     * 查询统计信息，日统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    List<ShmsShopBillDay> statisticsDayBill(ShmsShopBillLog paramCondition);

    /**
     * 根据店铺ID查询账单统计
     * @param paramCondition
     * @return
     */
    ShmsShopBillDay statisticsBillByShopId(ShmsShopBillLog paramCondition);


    /**
     * 查询待结算订单列表
     * @param  date
     * @author yzb
     * @Date 2020-02-13
     */
    List<ShmsShopBillLog> getWaitSettlementList(Date date);

}
