package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsHelpCategory;
import link.chengguo.orangemall.cms.mapper.CmsHelpCategoryMapper;
import link.chengguo.orangemall.service.cms.ICmsHelpCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助分类表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsHelpCategoryServiceImpl extends ServiceImpl<CmsHelpCategoryMapper, CmsHelpCategory> implements ICmsHelpCategoryService {

}
