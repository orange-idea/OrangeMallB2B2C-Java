package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;
import link.chengguo.orangemall.pms.mapper.PmsSmallNaviconCategoryMapper;
import link.chengguo.orangemall.service.pms.IPmsSmallNaviconCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Service
public class PmsSmallNaviconCategoryServiceImpl extends ServiceImpl<PmsSmallNaviconCategoryMapper, PmsSmallNaviconCategory> implements IPmsSmallNaviconCategoryService {
}
