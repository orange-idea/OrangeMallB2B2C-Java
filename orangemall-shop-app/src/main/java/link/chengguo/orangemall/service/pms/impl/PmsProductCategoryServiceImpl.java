package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.entity.PmsProductCategoryAttributeRelation;
import link.chengguo.orangemall.pms.mapper.PmsProductCategoryAttributeRelationMapper;
import link.chengguo.orangemall.pms.mapper.PmsProductCategoryMapper;
import link.chengguo.orangemall.pms.mapper.PmsProductMapper;
import link.chengguo.orangemall.service.pms.IPmsProductCategoryAttributeRelationService;
import link.chengguo.orangemall.service.pms.IPmsProductCategoryService;
import link.chengguo.orangemall.pms.vo.PmsProductCategoryWithChildrenItem;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 产品分类 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsProductCategoryServiceImpl extends ServiceImpl<PmsProductCategoryMapper, PmsProductCategory> implements IPmsProductCategoryService {

    @Resource
    private PmsProductCategoryMapper categoryMapper;
    @Resource
    private PmsProductMapper productMapper;
    @Resource
    private IPmsProductCategoryAttributeRelationService pmsProductCategoryAttributeRelationService;
    @Resource
    private PmsProductCategoryAttributeRelationMapper productCategoryAttributeRelationMapper;

    @Override
    public List<PmsProductCategoryWithChildrenItem> listWithChildren(PmsProductCategory category) {
//        category.setCityCode(UserUtils.getCurrentMember().getCityCode());
//        category.setShopId(UserUtils.getCurrentMember().getShopId());
        return categoryMapper.listWithChildren(category);
    }

    @Override
    public int updateNavStatus(List<Long> ids, Integer navStatus) {
        PmsProductCategory productCategory = new PmsProductCategory();
        productCategory.setNavStatus(navStatus);
        return categoryMapper.update(productCategory, new QueryWrapper<PmsProductCategory>().in("id", ids));
    }

    @Override
    public int updateShowStatus(List<Long> ids, Integer showStatus) {
        PmsProductCategory productCategory = new PmsProductCategory();
        productCategory.setShowStatus(showStatus);
        return categoryMapper.update(productCategory, new QueryWrapper<PmsProductCategory>().in("id", ids));
    }

    @Override
    public int updateIndexStatus(List<Long> ids, Integer indexStatus) {
        PmsProductCategory productCategory = new PmsProductCategory();
        productCategory.setIndexStatus(indexStatus);
        return categoryMapper.update(productCategory, new QueryWrapper<PmsProductCategory>().in("id", ids));
    }

    @Override
    public boolean updateAnd(PmsProductCategory entity) {
        PmsProductCategory productCategory = new PmsProductCategory();
        setCategoryLevel(entity);
        //更新商品分类时要更新商品中的名称
        PmsProduct product = new PmsProduct();
        product.setProductCategoryName(entity.getName());

        productMapper.update(product, new QueryWrapper<PmsProduct>().eq("product_category_id", entity.getId()));
        //同时更新筛选属性的信息
        if (!CollectionUtils.isEmpty(entity.getProductAttributeIdList())) {

            productCategoryAttributeRelationMapper.delete(new QueryWrapper<>(new PmsProductCategoryAttributeRelation()).eq("product_category_id", entity.getId()));
            insertRelationList(entity.getId(), entity.getProductAttributeIdList());
        } else {
            productCategoryAttributeRelationMapper.delete(new QueryWrapper<>(new PmsProductCategoryAttributeRelation()).eq("product_category_id", entity.getId()));

        }
        categoryMapper.updateById(entity);
        return true;
    }

    @Override
    public boolean saveAnd(PmsProductCategory entity) {
        PmsProductCategory productCategory = new PmsProductCategory();
        productCategory.setProductCount(0);
        BeanUtils.copyProperties(entity, productCategory);
        //没有父分类时为一级分类
        setCategoryLevel(productCategory);
        int count = categoryMapper.insert(productCategory);
        //创建筛选属性关联
        List<Long> productAttributeIdList = entity.getProductAttributeIdList();
        if (!CollectionUtils.isEmpty(productAttributeIdList)) {
            insertRelationList(productCategory.getId(), productAttributeIdList);
        }
        return true;
    }

    /**
     *  根据ID，查询上级分类
     * @param id
     * @return
     */
    @Override
    public PmsProductCategory getParentProductCategoryById(Long id) {
        PmsProductCategory category=categoryMapper.selectById(id);
        if (category.getParentId()!=null && category.getParentId()!=0){
            PmsProductCategory category1=categoryMapper.selectById(category.getParentId());
            return category1;
        }
        return category;
    }

    /**
     * 批量插入商品分类与筛选属性关系表
     *
     * @param productCategoryId      商品分类id
     * @param productAttributeIdList 相关商品筛选属性id集合
     */
    private void insertRelationList(Long productCategoryId, List<Long> productAttributeIdList) {
        List<PmsProductCategoryAttributeRelation> relationList = new ArrayList<>();
        for (Long productAttrId : productAttributeIdList) {
            PmsProductCategoryAttributeRelation relation = new PmsProductCategoryAttributeRelation();
            relation.setProductAttributeId(productAttrId);
            relation.setProductCategoryId(productCategoryId);
            relationList.add(relation);
        }
        pmsProductCategoryAttributeRelationService.saveBatch(relationList);
    }

    /**
     * 根据分类的parentId设置分类的level
     */
    private void setCategoryLevel(PmsProductCategory productCategory) {
        //没有父分类时为一级分类
        if (productCategory.getParentId() == 0) {
            productCategory.setLevel(0);
        } else {
            //有父分类时选择根据父分类level设置
            PmsProductCategory parentCategory = categoryMapper.selectById(productCategory.getParentId());
            if (parentCategory != null) {
                productCategory.setLevel(parentCategory.getLevel() + 1);
            } else {
                productCategory.setLevel(0);
            }
        }
    }
}
