package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.SysAppletSet;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
public interface ISysAppletSetService extends IService<SysAppletSet> {

}
