package link.chengguo.orangemall.service.shms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysOrderType;

import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopInfoService extends IService<ShmsShopInfo> {
    /**
     * 更新店铺
     * @param shmsShopInfo
     * @return
     */
    boolean updateShopInfo(ShmsShopInfo shmsShopInfo) throws Exception;

    List<SysOrderType> getSendTypes(QueryWrapper<SysOrderType> queryWrapper);
}
