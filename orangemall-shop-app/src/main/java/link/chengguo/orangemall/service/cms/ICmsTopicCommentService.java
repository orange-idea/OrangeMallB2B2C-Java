package link.chengguo.orangemall.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsTopicComment;

/**
 * <p>
 * 专题评论表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsTopicCommentService extends IService<CmsTopicComment> {

}
