package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.fms.entity.FmsShopWithdrawAccount;
import link.chengguo.orangemall.fms.mapper.FmsShopWithdrawAccountMapper;
import link.chengguo.orangemall.service.fms.IFmsShopWithdrawAccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsShopWithdrawAccountServiceImpl extends ServiceImpl
<FmsShopWithdrawAccountMapper, FmsShopWithdrawAccount> implements IFmsShopWithdrawAccountService {

@Resource
private  FmsShopWithdrawAccountMapper fmsShopWithdrawAccountMapper;


}
