package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsDrawUser;
import link.chengguo.orangemall.sms.mapper.SmsDrawUserMapper;
import link.chengguo.orangemall.service.sms.ISmsDrawUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抽奖与用户关联表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsDrawUserServiceImpl extends ServiceImpl<SmsDrawUserMapper, SmsDrawUser> implements ISmsDrawUserService {

}
