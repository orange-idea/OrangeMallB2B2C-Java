package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.fms.entity.FmsShopWithdraw;
import link.chengguo.orangemall.fms.mapper.FmsShopWithdrawMapper;
import link.chengguo.orangemall.service.fms.IFmsShopWithdrawService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsShopWithdrawServiceImpl extends ServiceImpl
<FmsShopWithdrawMapper, FmsShopWithdraw> implements IFmsShopWithdrawService {

@Resource
private  FmsShopWithdrawMapper fmsShopWithdrawMapper;


}
