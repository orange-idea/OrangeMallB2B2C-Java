package link.chengguo.orangemall.service.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;

import java.util.Date;
import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopBillDayService extends IService<ShmsShopBillDay> {


    /**
     * 商户账单生成日统计
     * @return
     */
    boolean  addShopBillDayForTask();


    /**
     * 商户账单生成日统计
     * @param date
     * @return
     */
    boolean  addShopBillDay(Date date);

    /**
     * 添加日账单
     * @param shmsShopBillDay
     * @return
     */
    boolean addOrUpdateShopBillDay(ShmsShopBillDay shmsShopBillDay);


    /**
     * 根据店铺ID和账单时间查询日账单
     * @param shopId
     * @param  date
     * @return
     */
    ShmsShopBillDay getShmsShopBillDay(Long shopId,String date);

    /**
     * 查询统计信息，日统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    List<ShmsShopBillMonth> statisticsMonthBill(ShmsShopBillDay paramCondition);



}
