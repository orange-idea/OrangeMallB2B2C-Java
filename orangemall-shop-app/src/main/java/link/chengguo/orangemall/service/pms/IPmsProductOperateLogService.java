package link.chengguo.orangemall.service.pms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductOperateLog;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductOperateLogService extends IService<PmsProductOperateLog> {

}
