package link.chengguo.orangemall.service.jms;

import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-27
*/

public interface IJmsGoodsTypeTrackService extends IService<JmsGoodsTypeTrack> {

    /**
     * 添加拣货商品分类信息
     * @return
     */
    boolean addGoodsTypeTrack(JmsGoodsTypeTrack goodsTypeTrack);

}
