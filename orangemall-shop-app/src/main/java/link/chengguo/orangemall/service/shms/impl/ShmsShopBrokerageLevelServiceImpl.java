package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopBrokerageLevel;
import link.chengguo.orangemall.shms.mapper.ShmsShopBrokerageLevelMapper;
import link.chengguo.orangemall.service.shms.IShmsShopBrokerageLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopBrokerageLevelServiceImpl extends ServiceImpl
<ShmsShopBrokerageLevelMapper, ShmsShopBrokerageLevel> implements IShmsShopBrokerageLevelService {

@Resource
private  ShmsShopBrokerageLevelMapper shmsShopBrokerageLevelMapper;


}
