package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.mapper.JmsPickingUserMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsPickingUserServiceImpl extends ServiceImpl
        <JmsPickingUserMapper, JmsPickingUser> implements IJmsPickingUserService {

    @Resource
    private JmsPickingUserMapper jmsPickingUserMapper;
    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public boolean addPickingUser(JmsPickingUser user) {
        //判定是否存在，如果已存在则无法重复添加
        JmsPickingUser re=jmsPickingUserMapper.selectOne(new QueryWrapper<JmsPickingUser>().eq("phone",user.getPhone()));
        if (re!=null){
            throw new RuntimeException("该手机号已存在，请勿重复添加!");
        }
        String md5Password = passwordEncoder.encode(user.getPwd());
        user.setPwd(md5Password);
        user.setCreateTime(new Date());
        jmsPickingUserMapper.insert(user);
        return true;
    }

    @Override
    public boolean updatePwd(JmsPickingUser user) {
        String md5Password = passwordEncoder.encode(user.getPwd());
        user.setPwd(md5Password);
        user.setUpdateTime(new Date());
        jmsPickingUserMapper.updateById(user);
        return true;
    }

    @Override
    public boolean updatePickingUser(JmsPickingUser user) {
        int count=jmsPickingUserMapper.selectCount(new QueryWrapper<JmsPickingUser>().eq("phone",user.getPhone()));
        if (count>1){
            throw new RuntimeException("该手机号已存在");
        }
        user.setPwd(null);
        user.setUpdateTime(new Date());
        jmsPickingUserMapper.updateById(user);
        return true;
    }
}
