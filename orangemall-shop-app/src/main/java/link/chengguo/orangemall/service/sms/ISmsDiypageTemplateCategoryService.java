package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsDiypageTemplateCategory;

/**
 * @author orangemall
 * @date 2019-12-04
 */

public interface ISmsDiypageTemplateCategoryService extends IService<SmsDiypageTemplateCategory> {

    Object selTemplateCategory();
}
