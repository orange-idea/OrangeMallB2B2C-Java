package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberTag;
import link.chengguo.orangemall.ums.mapper.UmsMemberTagMapper;
import link.chengguo.orangemall.service.ums.IUmsMemberTagService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户标签表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberTagServiceImpl extends ServiceImpl<UmsMemberTagMapper, UmsMemberTag> implements IUmsMemberTagService {

}
