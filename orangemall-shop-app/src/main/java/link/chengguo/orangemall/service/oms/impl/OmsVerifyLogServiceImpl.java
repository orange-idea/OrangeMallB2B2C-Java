package link.chengguo.orangemall.service.oms.impl;

import link.chengguo.orangemall.oms.entity.OmsVerifyLog;
import link.chengguo.orangemall.oms.mapper.OmsVerifyLogMapper;
import link.chengguo.orangemall.service.oms.IOmsVerifyLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-01-07
*/
@Service
public class OmsVerifyLogServiceImpl extends ServiceImpl
<OmsVerifyLogMapper, OmsVerifyLog> implements IOmsVerifyLogService {

@Resource
private  OmsVerifyLogMapper omsVerifyLogMapper;


}
