package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillMonthMapper;
import link.chengguo.orangemall.service.shms.IShmsShopBillDayService;
import link.chengguo.orangemall.service.shms.IShmsShopBillMonthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.util.DateUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillMonthServiceImpl extends ServiceImpl
        <ShmsShopBillMonthMapper, ShmsShopBillMonth> implements IShmsShopBillMonthService {

    @Resource
    private ShmsShopBillMonthMapper shmsShopBillMonthMapper;
    @Resource
    private IShmsShopBillDayService shopBillDayService;


    @Override
    public boolean addShopBillMonthForTask() {
        //查询结算的账单，并进行结算操作
        ShmsShopBillDay billDay=new ShmsShopBillDay();
        billDay.setStartTime(DateUtils.getFirstDayByMonth(-1));//上个月第一天，查询包含
        billDay.setEndTime(DateUtils.getFirstDayByMonth(0));//这个月第一天，查询不包含
        List<ShmsShopBillMonth> shopBillMonthList=shopBillDayService.statisticsMonthBill(billDay);
        for (int i=0;i<shopBillMonthList.size();i++){
            shopBillMonthList.get(i).setMonth(DateUtils.format(DateUtils.addDateMonths(new Date(),-1),"yyyy-MM"));
            shopBillMonthList.get(i).setCreateTime(new Date());
            addOrUpdateShopBillDay(shopBillMonthList.get(i));
        }
        return false;
    }

    @Override
    public boolean addShopBillMonth(Date date) {

        return false;
    }

    @Override
    public boolean addOrUpdateShopBillDay(ShmsShopBillMonth shmsShopBillMonth) {
        ShmsShopBillMonth tmp=getShmsShopBillMonth(shmsShopBillMonth.getShopId(),shmsShopBillMonth.getMonth());
        if (tmp!=null){
            shmsShopBillMonth.setId(tmp.getId());
            shmsShopBillMonthMapper.updateById(shmsShopBillMonth);
        }else{
            shmsShopBillMonthMapper.insert(shmsShopBillMonth);
        }
        return true;
    }

    @Override
    public ShmsShopBillMonth getShmsShopBillMonth(Long shopId, String month) {
        ShmsShopBillMonth shopBillDay=new ShmsShopBillMonth();
        shopBillDay.setShopId(shopId);
        shopBillDay.setMonth(month);
        return shmsShopBillMonthMapper.selectOne(new QueryWrapper<>(shopBillDay));
    }
}
