package link.chengguo.orangemall.service.pms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.vo.PmsProductCategoryWithChildrenItem;

import java.util.List;

/**
 * <p>
 * 产品分类 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductCategoryService extends IService<PmsProductCategory> {

    List<PmsProductCategoryWithChildrenItem> listWithChildren(PmsProductCategory category);

    int updateNavStatus(List<Long> ids, Integer navStatus);

    int updateShowStatus(List<Long> ids, Integer showStatus);

    int updateIndexStatus(List<Long> ids, Integer indexStatus);

    boolean updateAnd(PmsProductCategory entity);

    boolean saveAnd(PmsProductCategory entity);

    PmsProductCategory getParentProductCategoryById(Long id);
}
