package link.chengguo.orangemall.service.oms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;

import java.util.List;

/**
 * <p>
 * 订单中所包含的商品 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderItemService extends IService<OmsOrderItem> {

    /**
     * 根据订单编码查询订单下的商品信息
     * @return
     */
    List<OmsOrderItem> getGoodsListByOrderSn(String orderSn);

}
