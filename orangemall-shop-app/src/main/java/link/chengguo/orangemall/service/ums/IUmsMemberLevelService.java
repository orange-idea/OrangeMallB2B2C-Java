package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberLevel;

/**
 * <p>
 * 会员等级表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberLevelService extends IService<UmsMemberLevel> {

}
