package link.chengguo.orangemall.service.ums;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UserBankcards;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IUserBankcardsService extends IService<UserBankcards> {

}
