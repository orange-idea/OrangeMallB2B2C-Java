package link.chengguo.orangemall.service.pms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductFullReduction;

/**
 * <p>
 * 产品满减表(只针对同商品) 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductFullReductionService extends IService<PmsProductFullReduction> {

}
