package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsBargainConfig;
import link.chengguo.orangemall.sms.mapper.SmsBargainConfigMapper;
import link.chengguo.orangemall.service.sms.ISmsBargainConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 砍价免单设置表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsBargainConfigServiceImpl extends ServiceImpl<SmsBargainConfigMapper, SmsBargainConfig> implements ISmsBargainConfigService {

}
