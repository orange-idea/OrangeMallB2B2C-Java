package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingLog;
import link.chengguo.orangemall.jms.mapper.JmsPickingLogMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingLogServiceImpl extends ServiceImpl
<JmsPickingLogMapper, JmsPickingLog> implements IJmsPickingLogService {

@Resource
private  JmsPickingLogMapper jmsPickingLogMapper;


}
