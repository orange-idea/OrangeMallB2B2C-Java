package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsSignRecord;
import link.chengguo.orangemall.sms.mapper.SmsSignRecordMapper;
import link.chengguo.orangemall.service.sms.ISmsSignRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到记录 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsSignRecordServiceImpl extends ServiceImpl<SmsSignRecordMapper, SmsSignRecord> implements ISmsSignRecordService {

}
