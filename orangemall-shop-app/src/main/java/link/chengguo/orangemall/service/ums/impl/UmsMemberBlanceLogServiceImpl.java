package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberBlanceLog;
import link.chengguo.orangemall.ums.mapper.UmsMemberBlanceLogMapper;
import link.chengguo.orangemall.service.ums.IUmsMemberBlanceLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberBlanceLogServiceImpl extends ServiceImpl<UmsMemberBlanceLogMapper, UmsMemberBlanceLog> implements IUmsMemberBlanceLogService {

}
