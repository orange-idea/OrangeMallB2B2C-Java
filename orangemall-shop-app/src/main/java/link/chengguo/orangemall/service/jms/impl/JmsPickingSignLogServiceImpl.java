package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsPickingSignLog;
import link.chengguo.orangemall.jms.mapper.JmsPickingSignLogMapper;
import link.chengguo.orangemall.service.jms.IJmsPickingSignLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingSignLogServiceImpl extends ServiceImpl
<JmsPickingSignLogMapper, JmsPickingSignLog> implements IJmsPickingSignLogService {

@Resource
private  JmsPickingSignLogMapper jmsPickingSignLogMapper;


}
