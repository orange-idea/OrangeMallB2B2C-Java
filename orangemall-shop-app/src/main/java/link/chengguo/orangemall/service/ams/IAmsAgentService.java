package link.chengguo.orangemall.service.ams;

import link.chengguo.orangemall.ams.entity.AmsAgent;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
* @author yzb
* @date 2019-12-21
*/

public interface IAmsAgentService extends IService<AmsAgent> {

    /**
     * 结算增加余额
     * @param cityCode 代理城市ID
     * @param money  操作金额
     * @param  desc  操作说明
     * @return
     */
    boolean addAgentBalanceForSettlement(Long orderId,String cityCode, BigDecimal money);

    /**
     * 根据城市ID获取代理信息
     * @param cityCode
     * @return
     */
    AmsAgent getAgentByCityCode(String cityCode);

}
