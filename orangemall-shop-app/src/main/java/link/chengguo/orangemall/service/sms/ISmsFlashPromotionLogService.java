package link.chengguo.orangemall.service.sms;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotionLog;

/**
 * <p>
 * 限时购通知记录 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsFlashPromotionLogService extends IService<SmsFlashPromotionLog> {

}
