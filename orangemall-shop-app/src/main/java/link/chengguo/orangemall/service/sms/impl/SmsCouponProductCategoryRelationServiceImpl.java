package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsCouponProductCategoryRelation;
import link.chengguo.orangemall.sms.mapper.SmsCouponProductCategoryRelationMapper;
import link.chengguo.orangemall.service.sms.ISmsCouponProductCategoryRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券和产品分类关系表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsCouponProductCategoryRelationServiceImpl extends ServiceImpl<SmsCouponProductCategoryRelationMapper, SmsCouponProductCategoryRelation> implements ISmsCouponProductCategoryRelationService {

}
