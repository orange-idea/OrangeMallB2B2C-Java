package link.chengguo.orangemall.service.jms.impl;

import link.chengguo.orangemall.jms.entity.JmsOrderTrack;
import link.chengguo.orangemall.jms.mapper.JmsOrderTrackMapper;
import link.chengguo.orangemall.service.jms.IJmsOrderTrackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsOrderTrackServiceImpl extends ServiceImpl
        <JmsOrderTrackMapper, JmsOrderTrack> implements IJmsOrderTrackService {

    @Resource
    private JmsOrderTrackMapper jmsOrderTrackMapper;

    @Override
    public boolean addOrderTrack(JmsOrderTrack orderTrack) {
        orderTrack.setCreateTime(new Date());
        return jmsOrderTrackMapper.insert(orderTrack)>0?true:false;
    }
}
