package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.SysMessage;
import link.chengguo.orangemall.ums.mapper.SysMessageMapper;
import link.chengguo.orangemall.service.ums.ISysMessageService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站内信 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class SysMessageServiceImpl extends ServiceImpl<SysMessageMapper, SysMessage> implements ISysMessageService {

}
