package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-03-10
 * 第三方接入通用请求对象
 */
@Data
public class CommonDeliveryRequest implements Serializable {

    /**第三方订单ID，要求唯一*/
    private String third_order_id;

}
