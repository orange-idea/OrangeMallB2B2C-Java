package link.chengguo.orangemall.delivery.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.delivery.entity.DOrderStatus;
import link.chengguo.orangemall.delivery.entity.DRiderEvaluate;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IDeliveryService extends IService<OmsOrder> {

    /**
     * 添加订单到配送系统
     * @return
     */
    boolean addOrder(OmsOrder order);

    /**
     * 取消订单
     * @return
     */
    boolean cancelOrder(Long orderId,int reasonId,String reson);

    /**
     * 查询订单状态
     * @return
     */
    DOrderStatus getOrderStatus(Long orderId);

    /**
     * 查询骑手位置
     * @return
     */
    DOrderStatus getRiderLocation(Long orderId);

    /**
     * 评价骑手
     * @return
     */
    boolean evaluateRider(Long orderId, DRiderEvaluate riderEvaluate);

    /**
     * 更新訂單状态
     * @return
     */
    boolean updateOrderStatus(DOrderStatus orderStatus, ShmsShopInfo shopInfo);


}
