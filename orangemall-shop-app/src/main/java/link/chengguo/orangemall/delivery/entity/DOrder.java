package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yzb
 * @date 2020-03-10
 * 第三方接入订单实体
 */
@Data
public class DOrder implements Serializable {

    /**第三方订单ID，要求唯一*/
    private String third_order_id;
    /**订单日序号，店铺序号。*/
    private String third_serial_number;
    /**接口编码，配送系统提供*/
    private String interface_code;
    /**订单类型（0-即时单，1-预约单）*/
    private Integer order_type;
    /**订单金额*/
    private BigDecimal order_amount;
    /**实收金额*/
    private BigDecimal order_actual_amount;
    /**订单商品总数*/
    private Integer goods_count;
    /**配送费用,平台收取用户的配送费用*/
    private BigDecimal delivery_fee;
    /**期望送达，格式如：yyyy-MM-dd hh:mm:ss*/
    private String expected_time;
    /**订单备注*/
    private String order_remark;
    /**取货应付*/
    private BigDecimal pick_payable_cost;
    /**顾客地址*/
    private String customer_address;
    /**顾客姓名*/
    private String customer_name;
    /**顾客电话*/
    private String customer_phone;
    /**顾客经度*/
    private String customer_lng;
    /**顾客纬度*/
    private String customer_lat;
    /**送达应收*/
    private BigDecimal finish_receivable_cost;
    /**顾客付款时间*/
    private String payment_time;
    /**商家接单时间*/
    private String shop_take_time;
    /**回调地址，用于接收订单状态*/
    private String callback;
    /**商品详情*/
    private List<DGoodsDetail> goods_detail;
}
