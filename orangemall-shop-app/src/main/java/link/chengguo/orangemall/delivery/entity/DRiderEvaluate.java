package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-03-11
 * 骑手评价实体
 */
@Data
public class DRiderEvaluate implements Serializable {

    /** 订单编码，电商系统唯一的订单编码*/
    private String thrid_order_id;

    /** 评价级别：好中差：0-好评，1-中评，2-差评*/
    private int level;

    /** 星星数量，按照星数定义
     * 1-2：差评 3-4：中评 5：好评
     * */
    private int stars;

    /**评价内容*/
    private String content;

    /**评价图片,多图中间逗号分隔，图片路径必须为云存储图片*/
    private String images;
}
