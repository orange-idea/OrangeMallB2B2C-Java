package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author yzb
 * @date 2020-03-11
 * 取消原因实体
 */
@Data
public class CancelReason implements Serializable {

    /**
     * 订单编码，电商系统唯一的订单编码
     */
    private String thrid_order_id;

    /**
     * 取消原因类型
     */
    private int cancel_reason_id;

    /**
     * 取消描述
     */
    private String cancel_reson;
}
