package link.chengguo.orangemall.delivery.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yzb
 * @date 2020-03-10
 * 订单回调或查询返回的状态信息
 */
@Data
public class DOrderStatus implements Serializable {

    /**第三方订单ID，要求唯一*/
    private String third_order_id;
    /**订单状态*/
    private Integer status;
    /** 订单变更时间，格式如：yyyy-MM-dd hh:mm:ss*/
    private String operate_time;
    /**骑手ID*/
    private String rider_id;
    /**骑手姓名*/
    private String rider_name;
    /**骑手电话*/
    private String rider_phone;
    /**骑手当前纬度*/
    private String lat;
    /**骑手当前经度*/
    private String lng;
    /**取消原因类型*/
    private String cancel_reason_id;
    /**取消原因描述*/
    private String cancel_reason;
}
