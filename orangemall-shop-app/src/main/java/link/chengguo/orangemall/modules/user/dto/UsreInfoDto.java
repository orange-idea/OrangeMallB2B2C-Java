package link.chengguo.orangemall.modules.user.dto;

import lombok.Data;

/**
 * @Author: mxchen
 * @Date: 2020/9/21 16:22
 */
@Data
public class UsreInfoDto {
    private String icon;
    private String nickName;
    private String roleName;
    private String phone;
}
