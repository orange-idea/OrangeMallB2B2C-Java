package link.chengguo.orangemall.modules.auth.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: mxchen
 * @Date: 2020/9/21 8:54
 */
@Data
public class CheckPhoneDto {
    @NotNull(message = "手机号必填")
    private String phone;
}
