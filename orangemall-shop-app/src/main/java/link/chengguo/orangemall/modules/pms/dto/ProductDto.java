package link.chengguo.orangemall.modules.pms.dto;

import lombok.Data;

/**
 * @Author: mxchen
 * @Date: 2020/9/21 16:47
 */
@Data
public class ProductDto {

    private String keyword;
}
