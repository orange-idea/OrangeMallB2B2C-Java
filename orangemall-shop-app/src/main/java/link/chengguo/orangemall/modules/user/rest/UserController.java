package link.chengguo.orangemall.modules.user.rest;

import io.swagger.annotations.Api;
import link.chengguo.orangemall.modules.user.dto.UsreInfoDto;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.sys.service.ISysUserService;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: mxchen
 * @Date: 2020/9/19 17:33
 */
@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "用户中心",tags = "用户：用户中心")
public class UserController {
    private final ISysUserService sysUserService;

    /**获取用户信息**/
    @GetMapping("/getUserInfo")
    public CommonResult businessUserInfo(){
        SysUser user = UserUtils.getCurrentMember();
        UsreInfoDto userInfo = sysUserService.getBusinessUserInfoById(user);
        return CommonResult.ok().data("userInfo",userInfo);
    }
    /**获取个人中心菜单**/
    public CommonResult userMenus(){
        return CommonResult.ok();
    }
    /**用户修改密码**/
    public CommonResult updateUserPassword(){
        return CommonResult.ok();
    }
    /**申请入驻商家**/
    public CommonResult addBusinessUser(){
        return  CommonResult.ok();
    }

}
