package link.chengguo.orangemall.modules.service.sms;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import link.chengguo.orangemall.ums.entity.Sms;

/**
 * 短信服务
 * @Author: mxchen
 * @Date: 2020/9/19 19:06
 */
public interface ISmsService {
    /**
     * 保存短信
     *
     * @param sms
     * @param params
     */
    //void save(Sms sms, Map<String, String> params);

    void save(Sms sms, String params);

    /**
     * 修改短信
     *
     * @param sms
     */
    void update(Sms sms);

    /**
     * 查询短信
     *
     * @param id
     * @return
     */
    Sms findById(Long id);


    /**
     * 发送短信
     */
    SendSmsResponse sendSmsMsg(Sms sms);

    void saveSmsAndSendMessage(String phone, String code);
}
