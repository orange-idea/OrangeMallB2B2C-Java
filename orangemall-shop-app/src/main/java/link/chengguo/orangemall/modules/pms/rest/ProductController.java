package link.chengguo.orangemall.modules.pms.rest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import link.chengguo.orangemall.exception.BusinessMallException;
import link.chengguo.orangemall.modules.pms.dto.ProductDetailedDto;
import link.chengguo.orangemall.modules.pms.dto.ProductDto;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.service.pms.IPmsProductService;
import link.chengguo.orangemall.service.shms.IShmsShopInfoService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: mxchen
 * @Date: 2020/9/21 16:39
 */
@Slf4j
@RestController
@RequestMapping("/api/pms/product")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "商品信息管理", tags = "商家端:商品信息管理", description = "商品信息管理")
public class ProductController {

    private final IPmsProductService pmsProductService;
    private final IShmsShopInfoService shmsShopInfoService;

    /**按时间排序显示所有商品,可模糊查询**/
    @GetMapping("/listProductFuzzySearchByName")
    public CommonResult listProductFuzzySearchByName(String keyword,
                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize){

        IPage<PmsProduct> page = pmsProductService.pageByName(pageNum,pageSize,keyword);
        return CommonResult.ok().data("productPage",page);
    }
    /**添加商品**/
    @PostMapping("/addProduct")
    public CommonResult addProduct(@RequestBody ProductDetailedDto productDetailedDto){
        try {
            productDetailedDto.setCityCode(UserUtils.getCurrentMember().getCityCode());
            productDetailedDto.setShopId(UserUtils.getCurrentMember().getShopId());
            ShmsShopInfo shopInfo = shmsShopInfoService.getById(UserUtils.getCurrentMember().getShopId());
            productDetailedDto.setShopName(shopInfo.getName());
            int count = pmsProductService.create(productDetailedDto);
            if(count<1){
                throw new BusinessMallException("保存商品失败");
            }
            return CommonResult.ok();
        } catch (Exception e) {
            return CommonResult.error(e.getMessage());
        }
    }

}
