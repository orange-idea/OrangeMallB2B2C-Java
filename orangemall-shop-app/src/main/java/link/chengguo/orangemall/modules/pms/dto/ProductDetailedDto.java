package link.chengguo.orangemall.modules.pms.dto;

import io.swagger.annotations.ApiModelProperty;
import link.chengguo.orangemall.pms.entity.*;
import lombok.Data;

import java.util.List;

/**
 * @Author: mxchen
 * @Date: 2020/9/22 11:08
 */
@Data
public class ProductDetailedDto extends PmsProduct {
    @ApiModelProperty("商品阶梯价格设置")
    private List<PmsProductLadder> productLadderList;
    @ApiModelProperty("商品满减价格设置")
    private List<PmsProductFullReduction> productFullReductionList;
    @ApiModelProperty("商品会员价格设置")
    private List<PmsMemberPrice> memberPriceList;
    @ApiModelProperty("商品的sku库存信息")
    private List<PmsSkuStock> skuStockList;
    @ApiModelProperty("商品参数及自定义规格属性")
    private List<PmsProductAttributeValue> productAttributeValueList;
    @ApiModelProperty("专题和商品关系")
    private List<CmsSubjectProductRelation> subjectProductRelationList;
    @ApiModelProperty("优选专区和商品的关系")
    private List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList;

    private List<PmsProductAttributeValue> productCanShuValueList;
    private List<PmsProduct> typeGoodsList;

    private PmsProduct goods;
}
