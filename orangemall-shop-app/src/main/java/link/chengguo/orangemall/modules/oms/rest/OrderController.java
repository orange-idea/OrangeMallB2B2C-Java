package link.chengguo.orangemall.modules.oms.rest;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: mxchen
 * @Date: 2020/9/21 16:44
 */
@Slf4j
@RestController
@RequestMapping("/api/oms/order")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "订单管理", tags = "商家端:订单管理", description = "订单管理")
public class OrderController {

}
