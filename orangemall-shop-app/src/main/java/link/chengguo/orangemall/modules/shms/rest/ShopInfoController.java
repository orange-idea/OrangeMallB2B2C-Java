package link.chengguo.orangemall.modules.shms.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.service.shms.IShmsShopInfoService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: mxchen
 * @Date: 2020/9/19 16:37
 */
@Slf4j
@RestController
@RequestMapping("/api/shms/shopInfo")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "店铺信息管理模块", tags = "商家端:店铺信息管理", description = "店铺信息管理模块")
public class ShopInfoController {

    private final IShmsShopInfoService shmsShopInfoService;

    @SysLog(MODULE = "shms", REMARK = "查询用户拥有的店铺信息明细")
    @ApiOperation("查询用户拥有的店铺信息明细")
    @GetMapping(value = "/queryKindergartenById")
    public Object getShmsShopInfoById() {
        try {
            Long shopId= UserUtils.getCurrentMember().getShopId();
            ShmsShopInfo coupon = shmsShopInfoService.getById(shopId);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺信息明细：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }

    }


}
