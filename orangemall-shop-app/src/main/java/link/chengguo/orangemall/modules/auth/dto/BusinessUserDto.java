package link.chengguo.orangemall.modules.auth.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;

/**
 * @Author: mxchen
 * @Date: 2020/9/19 17:14
 */
@Data
public class BusinessUserDto {
    @NotNull(message = "用户账号必填")
    private String username;
    @NotNull(message = "用户密码必填")
    private String password;
    private String code;
}
