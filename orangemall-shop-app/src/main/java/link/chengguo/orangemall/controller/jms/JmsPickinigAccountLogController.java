package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickinigAccountLog;
import link.chengguo.orangemall.service.jms.IJmsPickinigAccountLogService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*  拣货员账户日志
*/
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickinigAccountLog")
public class JmsPickinigAccountLogController {

@Resource
private IJmsPickinigAccountLogService IJmsPickinigAccountLogService;

@SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货员账户日志列表")
@ApiOperation("根据条件查询所有拣货员账户日志列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:read')")
public Object getJmsPickinigAccountLogByPage(JmsPickinigAccountLog entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IJmsPickinigAccountLogService.page(new Page<JmsPickinigAccountLog>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有拣货员账户日志列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "保存拣货员账户日志")
@ApiOperation("保存拣货员账户日志")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:create')")
public Object saveJmsPickinigAccountLog(@RequestBody JmsPickinigAccountLog entity) {
try {

if (IJmsPickinigAccountLogService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存拣货员账户日志：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "更新拣货员账户日志")
@ApiOperation("更新拣货员账户日志")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:update')")
public Object updateJmsPickinigAccountLog(@RequestBody JmsPickinigAccountLog entity) {
try {
if (IJmsPickinigAccountLogService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新拣货员账户日志：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "删除拣货员账户日志")
@ApiOperation("删除拣货员账户日志")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:delete')")
public Object deleteJmsPickinigAccountLog(@ApiParam("拣货员账户日志id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员账户日志id");
}
if (IJmsPickinigAccountLogService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除拣货员账户日志：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "给拣货员账户日志分配拣货员账户日志")
@ApiOperation("查询拣货员账户日志明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:read')")
public Object getJmsPickinigAccountLogById(@ApiParam("拣货员账户日志id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货员账户日志id");
}
JmsPickinigAccountLog coupon = IJmsPickinigAccountLogService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询拣货员账户日志明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除拣货员账户日志")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "jms", REMARK = "批量删除拣货员账户日志")
@PreAuthorize("hasAuthority('jms:jmsPickinigAccountLog:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IJmsPickinigAccountLogService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickinigAccountLog entity) {
    // 模拟从数据库获取需要导出的数据
    List<JmsPickinigAccountLog> personList = IJmsPickinigAccountLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickinigAccountLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<JmsPickinigAccountLog> personList = EasyPoiUtils.importExcel(file, JmsPickinigAccountLog.class);
    IJmsPickinigAccountLogService.saveBatch(personList);
    }
    }


