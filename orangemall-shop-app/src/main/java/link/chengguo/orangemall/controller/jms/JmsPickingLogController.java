package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingLog;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*  拣货记录
*/
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingLog")
public class JmsPickingLogController {

@Resource
private link.chengguo.orangemall.service.jms.IJmsPickingLogService IJmsPickingLogService;

@SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货记录列表")
@ApiOperation("根据条件查询所有拣货记录列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:read')")
public Object getJmsPickingLogByPage(JmsPickingLog entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IJmsPickingLogService.page(new Page<JmsPickingLog>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有拣货记录列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "保存拣货记录")
@ApiOperation("保存拣货记录")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:create')")
public Object saveJmsPickingLog(@RequestBody JmsPickingLog entity) {
try {

if (IJmsPickingLogService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存拣货记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "更新拣货记录")
@ApiOperation("更新拣货记录")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:update')")
public Object updateJmsPickingLog(@RequestBody JmsPickingLog entity) {
try {
if (IJmsPickingLogService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新拣货记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "删除拣货记录")
@ApiOperation("删除拣货记录")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:delete')")
public Object deleteJmsPickingLog(@ApiParam("拣货记录id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货记录id");
}
if (IJmsPickingLogService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除拣货记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "给拣货记录分配拣货记录")
@ApiOperation("查询拣货记录明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:read')")
public Object getJmsPickingLogById(@ApiParam("拣货记录id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货记录id");
}
JmsPickingLog coupon = IJmsPickingLogService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询拣货记录明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除拣货记录")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "jms", REMARK = "批量删除拣货记录")
@PreAuthorize("hasAuthority('jms:jmsPickingLog:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IJmsPickingLogService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingLog entity) {
    // 模拟从数据库获取需要导出的数据
    List<JmsPickingLog> personList = IJmsPickingLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<JmsPickingLog> personList = EasyPoiUtils.importExcel(file, JmsPickingLog.class);
    IJmsPickingLogService.saveBatch(personList);
    }
    }


