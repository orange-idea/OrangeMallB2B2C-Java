package link.chengguo.orangemall.controller.oms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.oms.entity.OmsVerifyLog;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-01-07
*  订单核销记录
*/
@Slf4j
@RestController
@RequestMapping("/oms/omsVerifyLog")
public class OmsVerifyLogController {

@Resource
private link.chengguo.orangemall.service.oms.IOmsVerifyLogService IOmsVerifyLogService;

@SysLog(MODULE = "oms", REMARK = "根据条件查询所有订单核销记录列表")
@ApiOperation("根据条件查询所有订单核销记录列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:read')")
public Object getOmsVerifyLogByPage(OmsVerifyLog entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IOmsVerifyLogService.page(new Page<OmsVerifyLog>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有订单核销记录列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "oms", REMARK = "保存订单核销记录")
@ApiOperation("保存订单核销记录")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:create')")
public Object saveOmsVerifyLog(@RequestBody OmsVerifyLog entity) {
try {

if (IOmsVerifyLogService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存订单核销记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "oms", REMARK = "更新订单核销记录")
@ApiOperation("更新订单核销记录")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:update')")
public Object updateOmsVerifyLog(@RequestBody OmsVerifyLog entity) {
try {
if (IOmsVerifyLogService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新订单核销记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "oms", REMARK = "删除订单核销记录")
@ApiOperation("删除订单核销记录")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:delete')")
public Object deleteOmsVerifyLog(@ApiParam("订单核销记录id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("订单核销记录id");
}
if (IOmsVerifyLogService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除订单核销记录：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "oms", REMARK = "给订单核销记录分配订单核销记录")
@ApiOperation("查询订单核销记录明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:read')")
public Object getOmsVerifyLogById(@ApiParam("订单核销记录id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("订单核销记录id");
}
OmsVerifyLog coupon = IOmsVerifyLogService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询订单核销记录明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除订单核销记录")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "oms", REMARK = "批量删除订单核销记录")
@PreAuthorize("hasAuthority('oms:omsVerifyLog:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IOmsVerifyLogService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "oms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, OmsVerifyLog entity) {
    // 模拟从数据库获取需要导出的数据
    List<OmsVerifyLog> personList = IOmsVerifyLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", OmsVerifyLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "oms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<OmsVerifyLog> personList = EasyPoiUtils.importExcel(file, OmsVerifyLog.class);
    IOmsVerifyLogService.saveBatch(personList);
    }
    }


