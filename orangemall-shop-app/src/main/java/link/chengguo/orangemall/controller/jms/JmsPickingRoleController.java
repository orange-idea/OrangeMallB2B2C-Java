package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingRole;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货员角色
 */
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingRole")
public class JmsPickingRoleController {

    @Resource
    private link.chengguo.orangemall.service.jms.IJmsPickingRoleService IJmsPickingRoleService;

    @SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货员角色列表")
    @ApiOperation("根据条件查询所有拣货员角色列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('jms:jmsPickingRole:read')")
    public Object getJmsPickingRoleByPage(JmsPickingRole entity,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IJmsPickingRoleService.page(new Page<JmsPickingRole>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id").orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有拣货员角色列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "保存拣货员角色")
    @ApiOperation("保存拣货员角色")
    @PostMapping(value = "/create")
    public Object saveJmsPickingRole(@RequestBody JmsPickingRole entity) {
        try {
            entity.setCreateTime(new Date());
            if (IJmsPickingRoleService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存拣货员角色：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "更新拣货员角色")
    @ApiOperation("更新拣货员角色")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingRole:update')")
    public Object updateJmsPickingRole(@RequestBody JmsPickingRole entity) {
        try {
            entity.setUpdateTime(new Date());
            if (IJmsPickingRoleService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新拣货员角色：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "删除拣货员角色")
    @ApiOperation("删除拣货员角色")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingRole:delete')")
    public Object deleteJmsPickingRole(@ApiParam("拣货员角色id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货员角色id");
            }
            if (IJmsPickingRoleService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除拣货员角色：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "给拣货员角色分配拣货员角色")
    @ApiOperation("查询拣货员角色明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingRole:read')")
    public Object getJmsPickingRoleById(@ApiParam("拣货员角色id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货员角色id");
            }
            JmsPickingRole coupon = IJmsPickingRoleService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询拣货员角色明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除拣货员角色")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "jms", REMARK = "批量删除拣货员角色")
    @PreAuthorize("hasAuthority('jms:jmsPickingRole:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IJmsPickingRoleService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingRole entity) {
        // 模拟从数据库获取需要导出的数据
        List<JmsPickingRole> personList = IJmsPickingRoleService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingRole.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<JmsPickingRole> personList = EasyPoiUtils.importExcel(file, JmsPickingRole.class);
        IJmsPickingRoleService.saveBatch(personList);
    }
}


