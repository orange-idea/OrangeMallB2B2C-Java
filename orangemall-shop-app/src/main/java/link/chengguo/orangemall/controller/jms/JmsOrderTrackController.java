package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.JmsOrderTrack;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货订单跟踪
 */
@Slf4j
@RestController
@RequestMapping("/jms/jmsOrderTrack")
public class JmsOrderTrackController {

    @Resource
    private link.chengguo.orangemall.service.jms.IJmsOrderTrackService IJmsOrderTrackService;

    @SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货订单跟踪列表")
    @ApiOperation("根据条件查询所有拣货订单跟踪列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('jms:jmsOrderTrack:read')")
    public Object getJmsOrderTrackByPage(JmsOrderTrack entity,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper<>(entity).orderByDesc("id");
            if (ValidatorUtils.notEmpty(entity.getKeyword())){
                queryWrapper.like("order_id",entity.getKeyword());
//                queryWrapper.apply("( order_id like '%"+entity.getKeyword()+"%'  or id like '%"+entity.getKeyword()+"%')");
            }
            return new CommonResult().success(IJmsOrderTrackService.page(new Page<JmsOrderTrack>(pageNum, pageSize),queryWrapper ));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有拣货订单跟踪列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "查询订单跟踪统计数字")
    @ApiOperation("查询订单跟踪统计数字")
    @PostMapping(value = "/getCountNumber")
    public Object getCountNumber() {
        try {
            Map<String,Object> map=new HashMap<>();
            SysUser user= UserUtils.getCurrentMember();
            int allCount= IJmsOrderTrackService.count(new QueryWrapper<JmsOrderTrack>().eq("shop_id",user.getShopId()));
            int waitGrabCount=IJmsOrderTrackService.count(new QueryWrapper<JmsOrderTrack>().eq("shop_id",user.getShopId()).eq("status", PickingStatus.WaitGrab.getValue()));
            int pickingCount=IJmsOrderTrackService.count(new QueryWrapper<JmsOrderTrack>().eq("shop_id",user.getShopId()).eq("status",PickingStatus.Picking.getValue()));
            int finishCount=IJmsOrderTrackService.count(new QueryWrapper<JmsOrderTrack>().eq("shop_id",user.getShopId()).eq("status",PickingStatus.Picked.getValue()));
            map.put("allCount",allCount);
            map.put("waitGrabCount",waitGrabCount);
            map.put("pickingCount",pickingCount);
            map.put("finishCount",finishCount);
            return new CommonResult().success(map);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单跟踪统计数字：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "保存拣货订单跟踪")
    @ApiOperation("保存拣货订单跟踪")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('jms:jmsOrderTrack:create')")
    public Object saveJmsOrderTrack(@RequestBody JmsOrderTrack entity) {
        try {

            if (IJmsOrderTrackService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存拣货订单跟踪：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "更新拣货订单跟踪")
    @ApiOperation("更新拣货订单跟踪")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('jms:jmsOrderTrack:update')")
    public Object updateJmsOrderTrack(@RequestBody JmsOrderTrack entity) {
        try {
            if (IJmsOrderTrackService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新拣货订单跟踪：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "删除拣货订单跟踪")
    @ApiOperation("删除拣货订单跟踪")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('jms:jmsOrderTrack:delete')")
    public Object deleteJmsOrderTrack(@ApiParam("拣货订单跟踪id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货订单跟踪id");
            }
            if (IJmsOrderTrackService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除拣货订单跟踪：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "给拣货订单跟踪分配拣货订单跟踪")
    @ApiOperation("查询拣货订单跟踪明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('jms:jmsOrderTrack:read')")
    public Object getJmsOrderTrackById(@ApiParam("拣货订单跟踪id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("拣货订单跟踪id");
            }
            JmsOrderTrack coupon = IJmsOrderTrackService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询拣货订单跟踪明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
    }


}


