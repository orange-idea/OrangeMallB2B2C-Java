package link.chengguo.orangemall.controller.fms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.mapper.AmsBusinessCityMapper;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsShopWithdraw;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.service.shms.IShmsShopInfoService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 商家提现
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsShopWithdraw")
public class FmsShopWithdrawController {

    @Resource
    private link.chengguo.orangemall.service.fms.IFmsShopWithdrawService IFmsShopWithdrawService;
    @Resource
    private IShmsShopInfoService shopInfoService;
    @Resource
    private AmsBusinessCityMapper businessCityMapper;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有商家提现列表")
    @ApiOperation("根据条件查询所有商家提现列表")
    @GetMapping(value = "/list")
    public Object getFmsShopWithdrawByPage(FmsShopWithdraw entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setShopId(user.getShopId());
            return new CommonResult().success(IFmsShopWithdrawService.page(new Page<FmsShopWithdraw>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有商家提现列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存商家提现")
    @ApiOperation("保存商家提现")
    @PostMapping(value = "/create")
    public Object saveFmsShopWithdraw(@RequestBody FmsShopWithdraw entity) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setShopId(user.getShopId());
            //查询商家信息
            ShmsShopInfo shopInfo=shopInfoService.getById(user.getShopId());
            entity.setShopName(shopInfo.getName());
            entity.setShopPhone(shopInfo.getContactMobile());
            //添加城市名称
            AmsBusinessCity businessCity=new AmsBusinessCity();
            businessCity.setBusinessDistrict(user.getCityCode());
            AmsBusinessCity city= businessCityMapper.selectOne(new QueryWrapper<>(businessCity));
            entity.setCityName(city.getBusinessCityName());
            entity.setCityCode(user.getCityCode());
            entity.setCreateTime(new Date());
            if (IFmsShopWithdrawService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存商家提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新商家提现")
    @ApiOperation("更新商家提现")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsShopWithdraw(@RequestBody FmsShopWithdraw entity) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            entity.setShopId(user.getShopId());
            if (IFmsShopWithdrawService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新商家提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除商家提现")
    @ApiOperation("删除商家提现")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsShopWithdraw(@ApiParam("商家提现id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("商家提现id");
            }
            if (IFmsShopWithdrawService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除商家提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给商家提现分配商家提现")
    @ApiOperation("查询商家提现明细")
    @GetMapping(value = "/{id}")
    public Object getFmsShopWithdrawById(@ApiParam("商家提现id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("商家提现id");
            }
            FmsShopWithdraw coupon = IFmsShopWithdrawService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询商家提现明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除商家提现")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除商家提现")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsShopWithdrawService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsShopWithdraw entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsShopWithdraw> personList = IFmsShopWithdrawService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsShopWithdraw.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsShopWithdraw> personList = EasyPoiUtils.importExcel(file, FmsShopWithdraw.class);
        IFmsShopWithdrawService.saveBatch(personList);
    }
}


