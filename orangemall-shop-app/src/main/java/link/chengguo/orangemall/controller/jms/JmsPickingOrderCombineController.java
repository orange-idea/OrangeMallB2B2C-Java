package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderCombine;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 * 合并拣货订单
 */
@Slf4j
@RestController
@RequestMapping("/jms/jmsPickingOrderCombine")
public class JmsPickingOrderCombineController {

    @Resource
    private link.chengguo.orangemall.service.jms.IJmsPickingOrderCombineService IJmsPickingOrderCombineService;

    @SysLog(MODULE = "jms", REMARK = "根据条件查询所有合并拣货订单列表")
    @ApiOperation("根据条件查询所有合并拣货订单列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:read')")
    public Object getJmsPickingOrderCombineByPage(JmsPickingOrderCombine entity,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IJmsPickingOrderCombineService.page(new Page<JmsPickingOrderCombine>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有合并拣货订单列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "保存合并拣货订单")
    @ApiOperation("保存合并拣货订单")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:create')")
    public Object saveJmsPickingOrderCombine(@RequestBody JmsPickingOrderCombine entity) {
        try {

            if (IJmsPickingOrderCombineService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存合并拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "更新合并拣货订单")
    @ApiOperation("更新合并拣货订单")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:update')")
    public Object updateJmsPickingOrderCombine(@RequestBody JmsPickingOrderCombine entity) {
        try {
            if (IJmsPickingOrderCombineService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新合并拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "删除合并拣货订单")
    @ApiOperation("删除合并拣货订单")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:delete')")
    public Object deleteJmsPickingOrderCombine(@ApiParam("合并拣货订单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("合并拣货订单id");
            }
            if (IJmsPickingOrderCombineService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除合并拣货订单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "jms", REMARK = "给合并拣货订单分配合并拣货订单")
    @ApiOperation("查询合并拣货订单明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:read')")
    public Object getJmsPickingOrderCombineById(@ApiParam("合并拣货订单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("合并拣货订单id");
            }
            JmsPickingOrderCombine coupon = IJmsPickingOrderCombineService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询合并拣货订单明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除合并拣货订单")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "jms", REMARK = "批量删除合并拣货订单")
    @PreAuthorize("hasAuthority('jms:jmsPickingOrderCombine:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IJmsPickingOrderCombineService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsPickingOrderCombine entity) {
        // 模拟从数据库获取需要导出的数据
        List<JmsPickingOrderCombine> personList = IJmsPickingOrderCombineService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsPickingOrderCombine.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<JmsPickingOrderCombine> personList = EasyPoiUtils.importExcel(file, JmsPickingOrderCombine.class);
        IJmsPickingOrderCombineService.saveBatch(personList);
    }
}


