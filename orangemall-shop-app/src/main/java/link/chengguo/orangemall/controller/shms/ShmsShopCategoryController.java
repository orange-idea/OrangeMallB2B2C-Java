package link.chengguo.orangemall.controller.shms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.service.shms.IShmsShopCategoryService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺分类
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopCategoryController ", description = "店铺分类")
@RequestMapping("/shms/shmsShopCategory")
public class ShmsShopCategoryController {

    @Resource
    private IShmsShopCategoryService IShmsShopCategoryService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺分类列表")
    @ApiOperation("根据条件查询所有店铺分类列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:read')")
    public Object getShmsShopCategoryByPage(ShmsShopCategory entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopCategoryService.page(new Page<ShmsShopCategory>(pageNum, pageSize), new QueryWrapper<>(entity).isNull("city_code").orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺分类列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺分类")
    @ApiOperation("保存店铺分类")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:create')")
    public Object saveShmsShopCategory(@RequestBody ShmsShopCategory entity) {
        try {

            if (IShmsShopCategoryService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺分类：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺分类")
    @ApiOperation("更新店铺分类")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:update')")
    public Object updateShmsShopCategory(@RequestBody ShmsShopCategory entity) {
        try {
            if (IShmsShopCategoryService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺分类：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺分类")
    @ApiOperation("删除店铺分类")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:delete')")
    public Object deleteShmsShopCategory(@ApiParam("店铺分类id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺分类id");
            }
            if (IShmsShopCategoryService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺分类：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺分类分配店铺分类")
    @ApiOperation("查询店铺分类明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:read')")
    public Object getShmsShopCategoryById(@ApiParam("店铺分类id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺分类id");
            }
            ShmsShopCategory coupon = IShmsShopCategoryService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺分类明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺分类")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺分类")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopCategoryService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopCategory entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopCategory> personList = IShmsShopCategoryService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopCategory.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopCategory> personList = EasyPoiUtils.importExcel(file, ShmsShopCategory.class);
        IShmsShopCategoryService.saveBatch(personList);
    }
}


