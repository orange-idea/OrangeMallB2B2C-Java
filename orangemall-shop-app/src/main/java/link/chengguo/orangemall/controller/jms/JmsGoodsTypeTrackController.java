package link.chengguo.orangemall.controller.jms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2020-02-27
*  拣货商品分类跟踪
*/
@Slf4j
@RestController
@RequestMapping("/jms/jmsGoodsTypeTrack")
public class JmsGoodsTypeTrackController {

@Resource
private link.chengguo.orangemall.service.jms.IJmsGoodsTypeTrackService IJmsGoodsTypeTrackService;

@SysLog(MODULE = "jms", REMARK = "根据条件查询所有拣货商品分类跟踪列表")
@ApiOperation("根据条件查询所有拣货商品分类跟踪列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:read')")
public Object getJmsGoodsTypeTrackByPage(JmsGoodsTypeTrack entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(IJmsGoodsTypeTrackService.page(new Page<JmsGoodsTypeTrack>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有拣货商品分类跟踪列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "保存拣货商品分类跟踪")
@ApiOperation("保存拣货商品分类跟踪")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:create')")
public Object saveJmsGoodsTypeTrack(@RequestBody JmsGoodsTypeTrack entity) {
try {

if (IJmsGoodsTypeTrackService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存拣货商品分类跟踪：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "更新拣货商品分类跟踪")
@ApiOperation("更新拣货商品分类跟踪")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:update')")
public Object updateJmsGoodsTypeTrack(@RequestBody JmsGoodsTypeTrack entity) {
try {
if (IJmsGoodsTypeTrackService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新拣货商品分类跟踪：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "删除拣货商品分类跟踪")
@ApiOperation("删除拣货商品分类跟踪")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:delete')")
public Object deleteJmsGoodsTypeTrack(@ApiParam("拣货商品分类跟踪id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货商品分类跟踪id");
}
if (IJmsGoodsTypeTrackService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除拣货商品分类跟踪：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "jms", REMARK = "给拣货商品分类跟踪分配拣货商品分类跟踪")
@ApiOperation("查询拣货商品分类跟踪明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:read')")
public Object getJmsGoodsTypeTrackById(@ApiParam("拣货商品分类跟踪id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("拣货商品分类跟踪id");
}
JmsGoodsTypeTrack coupon = IJmsGoodsTypeTrackService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询拣货商品分类跟踪明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除拣货商品分类跟踪")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "jms", REMARK = "批量删除拣货商品分类跟踪")
@PreAuthorize("hasAuthority('jms:jmsGoodsTypeTrack:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = IJmsGoodsTypeTrackService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "jms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, JmsGoodsTypeTrack entity) {
    // 模拟从数据库获取需要导出的数据
    List<JmsGoodsTypeTrack> personList = IJmsGoodsTypeTrackService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", JmsGoodsTypeTrack.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "jms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<JmsGoodsTypeTrack> personList = EasyPoiUtils.importExcel(file, JmsGoodsTypeTrack.class);
    IJmsGoodsTypeTrackService.saveBatch(personList);
    }
    }


