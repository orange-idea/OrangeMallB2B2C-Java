package link.chengguo.orangemall.controller.fms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsShopWithdrawAccount;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2020-02-10
 * 商家提现账号
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsShopWithdrawAccount")
public class FmsShopWithdrawAccountController {

    @Resource
    private link.chengguo.orangemall.service.fms.IFmsShopWithdrawAccountService IFmsShopWithdrawAccountService;

    @SysLog(MODULE = "fms", REMARK = "查询商家账户")
    @ApiOperation("查询商家提现账号明细（ 账号类型：0->支付宝；1->微信；2->银联）")
    @GetMapping(value = "/getAccountByType")
    public Object getShopWithdrawAccountByType(@RequestParam(value = "type", defaultValue = "0") Integer type) {
        try {
            SysUser user=UserUtils.getCurrentMember();
            if (user==null){
                return new CommonResult().failed("账号信息异常");
            }
            FmsShopWithdrawAccount account=new FmsShopWithdrawAccount();
            account.setAccountType(type);
            account.setShopId(user.getShopId());
            FmsShopWithdrawAccount coupon = IFmsShopWithdrawAccountService.getOne(new QueryWrapper<>(account));
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询商家提现账号明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @SysLog(MODULE = "fms", REMARK = "更新商家提现账号")
    @ApiOperation("更新商家提现账号")
    @PostMapping(value = "/addOrupdateByType")
    public Object addOrupdateByType(@RequestBody FmsShopWithdrawAccount entity) {
        try {
            SysUser user=UserUtils.getCurrentMember();
            if (user==null){
                return new CommonResult().failed("账号信息异常");
            }
            FmsShopWithdrawAccount parm=new FmsShopWithdrawAccount();
            parm.setShopId(user.getShopId());
            parm.setAccountType(entity.getAccountType());
            entity.setShopId(user.getShopId());
            FmsShopWithdrawAccount result=IFmsShopWithdrawAccountService.getOne(new QueryWrapper<>(parm));
            if (result==null){
                //不能存在，新增
                if (IFmsShopWithdrawAccountService.save(entity)) {
                    return new CommonResult().success();
                }
            }else{
                if (IFmsShopWithdrawAccountService.update(entity,new QueryWrapper<>(parm))) {
                    return new CommonResult().success();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新商家提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }


}


