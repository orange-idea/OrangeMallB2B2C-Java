package link.chengguo.orangemall.controller.shms;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopQua;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺资质
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopQuaController ", description = "店铺资质")
@RequestMapping("/shms/shmsShopQua")
public class ShmsShopQuaController {

    @Resource
    private link.chengguo.orangemall.service.shms.IShmsShopQuaService IShmsShopQuaService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺资质列表")
    @ApiOperation("根据条件查询所有店铺资质列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:read')")
    public Object getShmsShopQuaByPage(ShmsShopQua entity,
                                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopQuaService.page(new Page<ShmsShopQua>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺资质列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺资质")
    @ApiOperation("保存店铺资质")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:create')")
    public Object saveShmsShopQua(@RequestBody ShmsShopQua entity) {
        try {

            if (IShmsShopQuaService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺资质：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺资质")
    @ApiOperation("更新店铺资质")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:update')")
    public Object updateShmsShopQua(@RequestBody ShmsShopQua entity) {
        try {
            if (IShmsShopQuaService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺资质：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺资质")
    @ApiOperation("删除店铺资质")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:delete')")
    public Object deleteShmsShopQua(@ApiParam("店铺资质id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺资质id");
            }
            if (IShmsShopQuaService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺资质：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺资质分配店铺资质")
    @ApiOperation("查询店铺资质明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:read')")
    public Object getShmsShopQuaById(@ApiParam("店铺资质id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺资质id");
            }
            ShmsShopQua coupon = IShmsShopQuaService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺资质明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺资质")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺资质")
    @PreAuthorize("hasAuthority('shms:shmsShopQua:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopQuaService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopQua entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopQua> personList = IShmsShopQuaService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopQua.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopQua> personList = EasyPoiUtils.importExcel(file, ShmsShopQua.class);
        IShmsShopQuaService.saveBatch(personList);
    }
}


