![](http://doc.chengguo.link/uploads/202009/5f744689c9975_5f744689.png)

# **项目介绍**
橙果商城多商户系统是一套新零售移动电商系统，后端基于当前流行技术：SpringBoot2+MybatisPlus+SpringSecurity+jwt+redis+Vue并采用前后端分离的企业级微服务敏捷开发系统架构，移动端采用Uniapp开发。支持平台自营、联营、招商等运营模式，覆盖移动APP、微信公众号、小程序等多终端，完善的商城功能、智能分析、精细化运作，是企业搭建商城系统的较好选择！系统端口包含平台后台、代理后台、商家后台；功能模块包含：商城、订单、商品、优惠券、积分、分销、会员、充值、配送、自提、到店核销等功能，更适合企业或个人二次开发。



# **思维导图**
![](http://doc.chengguo.link/uploads/202008/5f30c1fb4ea54_5f30c1fb.jpg)



# **项目演示**
演示后台:

平台后台：http://demo.chengguo.link:8091      账号：yanshi 密码：123456

代理后台：http://demo.chengguo.link:8091      账号：18079761010 密码：123456

商家后台：http://demo.chengguo.link:8091/business      账号：15797978596 密码：123456



# **相关截图**

### **前端截图：**
![](http://doc.chengguo.link/uploads/202009/5f73efc37d4ce_5f73efc3.jpg)
### 
![](http://doc.chengguo.link/uploads/202009/5f69ca84ebccb_5f69ca84.jpg)
### 
![](http://doc.chengguo.link/uploads/202009/5f69c6ca92a42_5f69c6ca.jpg)
### 
### **后台截图：**

![](http://doc.chengguo.link/uploads/202009/5f69ae5b00d3e_5f69ae5b.png)

![](http://doc.chengguo.link/uploads/202009/5f69ae66eed3a_5f69ae66.png)

![](http://doc.chengguo.link/uploads/202009/5f69ae70a1296_5f69ae70.png)

![](http://doc.chengguo.link/uploads/202009/5f69c85a90d9d_5f69c85a.png)

![](http://doc.chengguo.link/uploads/202009/5f69ae809abc8_5f69ae80.png)

![](http://doc.chengguo.link/uploads/202009/5f69c67187053_5f69c671.png)



# **开源版使用须知**
1. 允许用于个人学习、毕业设计、教学案例、公益事业;
2. 如果商用必须保留版权信息，请自觉遵守;
3. 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。
