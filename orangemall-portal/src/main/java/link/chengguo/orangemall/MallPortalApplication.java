package link.chengguo.orangemall;

import link.chengguo.orangemall.pay.config.StartupRunner;
import link.chengguo.orangemall.pms.mapper.PmsProductMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;

@SpringBootApplication
@MapperScan({"link.chengguo.orangemall.mapper", "link.chengguo.orangemall.ums.mapper", "link.chengguo.orangemall.sms.mapper", "link.chengguo.orangemall.cms.mapper", "link.chengguo.orangemall.sys.mapper", "link.chengguo.orangemall.oms.mapper", "link.chengguo.orangemall.pms.mapper"})
@EnableTransactionManagement
@EnableScheduling
public class MallPortalApplication {

    @Resource
    PmsProductMapper mapper;

    public static void main(String[] args) {
        System.out.println("start-------------");
        SpringApplication.run(MallPortalApplication.class, args);
        System.out.println("end-------------");
    }

    @Bean
    public StartupRunner startupRunner() {
        return new StartupRunner();
    }
}
