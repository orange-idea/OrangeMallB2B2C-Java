package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductConsult;

/**
 * <p>
 * 产品咨询表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductConsultService extends IService<PmsProductConsult> {

}
