package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsBrand;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.vo.GoodsDetailResult;
import link.chengguo.orangemall.pms.vo.PmsProductAndGroup;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品信息 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductService extends IService<PmsProduct> {

    /**
     * 查询拼团商品详情
     *
     * @param id
     * @return
     */
    PmsProductAndGroup getProductAndGroup(Long id);

    /**
     * 查询商品详情
     *
     * @param id
     * @return
     */
    PmsProduct getUpdateInfo(Long id);

    /**
     * 初始化商品到redis
     *
     * @return
     */
    Object initGoodsRedis();

    /**
     * 获取商品详情 优先取redis
     *
     * @param id
     * @return
     */
    GoodsDetailResult getGoodsRedisById(Long id);

    /**
     * 根据条形码获取商品详情 优先取redis
     *
     * @param barcode
     * @return
     */
    GoodsDetailResult getGoodsRedisByCode(String cityCode,Long shopId,String barcode);

    /**
     * 获取推荐品牌
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<PmsBrand> getRecommendBrandList(String cityCode,int pageNum, int pageSize);

    /**
     * 获取最新商品
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<PmsProduct> getNewProductList(String cityCode,boolean isAsc,String column,int pageNum, int pageSize);

    /**
     * 获取最热商品列表 按销量倒序
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<PmsProduct> getHotProductList(String cityCode,int pageNum, int pageSize);

    /**
     * 今日添加的商品
     *
     * @param id
     * @return
     */
    Integer countGoodsByToday(Long id);

    /**
     * 拍卖商品详情
     *
     * @param id
     * @return
     */
    Map<String, Object> queryPaiMaigoodsDetail(Long id);

    /**
     * 参与竞价
     *
     * @param goods
     * @return
     */
    Object updatePaiMai(PmsProduct goods);

    /**
     * 获取商品规格信息
     *
     * @param id
     * @return
     */
    GoodsDetailResult getGoodsSku(Long id);

    /**
     * 商品是否已收藏
     *
     * @return
     */
    boolean ifGoodsCollected(Long goodsId, Long userId);

}
