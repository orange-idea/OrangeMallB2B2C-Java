package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsGifts;

/**
 * <p>
 * 帮助表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface IPmsGiftsService extends IService<PmsGifts> {

}
