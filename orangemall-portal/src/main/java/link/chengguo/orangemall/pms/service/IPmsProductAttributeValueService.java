package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeValue;

/**
 * <p>
 * 存储产品参数信息的表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductAttributeValueService extends IService<PmsProductAttributeValue> {

}
