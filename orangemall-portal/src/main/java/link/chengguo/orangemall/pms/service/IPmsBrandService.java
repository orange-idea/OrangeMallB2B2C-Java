package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsBrand;

/**
 * <p>
 * 品牌表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsBrandService extends IService<PmsBrand> {

}
