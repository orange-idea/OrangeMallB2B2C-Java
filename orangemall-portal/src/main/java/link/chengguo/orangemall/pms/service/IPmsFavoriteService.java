package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsFavorite;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
public interface IPmsFavoriteService extends IService<PmsFavorite> {
    int addProduct(PmsFavorite productCollection);


    List<PmsFavorite> listProduct(Long memberId, int type);

    List<PmsFavorite> listCollect(Long memberId);
}
