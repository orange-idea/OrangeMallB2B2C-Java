package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;

public interface IPmsSmallNaviconCategoryService extends IService<PmsSmallNaviconCategory> {
}
