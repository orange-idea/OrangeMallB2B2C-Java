package link.chengguo.orangemall.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.exception.ApiMallPlusException;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;
import link.chengguo.orangemall.oms.service.IOmsOrderItemService;
import link.chengguo.orangemall.oms.service.IOmsOrderService;
import link.chengguo.orangemall.oms.vo.ConfirmOrderResult;
import link.chengguo.orangemall.oms.vo.OmsOrderDetail;
import link.chengguo.orangemall.oms.vo.OrderParam;
import link.chengguo.orangemall.oms.vo.TbThanks;
import link.chengguo.orangemall.single.ApiBaseAction;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.ums.service.RedisService;
import link.chengguo.orangemall.util.JsonUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.vo.Rediskey;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单管理Controller
 * https://github.com/orangemall on 2018/8/30.
 */
@Slf4j
@RestController
@Api(tags = "OmsPortalOrderController", description = "订单管理")
@RequestMapping("/api/order")
public class OmsPortalOrderController extends ApiBaseAction {

    @Autowired
    private IOmsOrderService orderService;
    @Autowired
    private IUmsMemberService memberService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private IOmsOrderItemService orderItemService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Object list(OmsOrder queryParam,
                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        queryParam.setMemberId(memberService.getNewCurrentMember().getId());
        List<OmsOrder> orderList = orderService.list(new QueryWrapper<>(queryParam));
        for (OmsOrder order : orderList) {
            OmsOrderItem query = new OmsOrderItem();
            query.setOrderId(queryParam.getId());
            List<OmsOrderItem> orderItemList = orderItemService.list(new QueryWrapper<>(query));
            order.setOrderItemList(orderItemList);
        }
        return new CommonResult().success(orderList);
    }

    @ApiOperation("获取订单详情:订单信息、商品信息、操作记录")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public Object detail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        OmsOrder orderDetailResult = null;
        String bannerJson = redisService.get(Rediskey.PmsProductResult + id);
        if (bannerJson != null) {
            orderDetailResult = JsonUtils.jsonToPojo(bannerJson, OmsOrderDetail.class);
        } else {
            orderDetailResult = orderService.getById(id);
            OmsOrderItem query = new OmsOrderItem();
            query.setOrderId(id);
            List<OmsOrderItem> orderItemList = orderItemService.list(new QueryWrapper<>(query));
            orderDetailResult.setOrderItemList(orderItemList);
            redisService.set(Rediskey.PmsProductResult + id, JsonUtils.objectToJson(orderDetailResult));
            redisService.expire(Rediskey.PmsProductResult + id, 10 * 60);
        }

        return new CommonResult().success(orderDetailResult);
    }


    @ResponseBody
    @GetMapping("/submitPreview")
    public Object submitPreview(OrderParam orderParam) {
        try {
            ConfirmOrderResult result = orderService.submitPreview(orderParam);
            return new CommonResult().success(result);
        } catch (ApiMallPlusException e) {
            return new CommonResult().failed(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 提交订单
     *
     * @param orderParam
     * @return
     */
    @ApiOperation("根据购物车信息生成订单")
    @RequestMapping(value = "/generateOrder")
    @ResponseBody
    public Object generateOrder(OrderParam orderParam) {
        return orderService.generateOrder(orderParam);
    }


    @RequestMapping(value = "/payOrder")
    @ApiOperation(value = "支付订单")
    @ResponseBody
    public Object payOrder(TbThanks tbThanks) {
        int result = orderService.payOrder(tbThanks);
        return new CommonResult().success(result);
    }

    @ApiOperation("自动取消超时订单")
    @RequestMapping(value = "/cancelTimeOutOrder", method = RequestMethod.POST)
    @ResponseBody
    public Object cancelTimeOutOrder() {
        return orderService.cancelTimeOutOrder();
    }

    @ApiOperation("取消单个超时订单")
    @RequestMapping(value = "/cancelOrder", method = RequestMethod.POST)
    @ResponseBody
    public Object cancelOrder(Long orderId) {
        orderService.sendDelayMessageCancelOrder(orderId);
        return new CommonResult().success(null);
    }

    /**
     * 查看物流
     */
    @ApiOperation("查看物流")
    @ResponseBody
    @RequestMapping("/getWayBillInfo")
    public Object getWayBillInfo(@RequestParam(value = "orderId", required = false, defaultValue = "0") Long orderId) throws Exception {
        try {
            UmsMember member = memberService.getNewCurrentMember();
            OmsOrder order = orderService.getById(orderId);
            if (order == null) {
                return null;
            }
            if (!order.getMemberId().equals(member.getId())) {
                return new CommonResult().success("非当前用户订单");
            }

            //    ExpressInfoModel expressInfoModel = orderService.queryExpressInfo(orderId);
            return new CommonResult().success(null);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("get waybillInfo error. error=" + e.getMessage(), e);
            return new CommonResult().failed("获取物流信息失败，请稍后重试");
        }

    }
}
