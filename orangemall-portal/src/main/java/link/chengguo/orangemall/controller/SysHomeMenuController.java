package link.chengguo.orangemall.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.sys.entity.SysHomeMenu;
import link.chengguo.orangemall.sys.service.SysHomeMenuService;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:22
 */
@Slf4j
@RestController
@RequestMapping("/api/SysHomeMenu")
public class SysHomeMenuController {
    @Resource
    private SysHomeMenuService homeMenuService;


    @ApiOperation("根据条件查询所有首页菜单")
    @GetMapping(value = "/list")
    @IgnoreAuth
    public Object getSysOrderTypeByPage(SysHomeMenu entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(homeMenuService.page(new Page<SysHomeMenu>(pageNum, pageSize), new QueryWrapper<>(entity).eq("type",1).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有首页菜单：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

}
