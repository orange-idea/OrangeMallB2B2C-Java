package link.chengguo.orangemall.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.shms.service.IShmsShopCategoryService;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺分类
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopCategoryController ", description = "店铺分类")
@RequestMapping("/api/shmsShopCategory")
public class ShmsShopCategoryController {

    @Resource
    private IShmsShopCategoryService IShmsShopCategoryService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺分类列表")
    @ApiOperation("根据条件查询所有店铺分类列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:read')")
    public Object getShmsShopCategoryByPage(ShmsShopCategory entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopCategoryService.page(new Page<ShmsShopCategory>(pageNum, pageSize), new QueryWrapper<>(entity).isNull("city_code").orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺分类列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺分类分配店铺分类")
    @ApiOperation("查询店铺分类明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCategory:read')")
    public Object getShmsShopCategoryById(@ApiParam("店铺分类id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺分类id");
            }
            ShmsShopCategory coupon = IShmsShopCategoryService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺分类明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }
}


