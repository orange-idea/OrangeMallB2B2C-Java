package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberMemberTagRelation;

/**
 * <p>
 * 用户和标签关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberMemberTagRelationService extends IService<UmsMemberMemberTagRelation> {

}
