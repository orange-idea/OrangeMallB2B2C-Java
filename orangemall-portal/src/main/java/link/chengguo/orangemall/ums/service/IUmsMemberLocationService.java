package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberLocation;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-25
 */
public interface IUmsMemberLocationService extends IService<UmsMemberLocation> {

}
