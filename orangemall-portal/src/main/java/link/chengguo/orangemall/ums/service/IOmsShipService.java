package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.OmsShip;

/**
 * <p>
 * 配送方式表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IOmsShipService extends IService<OmsShip> {

}
