package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsRewardLog;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-02
 */
public interface IUmsRewardLogService extends IService<UmsRewardLog> {

}
