package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsProtalMenu;

/**
 * 个人中心菜单
 * @Author: mxchen
 * @Date: 2020/8/31 17:47
 */
public interface IUmsProtalMenuService extends IService<UmsProtalMenu> {

}
