package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.*;
import link.chengguo.orangemall.jms.entity.JmsOrderTrack;
import link.chengguo.orangemall.jms.entity.JmsPickingOrder;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderGoods;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;

import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingOrderService extends IService<JmsPickingOrder> {

    /**
     * 拣货员抢单
     * @param user  拣货员信息
     * @param orderTrackId 订单跟踪ID
     * @return
     */
    boolean grabPickingOrder(JmsPickingUser user, Long orderTrackId);

    /**
     * 生成拣货订单
     * @return
     */
    boolean createPickingOrder(JmsOrderTrack orderTrack, JmsPickingUser user);

    /**
     * 抢单成功更新状态
     * @return
     */
    boolean updateStatusForGrabSuccess(JmsOrderTrack orderTrack, JmsPickingUser user);

    /**
     * 拣货完成更新状态
     * @return
     */
    boolean updateStatusForPicked(Long pickingOrderId, JmsPickingOrderGoods goods);


    /**
     * 添加拣货订单
     * @return
     */
    boolean addPickingOrder(JmsPickingOrder order);

    /**
     * 查询拣货员拣货订单
     * @param user  拣货员信息
     * @return
     */
    IPage<JmsPickingOrder> getWaitPickingOrderList(Integer pageNum,Integer pageSize,JmsPickingUser user);

    /**
     * 查询已完成的订单
     * @param user  拣货员信息
     * @return
     */
    IPage<JmsPickingOrder> getPickedOrderList(Integer pageNum, Integer pageSize, JmsPickingUser user);

    /**
     * 根据ids查询拣货订单列表
     * @param ids 拣货订单id逗号拼接而成
     * @return
     */
    List<JmsPickingOrder> getPickingOrdersByIds(String ids);

    /**
     * 根据ids更新拣货订单
     * @param ids 拣货订单id逗号拼接而成
     * @return
     */
    boolean updatePickingOrdersByIds(JmsPickingOrder order,String ids);


}
