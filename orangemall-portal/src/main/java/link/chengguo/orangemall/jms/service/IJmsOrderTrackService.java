package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsOrderTrack;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;

import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsOrderTrackService extends IService<JmsOrderTrack> {


    /**
     * 根据用户信息查询可抢订单内容
     * @return
     */
    JmsOrderTrack getOrderTrackByPickingUser(Long orderTrackId, JmsPickingUser user);

    /**
     * 根据角色ID查询符合条件订单信息
     * @return
     */
    JmsOrderTrack getOrderTrackByRoleId(JmsOrderTrack orderTrack,String[] typeIds,Integer status);

    /**
     * 根据拣货员查询订单信息
     * @param user
     * @return
     */
    List<JmsOrderTrack> getGrabList(JmsPickingUser user);

    /**
     * 查询跟踪订单列表
     * @return
     */
    List<JmsOrderTrack> getOrderTrackList(Long shopId,Integer status);


    /**
     *  拣货商品，更新跟踪订单状态
     * @param orderId 拣货订单
     * @param  number 拣货完成数量
     * @param pickingOrderId 拣货单ID
     * @return
     */
    boolean updateOrderTrackForPicking(Long orderId,int number,Long pickingOrderId);

}
