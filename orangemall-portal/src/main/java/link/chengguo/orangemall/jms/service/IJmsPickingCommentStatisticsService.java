package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingCommentStatistics;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingCommentStatisticsService extends IService<JmsPickingCommentStatistics> {

}
