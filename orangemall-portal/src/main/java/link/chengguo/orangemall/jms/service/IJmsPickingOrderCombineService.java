package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderCombine;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingOrderCombineService extends IService<JmsPickingOrderCombine> {

    /**
     * 开始拣货，生成合并订单
     * @param ids  拣货订单编号，逗号分隔
     * @return
     */
    boolean startPicking(String ids);

    /**
     * 添加合并订单
     * @return
     */
    boolean addPickingOrderCombin(JmsPickingOrderCombine pickingOrderCombine);

    /**
     * 查询拣货员拣货订单
     * @param user  拣货员信息
     * @return
     */
    IPage<JmsPickingOrderCombine> getPickingOrderList(Integer pageNum, Integer pageSize, JmsPickingUser user);


    /**
     * 根据拣货号查询拣货详情
     * @param id  拣货号
     * @return
     */
    JmsPickingOrderCombine getCombineDetail(Long id);


    /**
     * 更新拣货状态
     * @param combineId  拣货号
     * @return
     */
    boolean updateInfoForPicking(Long combineId,int number);
}
