package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingUserService extends IService<JmsPickingUser> {

    /**
     * 获取当前的拣货员信息
     * @return
     */
    JmsPickingUser getCurrentPickingUser();

    /**
     * 获取拣货员信息
     * @return
     */
    JmsPickingUser getByPhone(String phone);

    /**
     *  拣货员登录
     * @param phone 手机号
     * @param pwd 密码
     * @return
     */
    Object login(String phone,String pwd);

    /**
     *  更新拣货员信息
     * @return
     */
    boolean updateUserInfo(JmsPickingUser user);


    /**
     *  修改登录密码
     * @return
     */
    boolean updatePwd(JmsPickingUser user,String oldPwd,String newPwd);


}
