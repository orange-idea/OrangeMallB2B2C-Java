package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingLog;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingLogService extends IService<JmsPickingLog> {

}
