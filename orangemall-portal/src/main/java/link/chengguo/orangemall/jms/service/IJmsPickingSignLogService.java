package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingSignLog;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingSignLogService extends IService<JmsPickingSignLog> {

    /**
     *  拣货员打卡
     * @return
     */
    boolean  userSign(JmsPickingUser user, Integer type, String address, String lat, String lng);

}
