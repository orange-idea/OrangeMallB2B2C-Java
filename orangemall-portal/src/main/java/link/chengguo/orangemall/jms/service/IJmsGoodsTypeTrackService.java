package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;

import java.util.List;

/**
* @author yzb
* @date 2020-02-27
*/

public interface IJmsGoodsTypeTrackService extends IService<JmsGoodsTypeTrack> {

    /**
     * 根据订单ID查询分类信息
     * @param orderId  外部订单ID
     * @param status 抢单状态，如果传null则查询全部。
     * @return
     */
    List<JmsGoodsTypeTrack> getGoodsTypeTrackByOrderId(Long orderId,Integer status);



}
