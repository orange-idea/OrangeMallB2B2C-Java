package link.chengguo.orangemall.jms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderGoods;

import java.util.List;

/**
* @author yzb
* @date 2020-02-26
*/

public interface IJmsPickingOrderGoodsService extends IService<JmsPickingOrderGoods> {

    /**
     * 商品拣货
     * @param pickingOrderIds
     */
    boolean finishGoodsPick(String pickingOrderIds,Integer number);


    /**
     * 商品中关联订单
     * @param pickingOrderId 拣货订单ID
     * @param orderId 外部订单ID
     * @return
     */
    boolean  setPickingOrderIdForGoods(Long pickingOrderId,Long orderId);


    /**
     * 查询商品列表
     * @param combineId 拣货单号
     * @param typeId  分类ID
     * @param pickingOrderId 拣货订单ID
     * @return
     */
    List<JmsPickingOrderGoods> getPickingGoods(Long combineId,Long typeId,Long pickingOrderId);

}
