package link.chengguo.orangemall.mms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.mms.entity.MmsCardDetail;

/**
* @author yzb
* @date 2020-02-22
*/

public interface IMmsCardDetailService extends IService<MmsCardDetail> {

    /**
     * 卡券充值
     * @param uuid
     * @return
     */
    boolean cardRecharge(String uuid,Long memberId);

}
