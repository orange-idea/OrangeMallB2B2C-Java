package link.chengguo.orangemall.service.mms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import link.chengguo.orangemall.mms.mapper.MmsCardDetailMapper;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-22
 */
@Service
public class MmsCardDetailServiceImpl extends ServiceImpl
        <MmsCardDetailMapper, MmsCardDetail> implements IMmsCardDetailService {

    @Resource
    private MmsCardDetailMapper mmsCardDetailMapper;
    @Resource
    private IUmsMemberService memberService;


    @Transactional
    @Override
    public boolean cardRecharge(String uuid,Long memberId) {
        //根据UUID查询卡券信息
        MmsCardDetail detail=mmsCardDetailMapper.selectOne(new QueryWrapper<MmsCardDetail>().eq("uuid",uuid));
        if (detail==null){
            throw new RuntimeException("该卡券不存在，充值失败");
        }
        if (detail.getIfUse()==1){
            throw new RuntimeException("该卡券已被使用，充值失败");
        }
        if (detail.getIfActive()==0){
            throw new RuntimeException("该卡券未被激活，充值失败");
        }
        if (detail.getIfInvalid()==1){
            throw new RuntimeException("该卡券已作废，充值失败");
        }
        //查询用户信息
        UmsMember member= memberService.getById(memberId);
        detail.setMemberId(memberId);
        detail.setName(member.getNickname());
        detail.setPhone(member.getPhone());
        detail.setIfUse(1);
        detail.setRechargeTime(new Date());
        mmsCardDetailMapper.updateById(detail);
        //充值到会员账号
        //资金变动类型： 0-'全部', 1-'消费', 2-'退款', 3-'充值', 4-'提现', 5-'佣金', 6-'平台调整'
        memberService.addBlance(memberId,detail.getMoney(),3,"充值券扫码充值");
        return true;
    }
}
