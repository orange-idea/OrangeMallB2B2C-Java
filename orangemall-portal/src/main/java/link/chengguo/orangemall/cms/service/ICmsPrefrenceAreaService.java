package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsPrefrenceArea;

/**
 * <p>
 * 优选专区 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsPrefrenceAreaService extends IService<CmsPrefrenceArea> {

}
