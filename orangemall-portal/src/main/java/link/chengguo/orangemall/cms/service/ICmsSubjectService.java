package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.vo.timeline.Timeline;

import java.util.List;

/**
 * <p>
 * 专题表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsSubjectService extends IService<CmsSubject> {

    List<CmsSubject> getRecommendSubjectList(int pageNum, int pageSize);

    int countByToday(Long id);

    Object reward(Long aid, int coin);

    List<Timeline> listTimeLine();
}
