package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfoCategory;

/**
* @author yzb
* @date 2019-12-21
*/

public interface ICmsPlatformInfoCategoryService extends IService<CmsPlatformInfoCategory> {

}
