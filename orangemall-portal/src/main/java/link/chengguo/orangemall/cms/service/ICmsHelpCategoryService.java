package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsHelpCategory;

/**
 * <p>
 * 帮助分类表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsHelpCategoryService extends IService<CmsHelpCategory> {

}
