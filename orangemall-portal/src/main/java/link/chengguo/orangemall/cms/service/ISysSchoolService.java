package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysSchool;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface ISysSchoolService extends IService<SysSchool> {

}
