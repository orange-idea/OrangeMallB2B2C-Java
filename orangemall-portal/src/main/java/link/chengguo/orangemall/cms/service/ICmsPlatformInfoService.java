package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfo;

/**
* @author yzb
* @date 2019-12-21
*/

public interface ICmsPlatformInfoService extends IService<CmsPlatformInfo> {

}
