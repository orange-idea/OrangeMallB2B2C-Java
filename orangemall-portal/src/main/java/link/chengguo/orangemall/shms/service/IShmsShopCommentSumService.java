package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopCommentSum;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopCommentSumService extends IService<ShmsShopCommentSum> {

}
