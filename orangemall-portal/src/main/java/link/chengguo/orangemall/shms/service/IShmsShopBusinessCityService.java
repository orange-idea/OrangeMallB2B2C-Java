package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopBusinessCity;

/**
* @author yzb
* @date 2019-12-19
*/

public interface IShmsShopBusinessCityService extends IService<ShmsShopBusinessCity> {

}
