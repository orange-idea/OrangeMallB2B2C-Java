package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopAccountLogService extends IService<ShmsShopAccountLog> {

    /**
     * 添加店铺账户日志
     * @param shopAccountLog
     */
    boolean addShopAccountLog(ShmsShopAccountLog shopAccountLog);


}
