package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopSearchKeywords;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopSearchKeywordsService extends IService<ShmsShopSearchKeywords> {

}
