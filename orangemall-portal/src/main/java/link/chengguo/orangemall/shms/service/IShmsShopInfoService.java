package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopInfoService extends IService<ShmsShopInfo> {

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<ShmsShopInfo> customPageList(Integer page, Integer pageSize, ShmsShopInfo paramCondition);


    /**
     * 获取分页实体列表
     *
     * @param order 排序：0-降序，1-升序
     * @author yzb
     * @Date 2019-12-21
     */
    Page<ShmsShopInfo> customPageListByDistance(Integer pageIndex, Integer pageSize,ShmsShopInfo paramCondition,Integer order);


}
