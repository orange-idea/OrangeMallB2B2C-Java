package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopBillLogService extends IService<ShmsShopBillLog> {


    /**
     * 添加店铺账单记录
     * @param order
     * @param billOrderType 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单退款退货，5-订单退款退货通过,6-订单退款拒绝
     */
    boolean addShopBillLog(OmsOrder order,Integer billOrderType);

    /**
     * 修改店铺账单记录
     * @param shopBillLog
     */
    boolean updateShopBillLog(ShmsShopBillLog shopBillLog);


    /**
     * 根据订单ID查询账单信息
     * @return
     */
    ShmsShopBillLog getShopBillLogByOrderId(Long orderId);

    /**
     * 根据订单ID修改账单信息
     * @return
     */
    boolean updateShopBillByOrderId(ShmsShopBillLog shopBillLog);


}
