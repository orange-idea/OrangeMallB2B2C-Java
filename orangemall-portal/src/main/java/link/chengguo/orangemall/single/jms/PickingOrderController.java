package link.chengguo.orangemall.single.jms;

import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.service.*;
import link.chengguo.orangemall.jms.service.*;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 拣货订单管理
 *
 * @author yzb
 * @since 2020-02-28
 */
@Slf4j
@RestController
@Api(tags = "拣货订单管理", description = "PickingOrderController")
@RequestMapping("/api/jms/PickingOrderController")
public class PickingOrderController {
    @Resource
    private IJmsOrderTrackService orderTrackService;
    @Resource
    private IJmsPickingUserService pickingUserService;
    @Resource
    private IJmsPickingOrderService pickingOrderService;
    @Resource
    private IJmsPickingOrderCombineService pickingOrderCombineService;
    @Resource
    private IJmsPickingOrderGoodsService pickingOrderGoodsService;



    @SysLog(MODULE = "jms", REMARK = "查询待抢订单列表")
    @ApiOperation("查询待抢订单列表")
    @GetMapping(value = "/getGrabList")
    public Object getGrabList() {
        try {
            JmsPickingUser user = pickingUserService.getCurrentPickingUser();
            return new CommonResult().success(orderTrackService.getGrabList(user));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询待抢订单列表：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "拣货员抢单")
    @ApiOperation("拣货员抢单")
    @PostMapping(value = "/grabPickingOrder")
    public Object grabPickingOrder(@RequestParam Long id) {
        try {
            if (pickingOrderService.grabPickingOrder(pickingUserService.getCurrentPickingUser(), id)) {
                return new CommonResult().success();
            } else {
                return new CommonResult().failed("抢单失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("拣货员抢单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "查询拣待拣货订单")
    @ApiOperation("查询待拣货订单")
    @GetMapping(value = "/getWaitPickingOrderList")
    public Object getWaitPickingOrderList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            return new CommonResult().success(pickingOrderService.getWaitPickingOrderList(pageNum, pageSize, pickingUserService.getCurrentPickingUser()));
        } catch (Exception e) {
            System.err.println("查询拣货员待拣货订单：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "查询拣货中的订单")
    @ApiOperation("查询拣货中的订单")
    @GetMapping(value = "/getPickingOrderList")
    public Object getPickingOrderList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            return new CommonResult().success(pickingOrderCombineService.getPickingOrderList(pageNum, pageSize, pickingUserService.getCurrentPickingUser()));
        } catch (Exception e) {
            System.err.println("查询拣货员待拣货订单：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }


    @SysLog(MODULE = "jms", REMARK = "查询已完成的订单")
    @ApiOperation("查询已完成的订单")
    @GetMapping(value = "/getPickedOrderList")
    public Object getPickedOrderList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                     @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            return new CommonResult().success(pickingOrderService.getPickedOrderList(pageNum, pageSize, pickingUserService.getCurrentPickingUser()));
        } catch (Exception e) {
            System.err.println("查询拣货员待拣货订单：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }


    @SysLog(MODULE = "jms", REMARK = "合并拣货订单/开始拣货")
    @ApiOperation("合并拣货订单/开始拣货")
    @PostMapping(value = "/startPicking")
    public Object startPicking(@RequestParam(value = "ids") String ids) {
        try {
            pickingOrderCombineService.startPicking(ids);
            return new CommonResult().success();
        } catch (Exception e) {
            System.err.println("合并拣货订单/开始拣货：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }


    @SysLog(MODULE = "jms", REMARK = "查询合并拣货详情")
    @ApiOperation("查询合并拣货详情")
    @GetMapping(value = "/getCombineOrderDetail")
    public Object getCombineOrderDetail(@RequestParam(value = "id") Long id) {
        try {
            return new CommonResult().success(pickingOrderCombineService.getCombineDetail(id));
        } catch (Exception e) {
            System.err.println("合并拣货订单/开始拣货：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "查询拣货商品列表")
    @ApiOperation("查询拣货商品列表")
    @GetMapping(value = "/getPickingGoods")
    public Object getPickingGoods(@RequestParam(value = "combineId") Long combineId,
                                  @RequestParam(value = "typeId") Long typeId,
                                  @RequestParam(value = "pickingOrderId") Long pickingOrderId) {
        try {
            return new CommonResult().success(pickingOrderGoodsService.getPickingGoods(combineId,typeId,pickingOrderId));
        } catch (Exception e) {
            System.err.println("合并拣货订单/开始拣货：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }


    @SysLog(MODULE = "jms", REMARK = "商品拣货确认")
    @ApiOperation("商品拣货确认")
    @PostMapping(value = "/finishGoodsPick")
    public Object finishGoodsPick(@RequestParam(value = "pickingOrderIds") String pickingOrderIds,
                                  @RequestParam(value = "number") Integer number) {
        try {
            return new CommonResult().success(pickingOrderGoodsService.finishGoodsPick(pickingOrderIds,number));
        } catch (Exception e) {
            System.err.println("合并拣货订单/开始拣货：" + e.getMessage());
            return new CommonResult().failed(e.getMessage());
        }
    }

}
