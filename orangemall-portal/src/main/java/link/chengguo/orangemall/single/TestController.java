package link.chengguo.orangemall.single;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.cms.entity.CmsSubjectCategory;
import link.chengguo.orangemall.cms.mapper.CmsSubjectCategoryMapper;
import link.chengguo.orangemall.cms.mapper.CmsSubjectMapper;
import link.chengguo.orangemall.cms.service.ISysAreaService;
import link.chengguo.orangemall.cms.service.ISysSchoolService;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.mapper.PmsProductAttributeCategoryMapper;
import link.chengguo.orangemall.pms.mapper.PmsProductMapper;
import link.chengguo.orangemall.pms.service.IPmsFavoriteService;
import link.chengguo.orangemall.pms.service.IPmsProductService;
import link.chengguo.orangemall.sys.entity.SysArea;
import link.chengguo.orangemall.sys.entity.SysSchool;
import link.chengguo.orangemall.sys.mapper.SysAreaMapper;
import link.chengguo.orangemall.sys.mapper.SysSchoolMapper;
import link.chengguo.orangemall.sys.mapper.SysStoreMapper;
import link.chengguo.orangemall.ums.mapper.UmsEmployInfoMapper;
import link.chengguo.orangemall.ums.mapper.UmsRewardLogMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberMemberTagRelationService;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.ums.service.RedisService;
import link.chengguo.orangemall.service.ums.impl.RedisUtil;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@RestController
@Api(tags = "TestController", description = "测试")
@RequestMapping("/test")
public class TestController extends ApiBaseAction {

    @Resource
    CmsSubjectMapper subjectMapper;
    @Resource
    SysSchoolMapper schoolMapper;
    @Resource
    SysAreaMapper sysAreaMapper;
    @Resource
    CmsSubjectCategoryMapper categoryMapper;
    @Resource
    private ISysSchoolService schoolService;
    @Resource
    private IUmsMemberService memberService;
    @Resource
    private ISysAreaService areaService;
    @Resource
    private IUmsMemberMemberTagRelationService memberTagService;
    @Resource
    private UmsRewardLogMapper rewardLogMapper;
    @Resource
    private UmsEmployInfoMapper employInfoMapper;
    @Resource
    private SysStoreMapper storeMapper;
    @Resource
    private PmsProductMapper productMapper;
    @Resource
    private RedisService redisService;
    @Resource
    private IPmsProductService pmsProductService;
    @Resource
    private RedisUtil redisUtil;
    @Autowired
    private IPmsFavoriteService favoriteService;
    @Resource
    private PmsProductAttributeCategoryMapper productAttributeCategoryMapper;

    @ApiOperation("获取会员详情")
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public Object test() {
        PmsProduct product = new PmsProduct();
        product.setStoreName("北京皮包专卖");
        productMapper.update(product, new QueryWrapper<PmsProduct>().eq("store_id", 1));

        product = new PmsProduct();
        product.setStoreName("北京豪车专卖");
        productMapper.update(product, new QueryWrapper<PmsProduct>().eq("store_id", 2));

        product = new PmsProduct();
        product.setStoreName("北京服装专卖");
        productMapper.update(product, new QueryWrapper<PmsProduct>().eq("store_id", 3));

        product = new PmsProduct();
        product.setStoreName("北京手术专卖");
        productMapper.update(product, new QueryWrapper<PmsProduct>().eq("store_id", 4));

        return new CommonResult().success();
    }

    @ApiOperation("获取会员详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public Object detail() {
        List<SysArea> areas = sysAreaMapper.selectList(new QueryWrapper<>());
        List<SysSchool> schools = schoolMapper.selectList(new QueryWrapper<>());
        List<CmsSubject> list = subjectMapper.selectList(new QueryWrapper<>());
        for (CmsSubject subject : list) {
            Random r = new Random();
            Integer a = r.nextInt(100);
            Integer c = r.nextInt(3);
            Integer d = r.nextInt(5);
            CmsSubjectCategory cate = categoryMapper.selectById(d);

            if (cate != null) {
                subject.setCategoryName(cate.getName());
                subject.setCategoryId(Long.valueOf(d));
            }

            subject.setType(c);
            Integer b = r.nextInt(100);
            SysSchool school = schools.get(a);
            if (school != null) {
                subject.setSchoolId(school.getId());
                subject.setSchoolName(school.getName());
            } else {
                SysSchool school1 = schools.get(b);
                if (school1 != null) {
                    subject.setSchoolId(school1.getId());
                    subject.setSchoolName(school1.getName());
                }
            }

            SysArea area = areas.get(b);
            if (area != null) {
                subject.setAreaId(area.getId());
                subject.setAreaName(area.getName());
            } else {
                SysArea area1 = areas.get(a);
                if (area1 != null) {
                    subject.setAreaId(area1.getId());
                    subject.setAreaName(area1.getName());
                }
            }
            subjectMapper.updateById(subject);
        }
        return new CommonResult().success();
    }

}
