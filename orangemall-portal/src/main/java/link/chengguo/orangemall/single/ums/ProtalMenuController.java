package link.chengguo.orangemall.single.ums;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import link.chengguo.orangemall.ums.service.IUmsProtalMenuService;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: mxchen
 * @Date: 2020/8/31 17:37
 */
@Api
@Slf4j
@RestController
@RequestMapping("/api/protalMenu")
public class ProtalMenuController {
    @Autowired
    private IUmsProtalMenuService umsProtalMenuService;

    /**
     * 获取所有使用的菜单信息
     * @return
     */
    @SysLog(MODULE = "ums", REMARK = "查询菜单可用列表")
    @IgnoreAuth
    @ApiOperation(value = "查询菜单可用列表")
    @GetMapping(value = "/list")
    public Object getAllProtalMenu(UmsProtalMenu entity){
        List<UmsProtalMenu> menuList = umsProtalMenuService.list(new QueryWrapper<>(entity).eq("status", 1).orderByDesc("sort"));
        return new CommonResult().success(menuList);
    }
}
