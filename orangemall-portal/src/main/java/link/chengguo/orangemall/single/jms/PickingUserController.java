package link.chengguo.orangemall.single.jms;

import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.service.IJmsPickingSignLogService;
import link.chengguo.orangemall.jms.service.IJmsPickingUserService;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 拣货员管理
 *
 * @author yzb
 * @since 2020-02-28
 */
@Slf4j
@RestController
@Api(tags = "拣货员管理", description = "PickingUserController")
@RequestMapping("/api/jms/PickingUserController")
public class PickingUserController {
    @Resource
    private IJmsPickingUserService pickingUserService;
    @Resource
    private IJmsPickingSignLogService signLogService;

    @SysLog(MODULE = "ams", REMARK = "拣货员账号密码登录")
    @ApiOperation("拣货员账号密码登录")
    @GetMapping(value = "/login")
    public Object login(@RequestParam String phone,
                        @RequestParam String pwd) {
        try {
            return new CommonResult().success(pickingUserService.login(phone, pwd));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("拣货员账号密码登录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "拣货员信息查询")
    @ApiOperation("拣货员信息查询")
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    public Object getUserInfo() {
        try {
            JmsPickingUser user = pickingUserService.getCurrentPickingUser();
            if (user == null) {
                return new CommonResult().failed("用户不存在");
            } else {
                user.setPwd(null);
                return new CommonResult().success(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("拣货员信息查询：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "修改个人资料")
    @ApiOperation("修改个人资料")
    @RequestMapping(value = "/updateUserInfo", method = RequestMethod.GET)
    public Object updateUserInfo(JmsPickingUser pickingUser) {
        try {
            JmsPickingUser user = pickingUserService.getCurrentPickingUser();
            if (!user.getPhone().equals(pickingUser.getPhone())){
                return new CommonResult().failed("手机号不能通过此接口修改");
            }
            pickingUser.setId(user.getId());
            pickingUserService.updateUserInfo(pickingUser);
            return new CommonResult().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改个人资料：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "修改登录密码")
    @ApiOperation("修改登录密码")
    @RequestMapping(value = "/updatePwd", method = RequestMethod.GET)
    public Object updatePwd(@RequestParam String oldPwd,
                            @RequestParam String newPwd) {
        try {
            JmsPickingUser user = pickingUserService.getCurrentPickingUser();
            pickingUserService.updatePwd(user,oldPwd,newPwd);
            return new CommonResult().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改登录密码：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

    @SysLog(MODULE = "jms", REMARK = "拣货员上下班打卡")
    @ApiOperation("拣货员上下班打卡（0-下班，1-上班）")
    @PostMapping(value = "/userClock")
    public Object userClock(@RequestParam String address,
                            @RequestParam String lat,
                            @RequestParam String lng,
                            @RequestParam Integer type) {
        try {
            JmsPickingUser user = pickingUserService.getCurrentPickingUser();
            if (user.getWorkState().equals(type)){
                return new CommonResult().failed("当前已经是待切换状态，无需操作");
            }
            if (signLogService.userSign(user,type,address,lat,lng)){
                return new CommonResult().success();
            }else{
                return new CommonResult().failed("未知错误");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("拣货员上下班打卡：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }

}
