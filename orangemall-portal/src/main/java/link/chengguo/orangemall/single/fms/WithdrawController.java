package link.chengguo.orangemall.single.fms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.component.UserUtils;
import link.chengguo.orangemall.fms.entity.FmsMemberRecharge;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdraw;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;
import link.chengguo.orangemall.fms.service.IFmsMemberRechargeService;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawAccountService;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 提现管理
 *
 * @author yzb
 * @since 2019-12-22
 */
@Slf4j
@RestController
@Api(tags = "充值提现管理", description = "WithdrawController")
@RequestMapping("/api/fms/WithdrawController")
public class WithdrawController {
    @Resource
    private IFmsMemberWithdrawAccountService memberWithdrawAccountService;
    @Resource
    private IFmsMemberWithdrawService memberWithdrawService;
    @Resource
    private IFmsMemberRechargeService memberRechargeService;
    @Resource
    private IUmsMemberService memberService;


    @SysLog(MODULE = "fms", REMARK = "查询充值记录")
    @ApiOperation("查询充值记录")
    @GetMapping(value = "/getMemberRechargeList")
    public Object getMemberRechargeList(FmsMemberRecharge entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            entity.setMemberId(member.getId());
            return new CommonResult().success(memberRechargeService.page(new Page<>(pageNum,pageSize),
                    new QueryWrapper<>(entity).orderByDesc("id").orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询充值记录：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "查询提现记录")
    @ApiOperation("查询提现记录")
    @GetMapping(value = "/getMemberWithdrawList")
    public Object getMemberWithdrawList(FmsMemberWithdraw entity,
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            entity.setMemberId(member.getId());
            return new CommonResult().success(memberWithdrawService.page(new Page<>(pageNum,pageSize),
                    new QueryWrapper<>(entity).orderByDesc("id").orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询提现记录：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "fms", REMARK = "提交提现申请")
    @ApiOperation("提交提现申请")
    @PostMapping(value = "/addMemberWithdraw")
    public Object addMemberWithdraw(FmsMemberWithdraw entity) {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            entity.setMemberId(member.getId());
            UmsMember user=memberService.getById(member.getId());
            entity.setMemberName(user.getNickname());
            entity.setMemberPhone(user.getPhone());
            entity.setCreateTime(new Date());
            return new CommonResult().success(memberWithdrawService.save(entity));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("提交提现申请：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "增加/更新提现账号")
    @ApiOperation("增加/更新提现账号")
    @PostMapping(value = "/updateMemberWithdrawAccount")
    public Object updateMemberWithdrawAccount(FmsMemberWithdrawAccount entity) {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            entity.setMemberId(member.getId());
            memberWithdrawAccountService.addOrUpdateWithdrawAccount(entity);
            return new CommonResult().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("增加/更新提现账号：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "查询提现账号")
    @ApiOperation("查询提现账号")
    @GetMapping(value = "/getWithdrawAccount")
    public Object getWithdrawAccount() {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            FmsMemberWithdrawAccount account=new FmsMemberWithdrawAccount();
            account.setMemberId(member.getId());
            return new CommonResult().success(memberWithdrawAccountService.getOne(new QueryWrapper<>(account)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("增加/更新提现账号：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }



}
