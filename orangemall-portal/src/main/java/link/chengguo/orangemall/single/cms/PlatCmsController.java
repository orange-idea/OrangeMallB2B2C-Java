package link.chengguo.orangemall.single.cms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsFeedback;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfo;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfoCategory;
import link.chengguo.orangemall.cms.service.*;
import link.chengguo.orangemall.cms.service.*;
import link.chengguo.orangemall.component.UserUtils;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 平台类内容管理
 *
 * @author yzb
 * @since 2019-12-22
 */
@Slf4j
@RestController
@Api(tags = "平台类内容管理", description = "PlatCmsController")
@RequestMapping("/api/cms/PlatCmsController")
public class PlatCmsController {

    @Resource
    private ICmsAboutUsService aboutUsService;
    @Resource
    private ICmsContactUsService contactUsService;
    @Resource
    private ICmsFeedbackCategoryService feedbackCategoryService;
    @Resource
    private ICmsFeedbackService feedbackService;
    @Resource
    private ICmsPlatformInfoCategoryService platformInfoCategoryService;
    @Resource
    private ICmsPlatformInfoService platformInfoService;


    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "关于我们")
    @ApiOperation("关于我们")
    @GetMapping(value = "/getAboutUs")
    public Object getAboutUs() {
        try {
            return new CommonResult().success(aboutUsService.getById(1));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询关于我们：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "联系我们")
    @ApiOperation("联系我们")
    @GetMapping(value = "/getContactUs")
    public Object getContactUs() {
        try {
            return new CommonResult().success(contactUsService.getById(1));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询关于我们：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "意见反馈类型列表")
    @ApiOperation("意见反馈类型列表")
    @GetMapping(value = "/getFeedbackCategoryList")
    public Object getFeedbackCategoryList() {
        try {
            return new CommonResult().success(feedbackCategoryService.list(new QueryWrapper<>()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("意见反馈类型列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "提交意见反馈")
    @ApiOperation("提交意见反馈")
    @GetMapping(value = "/addFeedback")
    public Object addFeedback(CmsFeedback feedback) {
        try {
            UmsMember user= UserUtils.getCurrentMember();
            feedback.setMemberId(user.getId());
            feedbackService.save(feedback);
            return new CommonResult().success();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("意见反馈类型列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "查询平台说明类型")
    @ApiOperation("查询平台说明类型")
    @GetMapping(value = "/getPlatformInfoCategory")
    public Object getPlatformInfoCategory(Integer type) {
        try {
            CmsPlatformInfoCategory category=new CmsPlatformInfoCategory();
            if (type!=null){
                category.setHelpType(type);
            }
            return new CommonResult().success(platformInfoCategoryService.list(new QueryWrapper<>(category)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("意见反馈类型列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @IgnoreAuth
    @SysLog(MODULE = "cms", REMARK = "查询平台说明")
    @ApiOperation("查询平台说明")
    @GetMapping(value = "/getPlatformInfo")
    public Object getPlatformInfo(Integer id,Integer categoryId) {
        try {
            CmsPlatformInfo platformInfo=new CmsPlatformInfo();
            if (categoryId!=null){
                platformInfo.setCategoryId(categoryId);
            }
            if (categoryId!=null){
                platformInfo.setId(id);
            }
            return new CommonResult().success(platformInfoService.list(new QueryWrapper<>(platformInfo)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询平台说明：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }


}
