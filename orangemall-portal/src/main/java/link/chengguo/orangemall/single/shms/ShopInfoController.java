package link.chengguo.orangemall.single.shms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.service.ISysAreaService;
import link.chengguo.orangemall.pms.service.IPmsProductService;
import link.chengguo.orangemall.shms.entity.ShmsShopCommentLog;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.service.IShmsShopCommentLogService;
import link.chengguo.orangemall.single.ApiBaseAction;
import link.chengguo.orangemall.sys.entity.SysArea;
import link.chengguo.orangemall.ums.service.IUmsMemberBlanceLogService;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.vo.shms.ShopCommentVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: yzb
 * @Date: 2019/12/19 15:02
 * @Description:
 */
@RestController
@Slf4j
@Api(tags = "店铺信息管理", description = "ShopInfoController")
@RequestMapping("/api/shms/shopInfo")
public class ShopInfoController extends ApiBaseAction {
    @Resource
    private link.chengguo.orangemall.shms.service.IShmsShopInfoService IShmsShopInfoService;
    @Resource
    private IPmsProductService iPmsProductService;
    @Resource
    private ISysAreaService areaService;
    @Resource
    private IUmsMemberService umsMemberService;
    @Resource
    private IShmsShopCommentLogService shmsShopCommentLogService;
    @SysLog(MODULE = "ShopInfoController", REMARK = "根据条件查询所有店铺列表")
    @ApiOperation("根据条件查询所有店铺列表")
    @GetMapping(value = "/list")
    public Object getShmsShopCategoryByPage(ShmsShopInfo entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper = new QueryWrapper();
            if (!StringUtils.isEmpty(entity.getCategoryId())) {
                queryWrapper.in("category_id", entity.getCategoryId().split(","));
            }
            queryWrapper.eq("status",1);//启用状态
            queryWrapper.eq("account_status",2);//已通过审核
            if(entity.getCityCode()!=null){
                queryWrapper.eq("city_code",entity.getCityCode());
            }
            if (entity.getIsRecommend()!=null){
                queryWrapper.eq("is_recommend",entity.getIsRecommend());
            }
            queryWrapper.select("id","category_id","self_category_id","industry_id","add_type","manage_mode","name","logo","is_close","business_hours","longitude","latitude");
            IPage<ShmsShopInfo> subjectList = IShmsShopInfoService.page(new Page<>(pageNum, pageSize), queryWrapper);
//            for (int i=0;i<subjectList.getRecords().size();i++){
//                //给每个店铺添加4个商品,商品必须是店铺内推荐的
//                IPage<PmsProduct> page=iPmsProductService.page(new Page<>(1,4),
//                        new QueryWrapper<PmsProduct>().ne("delete_status",1).eq("publish_status",1)
//                        .eq("verify_status",1).eq("recommand_status",1)
//                        .eq("shop_id",subjectList.getRecords().get(i).getId())
//                );
//                subjectList.getRecords().get(i).setGoodsList(page.getRecords());
//            }

            return new CommonResult().success(subjectList);
        } catch (Exception e) {
            System.err.println("根据条件查询所有店铺列表：" + e.getMessage());
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "ShopInfoController", REMARK = "根据条件查询所有店铺列表")
    @ApiOperation("根据条件查询所有店铺列表")
    @GetMapping(value = "/listForDistance")
    public Object listForDistance(ShmsShopInfo entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
                                            @RequestParam(value = "order", defaultValue = "1") Integer order
    ) {
        try {
            log.info("开始查询商品信息");
            Page<ShmsShopInfo> subjectList = IShmsShopInfoService.customPageListByDistance(pageNum,pageSize,entity,order);
//            for (int i=0;i<subjectList.getRecords().size();i++){
//                //给每个店铺添加4个商品,商品必须是店铺内推荐的
//                IPage<PmsProduct> page=iPmsProductService.page(new Page<>(1,4),
//                        new QueryWrapper<PmsProduct>().ne("delete_status",1).eq("publish_status",1)
//                                .eq("verify_status",1).eq("recommand_status",1)
//                                .eq("shop_id",subjectList.getRecords().get(i).getId())
//                );
//                subjectList.getRecords().get(i).setGoodsList(page.getRecords());
//            }
//            SysArea areap = areaService.getById(shopInfo.getProvince());
//            SysArea areac = areaService.getById(shopInfo.getCity());
//            SysArea aread = areaService.getById(shopInfo.getDistance());
//            if(areap!=null && areap.getName()!=null){
//                shopInfo.setProvinceName(areap.getName());
//            }
//            if(areac!=null && areac.getName()!=null){
//                shopInfo.setCityName(areac.getName());
//            }
//            if(aread!=null && aread.getName()!=null){
//                shopInfo.setDistrictName(aread.getName());
//            }
            return new CommonResult().success(subjectList);
        } catch (Exception e) {
            System.err.println("根据条件查询所有店铺列表：" + e.getMessage());
        }
        return new CommonResult().failed();
    }



    @SysLog(MODULE = "ShopInfoController", REMARK = "根据条件查询所有扫码购店铺列表")
    @ApiOperation("根据条件查询所有扫码购店铺列表")
    @GetMapping(value = "/listCodeBuy")
    public Object listCodeBuy(ShmsShopInfo entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        try {
            entity.setScanOpen(1);
            Page<ShmsShopInfo> subjectList = IShmsShopInfoService.customPageListByDistance(pageNum,pageSize,entity,1);

//            QueryWrapper queryWrapper = new QueryWrapper();
//            queryWrapper.eq("status",1);//启用状态
//            queryWrapper.eq("account_status",2);//已通过审核
//            queryWrapper.eq("city_code",entity.getCityCode());
//            queryWrapper.eq("scan_open",1);//开通扫码购
//            queryWrapper.select("id","category_id","self_category_id","industry_id","add_type","manage_mode","name","logo","is_close","business_hours","longitude","latitude","address");
//            IPage<ShmsShopInfo> subjectList = IShmsShopInfoService.page(new Page<>(pageNum, pageSize), queryWrapper);
            return new CommonResult().success(subjectList);
        } catch (Exception e) {
            System.err.println("根据条件查询所有店铺列表：" + e.getMessage());
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "shms", REMARK = "查询店铺详情信息")
    @IgnoreAuth
    @GetMapping(value = "/detail")
    @ApiOperation(value = "查询店铺详情信息")
    public Object getDetail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        if (id==null){
            return  new CommonResult().failed("店铺ID不能为空");
        }
        ShmsShopInfo shopInfo=IShmsShopInfoService.getById(id);
        SysArea areap = areaService.getById(shopInfo.getProvince());
        SysArea areac = areaService.getById(shopInfo.getCity());
        SysArea aread = areaService.getById(shopInfo.getDistrict());
        if(areap!=null && areap.getExtName()!=null){
            shopInfo.setProvinceName(areap.getExtName());
        }
        if(areac!=null && areac.getExtName()!=null){
            shopInfo.setCityName(areac.getExtName());
        }
        if(aread!=null && aread.getExtName()!=null){
            shopInfo.setDistrictName(aread.getExtName());
        }
        return new CommonResult().success(shopInfo);
    }

    @SysLog(MODULE = "shms", REMARK = "查询店铺评价信息")
    @IgnoreAuth
    @GetMapping(value = "/assess")
    @ApiOperation(value = "查询店铺评价信息")
    public Object getAssess(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        if (id==null){
            return  new CommonResult().failed("店铺ID不能为空");
        }
        double starsAverage = 0;
        Map<String,Object> map = new HashMap<>();
        List<ShopCommentVo> commentVoList = new ArrayList<>();
        List<ShmsShopCommentLog> list = shmsShopCommentLogService.list(new QueryWrapper<ShmsShopCommentLog>().eq("shop_id", id));
        if(list.size()<1){
            return new CommonResult().success("店铺没有评价");
        }
        for(ShmsShopCommentLog shopCommentLog : list){
            starsAverage += shopCommentLog.getStars();
            ShopCommentVo shopCommentVo = new ShopCommentVo();
            shopCommentVo.setUmsMember(umsMemberService.getById(shopCommentLog.getMemberId()));
            shopCommentVo.setShmsShopCommentLog(shopCommentLog);
            commentVoList.add(shopCommentVo);
        }
        map.put("commentVoList",commentVoList);
        map.put("starsAverage",starsAverage/list.size());
        return new CommonResult().success(map);
    }
}
