package link.chengguo.orangemall.single;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.service.ISysAreaService;
import link.chengguo.orangemall.cms.service.ISysSchoolService;
import link.chengguo.orangemall.oms.mapper.OmsOrderItemMapper;
import link.chengguo.orangemall.pms.entity.PmsFavorite;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeCategory;
import link.chengguo.orangemall.pms.entity.PmsProductConsult;
import link.chengguo.orangemall.pms.mapper.PmsProductAttributeCategoryMapper;
import link.chengguo.orangemall.pms.mapper.PmsProductMapper;
import link.chengguo.orangemall.pms.service.IPmsFavoriteService;
import link.chengguo.orangemall.pms.service.IPmsProductConsultService;
import link.chengguo.orangemall.pms.service.IPmsProductService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.mapper.ShmsShopInfoMapper;
import link.chengguo.orangemall.sys.entity.SysArea;
import link.chengguo.orangemall.sys.entity.SysSchool;
import link.chengguo.orangemall.sys.entity.SysStore;
import link.chengguo.orangemall.sys.mapper.SysStoreMapper;
import link.chengguo.orangemall.sys.mapper.SysUserMapper;
import link.chengguo.orangemall.ums.entity.UmsEmployInfo;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.mapper.UmsEmployInfoMapper;
import link.chengguo.orangemall.ums.mapper.UmsRewardLogMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberMemberTagRelationService;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.ums.service.RedisService;
import link.chengguo.orangemall.service.ums.impl.RedisUtil;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.vo.Rediskey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/2 15:02
 * @Description:
 */
@RestController
@Api(tags = "UmsController", description = "会员关系管理")
@RequestMapping("/api/single/user")
public class SingeUmsController extends ApiBaseAction {

    @Resource
    private SysUserMapper userMapper;

    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private ISysSchoolService schoolService;
    @Resource
    private IUmsMemberService memberService;
    @Resource
    private ISysAreaService areaService;
    @Resource
    private IUmsMemberMemberTagRelationService memberTagService;
    @Resource
    private UmsRewardLogMapper rewardLogMapper;
    @Resource
    private UmsEmployInfoMapper employInfoMapper;
    @Resource
    private SysStoreMapper storeMapper;
    @Resource
    private PmsProductMapper productMapper;
    @Resource
    private RedisService redisService;
    @Resource
    private IPmsProductService pmsProductService;
    @Resource
    private RedisUtil redisUtil;
    @Autowired
    private IPmsFavoriteService favoriteService;
    @Resource
    private PmsProductAttributeCategoryMapper productAttributeCategoryMapper;
    @Autowired
    private IPmsProductConsultService pmsProductConsultService;
    @Resource
    private OmsOrderItemMapper omsOrderItemMapper;
    @Resource
    private ShmsShopInfoMapper shmsShopInfoMapper;

    @ApiOperation("获取会员详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @ResponseBody
    public Object detail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        UmsMember member = memberService.getById(id);
        return new CommonResult().success(member);
    }

    @IgnoreAuth
    @ApiOperation(value = "查询商铺列表")
    @GetMapping(value = "/store/list")
    @SysLog(MODULE = "ums", REMARK = "查询商铺列表")
    public Object storeList(SysStore entity,
                            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        return new CommonResult().success(storeMapper.selectList(new QueryWrapper<SysStore>(entity)));
    }
    @IgnoreAuth
    @ApiOperation("获取某个会员的所有评价")
    @RequestMapping(value = "/consult/list", method = RequestMethod.GET)
    @ResponseBody
    public Object consultList(@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                              @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        UmsMember member = memberService.getNewCurrentMember();
        IPage<PmsProductConsult> page = pmsProductConsultService.page(new Page<PmsProductConsult>(pageNum, pageSize), new QueryWrapper<PmsProductConsult>().eq("member_id", member.getId()).orderByDesc("consult_addtime"));
        if(page.getRecords().size()<1){
            return new CommonResult().success(page);
        }
        for(PmsProductConsult pmsProductConsult : page.getRecords()){
            pmsProductConsult.setOmsOrderItem(omsOrderItemMapper.selectById(pmsProductConsult.getOrderItemId()));
        }

        return new CommonResult().success(page);
    }

    @ApiOperation("获取商铺详情")
    @RequestMapping(value = "/storeDetail", method = RequestMethod.GET)
    @ResponseBody
    public Object storeDetail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        SysStore store = storeMapper.selectById(id);
        List<PmsProductAttributeCategory> list = productAttributeCategoryMapper.selectList(new QueryWrapper<PmsProductAttributeCategory>().eq("store_id", id));
        for (PmsProductAttributeCategory gt : list) {
            PmsProduct productQueryParam = new PmsProduct();
            productQueryParam.setProductAttributeCategoryId(gt.getId());
            productQueryParam.setPublishStatus(1);
            productQueryParam.setVerifyStatus(1);
            IPage<PmsProduct> goodsList = pmsProductService.page(new Page<PmsProduct>(0, 8), new QueryWrapper<>(productQueryParam));
            if (goodsList != null && goodsList.getRecords() != null && goodsList.getRecords().size() > 0) {
                gt.setGoodsList(goodsList.getRecords());
            } else {
                gt.setGoodsList(new ArrayList<>());
            }
        }
        store.setList(list);
        store.setGoodsCount(pmsProductService.count(new QueryWrapper<PmsProduct>().eq("store_id", id)));
        //记录浏览量到redis,然后定时更新到数据库
        String key = Rediskey.STORE_VIEWCOUNT_CODE + id;
        //找到redis中该篇文章的点赞数，如果不存在则向redis中添加一条
        Map<Object, Object> viewCountItem = redisUtil.hGetAll(Rediskey.STORE_VIEWCOUNT_KEY);
        Integer viewCount = 0;
        if (!viewCountItem.isEmpty()) {
            if (viewCountItem.containsKey(key)) {
                viewCount = Integer.parseInt(viewCountItem.get(key).toString()) + 1;
                redisUtil.hPut(Rediskey.STORE_VIEWCOUNT_KEY, key, viewCount + "");
            } else {
                viewCount = 1;
                redisUtil.hPut(Rediskey.STORE_VIEWCOUNT_KEY, key, 1 + "");
            }
        } else {
            redisUtil.hPut(Rediskey.STORE_VIEWCOUNT_KEY, key, 1 + "");
        }
        Map<String, Object> map = new HashMap<>();
        UmsMember umsMember = memberService.getNewCurrentMember();
        if (umsMember != null && umsMember.getId() != null) {

            PmsFavorite query = new PmsFavorite();
            query.setObjId(id);
            query.setMemberId(umsMember.getId());
            query.setType(3);
            PmsFavorite findCollection = favoriteService.getOne(new QueryWrapper<>(query));
            if (findCollection != null) {
                map.put("favorite", true);
            } else {
                map.put("favorite", false);
            }
        }
        store.setHit(viewCount);
        map.put("store", store);
        return new CommonResult().success(map);
    }

    @IgnoreAuth
    @ApiOperation(value = "查询学校列表")
    @GetMapping(value = "/school/list")
    @SysLog(MODULE = "ums", REMARK = "查询学校列表")
    public Object schoolList(SysSchool entity,
                             @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                             @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum) {
        return new CommonResult().success(schoolService.page(new Page<SysSchool>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
    }

    @ApiOperation("获取学校详情")
    @RequestMapping(value = "/schoolDetail", method = RequestMethod.GET)
    @ResponseBody
    public Object schoolDetail(@RequestParam(value = "id", required = false, defaultValue = "0") Long id) {
        SysSchool school = schoolService.getById(id);
        List<PmsProduct> list = productMapper.selectList(new QueryWrapper<PmsProduct>().eq("school_id", id));
        school.setGoodsList(list);
        school.setGoodsCount(list.size());
        return new CommonResult().success(school);
    }

    @IgnoreAuth
    @SysLog(MODULE = "ums", REMARK = "根据pid查询区域")
    @ApiOperation("根据pid查询区域")
    @RequestMapping(value = "/getAreaByPid", method = RequestMethod.GET)
    public Object getAreaByPid(@RequestParam(value = "pid", required = false, defaultValue = "0") Long pid) {
        SysArea queryPid = new SysArea();
        queryPid.setPid(pid);
        List<SysArea> list = areaService.list(new QueryWrapper<SysArea>(queryPid));
        return new CommonResult().success(list);
    }


    @ApiOperation("添加招聘")
    @SysLog(MODULE = "ums", REMARK = "添加招聘")
    @PostMapping(value = "/addJob")
    public Object addJob(UmsEmployInfo member) {
        return employInfoMapper.insert(member);
    }

    @ApiOperation(value = "会员绑定学校")
    @PostMapping(value = "/bindSchool")
    @SysLog(MODULE = "ums", REMARK = "会员绑定学校")
    public Object bindSchool(@RequestParam(value = "schoolId", required = true) Long schoolId) {
        try {
            UmsMember member = memberService.getNewCurrentMember();

            String countKey = "bindSchool:count:" + ":" + member.getId();
            String value = redisService.get(countKey);
            if (value != null) {
                Integer count = Integer.valueOf(value);
                if (count > 100) {
                    return new CommonResult().success("已超过当天最大次数");
                }
            }
            SysSchool area = schoolService.getById(schoolId);
            if (area == null) {
                return new CommonResult().failed("学校不存在");
            }
            member.setSchoolName(area.getName());
            member.setSchoolId(schoolId);
            memberService.updateById(member);
            // 当天发送验证码次数+1
            redisService.increment(countKey, 1L);
            redisService.expire(countKey, 1 * 3600 * 24 * 365);
            return new CommonResult().success("绑定学校成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new CommonResult().failed("绑定学校失败");
        }
    }

    @ApiOperation(value = "会员绑定区域")
    @PostMapping(value = "/bindArea")
    @SysLog(MODULE = "ums", REMARK = "会员绑定区域")
    public Object bindArea(@RequestParam(value = "areaId", required = true) Long areaId) {
        try {
            UmsMember member = memberService.getNewCurrentMember();
            String countKey = "bindArea:count:" + ":" + member.getId();
            String value = redisService.get(countKey);
            if (value != null) {
                Integer count = Integer.valueOf(value);
                if (count > 100) {
                    return new CommonResult().success("已超过当天最大次数");
                }
            }

            SysArea area = areaService.getById(areaId);
            if (area == null) {
                return new CommonResult().failed("区域不存在");
            }
            member.setAreaId(areaId);
            member.setAreaName(area.getName());
            memberService.updateById(member);
            // 当天发送验证码次数+1
            redisService.increment(countKey, 1L);
            redisService.expire(countKey, 1 * 3600 * 24 * 365);
            return new CommonResult().success(area);
        } catch (Exception e) {
            e.printStackTrace();
            return new CommonResult().failed("绑定区域失败");
        }
    }
    @SysLog(MODULE = "sys", REMARK = "保存")
    @ApiOperation("会员入驻商家信息获取")
    @GetMapping(value = "/getUmsShopInfo")
    @Transactional
    public Object getUmsShopInfo() {
        try {
            UmsMember member = memberService.getNewCurrentMember();
            if(!"none".equals(member.getIdentity())){
                long shopId =Integer.parseInt(member.getIdentity().substring("shop".length()));
                ShmsShopInfo shopInfo = shmsShopInfoMapper.selectOne(new QueryWrapper<ShmsShopInfo>().eq("id", shopId));
                return new CommonResult().success(shopInfo);
            }
        }catch (Exception e) {
            e.printStackTrace();
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed("未入驻商家");
    }
    @SysLog(MODULE = "sys", REMARK = "保存")
    @ApiOperation("会员入驻商家信息保存")
    @PostMapping(value = "/applyStore")
    @Transactional
    public Object applyStore(ShmsShopInfo entity) {
        try {
            entity.setCreateTime(new Date());
            Integer count = shmsShopInfoMapper.selectCount(new QueryWrapper<ShmsShopInfo>().eq("contact_mobile", entity.getContactMobile()));
            if(count<1&&shmsShopInfoMapper.insert(entity)>0){
                UmsMember member = memberService.getNewCurrentMember();
                ShmsShopInfo shopInfo = shmsShopInfoMapper.selectOne(new QueryWrapper<ShmsShopInfo>().eq("contact_mobile", entity.getContactMobile()));
                member.setIdentity("shop"+shopInfo.getId());
                memberService.updateById(member);
                return new CommonResult().success();
            }else{
                throw new DuplicateKeyException("手机号码重复,已被注册");
            }
        }catch (Exception e) {
            e.printStackTrace();
            if(e instanceof DuplicateKeyException){
                return new CommonResult().failed("手机号码重复,已被注册");
            }
            return new CommonResult().failed(e.getMessage());
        }
    }

    /*@ApiOperation(value = "会员绑定区域")
    @PostMapping(value = "/bindArea")
    @SysLog(MODULE = "ums", REMARK = "会员绑定区域")
    public Object bindArea(@RequestParam(value = "areaIds", required = true) String areaIds) {
        try {
            if (ValidatorUtils.empty(areaIds)) {
                return new CommonResult().failed("请选择区域");
            }
            UmsMember member = memberService.getNewCurrentMember();
            String[] areIdList = areaIds.split(",");
            List<UmsMemberMemberTagRelation> list = new ArrayList<>();
            for (String id : areIdList) {
                UmsMemberMemberTagRelation tag = new UmsMemberMemberTagRelation();
                tag.setMemberId(member.getId());
                tag.setTagId(Long.valueOf(id));
                list.add(tag);
            }
            if (list != null && list.size() > 0) {
                memberTagService.saveBatch(list);
            }
            return new CommonResult().success("绑定区域成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new CommonResult().failed("绑定区域失败");
        }
    }*/
}
