package link.chengguo.orangemall.single.pms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.service.IPmsProductCategoryService;
import link.chengguo.orangemall.pms.vo.PmsProductCategoryWithChildrenItem;
import link.chengguo.orangemall.single.ApiBaseAction;
import link.chengguo.orangemall.utils.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 产品分类
 * </p>
 *
 * @author yzb
 * @since 2019-12-25
 */
@RestController
@Slf4j
@Api(tags = "商品分类管理", description = "ProductCategoryController")
@RequestMapping("/api/pms/productCategory")
public class ProductCategoryController extends ApiBaseAction {

    @Resource
    private IPmsProductCategoryService IPmsProductCategoryService;

    @SysLog(MODULE = "pms", REMARK = "根据条件查询所有产品分类列表")
    @ApiOperation("根据条件查询所有产品分类列表")
    @GetMapping(value = "/list")
    public Object getPmsProductCategoryByPage(PmsProductCategory entity) {
        try {
            return new CommonResult().success(IPmsProductCategoryService.list(new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有产品分类列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("查询所有一级分类及子分类")
    @RequestMapping(value = "/list/withChildren", method = RequestMethod.GET)
    @ResponseBody
    public Object listWithChildren(PmsProductCategory category) {
        List<PmsProductCategoryWithChildrenItem> list = IPmsProductCategoryService.listWithChildren(category);
        return new CommonResult().success(list);
    }


    @SysLog(MODULE = "pms", REMARK = "查询商品分类")
    @IgnoreAuth
    @ApiOperation(value = "查询商品分类")
    @GetMapping(value = "/getGoodsTypes")
    public Object getGoodsTypes(PmsProductCategory pmsProductCategory) throws Exception {
//        List<PmsProductCategory> relList = new ArrayList<>();
//        String json = redisService.get(Rediskey.goodsCategorys + apiContext.getCurrentProviderId()+pmsProductCategory.getCityCode());
//        if (ValidatorUtils.notEmpty(json)) {
//            relList = JsonUtils.json2list(json, PmsProductCategory.class);
//            return new CommonResult().success(relList);
//        }
        //区域已经通过前端自动传过来了
//        pmsProductCategory.setShowStatus(1);
//        pmsProductCategory.setNavStatus(1);
//        pmsProductCategory.setType(AllEnum.CategoryType.Agent.code());
//        List<PmsProductCategory> categories = IPmsProductCategoryService.list(new QueryWrapper<>(pmsProductCategory));
//        for (PmsProductCategory v : categories) {
//            if (v.getParentId() == 0) {
//                relList.add(v);
//            }
//        }
        pmsProductCategory.setType(AllEnum.CategoryType.Agent.code());//筛选平台分类
        pmsProductCategory.setParentId(0L);
        List<PmsProductCategory> relList = IPmsProductCategoryService.list(new QueryWrapper<>(pmsProductCategory).orderByDesc("sort"));
        //组装二级分类
        for (PmsProductCategory productCategory : relList) {
            productCategory.setChildList(IPmsProductCategoryService.list(new QueryWrapper<PmsProductCategory>().eq("parent_id",productCategory.getId()).orderByDesc("sort")));
        }
//        redisService.set(Rediskey.goodsCategorys + apiContext.getCurrentProviderId()+pmsProductCategory.getCityCode(), JsonUtils.objectToJson(relList));
//        redisService.expire(Rediskey.goodsCategorys + apiContext.getCurrentProviderId()+pmsProductCategory.getCityCode(), 2);
//        pmsProductCategory.setType(AllEnum.CategoryType.Agent.code());//筛选平台分类

        return new CommonResult().success(relList);
    }

}
