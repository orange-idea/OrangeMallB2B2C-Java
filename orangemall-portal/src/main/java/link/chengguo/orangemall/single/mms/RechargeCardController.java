package link.chengguo.orangemall.single.mms;

import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.component.UserUtils;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 提现管理
 *
 * @author yzb
 * @since 2019-12-22
 */
@Slf4j
@RestController
@Api(tags = "充值卡券管理", description = "RechargeCardController")
@RequestMapping("/api/mms/RechargeCardController")
public class RechargeCardController {
    @Resource
    private IMmsCardDetailService cardDetailService;


    @SysLog(MODULE = "mms", REMARK = "卡券扫码充值")
    @ApiOperation("卡券扫码充值")
    @PostMapping(value = "/cardRecharge")
    public Object cardRecharge(String uuid) {
        try {
            UmsMember member= UserUtils.getCurrentMember();
            return new CommonResult().success(cardDetailService.cardRecharge(uuid,member.getId()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("卡券扫码充值：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
    }


}
