package link.chengguo.orangemall.single.ams;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.service.IAmsBusinessCityService;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.sys.entity.SysArea;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 区域管理
 *
 * @author yzb
 * @since 2019-12-22
 */
@Slf4j
@RestController
@Api(tags = "区域管理", description = "AreaController")
@RequestMapping("/api/ams/AreaController")
public class AreaController {
    @Resource
    private link.chengguo.orangemall.cms.service.ISysAreaService ISysAreaService;
    @Resource
    private IAmsBusinessCityService businessCityService;


    @IgnoreAuth
    @SysLog(MODULE = "ams", REMARK = "根据条件查询已开通服务城市")
    @ApiOperation("根据条件查询已开通服务城市")
    @GetMapping(value = "/getBusinessCitylist")
    public Object getRoleByPage(AmsBusinessCity entity) {
        try {
            QueryWrapper queryWrapper = new QueryWrapper();
            if (!StringUtils.isEmpty(entity.getBusinessCityName())) {
                queryWrapper.like("business_city_name", entity.getBusinessCityName());
            }
            return new CommonResult().success(businessCityService.list( new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询已开通服务城市：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @IgnoreAuth
    @SysLog(MODULE = "ams", REMARK = "根据城市名称查询编码")
    @ApiOperation("根据城市名称查询编码")
    @RequestMapping(value = "/getCityCodeByName", method = RequestMethod.GET)
    public Object getCityCodeByName(SysArea entity) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper();
            if (ISysAreaService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

}
