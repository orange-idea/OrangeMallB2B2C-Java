package link.chengguo.orangemall.single.shms;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.single.ApiBaseAction;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Auther: yzb
 * @Date: 2019/12/19 15:02
 * @Description:
 */
@RestController
@Slf4j
@Api(tags = "店铺分类管理", description = "ShopCategoryController")
@RequestMapping("/api/shms/shopCategory")
public class ShopCategoryController extends ApiBaseAction {
    @Resource
    private link.chengguo.orangemall.shms.service.IShmsShopCategoryService IShmsShopCategoryService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺分类列表")
    @ApiOperation("根据条件查询所有店铺分类列表")
    @GetMapping(value = "/list")
    public Object getShmsShopCategoryByPage(ShmsShopCategory entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopCategoryService.page(new Page<ShmsShopCategory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            System.err.println("根据条件查询所有店铺分类列表："+e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺分类列表")
    @ApiOperation("根据条件查询所有店铺分类列表")
    @GetMapping(value = "/listForTenant")
    public Object getShmsShopCategoryForTenantByPage(ShmsShopCategory entity,
                                            @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopCategoryService.page(new Page<ShmsShopCategory>(pageNum, pageSize), new QueryWrapper<>(entity).isNull("city_code").orderByDesc("id")));
        } catch (Exception e) {
            System.err.println("根据条件查询所有店铺分类列表："+e.getMessage());
        }
        return new CommonResult().failed();
    }

}
