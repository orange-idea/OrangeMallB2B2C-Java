package link.chengguo.orangemall.config;


import link.chengguo.orangemall.ApiContext;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.component.JwtAuthenticationTokenFilter;
import link.chengguo.orangemall.component.RestAuthenticationEntryPoint;
import link.chengguo.orangemall.component.RestfulAccessDeniedHandler;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.service.IJmsPickingUserService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.ums.service.RedisService;
import link.chengguo.orangemall.util.JsonUtils;
import link.chengguo.orangemall.vo.MemberDetails;
import link.chengguo.orangemall.vo.PickingUserDetails;
import link.chengguo.orangemall.vo.Rediskey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * SpringSecurity的配置
 * https://github.com/orangemall on 2018/4/26.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private IUmsMemberService memberService;
    @Autowired
    private IJmsPickingUserService pickingUserService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ApiContext apiContext;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        Map<RequestMappingInfo, HandlerMethod> handlerMethodMap = applicationContext.getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class).getHandlerMethods();
        Set<String> anonymousUrls = new HashSet<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> infoEntry : handlerMethodMap.entrySet()) {
            HandlerMethod handlerMethod = infoEntry.getValue();
            IgnoreAuth ignoreAuth = handlerMethod.getMethodAnnotation(IgnoreAuth.class);
            if (null != ignoreAuth) {
                anonymousUrls.addAll(infoEntry.getKey().getPatternsCondition().getPatterns());
            }
        }
        httpSecurity.csrf()// 由于使用的是JWT，我们这里不需要csrf
                .disable()
                .sessionManagement()// 基于token，所以不需要session
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, // 允许对于网站静态资源的无授权访问
                        "/",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/swagger-resources/**",
                        "/v2/api-docs/**"
                )
                .permitAll()
                .antMatchers("/admin/login", "/admin/register")// 对登录注册要允许匿名访问
                .permitAll()
                .antMatchers("/api/single/home/**")//首页内容
                .permitAll()
                .antMatchers("/api/single/pms/**")//商品内容
                .permitAll()
                .antMatchers("/api/single/sms/**")//营销模块
                .permitAll()
                .antMatchers("/api/shms/**")//店铺管理模块
                .permitAll()
                .antMatchers("/api/applet/**")//小程序登录授权
                .permitAll()
                .antMatchers("/api/ams/**")//区域管理模块
                .permitAll()
                .antMatchers("/api/pms/**")//商品模块
                .permitAll()
                .antMatchers("/api/wxPay/**")//支付回调
                .permitAll()
                .antMatchers("/api/aliPay/**")//支付回调
                .permitAll()
                .antMatchers("/api/oms/order/verify")//订单核销
                .permitAll()
                .antMatchers("/api/cms/PlatCmsController/**")//平台内容管理
                .permitAll()
                .antMatchers("/api/jms/PickingUserController/login")//拣货员登录
                .permitAll()

                // .antMatchers(HttpMethod.OPTIONS)//跨域请求会先进行一次options请求
                //.permitAll()
//                .antMatchers("/**")//测试时全部运行访问
//                .permitAll()
                // 自定义匿名访问所有url放行 ： 允许匿名和带权限以及登录用户访问
                .antMatchers(anonymousUrls.toArray(new String[0])).permitAll()
                .anyRequest()// 除上面外的所有请求全部需要鉴权认证
                .authenticated();
        // 禁用缓存
        httpSecurity.headers().cacheControl();
        // 添加JWT filter
        httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        //添加自定义未授权和未登录结果返回
        httpSecurity.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {

        return username -> {
            if (username.startsWith("#picking#")) {
                JmsPickingUser pickingUser = JsonUtils.jsonToPojo(redisService.get(String.format(Rediskey.PICKING_USER, username)), JmsPickingUser.class);
                //   UmsMember member = JsonUtils.jsonToPojo(redisService.get(String.format(Rediskey.MEMBER, username)),UmsMember.class);
                if (pickingUser == null) {
                    pickingUser = pickingUserService.getByPhone(username.substring("#picking#".length()));
                    redisService.set(String.format(Rediskey.PICKING_USER, username), JsonUtils.objectToJson(pickingUser));
                }
                return new PickingUserDetails(pickingUser);
            } else {

                //UmsMember member = JsonUtils.jsonToPojo(redisService.get(apiContext.getCurrentProviderId() + ":" + String.format(Rediskey.MEMBER, username)), UmsMember.class);
                UmsMember member = JsonUtils.jsonToPojo(redisService.get(String.format(Rediskey.MEMBER, username)), UmsMember.class);
                if (member == null) {
                    member = memberService.getByUsername(username);
                    redisService.set(String.format(Rediskey.MEMBER, username), JsonUtils.objectToJson(member));
                }
                return new MemberDetails(member);
            }
        };
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }

    /**
     * 允许跨域调用的过滤器
     */
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedOrigin("*");
        config.setAllowCredentials(true);
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return new CorsFilter(source);
    }
}
