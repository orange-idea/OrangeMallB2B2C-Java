package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.AccountChangeType;
import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopAccountLogMapper;
import link.chengguo.orangemall.shms.service.IShmsShopAccountLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopAccountLogServiceImpl extends ServiceImpl
        <ShmsShopAccountLogMapper, ShmsShopAccountLog> implements IShmsShopAccountLogService {

    @Resource
    private ShmsShopAccountLogMapper shmsShopAccountLogMapper;

    @Override
    public boolean addShopAccountLog(ShmsShopAccountLog shopAccountLog) {
        //0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
        if (shopAccountLog.getChangeType()== AccountChangeType.OrderPay.getValue()){
            shopAccountLog.setChangeTypeName("订单支付");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.Refund.getValue()){
            shopAccountLog.setChangeTypeName("订单退款申请");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.RefundSuccess.getValue()){
            shopAccountLog.setChangeTypeName("订单退款通过");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.RefundFailure.getValue()){
            shopAccountLog.setChangeTypeName("订单退款拒绝");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.OrderSettlement.getValue()){
            shopAccountLog.setChangeTypeName("订单结算");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.WithdrawApply.getValue()){
            shopAccountLog.setChangeTypeName("提现申请");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.WithdrawSuccess.getValue()){
            shopAccountLog.setChangeTypeName("提现申请通过");
        }else if(shopAccountLog.getChangeType()== AccountChangeType.WithdrawFailure.getValue()){
            shopAccountLog.setChangeTypeName("提现申请拒绝");
        }
        shopAccountLog.setCreateTime(new Date());
       int result = shmsShopAccountLogMapper.insert(shopAccountLog);
       return result >0?true:false;
    }
}
