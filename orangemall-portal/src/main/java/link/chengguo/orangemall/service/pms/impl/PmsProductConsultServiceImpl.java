package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProductConsult;
import link.chengguo.orangemall.pms.mapper.PmsProductConsultMapper;
import link.chengguo.orangemall.pms.service.IPmsProductConsultService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品咨询表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsProductConsultServiceImpl extends ServiceImpl<PmsProductConsultMapper, PmsProductConsult> implements IPmsProductConsultService {

}
