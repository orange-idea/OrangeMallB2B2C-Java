package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillMonthMapper;
import link.chengguo.orangemall.shms.service.IShmsShopBillMonthService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillMonthServiceImpl extends ServiceImpl
        <ShmsShopBillMonthMapper, ShmsShopBillMonth> implements IShmsShopBillMonthService {

    @Resource
    private ShmsShopBillMonthMapper shmsShopBillMonthMapper;


}
