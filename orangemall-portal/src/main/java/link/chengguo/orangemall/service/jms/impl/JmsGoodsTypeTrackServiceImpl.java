package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;
import link.chengguo.orangemall.jms.mapper.JmsGoodsTypeTrackMapper;
import link.chengguo.orangemall.jms.service.IJmsGoodsTypeTrackService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-27
 */
@Service
public class JmsGoodsTypeTrackServiceImpl extends ServiceImpl
        <JmsGoodsTypeTrackMapper, JmsGoodsTypeTrack> implements IJmsGoodsTypeTrackService {

    @Resource
    private JmsGoodsTypeTrackMapper jmsGoodsTypeTrackMapper;

    @Override
    public List<JmsGoodsTypeTrack> getGoodsTypeTrackByOrderId(Long orderId,Integer status) {
        JmsGoodsTypeTrack goodsTypeTrack=new JmsGoodsTypeTrack();
        goodsTypeTrack.setOrderId(orderId);
        if (status!=null){
            goodsTypeTrack.setStatus(status);
        }
        return jmsGoodsTypeTrackMapper.selectList(new QueryWrapper<>(goodsTypeTrack));
    }
}
