package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsGroupMember;
import link.chengguo.orangemall.sms.mapper.SmsGroupMemberMapper;
import link.chengguo.orangemall.sms.service.ISmsGroupMemberService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsGroupMemberServiceImpl extends ServiceImpl<SmsGroupMemberMapper, SmsGroupMember> implements ISmsGroupMemberService {

}
