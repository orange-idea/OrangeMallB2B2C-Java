package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsPickingRole;
import link.chengguo.orangemall.jms.mapper.JmsPickingRoleMapper;
import link.chengguo.orangemall.jms.service.IJmsPickingRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingRoleServiceImpl extends ServiceImpl
<JmsPickingRoleMapper, JmsPickingRole> implements IJmsPickingRoleService {

@Resource
private  JmsPickingRoleMapper jmsPickingRoleMapper;


}
