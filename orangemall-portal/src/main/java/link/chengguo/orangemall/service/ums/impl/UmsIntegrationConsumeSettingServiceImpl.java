package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsIntegrationConsumeSetting;
import link.chengguo.orangemall.ums.mapper.UmsIntegrationConsumeSettingMapper;
import link.chengguo.orangemall.ums.service.IUmsIntegrationConsumeSettingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分消费设置 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsIntegrationConsumeSettingServiceImpl extends ServiceImpl<UmsIntegrationConsumeSettingMapper, UmsIntegrationConsumeSetting> implements IUmsIntegrationConsumeSettingService {

}
