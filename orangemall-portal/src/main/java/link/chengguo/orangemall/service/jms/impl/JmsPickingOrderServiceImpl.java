package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.*;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderMapper;
import link.chengguo.orangemall.jms.service.IJmsGoodsTypeTrackService;
import link.chengguo.orangemall.jms.service.IJmsOrderTrackService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderGoodsService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsPickingOrderServiceImpl extends ServiceImpl
        <JmsPickingOrderMapper, JmsPickingOrder> implements IJmsPickingOrderService {

    @Resource
    private JmsPickingOrderMapper jmsPickingOrderMapper;
    @Resource
    private IJmsOrderTrackService orderTrackService;
    @Resource
    private IJmsGoodsTypeTrackService goodsTypeTrackService;
    @Resource
    private IJmsPickingOrderGoodsService orderGoodsService;

    @Transactional
    @Override
    public boolean grabPickingOrder(JmsPickingUser user, Long orderTrackId) {
        //查询可抢订单消息
        JmsOrderTrack orderTrack=orderTrackService.getOrderTrackByPickingUser(orderTrackId,user);
        if (orderTrack==null){
            //抢单失败
            throw new RuntimeException("订单已被抢");
        }
        //生成订单
        this.createPickingOrder(orderTrack,user);
        //更新抢单状态
        this.updateStatusForGrabSuccess(orderTrack,user);
        return true;
    }

    /**
     *  生成拣货订单
     * @param orderTrack
     * @param user
     * @return
     */
    @Transactional
    @Override
    public boolean createPickingOrder(JmsOrderTrack orderTrack, JmsPickingUser user) {
        //生成拣货订单
        JmsPickingOrder order=new JmsPickingOrder();
        order.setCityCode(user.getCityCode());
        order.setShopId(user.getShopId());
        order.setGoodsTypeIds(orderTrack.getGoodsTypeIds());
        order.setGoodsTypeNames(orderTrack.getGoodsTypeNames());
        order.setOrderId(orderTrack.getOrderId());
        order.setStatus(PickingStatus.WaitPicking.getValue());
        order.setUserId(user.getId());
        order.setGoodsNumber(orderTrack.getGoodsNumber());
        this.addPickingOrder(order);

        //关联对应的商品信息
        JmsPickingOrderGoods goods=new JmsPickingOrderGoods();
        goods.setPickingOrderId(order.getId());
        orderGoodsService.setPickingOrderIdForGoods(order.getId(),order.getOrderId());
        return true;
    }

    /**
     * 更新抢单状态
     * @param orderTrack
     * @param user
     * @return
     */
    @Transactional
    @Override
    public boolean updateStatusForGrabSuccess(JmsOrderTrack orderTrack, JmsPickingUser user) {
        //更新商品跟踪表
        JmsGoodsTypeTrack goodsTypeTrack=new JmsGoodsTypeTrack();
        goodsTypeTrack.setStatus(PickingStatus.WaitPicking.getValue());
        goodsTypeTrack.setUserId(user.getId());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("order_id",orderTrack.getOrderId());
        queryWrapper.eq("status",PickingStatus.WaitGrab.getValue());
        goodsTypeTrackService.update(goodsTypeTrack,queryWrapper);
        //更新订单跟踪表
        JmsOrderTrack orderTrack1=orderTrackService.getById(orderTrack.getId());
        orderTrack1.setUserNumber(orderTrack.getUserNumber()+1);//人员信息添加
        //查询是否已经全部抢完
        List<JmsGoodsTypeTrack> list=goodsTypeTrackService.getGoodsTypeTrackByOrderId(orderTrack.getOrderId(),PickingStatus.WaitGrab.getValue());
        if (list.size()==0){
            orderTrack1.setStatus(PickingStatus.Picking.getValue());//更新订单追踪
        }
        orderTrackService.updateById(orderTrack1);
        return true;
    }

    @Transactional
    @Override
    public boolean updateStatusForPicked(Long pickingOrderId, JmsPickingOrderGoods goods) {
        //查询拣货订单
        JmsPickingOrder order=jmsPickingOrderMapper.selectById(pickingOrderId);
        order.setFinishGoodsNumber(goods.getGoodsNumber()+order.getFinishGoodsNumber());
        if (order.getFinishGoodsNumber().equals(order.getGoodsNumber())){
            //拣货完成
            order.setStatus(PickingStatus.Picked.getValue());
            order.setFinishTime(new Date());
        }
        jmsPickingOrderMapper.updateById(order);
        return true;
    }

    @Transactional
    @Override
    public boolean addPickingOrder(JmsPickingOrder order) {
        order.setCreateTime(new Date());
        return jmsPickingOrderMapper.insert(order)>0?true:false;
    }

    @Override
    public IPage<JmsPickingOrder> getWaitPickingOrderList(Integer pageNum,Integer pageSize,JmsPickingUser user) {
        JmsPickingOrder order=new JmsPickingOrder();
        order.setUserId(user.getId());
        order.setStatus(PickingStatus.WaitPicking.getValue());
        return jmsPickingOrderMapper.selectPage(new Page<>(pageNum,pageSize),new QueryWrapper<>(order));
    }

    @Override
    public IPage<JmsPickingOrder> getPickedOrderList(Integer pageNum, Integer pageSize, JmsPickingUser user) {
        JmsPickingOrder order=new JmsPickingOrder();
        order.setUserId(user.getId());
        order.setStatus(PickingStatus.Picked.getValue());
        return jmsPickingOrderMapper.selectPage(new Page<>(pageNum,pageSize),new QueryWrapper<>(order));
    }

    @Override
    public List<JmsPickingOrder> getPickingOrdersByIds(String ids) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("status",PickingStatus.WaitPicking.getValue());
        queryWrapper.in("id", ids.split(","));
        return  jmsPickingOrderMapper.selectList(queryWrapper);
    }

    @Override
    public boolean updatePickingOrdersByIds(JmsPickingOrder order, String ids) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.in("id", ids.split(","));
        return jmsPickingOrderMapper.update(order,queryWrapper)>0?true:false;
    }
}
