package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProductVertifyRecord;
import link.chengguo.orangemall.pms.mapper.PmsProductVertifyRecordMapper;
import link.chengguo.orangemall.pms.service.IPmsProductVertifyRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品审核记录 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsProductVertifyRecordServiceImpl extends ServiceImpl<PmsProductVertifyRecordMapper, PmsProductVertifyRecord> implements IPmsProductVertifyRecordService {

}
