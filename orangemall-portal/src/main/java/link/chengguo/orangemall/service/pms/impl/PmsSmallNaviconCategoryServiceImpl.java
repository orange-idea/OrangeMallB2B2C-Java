package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;
import link.chengguo.orangemall.pms.mapper.PmsSmallNaviconCategoryMapper;
import link.chengguo.orangemall.pms.service.IPmsSmallNaviconCategoryService;
import org.springframework.stereotype.Service;

@Service
public class PmsSmallNaviconCategoryServiceImpl extends ServiceImpl<PmsSmallNaviconCategoryMapper, PmsSmallNaviconCategory> implements IPmsSmallNaviconCategoryService {
}
