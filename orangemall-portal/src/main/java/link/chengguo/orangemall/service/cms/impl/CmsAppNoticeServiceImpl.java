package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsAppNotice;
import link.chengguo.orangemall.cms.mapper.CmsAppNoticeMapper;
import link.chengguo.orangemall.cms.service.ICmsAppNoticeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsAppNoticeServiceImpl extends ServiceImpl
<CmsAppNoticeMapper, CmsAppNotice> implements ICmsAppNoticeService {

@Resource
private  CmsAppNoticeMapper cmsAppNoticeMapper;


}
