package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsHomeBrand;
import link.chengguo.orangemall.sms.mapper.SmsHomeBrandMapper;
import link.chengguo.orangemall.sms.service.ISmsHomeBrandService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页推荐品牌表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsHomeBrandServiceImpl extends ServiceImpl<SmsHomeBrandMapper, SmsHomeBrand> implements ISmsHomeBrandService {

}
