package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillReshipItems;
import link.chengguo.orangemall.bill.mapper.BillReshipItemsMapper;
import link.chengguo.orangemall.bill.service.IBillReshipItemsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货单明细表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillReshipItemsServiceImpl extends ServiceImpl<BillReshipItemsMapper, BillReshipItems> implements IBillReshipItemsService {

}
