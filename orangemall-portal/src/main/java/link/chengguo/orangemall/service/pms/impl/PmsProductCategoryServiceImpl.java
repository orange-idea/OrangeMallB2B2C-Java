package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.mapper.PmsProductCategoryMapper;
import link.chengguo.orangemall.pms.service.IPmsProductCategoryService;
import link.chengguo.orangemall.pms.vo.PmsProductCategoryWithChildrenItem;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 产品分类 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsProductCategoryServiceImpl extends ServiceImpl<PmsProductCategoryMapper, PmsProductCategory> implements IPmsProductCategoryService {

    @Resource
    private PmsProductCategoryMapper categoryMapper;

    @Override
    public List<PmsProductCategoryWithChildrenItem> listWithChildren(PmsProductCategory category) {
        return categoryMapper.listWithChildren(category);
    }
}
