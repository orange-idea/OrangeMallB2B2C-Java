package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.fms.entity.FmsAgentWithdraw;
import link.chengguo.orangemall.fms.mapper.FmsAgentWithdrawMapper;
import link.chengguo.orangemall.fms.service.IFmsAgentWithdrawService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsAgentWithdrawServiceImpl extends ServiceImpl
<FmsAgentWithdrawMapper, FmsAgentWithdraw> implements IFmsAgentWithdrawService {

@Resource
private  FmsAgentWithdrawMapper fmsAgentWithdrawMapper;


}
