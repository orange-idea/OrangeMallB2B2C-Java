package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdraw;
import link.chengguo.orangemall.fms.mapper.FmsMemberWithdrawMapper;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsMemberWithdrawServiceImpl extends ServiceImpl
<FmsMemberWithdrawMapper, FmsMemberWithdraw> implements IFmsMemberWithdrawService {

@Resource
private  FmsMemberWithdrawMapper fmsMemberWithdrawMapper;


}
