package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsSubjectComment;
import link.chengguo.orangemall.cms.mapper.CmsSubjectCommentMapper;
import link.chengguo.orangemall.cms.service.ICmsSubjectCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专题评论表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsSubjectCommentServiceImpl extends ServiceImpl<CmsSubjectCommentMapper, CmsSubjectComment> implements ICmsSubjectCommentService {

}
