package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsOrderReturnReason;
import link.chengguo.orangemall.oms.mapper.OmsOrderReturnReasonMapper;
import link.chengguo.orangemall.oms.service.IOmsOrderReturnReasonService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货原因表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsOrderReturnReasonServiceImpl extends ServiceImpl<OmsOrderReturnReasonMapper, OmsOrderReturnReason> implements IOmsOrderReturnReasonService {

}
