package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfo;
import link.chengguo.orangemall.cms.mapper.CmsPlatformInfoMapper;
import link.chengguo.orangemall.cms.service.ICmsPlatformInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsPlatformInfoServiceImpl extends ServiceImpl
<CmsPlatformInfoMapper, CmsPlatformInfo> implements ICmsPlatformInfoService {

@Resource
private  CmsPlatformInfoMapper cmsPlatformInfoMapper;


}
