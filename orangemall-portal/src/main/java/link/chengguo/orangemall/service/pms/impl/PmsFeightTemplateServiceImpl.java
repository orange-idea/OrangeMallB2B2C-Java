package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsFeightTemplate;
import link.chengguo.orangemall.pms.mapper.PmsFeightTemplateMapper;
import link.chengguo.orangemall.pms.service.IPmsFeightTemplateService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运费模版 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsFeightTemplateServiceImpl extends ServiceImpl<PmsFeightTemplateMapper, PmsFeightTemplate> implements IPmsFeightTemplateService {

}
