package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ams.entity.AmsAgent;
import link.chengguo.orangemall.ams.mapper.AmsAgentMapper;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import link.chengguo.orangemall.shms.entity.ShmsShopBrokerageLevel;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillLogMapper;
import link.chengguo.orangemall.shms.mapper.ShmsShopBrokerageLevelMapper;
import link.chengguo.orangemall.shms.service.IShmsShopBillLogService;
import link.chengguo.orangemall.shms.service.IShmsShopInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillLogServiceImpl extends ServiceImpl
        <ShmsShopBillLogMapper, ShmsShopBillLog> implements IShmsShopBillLogService {

    @Resource
    private ShmsShopBillLogMapper shmsShopBillLogMapper;
    @Resource
    private IShmsShopInfoService shopInfoService;
    @Resource
    private ShmsShopBrokerageLevelMapper shopBrokerageLevelMapper;
    @Resource
    private AmsAgentMapper agentMapper;

    /**
     * 添加店铺账单记录
     * @param order
     * @param billOrderType 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单退款退货，5-订单退款退货通过,6-订单退款拒绝
     */
    @Override
    public boolean addShopBillLog(OmsOrder order,Integer billOrderType) {
        ShmsShopInfo shopInfo=shopInfoService.getById(order.getShopId());
        //分账处理
        ShmsShopBillLog shopBillLog=new ShmsShopBillLog();
        shopBillLog.setCreateTime(new Date());
        shopBillLog.setBillOrderType(billOrderType);
        shopBillLog.setShopId(order.getShopId());
        shopBillLog.setOrderId(order.getId());
        shopBillLog.setCityCode(order.getCityCode());
        shopBillLog.setOrderSn(order.getOrderSn());
        shopBillLog.setOrderAmount(order.getTotalAmount());
        //店铺抽佣等级/代理抽佣
        ShmsShopBrokerageLevel shopBrokerageLevel =shopBrokerageLevelMapper.selectById(shopInfo.getCommisionLevel());
        //代理抽佣等级/平台抽佣
        AmsAgent agentParm=new AmsAgent();
        agentParm.setCityCode(shopInfo.getCityCode());
        AmsAgent agent=agentMapper.selectOne(new QueryWrapper<>(agentParm));
        //抽佣金额 = 订单总金额X(抽佣比例除以100)
        BigDecimal totalPercent=order.getTotalAmount().multiply(shopBrokerageLevel.getPercent().divide(new BigDecimal(100)));
        //平台金额 =
        BigDecimal plat=order.getTotalAmount().multiply(agent.getBrokeragePercent()).divide(new BigDecimal(100));
        //代理金额
        BigDecimal ag=totalPercent.subtract(plat);
        shopBillLog.setBrokerageAmount(totalPercent);//抽佣金额
        shopBillLog.setBrokeragePercent(shopBrokerageLevel.getPercent());//抽佣比例
        shopBillLog.setPlatAmount(plat);
        shopBillLog.setAgentAmount(ag);
        //商家实收金额：支付金额-抽佣金额-配送费
        BigDecimal shopIncome=order.getPayAmount().subtract(totalPercent).subtract(order.getFreightAmount());
        shopBillLog.setShopIncome(shopIncome);
        //积分抵扣金额
        shopBillLog.setIntegralMoney(order.getIntegrationAmount());
        //优惠券抵扣
        shopBillLog.setCouponMoney(order.getCouponAmount());
        //促销减少金额
        shopBillLog.setDiscountMoney(order.getPromotionAmount());
        shopBillLog.setMoneyPaid(order.getPayAmount());
        //商品金额：订单金额-配送费用-包装费用
//      shopBillLog.setGoodsAmount(0);//商品价格
        shopBillLog.setExpressFee(order.getFreightAmount());
       return  shmsShopBillLogMapper.insert(shopBillLog) > 0 ? true:false;
    }

    @Override
    public boolean updateShopBillLog(ShmsShopBillLog shopBillLog) {
       return  shmsShopBillLogMapper.updateById(shopBillLog) > 0 ? true:false;
    }

    @Override
    public ShmsShopBillLog getShopBillLogByOrderId(Long orderId) {
        ShmsShopBillLog billLog=new ShmsShopBillLog();
        billLog.setOrderId(orderId);
        return shmsShopBillLogMapper.selectOne(new QueryWrapper<>(billLog));
    }

    @Override
    public boolean updateShopBillByOrderId(ShmsShopBillLog shopBillLog) {
        ShmsShopBillLog billLog=new ShmsShopBillLog();
        billLog.setOrderId(shopBillLog.getOrderId());
        return shmsShopBillLogMapper.update(shopBillLog,new QueryWrapper<>(billLog))>0?true:false;
    }
}
