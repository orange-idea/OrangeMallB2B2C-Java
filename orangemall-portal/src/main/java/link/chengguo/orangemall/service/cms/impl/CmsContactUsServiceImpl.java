package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsContactUs;
import link.chengguo.orangemall.cms.mapper.CmsContactUsMapper;
import link.chengguo.orangemall.cms.service.ICmsContactUsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsContactUsServiceImpl extends ServiceImpl
<CmsContactUsMapper, CmsContactUs> implements ICmsContactUsService {

@Resource
private  CmsContactUsMapper cmsContactUsMapper;


}
