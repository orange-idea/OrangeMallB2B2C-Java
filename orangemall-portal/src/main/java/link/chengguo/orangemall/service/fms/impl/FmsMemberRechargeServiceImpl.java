package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.PayStatus;
import link.chengguo.orangemall.fms.entity.FmsMemberRecharge;
import link.chengguo.orangemall.fms.mapper.FmsMemberRechargeMapper;
import link.chengguo.orangemall.fms.service.IFmsMemberRechargeService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-10
 */
@Service
public class FmsMemberRechargeServiceImpl extends ServiceImpl
        <FmsMemberRechargeMapper, FmsMemberRecharge> implements IFmsMemberRechargeService {

    @Resource
    private FmsMemberRechargeMapper fmsMemberRechargeMapper;
    @Resource
    private IUmsMemberService memberService;


    @Override
    public void add(Long memberId,String orderSn,String money,String content,Integer payType) {
        //添加充值记录
        FmsMemberRecharge recharge=new FmsMemberRecharge();
        recharge.setMemberId(memberId);
        UmsMember user=memberService.getById(memberId);
        recharge.setMemberName(user.getNickname());
        recharge.setMemberPhone(user.getPhone());
        recharge.setCreateTime(new Date());
        recharge.setOrderSn(orderSn);
        recharge.setMoney(new BigDecimal(money));
        //0->待付款；1->付款中；2->已付款
        recharge.setRechargeStatus(PayStatus.WaitPay.getValue());
        //0->未支付；1->微信小程序；2->微信App,  3-支付宝，4-余额支付，5-积分，6-银联，7-划拨
        recharge.setRechargeType(payType);
        recharge.setContent(content);
        fmsMemberRechargeMapper.insert(recharge);
    }

    @Transactional
    @Override
    public boolean updateForSuccess(FmsMemberRecharge recharge) {
        recharge.setRechargeStatus(PayStatus.Paid.getValue());
        recharge.setUpdateTime(new Date());
        fmsMemberRechargeMapper.updateById(recharge);
        //添加充值资金
        //资金变动类型： 0-'全部', 1-'消费', 2-'退款', 3-'充值', 4-'提现', 5-'佣金', 6-'平台调整'
        memberService.addBlance(recharge.getMemberId(),recharge.getMoney(),3,recharge.getContent());
        return true;
    }

}
