package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsOrderReturnApply;
import link.chengguo.orangemall.oms.mapper.OmsOrderReturnApplyMapper;
import link.chengguo.orangemall.oms.service.IOmsOrderReturnApplyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单退货申请 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsOrderReturnApplyServiceImpl extends ServiceImpl<OmsOrderReturnApplyMapper, OmsOrderReturnApply> implements IOmsOrderReturnApplyService {

}
