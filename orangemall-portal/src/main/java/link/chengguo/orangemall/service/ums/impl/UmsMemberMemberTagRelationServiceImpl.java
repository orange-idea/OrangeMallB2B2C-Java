package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberMemberTagRelation;
import link.chengguo.orangemall.ums.mapper.UmsMemberMemberTagRelationMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberMemberTagRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和标签关系表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberMemberTagRelationServiceImpl extends ServiceImpl<UmsMemberMemberTagRelationMapper, UmsMemberMemberTagRelation> implements IUmsMemberMemberTagRelationService {

}
