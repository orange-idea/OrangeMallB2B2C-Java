package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsPickingComment;
import link.chengguo.orangemall.jms.mapper.JmsPickingCommentMapper;
import link.chengguo.orangemall.jms.service.IJmsPickingCommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingCommentServiceImpl extends ServiceImpl
<JmsPickingCommentMapper, JmsPickingComment> implements IJmsPickingCommentService {

@Resource
private  JmsPickingCommentMapper jmsPickingCommentMapper;


}
