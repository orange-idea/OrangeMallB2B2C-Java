package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsRewardLog;
import link.chengguo.orangemall.ums.mapper.UmsRewardLogMapper;
import link.chengguo.orangemall.ums.service.IUmsRewardLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-02
 */
@Service
public class UmsRewardLogServiceImpl extends ServiceImpl<UmsRewardLogMapper, UmsRewardLog> implements IUmsRewardLogService {

}
