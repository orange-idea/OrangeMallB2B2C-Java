package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillDayMapper;
import link.chengguo.orangemall.shms.service.IShmsShopBillDayService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillDayServiceImpl extends ServiceImpl
        <ShmsShopBillDayMapper, ShmsShopBillDay> implements IShmsShopBillDayService {

    @Resource
    private ShmsShopBillDayMapper shmsShopBillDayMapper;


}
