package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsPickinigAccountLog;
import link.chengguo.orangemall.jms.mapper.JmsPickinigAccountLogMapper;
import link.chengguo.orangemall.jms.service.IJmsPickinigAccountLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickinigAccountLogServiceImpl extends ServiceImpl
<JmsPickinigAccountLogMapper, JmsPickinigAccountLog> implements IJmsPickinigAccountLogService {

@Resource
private  JmsPickinigAccountLogMapper jmsPickinigAccountLogMapper;


}
