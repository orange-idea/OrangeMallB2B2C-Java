package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.JmsPickingOrder;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderCombine;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderCombineMapper;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderCombineService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderService;
import link.chengguo.orangemall.util.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsPickingOrderCombineServiceImpl extends ServiceImpl
        <JmsPickingOrderCombineMapper, JmsPickingOrderCombine> implements IJmsPickingOrderCombineService {

    @Resource
    private JmsPickingOrderCombineMapper jmsPickingOrderCombineMapper;
    @Resource
    private IJmsPickingOrderService pickingOrderService;

    @Transactional
    @Override
    public boolean startPicking(String ids) {
        List<JmsPickingOrder> list = pickingOrderService.getPickingOrdersByIds(ids);
        if (list.size()==0){
            throw new RuntimeException("订单已开始或不存在");
        }
        int goodsNumber=0;
        StringBuilder typeIds=new StringBuilder();
        StringBuilder typeNames=new StringBuilder();
        StringBuilder orderIds=new StringBuilder();
        LinkedHashSet<Long> typeIdList=new LinkedHashSet<>();
        JmsPickingOrder first=null;
        for (JmsPickingOrder order:list){
            first=order;
            goodsNumber+=order.getGoodsNumber();
            typeIds.append(","+order.getGoodsTypeIds());
            typeNames.append(","+order.getGoodsTypeNames());
            orderIds.append(","+order.getId());
        }

        JmsPickingOrderCombine combine = new JmsPickingOrderCombine();
        combine.setGoodsNumber(goodsNumber);
        combine.setUserId(first.getUserId());
        combine.setCityCode(first.getCityCode());
        combine.setShopId(first.getShopId());
        combine.setGoodsTypeIds(StringUtils.clearDuplicate(typeIds.delete(0,1).toString()));
        combine.setGoodsTypeNames(StringUtils.clearDuplicate(typeNames.delete(0,1).toString()));
        combine.setPickingOrderIds(orderIds.delete(0,1).toString());
        combine.setStatus(PickingStatus.Picking.getValue());
        addPickingOrderCombin(combine);
        //更新订单消息
        JmsPickingOrder order=new JmsPickingOrder();
        order.setCombineId(combine.getId());
        order.setStatus(PickingStatus.Picking.getValue());
        pickingOrderService.updatePickingOrdersByIds(order,ids);
        return true;
    }

    @Override
    public boolean addPickingOrderCombin(JmsPickingOrderCombine pickingOrderCombine) {
        pickingOrderCombine.setCreateTime(new Date());
        return jmsPickingOrderCombineMapper.insert(pickingOrderCombine) > 0 ? true : false;
    }

    @Override
    public IPage<JmsPickingOrderCombine> getPickingOrderList(Integer pageNum, Integer pageSize, JmsPickingUser user) {
        JmsPickingOrderCombine order=new JmsPickingOrderCombine();
        order.setUserId(user.getId());
        order.setStatus(PickingStatus.Picking.getValue());
        return jmsPickingOrderCombineMapper.selectPage(new Page<>(pageNum,pageSize),new QueryWrapper<>(order));
    }

    @Override
    public JmsPickingOrderCombine getCombineDetail(Long id) {
        return jmsPickingOrderCombineMapper.selectById(id);
    }

    @Override
    public boolean updateInfoForPicking(Long combineId, int number) {
        JmsPickingOrderCombine combine=jmsPickingOrderCombineMapper.selectById(combineId);
        combine.setFinishGoodsNumber(number+combine.getFinishGoodsNumber());
        if (combine.getFinishGoodsNumber().equals(combine.getGoodsNumber())){
            //拣货完成
            combine.setStatus(PickingStatus.Picked.getValue());
            combine.setFinishTime(new Date());
        }
        jmsPickingOrderCombineMapper.updateById(combine);
        return true;
    }
}
