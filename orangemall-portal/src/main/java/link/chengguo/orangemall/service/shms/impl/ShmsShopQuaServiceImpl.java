package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.entity.ShmsShopQua;
import link.chengguo.orangemall.shms.mapper.ShmsShopQuaMapper;
import link.chengguo.orangemall.shms.service.IShmsShopQuaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopQuaServiceImpl extends ServiceImpl
        <ShmsShopQuaMapper, ShmsShopQua> implements IShmsShopQuaService {

    @Resource
    private ShmsShopQuaMapper shmsShopQuaMapper;


}
