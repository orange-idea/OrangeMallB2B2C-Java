package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.JmsPickingOrder;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderCombine;
import link.chengguo.orangemall.jms.entity.JmsPickingOrderGoods;
import link.chengguo.orangemall.jms.mapper.JmsPickingOrderGoodsMapper;
import link.chengguo.orangemall.jms.service.IJmsOrderTrackService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderCombineService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderGoodsService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderService;
import link.chengguo.orangemall.utils.ValidatorUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsPickingOrderGoodsServiceImpl extends ServiceImpl
        <JmsPickingOrderGoodsMapper, JmsPickingOrderGoods> implements IJmsPickingOrderGoodsService {

    @Resource
    private JmsPickingOrderGoodsMapper jmsPickingOrderGoodsMapper;
    @Resource
    private IJmsPickingOrderCombineService jmsPickingOrderCombineService;
    @Resource
    private IJmsPickingOrderService jmsPickingOrderService;
    @Resource
    private IJmsOrderTrackService jmsOrderTrackService;

    @Transactional
    @Override
    public boolean finishGoodsPick(String pickingOrderIds,Integer number) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.in("picking_order_id",pickingOrderIds.split(","));
        List<JmsPickingOrderGoods> list=jmsPickingOrderGoodsMapper.selectList(queryWrapper);
        int totalNumber=0;
        for (JmsPickingOrderGoods goods:list){
            totalNumber+=goods.getGoodsNumber();
            if (goods.getStatus()== PickingStatus.Picked.getValue()){
                throw new RuntimeException("存在已拣过的商品");
            }
        }
        if (totalNumber!=number){
            throw new RuntimeException("拣货数量不对");
        }
        //更新商品状态
        JmsPickingOrderGoods goods=new JmsPickingOrderGoods();
        goods.setStatus(PickingStatus.Picked.getValue());
        goods.setFinishTime(new Date());
        jmsPickingOrderGoodsMapper.update(goods,queryWrapper);
        for (JmsPickingOrderGoods gds:list){
            //更新订单状态
            jmsPickingOrderService.updateStatusForPicked(gds.getPickingOrderId(),gds);
            //更新合并订单状态
            JmsPickingOrder order=jmsPickingOrderService.getById(gds.getPickingOrderId());
            jmsPickingOrderCombineService.updateInfoForPicking(order.getCombineId(),gds.getGoodsNumber());
            //更新跟踪订单状态
            jmsOrderTrackService.updateOrderTrackForPicking(gds.getOrderId(),gds.getGoodsNumber(),gds.getPickingOrderId());
        }
        return true;
    }

    @Transactional
    @Override
    public boolean setPickingOrderIdForGoods(Long pickingOrderId, Long orderId) {
        JmsPickingOrderGoods goods=new JmsPickingOrderGoods();
        goods.setPickingOrderId(pickingOrderId);
        goods.setUpdateTime(new Date());
        return jmsPickingOrderGoodsMapper.update(goods,new QueryWrapper<JmsPickingOrderGoods>().eq("order_id",orderId))>0?true:false;
    }

    @Override
    public List<JmsPickingOrderGoods> getPickingGoods(Long combineId, Long goodsTypeId, Long pickingOrderId) {
        //查询合并拣货订单信息
        JmsPickingOrderCombine combine=jmsPickingOrderCombineService.getById(combineId);
        //查询商品信息
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.in("picking_order_id",combine.getPickingOrderIds().split(","));
        if (ValidatorUtils.notEmpty(pickingOrderId) && pickingOrderId !=0){
            queryWrapper.eq("picking_order_id",pickingOrderId);
        }
        if (ValidatorUtils.notEmpty(goodsTypeId)){
            queryWrapper.eq("goods_type_id",goodsTypeId);
        }
        //合并重复商品
        List<JmsPickingOrderGoods> list=jmsPickingOrderGoodsMapper.selectList(queryWrapper);
        List<JmsPickingOrderGoods> newList=new ArrayList<>();
        int size=0;
        for (int i=0;i<list.size();i++){
            if (newList.size()==0){
                list.get(i).setPickingOrderIds(list.get(i).getPickingOrderId().toString());
                newList.add(list.get(i));
            }else{
                size=newList.size();
                for (int j=0;j<size;j++){
                    //条码相同的商品存在，则合并数量和ID
                    if (newList.get(j).getGoodsId().equals(list.get(i).getGoodsId())){
                        newList.get(j).setGoodsNumber(newList.get(j).getGoodsNumber()+list.get(i).getGoodsNumber());
                        newList.get(j).setPickingOrderIds(newList.get(j).getPickingOrderIds()+","+list.get(i).getPickingOrderId());
                    }else{
                        list.get(i).setPickingOrderIds(list.get(i).getPickingOrderId().toString());
                        newList.add(list.get(i));
                    }
                }
            }
        }
        return newList;
    }

}
