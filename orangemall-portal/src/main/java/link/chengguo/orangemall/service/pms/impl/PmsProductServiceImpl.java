package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.service.ICmsPrefrenceAreaProductRelationService;
import link.chengguo.orangemall.cms.service.ICmsSubjectProductRelationService;
import link.chengguo.orangemall.pms.entity.*;
import link.chengguo.orangemall.pms.mapper.*;
import link.chengguo.orangemall.pms.service.*;
import link.chengguo.orangemall.pms.vo.GoodsDetailResult;
import link.chengguo.orangemall.pms.vo.PmsProductAndGroup;
import link.chengguo.orangemall.pms.vo.PmsProductParam;
import link.chengguo.orangemall.sms.entity.*;
import link.chengguo.orangemall.sms.mapper.SmsGroupMapper;
import link.chengguo.orangemall.sms.mapper.SmsGroupMemberMapper;
import link.chengguo.orangemall.sms.mapper.SmsPaimaiLogMapper;
import link.chengguo.orangemall.sms.service.ISmsHomeBrandService;
import link.chengguo.orangemall.sms.service.ISmsHomeNewProductService;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendProductService;
import link.chengguo.orangemall.sys.mapper.SysStoreMapper;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.ums.service.RedisService;
import link.chengguo.orangemall.service.ums.impl.RedisUtil;
import link.chengguo.orangemall.util.DateUtils;
import link.chengguo.orangemall.util.JsonUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import link.chengguo.orangemall.vo.Rediskey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品信息 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Slf4j
@Service
public class PmsProductServiceImpl extends ServiceImpl<PmsProductMapper, PmsProduct> implements IPmsProductService {

    @Resource
    private IPmsBrandService brandService;
    @Resource
    private ISmsHomeBrandService homeBrandService;
    @Resource
    private ISmsHomeNewProductService homeNewProductService;
    @Resource
    private ISmsHomeRecommendProductService homeRecommendProductService;
    @Resource
    private PmsProductMapper productMapper;
    @Resource
    private IPmsMemberPriceService memberPriceDao;
    @Resource
    private PmsMemberPriceMapper memberPriceMapper;
    @Resource
    private IPmsProductLadderService productLadderDao;
    @Resource
    private PmsProductLadderMapper productLadderMapper;
    @Resource
    private IPmsProductFullReductionService productFullReductionDao;
    @Resource
    private PmsProductFullReductionMapper productFullReductionMapper;
    @Resource
    private IPmsSkuStockService skuStockDao;
    @Resource
    private PmsSkuStockMapper skuStockMapper;
    @Resource
    private IPmsProductAttributeValueService productAttributeValueDao;
    @Resource
    private PmsProductAttributeValueMapper productAttributeValueMapper;
    @Resource
    private ICmsSubjectProductRelationService subjectProductRelationDao;
    @Resource
    private CmsSubjectProductRelationMapper subjectProductRelationMapper;
    @Resource
    private ICmsPrefrenceAreaProductRelationService prefrenceAreaProductRelationDao;
    @Resource
    private CmsPrefrenceAreaProductRelationMapper prefrenceAreaProductRelationMapper;

    @Resource
    private PmsProductVertifyRecordMapper productVertifyRecordDao;

    @Resource
    private PmsProductVertifyRecordMapper productVertifyRecordMapper;

    @Resource
    private SmsGroupMapper groupMapper;
    @Resource
    private SmsGroupMemberMapper groupMemberMapper;
    @Resource
    private RedisService redisService;
    @Resource
    private SysStoreMapper storeMapper;
    @Resource
    private RedisUtil redisUtil;
    @Autowired
    private IPmsFavoriteService favoriteService;

    @Resource
    private SmsPaimaiLogMapper paimaiLogMapper;
    @Autowired
    private IUmsMemberService memberService;

    @Override
    public PmsProductAndGroup getProductAndGroup(Long id) {
        PmsProduct goods = productMapper.selectById(id);
        PmsProductAndGroup vo = new PmsProductAndGroup();
        try {
            BeanUtils.copyProperties(goods, vo);
            SmsGroup queryG = new SmsGroup();
            queryG.setGoodsId(id);
            SmsGroup group = groupMapper.selectOne(new QueryWrapper<>(queryG));
            SmsGroupMember newG = new SmsGroupMember();
            newG.setGoodsId(id);
            List<SmsGroupMember> list = groupMemberMapper.selectList(new QueryWrapper<>(newG));
            if (group != null) {
                Map<String, List<SmsGroupMember>> map = groupMemberByMainId(list, group);
                vo.setMap(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return vo;
    }

    /**
     * 按照异常批次号对已开单数据进行分组
     *
     * @param billingList
     * @return
     * @throws Exception
     */
    private Map<String, List<SmsGroupMember>> groupMemberByMainId(List<SmsGroupMember> billingList, SmsGroup group) throws Exception {
        Map<String, List<SmsGroupMember>> resultMap = new HashMap<String, List<SmsGroupMember>>();
        Map<String, List<SmsGroupMember>> map = new HashMap<String, List<SmsGroupMember>>();
        try {
            List<Long> ids = new ArrayList<>();
            for (SmsGroupMember tmExcpNew : billingList) {
                if (tmExcpNew.getMemberId().equals(tmExcpNew.getMainId())) {
                    Date cr = tmExcpNew.getCreateTime();
                    Long nowT = System.currentTimeMillis();
                    Date endTime = DateUtils.convertStringToDate(DateUtils.addHours(cr, group.getHours()), "yyyy-MM-dd HH:mm:ss");
                    if (nowT <= endTime.getTime()) {
                        ids.add(tmExcpNew.getMainId());
                    }
                }
                if (resultMap.containsKey(tmExcpNew.getMainId() + "")) {//map中异常批次已存在，将该数据存放到同一个key（key存放的是异常批次）的map中
                    resultMap.get(tmExcpNew.getMainId() + "").add(tmExcpNew);
                } else {//map中不存在，新建key，用来存放数据
                    List<SmsGroupMember> tmpList = new ArrayList<SmsGroupMember>();
                    tmpList.add(tmExcpNew);
                    resultMap.put(tmExcpNew.getMainId() + "", tmpList);
                }
            }
            for (Long id : ids) {
                if (resultMap.get(id + "") != null) {
                    map.put(id + "", resultMap.get(id + ""));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("按照异常批次号对已开单数据进行分组时出现异常", e);
        }

        return map;
    }

    @Override
    public PmsProduct getUpdateInfo(Long id) {
        return productMapper.selectById(id);
    }

    @Override
    public Object initGoodsRedis() {
        List<PmsProduct> list = productMapper.selectList(new QueryWrapper<>());
        for (PmsProduct goods : list) {
            PmsProductParam param = new PmsProductParam();
            param.setGoods(goods);

            List<PmsProductLadder> productLadderList = productLadderMapper.selectList(new QueryWrapper<PmsProductLadder>().eq("product_id", goods.getId()));

            List<PmsProductFullReduction> productFullReductionList = productFullReductionMapper.selectList(new QueryWrapper<PmsProductFullReduction>().eq("product_id", goods.getId()));

            List<PmsMemberPrice> memberPriceList = memberPriceMapper.selectList(new QueryWrapper<PmsMemberPrice>().eq("product_id", goods.getId()));

            List<PmsSkuStock> skuStockList = skuStockMapper.selectList(new QueryWrapper<PmsSkuStock>().eq("product_id", goods.getId()));

            List<PmsProductAttributeValue> productAttributeValueList = productAttributeValueMapper.selectList(new QueryWrapper<PmsProductAttributeValue>().eq("product_id", goods.getId()));

            List<CmsSubjectProductRelation> subjectProductRelationList = subjectProductRelationMapper.selectList(new QueryWrapper<CmsSubjectProductRelation>().eq("product_id", goods.getId()));

            List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList = prefrenceAreaProductRelationMapper.selectList(new QueryWrapper<CmsPrefrenceAreaProductRelation>().eq("product_id", goods.getId()));

            param.setMemberPriceList(memberPriceList);
            param.setPrefrenceAreaProductRelationList(prefrenceAreaProductRelationList);
            param.setProductAttributeValueList(productAttributeValueList);
            param.setProductFullReductionList(productFullReductionList);
            param.setProductLadderList(productLadderList);
            param.setSkuStockList(skuStockList);
            param.setSubjectProductRelationList(subjectProductRelationList);
            redisService.set(String.format(Rediskey.GOODSDETAIL, goods.getId()), JsonUtils.objectToJson(param));
        }
        return 1;
    }

    @Override
    public GoodsDetailResult getGoodsRedisById(Long id) {
        PmsProduct goods = productMapper.selectById(id);

        GoodsDetailResult param = new GoodsDetailResult();
        param.setGoods(goods);

      /*  List<PmsProductLadder> productLadderList = productLadderMapper.selectList(new QueryWrapper<PmsProductLadder>().eq("product_id", goods.getId()));

        List<PmsProductFullReduction> productFullReductionList = productFullReductionMapper.selectList(new QueryWrapper<PmsProductFullReduction>().eq("product_id", goods.getId()));

        List<PmsMemberPrice> memberPriceList = memberPriceMapper.selectList(new QueryWrapper<PmsMemberPrice>().eq("product_id", goods.getId()));
  param.setMemberPriceList(memberPriceList);
        param.setProductFullReductionList(productFullReductionList);
        param.setProductLadderList(productLadderList);
*/
        List<PmsSkuStock> skuStockList = skuStockMapper.selectList(new QueryWrapper<PmsSkuStock>().eq("product_id", goods.getId()));

        List<PmsProductAttributeValue> productAttributeValueList = productAttributeValueMapper.selectList(new QueryWrapper<PmsProductAttributeValue>().eq("product_id", goods.getId()).eq("type", 1));

        List<CmsSubjectProductRelation> subjectProductRelationList = subjectProductRelationMapper.selectList(new QueryWrapper<CmsSubjectProductRelation>().eq("product_id", goods.getId()));

        List<CmsPrefrenceAreaProductRelation> prefrenceAreaProductRelationList = prefrenceAreaProductRelationMapper.selectList(new QueryWrapper<CmsPrefrenceAreaProductRelation>().eq("product_id", goods.getId()));

        List<PmsProductAttributeValue> productCanShuValueList = productAttributeValueMapper.selectList(new QueryWrapper<PmsProductAttributeValue>().eq("product_id", goods.getId()).eq("type", 2));
        param.setProductCanShuValueList(productCanShuValueList);

        param.setPrefrenceAreaProductRelationList(prefrenceAreaProductRelationList);
        param.setProductAttributeValueList(productAttributeValueList);

        param.setSkuStockList(skuStockList);
        param.setSubjectProductRelationList(subjectProductRelationList);

        List<PmsProduct> typeGoodsList = productMapper.selectList(new QueryWrapper<PmsProduct>().eq("product_attribute_category_id", goods.getProductAttributeCategoryId()));
        param.setTypeGoodsList(typeGoodsList.subList(0, typeGoodsList.size() > 8 ? 8 : typeGoodsList.size()));
//        redisService.set(String.format(Rediskey.GOODSDETAIL, goods.getId()), JsonUtils.objectToJson(param));
        return param;
    }

    /**
     * 根据商品条形码获取商品信息，扫码购使用
     * @param barcode
     * @return
     */
    @Override
    public GoodsDetailResult getGoodsRedisByCode(String cityCode,Long shopId,String barcode) {
        GoodsDetailResult param = new GoodsDetailResult();
        //先查出单规格商品
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("barcode",barcode);
        wrapper.eq("shop_id",shopId);
        wrapper.eq("city_code",cityCode);
        PmsProduct goods = productMapper.selectOne(wrapper);
        // 商品存在且为单规格
        if (goods!=null && goods.getMultiSpec()==0){
            param.setGoods(goods);
            return param;
        }
        //单规格不存在，查询多规格商品
        PmsSkuStock skuStock=skuStockMapper.selectOne(wrapper);
        if (skuStock!=null){
            //查询商品
            PmsProduct goods2 = productMapper.selectById(skuStock.getProductId());
            if (goods2!=null){
                param.setGoods(goods2);
                param.setSkuStock(skuStock);
                return param;
            }
        }
        return null;
    }

    @Override
    public GoodsDetailResult getGoodsSku(Long id) {
        GoodsDetailResult param = new GoodsDetailResult();
        List<PmsSkuStock> skuStockList = skuStockMapper.selectList(new QueryWrapper<PmsSkuStock>().eq("product_id", id));
        List<PmsProductAttributeValue> productAttributeValueList = productAttributeValueMapper.selectList(new QueryWrapper<PmsProductAttributeValue>().eq("product_id", id).eq("type", 1));
        param.setProductAttributeValueList(productAttributeValueList);
        param.setSkuStockList(skuStockList);
        return param;
    }

    @Override
    public boolean ifGoodsCollected(Long goodsId, Long userId) {
        PmsFavorite query = new PmsFavorite();
        query.setObjId(goodsId);
        query.setMemberId(userId);
        query.setType(1);
        PmsFavorite findCollection = favoriteService.getOne(new QueryWrapper<>(query));
        if (findCollection != null) {
           return true;
        } else {
            return false;
        }
    }

    @Override
    public List<PmsBrand> getRecommendBrandList(String cityCode,int pageNum, int pageSize) {

        List<SmsHomeBrand> brands = homeBrandService.list(new QueryWrapper<>());
        if (brands != null && brands.size() > 0) {
            List<Long> ids = brands.stream()
                    .map(SmsHomeBrand::getBrandId)
                    .collect(Collectors.toList());
            if (ids != null && ids.size() > 0) {
                return (List<PmsBrand>) brandService.listByIds(ids);
            }
        }
        return new ArrayList<>();

    }

    @Override
    public List<PmsProduct> getNewProductList(String cityCode,boolean isAsc,String column,int pageNum, int pageSize) {
        QueryWrapper<SmsHomeNewProduct> queryWrapper = new QueryWrapper<>();
        if(cityCode != null){
            queryWrapper.eq("city_code",cityCode);
        }
        List<SmsHomeNewProduct> brands = homeNewProductService.list(queryWrapper.orderBy(true,isAsc,column));
        if (brands != null && brands.size() > 0) {
            List<Long> ids = brands.stream()
                    .map(SmsHomeNewProduct::getProductId)
                    .collect(Collectors.toList());
            if (ids != null && ids.size() > 0) {
                return productMapper.selectList(new QueryWrapper<PmsProduct>().in("id", ids));
            }
        }
        return new ArrayList<>();
    }

    @Override
    public List<PmsProduct> getHotProductList(String cityCode,int pageNum, int pageSize) {
        List<SmsHomeRecommendProduct> brands = homeRecommendProductService.list(new QueryWrapper<SmsHomeRecommendProduct>().eq("city_code",cityCode));
        if (brands != null && brands.size() > 0) {
            List<Long> ids = brands.stream()
                    .map(SmsHomeRecommendProduct::getProductId)
                    .collect(Collectors.toList());
            if (ids != null && ids.size() > 0) {
                return productMapper.selectList(new QueryWrapper<PmsProduct>().in("id", ids));
            }
        }
        return new ArrayList<>();
    }

    @Override
    public Integer countGoodsByToday(Long id) {
        return productMapper.countGoodsByToday(id);
    }

    @Override
    public Map<String, Object> queryPaiMaigoodsDetail(Long id) {
        Map<String, Object> map = new HashMap<>();
        PmsProduct goods = productMapper.selectById(id);
        List<SmsPaimaiLog> paimaiLogList = paimaiLogMapper.selectList(new QueryWrapper<SmsPaimaiLog>().eq("goods_id", id).orderByDesc("create_time"));
        map.put("paimaiLogList", paimaiLogList);
        UmsMember umsMember =null;
        try{
            umsMember = memberService.getNewCurrentMember();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        map.put("favorite", false);
        if (umsMember != null && umsMember.getId() != null) {
            PmsFavorite query = new PmsFavorite();
            query.setObjId(goods.getId());
            query.setMemberId(umsMember.getId());
            query.setType(1);
            PmsFavorite findCollection = favoriteService.getOne(new QueryWrapper<>(query));
            if (findCollection != null) {
                map.put("favorite", true);
            }
        }
        //记录浏览量到redis,然后定时更新到数据库
        String key = Rediskey.GOODS_VIEWCOUNT_CODE + id;
        //找到redis中该篇文章的点赞数，如果不存在则向redis中添加一条
        Map<Object, Object> viewCountItem = redisUtil.hGetAll(Rediskey.GOODS_VIEWCOUNT_KEY);
        Integer viewCount = 0;
        if (!viewCountItem.isEmpty()) {
            if (viewCountItem.containsKey(key)) {
                viewCount = Integer.parseInt(viewCountItem.get(key).toString()) + 1;
                redisUtil.hPut(Rediskey.GOODS_VIEWCOUNT_KEY, key, viewCount + "");
            } else {
                redisUtil.hPut(Rediskey.GOODS_VIEWCOUNT_KEY, key, 1 + "");
            }
        } else {
            redisUtil.hPut(Rediskey.GOODS_VIEWCOUNT_KEY, key, 1 + "");
        }
        goods.setTimeSecound(ValidatorUtils.getTimeSecound(goods.getExpireTime()));
        map.put("goods", goods);
        return map;
    }

    @Transactional
    @Override
    public Object updatePaiMai(PmsProduct goods) {
        goods.setExpireTime(DateUtils.strToDate(DateUtils.addMins(goods.getExpireTime(), 5)));
        productMapper.updateById(goods);
        SmsPaimaiLog log = new SmsPaimaiLog();
        log.setCreateTime(new Date());
        log.setGoodsId(goods.getId());
        log.setMemberId(memberService.getNewCurrentMember().getId());
        log.setPrice(goods.getOriginalPrice());
        log.setPic(memberService.getNewCurrentMember().getIcon());
        paimaiLogMapper.insert(log);
        return new CommonResult().success();
    }

}
