package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillReship;
import link.chengguo.orangemall.bill.mapper.BillReshipMapper;
import link.chengguo.orangemall.bill.service.IBillReshipService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货单表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillReshipServiceImpl extends ServiceImpl<BillReshipMapper, BillReship> implements IBillReshipService {

}
