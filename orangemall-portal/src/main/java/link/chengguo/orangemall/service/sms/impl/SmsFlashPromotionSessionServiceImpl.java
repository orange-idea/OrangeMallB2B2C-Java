package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotionSession;
import link.chengguo.orangemall.sms.mapper.SmsFlashPromotionSessionMapper;
import link.chengguo.orangemall.sms.service.ISmsFlashPromotionSessionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 限时购场次表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsFlashPromotionSessionServiceImpl extends ServiceImpl<SmsFlashPromotionSessionMapper, SmsFlashPromotionSession> implements ISmsFlashPromotionSessionService {

}
