package link.chengguo.orangemall.service.oms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.oms.entity.OmsCompanyAddress;
import link.chengguo.orangemall.oms.mapper.OmsCompanyAddressMapper;
import link.chengguo.orangemall.oms.service.IOmsCompanyAddressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司收发货地址表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class OmsCompanyAddressServiceImpl extends ServiceImpl<OmsCompanyAddressMapper, OmsCompanyAddress> implements IOmsCompanyAddressService {

}
