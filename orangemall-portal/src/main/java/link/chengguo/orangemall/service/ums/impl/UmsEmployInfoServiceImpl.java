package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsEmployInfo;
import link.chengguo.orangemall.ums.mapper.UmsEmployInfoMapper;
import link.chengguo.orangemall.ums.service.IUmsEmployInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-02
 */
@Service
public class UmsEmployInfoServiceImpl extends ServiceImpl<UmsEmployInfoMapper, UmsEmployInfo> implements IUmsEmployInfoService {

}
