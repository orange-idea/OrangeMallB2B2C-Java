package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsSubjectCategory;
import link.chengguo.orangemall.cms.mapper.CmsSubjectCategoryMapper;
import link.chengguo.orangemall.cms.service.ICmsSubjectCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专题分类表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsSubjectCategoryServiceImpl extends ServiceImpl<CmsSubjectCategoryMapper, CmsSubjectCategory> implements ICmsSubjectCategoryService {

}
