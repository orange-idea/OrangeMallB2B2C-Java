package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillDeliveryItems;
import link.chengguo.orangemall.bill.mapper.BillDeliveryItemsMapper;
import link.chengguo.orangemall.bill.service.IBillDeliveryItemsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发货单详情表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillDeliveryItemsServiceImpl extends ServiceImpl<BillDeliveryItemsMapper, BillDeliveryItems> implements IBillDeliveryItemsService {

}
