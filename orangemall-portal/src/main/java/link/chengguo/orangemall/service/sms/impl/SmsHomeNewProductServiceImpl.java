package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsHomeNewProduct;
import link.chengguo.orangemall.sms.mapper.SmsHomeNewProductMapper;
import link.chengguo.orangemall.sms.service.ISmsHomeNewProductService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 新鲜好物表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsHomeNewProductServiceImpl extends ServiceImpl<SmsHomeNewProductMapper, SmsHomeNewProduct> implements ISmsHomeNewProductService {

}
