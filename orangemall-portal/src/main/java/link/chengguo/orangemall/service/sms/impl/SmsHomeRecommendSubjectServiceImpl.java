package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendSubject;
import link.chengguo.orangemall.sms.mapper.SmsHomeRecommendSubjectMapper;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendSubjectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 首页推荐专题表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsHomeRecommendSubjectServiceImpl extends ServiceImpl<SmsHomeRecommendSubjectMapper, SmsHomeRecommendSubject> implements ISmsHomeRecommendSubjectService {

}
