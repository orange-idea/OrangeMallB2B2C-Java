package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.enums.PickingStatus;
import link.chengguo.orangemall.jms.entity.*;
import link.chengguo.orangemall.jms.mapper.JmsOrderTrackMapper;
import link.chengguo.orangemall.jms.service.IJmsGoodsTypeTrackService;
import link.chengguo.orangemall.jms.service.IJmsOrderTrackService;
import link.chengguo.orangemall.jms.service.IJmsPickingOrderService;
import link.chengguo.orangemall.jms.service.IJmsPickingRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsOrderTrackServiceImpl extends ServiceImpl
        <JmsOrderTrackMapper, JmsOrderTrack> implements IJmsOrderTrackService {

    @Resource
    private JmsOrderTrackMapper jmsOrderTrackMapper;
    @Resource
    private IJmsPickingRoleService roleService;
    @Resource
    private IJmsPickingOrderService pickingOrderService;
    @Resource
    private IJmsGoodsTypeTrackService goodsTypeTrackService;


    @Override
    public JmsOrderTrack getOrderTrackByPickingUser(Long orderTrackId, JmsPickingUser user) {
        //根据用户角色，判定是否存在待抢订单信息
        if (user.getRoleId()==null){
            throw new RuntimeException("该拣货员未配置角色");
        }
        //查询角色包含分类
        JmsPickingRole role=roleService.getById(user.getRoleId());
        String[] types=role.getTypeId().split(",");
        if (types.length==0){
            throw new RuntimeException("该拣货员未配置角色内容为空");
        }
        JmsOrderTrack orderTrack= jmsOrderTrackMapper.selectById(orderTrackId);
        return this.getOrderTrackByRoleId(orderTrack,types, PickingStatus.WaitGrab.getValue());
    }

    @Override
    public JmsOrderTrack getOrderTrackByRoleId(JmsOrderTrack orderTrack, String[] typeIds,Integer status) {
        List<JmsGoodsTypeTrack> goodsTypeTracks=goodsTypeTrackService.getGoodsTypeTrackByOrderId(orderTrack.getOrderId(),status);
        StringBuilder goodsTypeIds=new StringBuilder();
        StringBuilder goodsTypeNames=new StringBuilder();
        StringBuilder itemIds=new StringBuilder();
        int goodsNumber=0;
        for (JmsGoodsTypeTrack goodsTypeTrack:goodsTypeTracks){
            for (String typeId:typeIds){
                if (goodsTypeTrack.getGoodsTypeId().toString().equals(typeId)){
                    goodsTypeIds.append(","+goodsTypeTrack.getGoodsTypeId());
                    goodsTypeNames.append(","+goodsTypeTrack.getGoodsTypeName());
                    goodsNumber+=goodsTypeTrack.getGoodsNumber();
                }
                itemIds.append(","+goodsTypeTrack.getOrderItemIds());
            }
        }
        if (goodsTypeIds.length() > 0){
            goodsTypeNames.delete(0,1);
            goodsTypeIds.delete(0,1);
            itemIds.delete(0,1);
            orderTrack.setOrderItemIds(itemIds.toString());
            orderTrack.setGoodsNumber(goodsNumber);
            orderTrack.setGoodsTypeNumber(goodsTypeIds.toString().split(",").length);
            orderTrack.setGoodsTypeIds(goodsTypeIds.toString());
            orderTrack.setGoodsTypeNames(goodsTypeNames.toString());
            orderTrack.setGoodsTypeTracks(null);
            return orderTrack;
        }else{
            return null;
        }
    }

    /**
     *  查询待抢订单信息
     * @param user
     * @return
     */
    @Override
    public  List<JmsOrderTrack> getGrabList(JmsPickingUser user) {
        //根据用户角色，判定是否存在待抢订单信息
        if (user.getRoleId()==null){
            throw new RuntimeException("该拣货员未配置角色");
        }
        //查询角色包含分类
        JmsPickingRole role=roleService.getById(user.getRoleId());
        String[] types=role.getTypeId().split(",");
        if (types.length==0){
            throw new RuntimeException("该拣货员未配置角色内容为空");
        }
        //查询待抢订单
        List<JmsOrderTrack> orderTrackList=getOrderTrackList(user.getShopId(), PickingStatus.WaitGrab.getValue());
        List<JmsOrderTrack> orderTrackList1=new ArrayList<>();
        for (JmsOrderTrack orderTrack:orderTrackList){
            JmsOrderTrack orderTrack2=this.getOrderTrackByRoleId(orderTrack,types,PickingStatus.WaitGrab.getValue());
            if (orderTrack2!=null){
                orderTrackList1.add(orderTrack2);
            }
        }
        return orderTrackList1;
    }

    /**
     * 临时备份，后续可删除
     * @param user
     * @return
     */
    public  List<JmsOrderTrack> getGrabList2(JmsPickingUser user) {
        //根据用户角色，判定是否存在待抢订单信息
        if (user.getRoleId()==null){
            return null;
        }
        //查询角色包含分类
        JmsPickingRole role=roleService.getById(user.getRoleId());
        String[] types=role.getTypeId().split(",");
        if (types.length==0){
            return null;
        }
        //查询待抢订单
        List<JmsOrderTrack> orderTrackList=getOrderTrackList(user.getShopId(), PickingStatus.WaitGrab.getValue());
        List<JmsOrderTrack> orderTrackList1=new ArrayList<>();
        StringBuilder goodsTypeIds=new StringBuilder();
        StringBuilder goodsTypeNames=new StringBuilder();
        for (JmsOrderTrack orderTrack:orderTrackList){
            goodsTypeIds.setLength(0);
            goodsTypeNames.setLength(0);
            for (JmsGoodsTypeTrack goodsTypeTrack:orderTrack.getGoodsTypeTracks()){
                for (String typeId:types){
                    if (goodsTypeTrack.getGoodsTypeId().toString().equals(typeId)){
                        goodsTypeIds.append(","+goodsTypeTrack.getGoodsTypeId());
                        goodsTypeNames.append(","+goodsTypeTrack.getGoodsTypeName());
                    }
                }
            }
            if (goodsTypeIds.length() > 0){
                goodsTypeNames.delete(0,1);
                goodsTypeIds.delete(0,1);
                orderTrack.setGoodsTypeIds(goodsTypeIds.toString());
                orderTrack.setGoodsTypeNames(goodsTypeNames.toString());
                orderTrack.setGoodsTypeTracks(null);
                orderTrackList1.add(orderTrack);
            }
        }
        return orderTrackList1;
    }

    @Override
    public List<JmsOrderTrack> getOrderTrackList(Long shopId,Integer status) {
        JmsOrderTrack orderTrack=new JmsOrderTrack();
        orderTrack.setShopId(shopId);
        orderTrack.setStatus(status);
        List<JmsOrderTrack> list=jmsOrderTrackMapper.selectList(new QueryWrapper<>(orderTrack));
//        for (int i=0;i<list.size();i++){
//            list.get(i).setGoodsTypeTracks(goodsTypeTrackService.getGoodsTypeTrackByOrderId(list.get(i).getOrderId()));
//        }
        return list;
    }

    @Transactional
    @Override
    public boolean updateOrderTrackForPicking(Long orderId,int number,Long pickingOrderId) {
        JmsOrderTrack orderTrack=jmsOrderTrackMapper.selectOne(new QueryWrapper<JmsOrderTrack>().eq("order_id",orderId));
        orderTrack.setFinishGoodsNumber(number+orderTrack.getFinishGoodsNumber());
        if (orderTrack.getFinishGoodsNumber().equals(orderTrack.getGoodsNumber())){
            //拣货完成
            orderTrack.setStatus(PickingStatus.Picked.getValue());
            orderTrack.setFinishTime(new Date());
        }else{
            //该订单已完成
            JmsPickingOrder order=pickingOrderService.getById(pickingOrderId);
            if (order.getStatus()==PickingStatus.Picked.getValue()){
                orderTrack.setFinishUserNumber(orderTrack.getFinishUserNumber()+1);
            }
        }
        jmsOrderTrackMapper.updateById(orderTrack);
        return true;
    }
}
