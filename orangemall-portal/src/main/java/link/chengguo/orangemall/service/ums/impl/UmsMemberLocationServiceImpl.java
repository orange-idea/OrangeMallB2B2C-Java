package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberLocation;
import link.chengguo.orangemall.ums.mapper.UmsMemberLocationMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberLocationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-25
 */
@Service
public class UmsMemberLocationServiceImpl extends ServiceImpl<UmsMemberLocationMapper, UmsMemberLocation> implements IUmsMemberLocationService {

}
