package link.chengguo.orangemall.service.fms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;
import link.chengguo.orangemall.fms.mapper.FmsMemberWithdrawAccountMapper;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawAccountService;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-10
 */
@Service
public class FmsMemberWithdrawAccountServiceImpl extends ServiceImpl
        <FmsMemberWithdrawAccountMapper, FmsMemberWithdrawAccount> implements IFmsMemberWithdrawAccountService {

    @Resource
    private FmsMemberWithdrawAccountMapper fmsMemberWithdrawAccountMapper;
    @Resource
    private IUmsMemberService memberService;

    @Override
    public boolean addOrUpdateWithdrawAccount(FmsMemberWithdrawAccount account) {
        FmsMemberWithdrawAccount result=fmsMemberWithdrawAccountMapper.selectOne(new QueryWrapper<FmsMemberWithdrawAccount>().eq("member_id",account.getMemberId()));
        if (result==null){
            fmsMemberWithdrawAccountMapper.insert(account);
        }else{
            account.setId(result.getId());
            account.setUpdateTime(new Date());
            fmsMemberWithdrawAccountMapper.updateById(account);
        }
        return true;
    }
}
