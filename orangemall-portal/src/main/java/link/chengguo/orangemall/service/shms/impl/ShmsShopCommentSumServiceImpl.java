package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.entity.ShmsShopCommentSum;
import link.chengguo.orangemall.shms.mapper.ShmsShopCommentSumMapper;
import link.chengguo.orangemall.shms.service.IShmsShopCommentSumService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopCommentSumServiceImpl extends ServiceImpl
        <ShmsShopCommentSumMapper, ShmsShopCommentSum> implements IShmsShopCommentSumService {

    @Resource
    private ShmsShopCommentSumMapper shmsShopCommentSumMapper;


}
