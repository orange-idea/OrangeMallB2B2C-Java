package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillRefund;
import link.chengguo.orangemall.bill.mapper.BillRefundMapper;
import link.chengguo.orangemall.bill.service.IBillRefundService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退款单表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillRefundServiceImpl extends ServiceImpl<BillRefundMapper, BillRefund> implements IBillRefundService {

}
