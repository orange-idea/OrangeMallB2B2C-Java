package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsGroupActivity;
import link.chengguo.orangemall.sms.mapper.SmsGroupActivityMapper;
import link.chengguo.orangemall.sms.service.ISmsGroupActivityService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-12
 */
@Service
public class SmsGroupActivityServiceImpl extends ServiceImpl<SmsGroupActivityMapper, SmsGroupActivity> implements ISmsGroupActivityService {

}
