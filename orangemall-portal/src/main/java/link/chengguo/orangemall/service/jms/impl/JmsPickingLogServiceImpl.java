package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsPickingLog;
import link.chengguo.orangemall.jms.mapper.JmsPickingLogMapper;
import link.chengguo.orangemall.jms.service.IJmsPickingLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-26
*/
@Service
public class JmsPickingLogServiceImpl extends ServiceImpl
<JmsPickingLogMapper, JmsPickingLog> implements IJmsPickingLogService {

@Resource
private  JmsPickingLogMapper jmsPickingLogMapper;


}
