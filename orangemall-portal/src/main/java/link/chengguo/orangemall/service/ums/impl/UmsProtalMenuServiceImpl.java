package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import link.chengguo.orangemall.ums.mapper.UmsProtalMenuMapper;
import link.chengguo.orangemall.ums.service.IUmsProtalMenuService;
import org.springframework.stereotype.Service;

/**
 * @Author: mxchen
 * @Date: 2020/8/31 17:48
 */
@Service
public class UmsProtalMenuServiceImpl extends ServiceImpl<UmsProtalMenuMapper,UmsProtalMenu> implements  IUmsProtalMenuService {
}
