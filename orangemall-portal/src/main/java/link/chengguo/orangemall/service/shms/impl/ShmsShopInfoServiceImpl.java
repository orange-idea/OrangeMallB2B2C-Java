package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.mapper.ShmsShopInfoMapper;
import link.chengguo.orangemall.shms.service.IShmsShopInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopInfoServiceImpl extends ServiceImpl
        <ShmsShopInfoMapper, ShmsShopInfo> implements IShmsShopInfoService {

    @Resource
    private ShmsShopInfoMapper shmsShopInfoMapper;

    @Override
    public Page<ShmsShopInfo> customPageList(Integer page, Integer pageSize, ShmsShopInfo paramCondition) {
        return null;
    }

    @Override
    public Page<ShmsShopInfo> customPageListByDistance(Integer pageIndex, Integer pageSize, ShmsShopInfo paramCondition, Integer order) {
       Page page= new Page<>();
       page.setSize(pageSize);
       page.setCurrent(pageIndex);
        return  shmsShopInfoMapper.customPageListByDistance(page,paramCondition,order);
    }
}
