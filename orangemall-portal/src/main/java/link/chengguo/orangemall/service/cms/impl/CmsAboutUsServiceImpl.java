package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsAboutUs;
import link.chengguo.orangemall.cms.mapper.CmsAboutUsMapper;
import link.chengguo.orangemall.cms.service.ICmsAboutUsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsAboutUsServiceImpl extends ServiceImpl
<CmsAboutUsMapper, CmsAboutUs> implements ICmsAboutUsService {

@Resource
private  CmsAboutUsMapper cmsAboutUsMapper;


}
