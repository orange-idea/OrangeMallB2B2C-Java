package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsPrefrenceArea;
import link.chengguo.orangemall.cms.mapper.CmsPrefrenceAreaMapper;
import link.chengguo.orangemall.cms.service.ICmsPrefrenceAreaService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优选专区 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsPrefrenceAreaServiceImpl extends ServiceImpl<CmsPrefrenceAreaMapper, CmsPrefrenceArea> implements ICmsPrefrenceAreaService {

}
