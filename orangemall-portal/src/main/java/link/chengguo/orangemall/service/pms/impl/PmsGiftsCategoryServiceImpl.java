package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsGiftsCategory;
import link.chengguo.orangemall.pms.mapper.PmsGiftsCategoryMapper;
import link.chengguo.orangemall.pms.service.IPmsGiftsCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助分类表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
@Service
public class PmsGiftsCategoryServiceImpl extends ServiceImpl<PmsGiftsCategoryMapper, PmsGiftsCategory> implements IPmsGiftsCategoryService {

}
