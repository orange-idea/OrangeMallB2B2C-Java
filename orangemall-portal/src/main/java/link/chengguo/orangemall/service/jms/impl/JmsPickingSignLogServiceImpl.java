package link.chengguo.orangemall.service.jms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.jms.entity.JmsPickingSignLog;
import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import link.chengguo.orangemall.jms.mapper.JmsPickingSignLogMapper;
import link.chengguo.orangemall.jms.service.IJmsPickingSignLogService;
import link.chengguo.orangemall.jms.service.IJmsPickingUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-26
 */
@Service
public class JmsPickingSignLogServiceImpl extends ServiceImpl
        <JmsPickingSignLogMapper, JmsPickingSignLog> implements IJmsPickingSignLogService {

    @Resource
    private JmsPickingSignLogMapper jmsPickingSignLogMapper;
    @Resource
    private IJmsPickingUserService jmsPickingUserService;


    @Transactional
    @Override
    public boolean userSign(JmsPickingUser user, Integer type, String address, String lat, String lng) {
        user.setWorkState(type);
        jmsPickingUserService.updateById(user);
        //添加记录
        JmsPickingSignLog log=new JmsPickingSignLog();
        log.setAddress(address);
        log.setCityCode(user.getCityCode());
        log.setShopId(user.getShopId());
        log.setLatitude(lat);
        log.setType(type);
        log.setLongitude(lng);
        log.setSignTime(new Date());
        log.setUserId(user.getId());
        log.setName(user.getName());
        jmsPickingSignLogMapper.insert(log);
        return true;
    }
}
