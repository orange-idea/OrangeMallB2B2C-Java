package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.cms.mapper.CmsSubjectCategoryMapper;
import link.chengguo.orangemall.cms.mapper.CmsSubjectMapper;
import link.chengguo.orangemall.cms.service.ICmsSubjectService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendSubject;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendSubjectService;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.entity.UmsRewardLog;
import link.chengguo.orangemall.ums.mapper.UmsMemberMapper;
import link.chengguo.orangemall.ums.mapper.UmsRewardLogMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.vo.timeline.Timeline;
import link.chengguo.orangemall.vo.timeline.TimelineMonth;
import link.chengguo.orangemall.vo.timeline.TimelinePost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 专题表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsSubjectServiceImpl extends ServiceImpl<CmsSubjectMapper, CmsSubject> implements ICmsSubjectService {


    @Resource
    private CmsSubjectMapper subjectMapper;
    @Resource
    private ISmsHomeRecommendSubjectService homeRecommendSubjectService;

    @Resource
    private UmsMemberMapper memberMapper;
    @Resource
    private UmsRewardLogMapper rewardLogMapper;
    @Resource
    private CmsSubjectCategoryMapper subjectCategoryMapper;
    @Autowired
    private IUmsMemberService memberService;


    /**
     * timelineMapper
     * 获取timeLine数据
     *
     * @return
     */
    @Override
    public List<Timeline> listTimeLine() {
        List<Timeline> timelineList = subjectMapper.listTimeline();
        genTimelineMonth(timelineList);
        return timelineList;
    }

    private List<Timeline> genTimelineMonth(List<Timeline> timelineList) {
        for (Timeline timeline : timelineList) {
            List<TimelineMonth> timelineMonthList = new ArrayList<>();
            for (int i = Calendar.DECEMBER + 1; i > 0; i--) {
                List<TimelinePost> postList = subjectMapper.listTimelinePost(timeline.getYear(), i);
                if (CollectionUtils.isEmpty(postList)) {
                    continue;
                }
                TimelineMonth month = new TimelineMonth();
                month.setCount(postList.size());
                month.setMonth(i);
                month.setPosts(postList);
                timelineMonthList.add(month);
            }
            timeline.setMonths(timelineMonthList);
        }
        return timelineList;
    }


    @Override
    public List<CmsSubject> getRecommendSubjectList(int pageNum, int pageSize) {
        List<SmsHomeRecommendSubject> brands = homeRecommendSubjectService.list(new QueryWrapper<>());
        List<Long> ids = brands.stream()
                .map(SmsHomeRecommendSubject::getId)
                .collect(Collectors.toList());
        return (List<CmsSubject>) subjectMapper.selectBatchIds(ids);
    }

    @Override
    public int countByToday(Long id) {
        return subjectMapper.countByToday(id);
    }

    @Transactional
    @Override
    public Object reward(Long aid, int coin) {
        try {
            UmsMember member = memberService.getNewCurrentMember();
            if (member != null && member.getBlance().compareTo(new BigDecimal(coin)) < 0) {
                return new CommonResult().failed("余额不够");
            }
            member.setBlance(member.getBlance().subtract(new BigDecimal(coin)));
            memberMapper.updateById(member);
            CmsSubject subject = subjectMapper.selectById(aid);
            UmsMember remember = memberMapper.selectById(subject.getMemberId());
            if (remember != null) {
                subject.setReward(subject.getReward() + coin);
                subjectMapper.updateById(subject);
                remember.setBlance(remember.getBlance().add(new BigDecimal(coin)));
                memberMapper.updateById(remember);
                UmsRewardLog log = new UmsRewardLog();
                log.setCoin(coin);
                log.setSendMemberId(member.getId());
                log.setMemberIcon(member.getIcon());
                log.setMemberNickName(member.getNickname());
                log.setRecMemberId(remember.getId());
                log.setCreateTime(new Date());
                log.setObjid(aid);
                rewardLogMapper.insert(log);
            }
            return new CommonResult().success("打赏文章成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new CommonResult().failed("打赏文章失败");
        }
    }
}
