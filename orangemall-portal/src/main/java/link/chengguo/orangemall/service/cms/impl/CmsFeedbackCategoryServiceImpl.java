package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsFeedbackCategory;
import link.chengguo.orangemall.cms.mapper.CmsFeedbackCategoryMapper;
import link.chengguo.orangemall.cms.service.ICmsFeedbackCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsFeedbackCategoryServiceImpl extends ServiceImpl
<CmsFeedbackCategoryMapper, CmsFeedbackCategory> implements ICmsFeedbackCategoryService {

@Resource
private  CmsFeedbackCategoryMapper cmsFeedbackCategoryMapper;


}
