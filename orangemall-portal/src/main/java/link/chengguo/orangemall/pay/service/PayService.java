package link.chengguo.orangemall.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.vo.OrderParam;

public interface PayService extends IService<OmsOrder>{


    //微信支付回调处理
    String wxPayNotifyManage(String out_trade_no);

    /**
     * 更新订单状态，支付成功，支付宝微信等
     * @param order
     * @return
     */
    boolean updateOrderStatus(OmsOrder order);

    /**
     * 更新订单状态，支付成功,余额、其他支付公共部分
     * @param order
     * @return
     */
    boolean updateOrderStatus2(OmsOrder order);


    /**
     * 余额支付
     *
     * @param order
     * @return
     */
    OmsOrder blancePay(OmsOrder order);

    /**
     * 积分兑换
     *
     * @param payParam
     * @return
     */
    Object jifenPay(OrderParam payParam);


}
