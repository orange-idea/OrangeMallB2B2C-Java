package link.chengguo.orangemall.fms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.fms.entity.FmsMemberRecharge;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsMemberRechargeService extends IService<FmsMemberRecharge> {

    /**
     * 添加用户充值记录
     */
    void add(Long memberId,String orderSn,String money,String content,Integer payType);


    /**
     * 充值成功更新状态
     * @param recharge
     */
    boolean updateForSuccess(FmsMemberRecharge recharge);

}
