package link.chengguo.orangemall.fms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.fms.entity.FmsAgentWithdrawAccount;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsAgentWithdrawAccountService extends IService<FmsAgentWithdrawAccount> {

}
