package link.chengguo.orangemall.fms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsMemberWithdrawAccountService extends IService<FmsMemberWithdrawAccount> {

    /**
     * 增加提现账号
     * @return
     */
    boolean addOrUpdateWithdrawAccount(FmsMemberWithdrawAccount account);


}
