package link.chengguo.orangemall.fms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.fms.entity.FmsAgentWithdraw;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsAgentWithdrawService extends IService<FmsAgentWithdraw> {

}
