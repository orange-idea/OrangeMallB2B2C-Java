package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsPayments;

/**
 * <p>
 * 支付方式表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-14
 */
public interface IOmsPaymentsService extends IService<OmsPayments> {

}
