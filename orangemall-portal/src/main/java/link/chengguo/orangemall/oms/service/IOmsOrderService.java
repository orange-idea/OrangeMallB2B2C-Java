package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;
import link.chengguo.orangemall.oms.vo.ConfirmListOrderResult;
import link.chengguo.orangemall.oms.vo.ConfirmOrderResult;
import link.chengguo.orangemall.oms.vo.OrderParam;
import link.chengguo.orangemall.oms.vo.TbThanks;
import link.chengguo.orangemall.ums.entity.UmsMember;
import link.chengguo.orangemall.ums.entity.UmsMemberReceiveAddress;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.vo.oms.CartParam;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderService extends IService<OmsOrder> {
    /**
     * 根据提交信息生成订单
     */
    @Transactional
    CommonResult generateOrder(OrderParam orderParam);

    /**
     * 支付成功后的回调
     */
    @Transactional
    CommonResult paySuccess(Long orderId);

    /**
     * 自动取消超时订单
     */
    @Transactional
    CommonResult cancelTimeOutOrder();

    /**
     * 取消单个超时订单
     */
    @Transactional
    void cancelOrder(Long orderId);

    /**
     * 发送延迟消息取消订单
     */
    void sendDelayMessageCancelOrder(Long orderId);

    /**
     * 预览订单
     *
     * @param orderParam
     * @return
     */
    ConfirmOrderResult submitPreview(OrderParam orderParam);

    /**
     * 多店铺预览订单
     *
     * @param orderParam
     * @return
     */
    ConfirmListOrderResult submitStorePreview(OrderParam orderParam);

    /**
     * pc 支付
     *
     * @param tbThanks
     * @return
     */
    int payOrder(TbThanks tbThanks);

    /**
     * 添加购物车
     *
     * @param cartParam
     * @return
     */
    Object addCart(CartParam cartParam);

    /**
     * 开团
     *
     * @param orderParam
     * @return
     */
    ConfirmOrderResult addGroup(OrderParam orderParam);

    /**
     * 参团
     *
     * @param orderParam
     * @return
     */
    Object acceptGroup(OrderParam orderParam);


    /**
     * 关闭订单
     *
     * @param newE
     * @return
     */
    boolean closeOrder(OmsOrder newE);

    /**
     * 取消订单
     * @param newE
     * @return
     */
    boolean cancelOrder(OmsOrder newE);
    /**
     * 释放库存和销量
     *
     * @param newE
     */
    void releaseStock(OmsOrder newE);

    /**
     * 取消发货
     *
     * @param order
     * @param remark
     * @return
     */
    int cancleDelivery(OmsOrder order, String remark);

    /**
     * 确认收货
     *
     * @param id
     * @return
     */
    Object confimDelivery(Long id);

    /**
     * 确认收货
     *
     * @param id
     * @return
     */
    Object confimGet(Long id);

    /**
     * 团购商品订单预览
     *
     * @param orderParam
     * @return
     */
    Object preGroupActivityOrder(OrderParam orderParam);

    /**
     * 申请退款
     *
     * @param id
     * @return
     */
    Object applyRefund(Long id);

    /**
     * 订单评论
     *
     * @param orderId
     * @param shopItem
     * @param items
     * @return
     */
    Object orderComment(Long orderId, String items,String shopItem);

    CommonResult generateStoreOrder(OrderParam orderParam);

    /**
     * 创建订单对象
     *
     * @param order
     * @param orderParam
     * @param currentMember
     * @param orderItemList
     * @param address
     * @return
     */
    OmsOrder createOrderObj(OmsOrder order, OrderParam orderParam, UmsMember currentMember, List<OmsOrderItem> orderItemList, UmsMemberReceiveAddress address);


    /**
     * 订单核销
     *
     * @param code 核销码
     * @param type 核销类型：0-扫码，1-手动
     * @return
     */
    Object orderVerify(String code, Integer type);

    String  getPayTypeName(Integer payType);

    String getSourceTypeName(Integer sourceType);

    String getStatusName(Integer status);

    String getPayStatusName(Integer payStatus);

    String getExpressStatusName(Integer expressStatus);

    String getShippingStatusName(Integer shippingStatus);

    String getPickingStatusName(Integer pickingStatus);

    String getVerifyStatusName(Integer verifyStatus);

    String getAfterSaleStatusName(Integer afterSaleStatus);

    String getIsCommentName(Integer isComment);
}

