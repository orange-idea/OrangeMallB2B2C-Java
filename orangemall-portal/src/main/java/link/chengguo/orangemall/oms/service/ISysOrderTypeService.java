package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysOrderType;

/**
* @author yzb
* @date 2019-12-26
*/

public interface ISysOrderTypeService extends IService<SysOrderType> {

}
