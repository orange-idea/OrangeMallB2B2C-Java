package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsCompanyAddress;

/**
 * <p>
 * 公司收发货地址表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsCompanyAddressService extends IService<OmsCompanyAddress> {

}
