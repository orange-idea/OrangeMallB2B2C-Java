package link.chengguo.orangemall.vo.shms;

import link.chengguo.orangemall.shms.entity.ShmsShopCommentLog;
import link.chengguo.orangemall.ums.entity.UmsMember;
import lombok.Data;

/**
 * @Author: mxchen
 * @Date: 2020/9/14 16:01
 */
@Data
public class ShopCommentVo {
    private UmsMember umsMember;
    private ShmsShopCommentLog shmsShopCommentLog;
}
