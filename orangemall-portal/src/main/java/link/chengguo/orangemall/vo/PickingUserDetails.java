package link.chengguo.orangemall.vo;


import link.chengguo.orangemall.jms.entity.JmsPickingUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;

/**
 * 拣货员详情封装
 */
public class PickingUserDetails implements UserDetails {
    private JmsPickingUser umsMember;

    public PickingUserDetails(JmsPickingUser umsMember) {
        this.umsMember = umsMember;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的权限
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"), new SimpleGrantedAuthority("ADMIN"));
    }

    @Override
    public String getPassword() {
        return umsMember.getPwd();
    }

    @Override
    public String getUsername() {
        return umsMember.getPhone();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return umsMember.getStatus() == 1;
    }

    public JmsPickingUser getUmsMember() {
        return umsMember;
    }
}
