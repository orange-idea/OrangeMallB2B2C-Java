package link.chengguo.orangemall.vo.oms;

import lombok.Data;

/**
 * @Auther: shenzhuan
 * @Date: 2019/4/21 16:21
 * @Description:
 */
@Data
public class CartParam {
    private Long cartId;
    private Long skuId;
    private Long goodsId;
    private Integer total;
    private Long memberId;
    /**
     * 店铺ID
     */
    private Long shopId;
    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 购物车类型：0-普通，1-扫码购
     */
    private Integer cartType;

    /**
     * 区域编码
     */
    private String cityCode;

    private String ids;
    // 1 选中 2 未选中
    private Integer isSelected;
    // 1sku 2spu
    private Integer type;
    private Integer orderType;
}
