package link.chengguo.orangemall.vo;


import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeCategory;
import link.chengguo.orangemall.sms.entity.SmsCoupon;
import link.chengguo.orangemall.sms.entity.SmsHomeAdvertise;
import link.chengguo.orangemall.sms.entity.SmsRedPacket;
import link.chengguo.orangemall.sms.vo.HomeFlashPromotion;
import link.chengguo.orangemall.sms.vo.HomeProductAttr;
import link.chengguo.orangemall.vo.pms.CateProduct;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2017/10/18 0018.
 */
@Data
public class IndexData {
    private List<TArticleDO> module_list;
    private List<SmsHomeAdvertise> banner_list;
    private List<TArticleDO> nav_icon_list;
    private List<PmsProductAttributeCategory> cat_list;
    private HomeFlashPromotion homeFlashPromotion;
    private List<HomeProductAttr> new_products;
    private List<HomeProductAttr> hot_products;
    private List<CateProduct> cate_products;
    private int cat_goods_cols;
    private List<TArticleDO> block_list;
    private List<SmsCoupon> coupon_list;
    private List<CmsSubject> subjectList;

    private List<SmsRedPacket> redPacketList;


}
