package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendProduct;

/**
 * <p>
 * 人气推荐商品表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsHomeRecommendProductService extends IService<SmsHomeRecommendProduct> {

}
