package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsGroup;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsGroupService extends IService<SmsGroup> {


}
