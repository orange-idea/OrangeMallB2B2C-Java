package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.oms.vo.HomeContentResult;
import link.chengguo.orangemall.pms.entity.PmsBrand;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeCategory;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotion;
import link.chengguo.orangemall.sms.entity.SmsGroup;
import link.chengguo.orangemall.sms.entity.SmsHomeAdvertise;
import link.chengguo.orangemall.sms.vo.HomeFlashPromotion;
import link.chengguo.orangemall.sys.entity.SysStore;
import link.chengguo.orangemall.vo.home.Pages;

import java.util.List;

/**
 * <p>
 * 首页轮播广告表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsHomeAdvertiseService extends IService<SmsHomeAdvertise> {

    HomeContentResult singelContent();

    HomeContentResult singelContent1();

    List<PmsBrand> getRecommendBrandList(int pageNum, int pageSize);

    HomeFlashPromotion getHomeFlashPromotion();

    List<PmsProduct> getNewProductList(int pageNum, int pageSize);

    List<PmsProduct> getSaleProductList(int pageNum, int pageSize);

    List<PmsProduct> getHotProductList(int pageNum, int pageSize);

    List<CmsSubject> getRecommendSubjectList(int pageNum, int pageSize);

    List<SmsHomeAdvertise> getHomeAdvertiseList(int type);

    List<SmsHomeAdvertise> getHomeAdvertiseList(SmsHomeAdvertise advertise);

    List<SmsHomeAdvertise> getHomeAdvertiseList();

    List<PmsProductAttributeCategory> getPmsProductAttributeCategories();

    List<SysStore> getStoreList(int pageNum, int pageSize);

    List<HomeFlashPromotion> homeFlashPromotionList(SmsFlashPromotion promotion);

    List<PmsSmallNaviconCategory> getNav();

    List<SmsGroup> lastGroupGoods(Integer pageNum);

    Pages contentNew();

    HomeContentResult contentNew1();

    HomeContentResult contentPc();

    HomeContentResult singelmobileContent();
}
