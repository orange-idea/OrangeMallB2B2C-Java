package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsCouponProductCategoryRelation;

/**
 * <p>
 * 优惠券和产品分类关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsCouponProductCategoryRelationService extends IService<SmsCouponProductCategoryRelation> {

}
