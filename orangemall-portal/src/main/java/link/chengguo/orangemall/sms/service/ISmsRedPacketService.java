package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsRedPacket;

/**
 * <p>
 * 红包 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsRedPacketService extends IService<SmsRedPacket> {

    int createRedPacket(SmsRedPacket smsRedPacket);

    int acceptRedPacket(Integer id);
}
