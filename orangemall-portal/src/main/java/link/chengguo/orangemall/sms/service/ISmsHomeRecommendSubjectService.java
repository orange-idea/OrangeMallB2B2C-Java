package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendSubject;

/**
 * <p>
 * 首页推荐专题表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsHomeRecommendSubjectService extends IService<SmsHomeRecommendSubject> {

}
