package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotionProductRelation;

/**
 * <p>
 * 商品限时购与商品关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsFlashPromotionProductRelationService extends IService<SmsFlashPromotionProductRelation> {

}
