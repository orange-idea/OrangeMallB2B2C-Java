package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsHomeNewProduct;

/**
 * <p>
 * 新鲜好物表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsHomeNewProductService extends IService<SmsHomeNewProduct> {

}
