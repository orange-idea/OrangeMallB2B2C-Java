package link.chengguo.orangemall.ams.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;

/**
* @author yzb
* @date 2019-12-21
*/

public interface IAmsBusinessCityService extends IService<AmsBusinessCity> {

}
