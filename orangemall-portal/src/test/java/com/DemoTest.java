package com;


import java.util.Random;

/**
 * @Author: mxchen
 * @Date: 2020/9/3 14:25
 */
public class DemoTest {
    public static void main(String[] args) {
        System.out.println(getStringRandom(10));
    }
    public static String getStringRandom(int size){
        StringBuffer stringBuffer = new StringBuffer();
        Random ran = new Random();
        for(int i = 0;i<size;i++) {
            //nextBoolean() 方法用于从该随机数生成器的序列得到下一个伪均匀分布的布尔值。
            if (ran.nextBoolean()) {
                //nextInt(n)将返回一个大于等于0小于n的随机数
                stringBuffer.append(ran.nextInt(10));
            } else {
                //小写字母
                stringBuffer.append((char) (ran.nextInt(26) + 97));
            }
        }
        return stringBuffer.toString();
    }
}
