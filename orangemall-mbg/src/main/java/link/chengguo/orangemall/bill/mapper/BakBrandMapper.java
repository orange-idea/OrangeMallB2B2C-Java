package link.chengguo.orangemall.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.bill.entity.BakBrand;

/**
 * <p>
 * 品牌商表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-18
 */
public interface BakBrandMapper extends BaseMapper<BakBrand> {

}
