package link.chengguo.orangemall.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.bill.entity.BillDelivery;

/**
 * <p>
 * 发货单表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface BillDeliveryMapper extends BaseMapper<BillDelivery> {

}
