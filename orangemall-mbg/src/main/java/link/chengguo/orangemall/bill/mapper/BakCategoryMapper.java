package link.chengguo.orangemall.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.bill.entity.BakCategory;

/**
 * <p>
 * 类目表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-17
 */
public interface BakCategoryMapper extends BaseMapper<BakCategory> {

}
