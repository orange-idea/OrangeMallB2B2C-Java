package link.chengguo.orangemall.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.bill.entity.BillReshipItems;

/**
 * <p>
 * 退货单明细表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface BillReshipItemsMapper extends BaseMapper<BillReshipItems> {

}
