package link.chengguo.orangemall.comment;

/**
 * @Author: mxchen
 * @Date: 2020/9/3 10:13
 */
public final class SysConstant {
    /**审核状态
     * 0：待审核
     * 1：审核中
     * 2：审核通过
     * 3：审核拒绝
     * **/
    public static final Integer SHOP_ACCOUNT_STATUS_WAIT_REVIEW= 0;
    public static final Integer SHOP_ACCOUNT_STATUS_UNDER_REVIEW = 1;
    public static final Integer SHOP_ACCOUNT_STATUS_PASS_REVIEW = 2;
    public static final Integer SHOP_ACCOUNR_STATUS_REFUSE_REVIEW = 3;
    /**订单操作人**/
    public static final String ORDER_OPERATE_MAN_0 = "系统";
    public static final String ORDER_OPERATE_MAN_1 = "用户";
    public static final String ORDER_OPERATE_MAN_2 = "后台管理员";
    /**会员昵称长度**/
    public static final Integer UMS_MEMBER_NICK_SIZE = 12;
    /**会员默认分组
     * 1 ： 普通会员
     * **/
    public static final Integer UMS_MEMBER_DEFAULT_GROUP = 1;
    /**会员默认等级
     * 1L ：初级
     * **/
    public static final Long UMS_MEMBER_DEFAULT_LEVEL = 1L;
    /**会员默认头像**/
    public static final String MEMBER_DEFAULT_ICON = "http://orangemall.oss-cn-shanghai.aliyuncs.com/orangemall20200724/用户头像.png";

    /**
     * 订单表的相关状态名
     *
     */
    /**支付类型**/
    public static final String ORDER_PAY_TYPE_NAME_0 = "未支付";
    public static final String ORDER_PAY_TYPE_NAME_1 = "支付宝";
    public static final String ORDER_PAY_TYPE_NAME_2 = "微信";
    public static final String ORDER_PAY_TYPE_NAME_3 = "余额支付";

    /**订单来源**/
    public static final String ORDER_SOURCE_TYPE_NAME_1 = "小程序订单";
    public static final String ORDER_SOURCE_TYPE_NAME_2 = "h5订单";
    public static final String ORDER_SOURCE_TYPE_NAME_3 = "PC订单 ";
    public static final String ORDER_SOURCE_TYPE_NAME_4 = "app";

    /**订单状态**/

    public static final String ORDER_STATUS_NAME_0 = "待付款";
    public static final String ORDER_STATUS_NAME_1 = "待确认";
    public static final String ORDER_STATUS_NAME_2 = "已确认";
    public static final String ORDER_STATUS_NAME_3 = "已取消";
    public static final String ORDER_STATUS_NAME_4 = "已完成";
    public static final String ORDER_STATUS_NAME_5 = "已关闭";
    public static final String ORDER_STATUS_NAME_6 = "售后中";

    /**支付状态**/
    public static final String ORDER_PAY_STATUS_NAME_0 = "待付款";
    public static final String ORDER_PAY_STATUS_NAME_1 = "付款中";
    public static final String ORDER_PAY_STATUS_NAME_2 = "已付款";

    public static final String ORDER_EXPRESS_STATUS_NAME_0 = "当前流程未到物流";
    public static final String ORDER_EXPRESS_STATUS_NAME_1 = "待发货";
    public static final String ORDER_EXPRESS_STATUS_NAME_2 = "待收货";
    public static final String ORDER_EXPRESS_STATUS_NAME_3 = "已收货";

    public static final String ORDER_SHIPPING_STATUS_NAME_0 = "当前流程未到配送";
    public static final String ORDER_SHIPPING_STATUS_NAME_1 = "待抢单";
    public static final String ORDER_SHIPPING_STATUS_NAME_2 = "待取货";
    public static final String ORDER_SHIPPING_STATUS_NAME_3 = "取货中";
    public static final String ORDER_SHIPPING_STATUS_NAME_4 = "配送中";
    public static final String ORDER_SHIPPING_STATUS_NAME_5 = "已收货";

    public static final String ORDER_PICKING_STATUS_NAME_0  = "当前流程未到拣货";
    public static final String ORDER_PICKING_STATUS_NAME_1 = "待抢单";
    public static final String ORDER_PICKING_STATUS_NAME_2 = "拣货中";
    public static final String ORDER_PICKING_STATUS_NAME_3 = "拣货完成";

    public static final String ORDER_VERIFY_STATUS_NAME_0 = "当前流程未到核销";
    public static final String ORDER_VERIFY_STATUS_NAME_1 = "待核销";
    public static final String ORDER_VERIFY_STATUS_NAME_2  = "已核销";

    public static final String ORDER_AFTER_SALE_STATUS_NAME_0 = "当前流程未到售后";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_1 = "退款申请";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_2 = "同意退款";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_3 = "拒绝退款";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_4 = "取消申请";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_5 = "退款退货申请";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_6 = "同意退款退货";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_7 = "拒绝退款退货";
    public static final String ORDER_AFTER_SALE_STATUS_NAME_8 = "确认收货";

    public static final String ORDER_IS_COMMENT_NAME_1= "未评论";
    public static final String ORDER_IS_COMMENT_NAME_2= "已评论";


    public static final String BUSINESS_WEBSITE = "http://demo.chengguo.link:8091/business";
}
