package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysUserPermission;

/**
 * <p>
 * 后台用户和权限关系表(除角色中定义的权限以外的加减权限) Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysUserPermissionMapper extends BaseMapper<SysUserPermission> {

}
