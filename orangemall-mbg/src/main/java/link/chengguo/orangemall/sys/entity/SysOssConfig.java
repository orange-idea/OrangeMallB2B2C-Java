package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:08
 */
@Data
public class SysOssConfig {
    @TableId
    private Integer id;
    private String accessKey;
    private String secret;
    private String endpoint;
    private String bucketName;
    private String styleName;
    private String prefix;
}
