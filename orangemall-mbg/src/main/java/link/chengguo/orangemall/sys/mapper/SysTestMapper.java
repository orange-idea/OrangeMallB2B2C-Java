package link.chengguo.orangemall.sys.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysTest;

/**
 * @author orangemall
 * @date 2019-12-02
 */
public interface SysTestMapper extends BaseMapper<SysTest> {
}
