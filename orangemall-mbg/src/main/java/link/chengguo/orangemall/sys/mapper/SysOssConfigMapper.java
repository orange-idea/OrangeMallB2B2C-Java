package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysOssConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:18
 */
@Mapper
public interface SysOssConfigMapper extends BaseMapper<SysOssConfig> {
}
