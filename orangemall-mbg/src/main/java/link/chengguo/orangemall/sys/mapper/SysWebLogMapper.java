package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysWebLog;
import link.chengguo.orangemall.vo.LogParam;
import link.chengguo.orangemall.vo.LogStatisc;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysWebLogMapper extends BaseMapper<SysWebLog> {

    List<LogStatisc> getLogStatisc(LogParam entity);
}
