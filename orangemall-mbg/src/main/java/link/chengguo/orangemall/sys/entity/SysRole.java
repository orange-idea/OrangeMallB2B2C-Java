package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 后台用户角色表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Data
@TableName("sys_role")
public class SysRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 名称
     */
    private String name;

    @TableField(exist = false)
    private boolean checked = false;

    /**
     * 描述
     */
    private String description;

    /**
     * 后台用户数量
     */
    @TableField("admin_count")
    private Integer adminCount;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 启用状态：0->禁用；1->启用
     */
    private Integer status;

    private Integer sort;
    @TableField(exist = false)
    private String menuIds;

    /**
     * 归属平台（1-租户，2-代理，3-商户）
     */
    @TableField("platform")
    private Integer platform;

    /**
     * 代理城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺ID
     */
    @TableField("shop_id")
    private Long shopId;
}
