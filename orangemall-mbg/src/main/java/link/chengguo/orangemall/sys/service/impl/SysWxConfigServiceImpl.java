package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysWxConfig;
import link.chengguo.orangemall.sys.mapper.SysWxConfigMapper;
import link.chengguo.orangemall.sys.service.SysWxConfigService;
import org.springframework.stereotype.Service;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:03
 */
@Service
public class SysWxConfigServiceImpl extends ServiceImpl<SysWxConfigMapper, SysWxConfig> implements SysWxConfigService {
}
