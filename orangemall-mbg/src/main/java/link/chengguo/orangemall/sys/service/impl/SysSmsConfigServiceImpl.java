package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysSmsConfig;
import link.chengguo.orangemall.sys.mapper.SysSmsConfigMapper;
import link.chengguo.orangemall.sys.service.SysSmsConfigService;
import org.springframework.stereotype.Service;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:03
 */
@Service
public class SysSmsConfigServiceImpl extends ServiceImpl<SysSmsConfigMapper, SysSmsConfig> implements SysSmsConfigService {
}
