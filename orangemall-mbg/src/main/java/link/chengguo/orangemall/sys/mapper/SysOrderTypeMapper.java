package link.chengguo.orangemall.sys.mapper;


import link.chengguo.orangemall.sys.entity.SysOrderType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2019-12-26
*/
public interface SysOrderTypeMapper extends BaseMapper<SysOrderType> {
}
