package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysAdminLog;
import link.chengguo.orangemall.vo.LogParam;
import link.chengguo.orangemall.vo.LogStatisc;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysAdminLogMapper extends BaseMapper<SysAdminLog> {

    List<LogStatisc> getLogStatisc(LogParam entity);


}
