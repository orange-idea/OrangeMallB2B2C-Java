package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysOssConfig;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:19
 */
public interface SysOssConfigService extends IService<SysOssConfig> {
}
