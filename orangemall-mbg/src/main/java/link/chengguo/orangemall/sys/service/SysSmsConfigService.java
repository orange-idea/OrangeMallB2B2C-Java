package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysSmsConfig;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:02
 */
public interface SysSmsConfigService extends IService<SysSmsConfig> {
}
