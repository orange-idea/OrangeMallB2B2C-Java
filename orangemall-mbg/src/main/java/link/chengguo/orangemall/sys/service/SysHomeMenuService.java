package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysHomeMenu;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:02
 */
public interface SysHomeMenuService extends IService<SysHomeMenu> {
}
