package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.AdminSysJob;

/**
 * <p>
 * 定时任务调度表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-11-26
 */
public interface AdminSysJobMapper extends BaseMapper<AdminSysJob> {

}
