package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysPermissionCategory;
import org.apache.ibatis.annotations.Param;


/**
 * 权限类别表
 *
 * @author chengguo
 * @email 1264395832@qq.com
 * @date 2019-04-27 18:52:51
 */

public interface SysPermissionCategoryMapper extends BaseMapper<SysPermissionCategory> {

    SysPermissionCategory get(@Param("id")Long id);
//
//	List<SysPermissionCategory> list(SysPermissionCategory permissionCategory);
//
//    int count(SysPermissionCategory permissionCategory);
//
//	int save(SysPermissionCategory permissionCategory);
//
//	int update(SysPermissionCategory permissionCategory);
//
//	int remove(Long id);
//
//	int batchRemove(Long[] ids);
}
