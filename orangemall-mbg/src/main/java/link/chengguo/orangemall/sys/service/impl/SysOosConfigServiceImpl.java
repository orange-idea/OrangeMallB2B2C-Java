package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysOssConfig;
import link.chengguo.orangemall.sys.mapper.SysOssConfigMapper;
import link.chengguo.orangemall.sys.service.SysOssConfigService;
import org.springframework.stereotype.Service;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:20
 */
@Service
public class SysOosConfigServiceImpl extends ServiceImpl<SysOssConfigMapper, SysOssConfig> implements SysOssConfigService {
}
