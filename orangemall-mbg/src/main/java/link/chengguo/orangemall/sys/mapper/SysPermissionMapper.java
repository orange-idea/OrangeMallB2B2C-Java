package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 后台用户权限表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermission> listMenuByUserId(@Param("id")Long id, @Param("platform")Integer platform);

    List<SysPermission> getPermissionListByUserId(@Param("id")Long id);

    List<SysPermission> getPermissionList(@Param("roleId")Long roleId);

    List<SysPermission> listUserPerms(@Param("id")Long id);
}
