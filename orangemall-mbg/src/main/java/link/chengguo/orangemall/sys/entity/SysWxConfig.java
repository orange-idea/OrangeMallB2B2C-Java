package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 9:59
 */
@Data
public class SysWxConfig {
    @TableId
    private Integer id;
    private String xcxappid;//小程序appid
    private String xcxsecret;//小程序密钥
    private String gzhappid;//公众号appid
    private String gzhsecret;//公众号密钥
    private String gzhaeskey;//公众号EncodingAESKey
    private String gzhservice;//微信服务器地址
    private String gzhsharePictures;//微信分享图片
    private String gzhsharetTitle;//微信分享标题
    private String gzhshareSynopsis;//微信分享简介
}
