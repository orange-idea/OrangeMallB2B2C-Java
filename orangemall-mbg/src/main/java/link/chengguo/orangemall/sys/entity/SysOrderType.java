package link.chengguo.orangemall.sys.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2019-12-26
 * 配送模式
 */
@Data
@TableName("sys_order_type")
public class SysOrderType implements Serializable {


    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;


    @TableField("name")
    private String name;


    @TableField("introduce")
    private String introduce;


    @TableField("store_id")
    private Integer storeId;

    /**
     * 店铺ID
     */
    @TableField(exist = false)
    private Long shopId;


    @TableField("ctime")
    private Date ctime;


    @TableField("utime")
    private Date utime;


    /**
     * 状态：0-停用，1-启用
     */
    @TableField("status")
    private Integer status;


}
