package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author orangemall
 * @date 2019-12-02
 * 测试
 */

@Data
public class SysHomeMenu implements Serializable {

    @TableId
    private Long id;

    private String name;
    private String uniapp;
    private String img;
    private Integer sort;
    private Integer type;

    private Date createTime;


}
