package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysHomeMenu;
import link.chengguo.orangemall.sys.mapper.SysHomeMenuMapper;
import link.chengguo.orangemall.sys.service.SysHomeMenuService;
import org.springframework.stereotype.Service;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:20
 */
@Service
public class SysHomeMenuServiceImpl extends ServiceImpl<SysHomeMenuMapper, SysHomeMenu> implements SysHomeMenuService {
}
