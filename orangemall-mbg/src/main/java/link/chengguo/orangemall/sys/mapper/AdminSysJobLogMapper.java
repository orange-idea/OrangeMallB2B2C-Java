package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.AdminSysJobLog;

/**
 * <p>
 * 定时任务调度日志表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-11-26
 */
public interface AdminSysJobLogMapper extends BaseMapper<AdminSysJobLog> {

}
