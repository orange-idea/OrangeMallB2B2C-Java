package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 9:59
 */
@Data
public class SysSmsConfig {
    @TableId
    private Integer id;
    private String accessKeyId;
    private String accessKeySecret;
    private String signName;
    private String templateCode;
    private Integer expireMinute;
    private Integer dayCount;
}
