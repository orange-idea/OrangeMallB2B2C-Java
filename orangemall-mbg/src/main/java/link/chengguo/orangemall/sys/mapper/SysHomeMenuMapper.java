package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysHomeMenu;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysHomeMenuMapper extends BaseMapper<SysHomeMenu> {

}
