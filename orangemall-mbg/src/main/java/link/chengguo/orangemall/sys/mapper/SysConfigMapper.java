package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:02
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
