package link.chengguo.orangemall.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sys.entity.SysRolePermission;

/**
 * <p>
 * 后台用户角色和权限关系表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
