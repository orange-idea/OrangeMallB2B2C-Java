package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 9:59
 */
@Data
public class SysConfig {
    @TableId
    private Integer id;
    private String storeSelfMention;
    private String systemMobileName;
    private String systemAbout;
    private String systemBackstageLogo;
    private String systemCopyRight;
    private String systemLoginLogo;
    private String systemMobileLogo;
    private String systemName;
    private String systemNumber;
    private String systemSquareLogo;
}
