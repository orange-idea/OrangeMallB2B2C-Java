package link.chengguo.orangemall.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 后台用户表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Data
@TableName("sys_user")
public class SysUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String username;

    private String password;

    /**
     * 头像
     */
    private String icon;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 备注信息
     */
    private String note;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 最后登录时间
     */
    @TableField("login_time")
    private Date loginTime;

    /**
     * 帐号启用状态：0->禁用；1->启用
     */
    private Integer status;

    /**
     * 供应商
     */
    @TableField("supply_id")
    private Long supplyId;

    //角色
    @TableField(exist = false)
    private String roleIds;

    //角色名称
    @TableField(exist = false)
    private String roleName;

    @TableField(exist = false)
    private String confimpassword;

    @TableField(exist = false)
    private String code;

    /**
     * 归属平台（1-租户，2-代理，3-商户）
     */
    @TableField("platform")
    private Integer platform;

    /**
     * 上级代理ID
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 代理城市编码
     */
    @TableField("city_code")
    private String cityCode;


    /**
     * 店铺ID
     */
    @TableField("shop_id")
    private Long shopId;
}
