package link.chengguo.orangemall.pms.vo;

import link.chengguo.orangemall.pms.entity.PmsComment;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import lombok.Data;

import java.util.List;

/**
 * 查询单个产品进行修改时返回的结果
 * https://github.com/orangemall on 2018/4/26.
 */
@Data
public class PmsProductResult extends PmsProductParam {
    //商品所选分类的父id
    private Long cateParentId;
    private int is_favorite;// 1 已收藏 2 未收藏
    private List<PmsProductAttr> pmsProductAttrList;
    private PmsProduct product;
    private List<PmsComment> pmsComments;


}
