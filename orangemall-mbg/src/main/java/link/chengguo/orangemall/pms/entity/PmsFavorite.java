package link.chengguo.orangemall.pms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
@Data
@TableName("pms_favorite")
public class PmsFavorite extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("add_time")
    private Date addTime;
    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;
    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;
    /**
     * 1 商品 2 文章 3 店铺 4 及赠品  7 学校
     */
    private Integer type;
    @TableField("obj_id")
    private Long objId;
    @TableField("member_id")
    private Long memberId;
    private String name;
    private String meno1;
    private String meno2;
    private String meno3;
    @TableField("meno4")
    private String meno4;
    @TableField("meno5")
    private String meno5;



}
