package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsProductConsult;

/**
 * <p>
 * 产品咨询表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsProductConsultMapper extends BaseMapper<PmsProductConsult> {

}
