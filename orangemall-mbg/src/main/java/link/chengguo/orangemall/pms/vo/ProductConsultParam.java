package link.chengguo.orangemall.pms.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/9/15.
 */
@Data
public class ProductConsultParam implements Serializable {
    private Long orderItemId;
    private Long goodsId;
    private String goodsName;
    private Integer score;
    private String images;
    private String textarea;

    private Long shopId;
    private String shopTextarea;
    private Integer shopStars;

}
