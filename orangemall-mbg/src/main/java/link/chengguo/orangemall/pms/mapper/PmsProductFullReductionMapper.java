package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsProductFullReduction;

/**
 * <p>
 * 产品满减表(只针对同商品) Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsProductFullReductionMapper extends BaseMapper<PmsProductFullReduction> {

}
