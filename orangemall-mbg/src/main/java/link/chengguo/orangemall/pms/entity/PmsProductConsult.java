package link.chengguo.orangemall.pms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 产品咨询表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
@TableName("pms_product_consult")
public class PmsProductConsult extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private OmsOrderItem omsOrderItem;
    /**
     * 咨询编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;


    /**
     * 订单包含的商品编号
     */
    @TableField("order_item_id")
    private Long orderItemId;
    /**
     * 商品ID
     */
    @TableField("goods_id")
    private Long goodsId;
    /**
     * 订单编号
     */
    @TableField("order_id")
    private Long orderId;
    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 咨询发布者会员编号(0：游客)
     */
    @TableField("member_id")
    private Long memberId;

    /**
     * 会员名称
     */
    @TableField("member_name")
    private String memberName;
    /**
     * 会员头像
     */
    @TableField("avatar")
    private String avatar;
    /**
     * 图片路径
     */
    @TableField("pic")
    private String pic;


    /**
     * 咨询发布者邮箱
     */
    private String email;

    /**
     * 咨询内容
     */
    @TableField("consult_content")
    private String consultContent;

    /**
     * 咨询添加时间
     */
    @TableField("consult_addtime")
    private Date consultAddtime;

    /**
     * 咨询回复内容
     */
    @TableField("consult_reply")
    private String consultReply;

    /**
     * 咨询回复时间
     */
    @TableField("consult_reply_time")
    private Date consultReplyTime;

    /**
     * 0表示不匿名 1表示匿名
     */
    private Boolean isanonymous;

    /**
     * 五星好评
     */
    private Integer stars;
    /**
     * 1：商品
     * 2：订单
     */
    private Integer type;


}
