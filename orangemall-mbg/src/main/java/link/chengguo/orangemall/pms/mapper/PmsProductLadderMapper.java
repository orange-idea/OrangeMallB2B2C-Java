package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsProductLadder;

/**
 * <p>
 * 产品阶梯价格表(只针对同商品) Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsProductLadderMapper extends BaseMapper<PmsProductLadder> {

}
