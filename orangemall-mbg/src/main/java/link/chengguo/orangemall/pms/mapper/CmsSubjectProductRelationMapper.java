package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.CmsSubjectProductRelation;

/**
 * <p>
 * 专题商品关系表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsSubjectProductRelationMapper extends BaseMapper<CmsSubjectProductRelation> {

}
