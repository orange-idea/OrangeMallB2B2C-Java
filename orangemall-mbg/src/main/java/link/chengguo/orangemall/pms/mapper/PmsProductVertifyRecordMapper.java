package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsProductVertifyRecord;

/**
 * <p>
 * 商品审核记录 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsProductVertifyRecordMapper extends BaseMapper<PmsProductVertifyRecord> {

}
