package link.chengguo.orangemall.pms.vo;


import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.sms.entity.SmsGroupMember;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 创建和修改商品时使用的参数
 * https://github.com/orangemall on 2018/4/26.
 */
@Data
public class PmsProductAndGroup extends PmsProduct {
    private Map<String, List<SmsGroupMember>> map;
    private String isGroup = "1"; //1 可以发起团购
    private String is_favorite;
}
