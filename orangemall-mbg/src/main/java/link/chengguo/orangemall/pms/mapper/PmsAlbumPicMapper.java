package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsAlbumPic;

/**
 * <p>
 * 画册图片表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsAlbumPicMapper extends BaseMapper<PmsAlbumPic> {

}
