package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsFavorite;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
public interface PmsFavoriteMapper extends BaseMapper<PmsFavorite> {

}
