package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsGiftsCategory;

/**
 * <p>
 * 帮助分类表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface PmsGiftsCategoryMapper extends BaseMapper<PmsGiftsCategory> {

}
