package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsGifts;

/**
 * <p>
 * 帮助表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface PmsGiftsMapper extends BaseMapper<PmsGifts> {

}
