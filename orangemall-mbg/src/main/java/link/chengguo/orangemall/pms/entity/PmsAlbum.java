package link.chengguo.orangemall.pms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 相册表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
@TableName("pms_album")
public class PmsAlbum extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;


    private String name;


    private String pic;

    private String type;

    private Integer sort;

    private String description;


}
