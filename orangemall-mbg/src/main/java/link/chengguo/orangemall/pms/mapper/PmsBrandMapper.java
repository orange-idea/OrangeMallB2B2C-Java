package link.chengguo.orangemall.pms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.pms.entity.PmsBrand;

/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface PmsBrandMapper extends BaseMapper<PmsBrand> {

}
