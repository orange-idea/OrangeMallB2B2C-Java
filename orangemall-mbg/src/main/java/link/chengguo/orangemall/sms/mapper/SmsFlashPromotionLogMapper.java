package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotionLog;

/**
 * <p>
 * 限时购通知记录 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsFlashPromotionLogMapper extends BaseMapper<SmsFlashPromotionLog> {

}
