package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsSignConfig;

/**
 * <p>
 * 签到配置表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsSignConfigMapper extends BaseMapper<SmsSignConfig> {

}
