package link.chengguo.orangemall.sms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 限时购场次表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
@TableName("sms_flash_promotion_session")
public class SmsFlashPromotionSession extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 店铺名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 场次名称
     */
    private String name;

    /**
     * 每日开始时间
     */
    @TableField("start_time")
    private String startTime;

    /**
     * 每日结束时间
     */
    @TableField("end_time")
    private String endTime;

    /**
     * 启用状态：0->不启用；1->启用
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    @TableField(exist = false)
    private String state;
    @TableField(exist = false)
    private long stop;

}
