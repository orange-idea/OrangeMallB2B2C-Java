package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsDetailedCommission;

/**
 * <p>
 * 分销佣金明细表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsDetailedCommissionMapper extends BaseMapper<SmsDetailedCommission> {

}
