package link.chengguo.orangemall.sms.entity;

import com.baomidou.mybatisplus.annotation.*;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 首页推荐专题表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
@TableName("sms_home_recommend_category")
public class SmsHomeRecommendCatetory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("category_ids")
    private String categoryIds;
    @TableField("category_name")
    private String categoryName;
    @TableField("category_des")
    private String categoryDes;
    @TableField("recommend_status")
    private Integer recommendStatus;
    @TableField("project_ids")
    private String projectIds;
    @TableField("sort")
    private Integer sort;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @Override
    public String toString() {
        return "SmsHomeRecommendCatetory{" +
                "id=" + id +
                ", category_ids=" + categoryIds +
                ", categoryName='" + categoryName + '\'' +
                ", categoryDes='" + categoryDes + '\'' +
                ", recommendStatus=" + recommendStatus +
                ", projectIds='" + projectIds + '\'' +
                ", sort=" + sort +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
