package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsFlashPromotion;

/**
 * <p>
 * 限时购表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsFlashPromotionMapper extends BaseMapper<SmsFlashPromotion> {

}
