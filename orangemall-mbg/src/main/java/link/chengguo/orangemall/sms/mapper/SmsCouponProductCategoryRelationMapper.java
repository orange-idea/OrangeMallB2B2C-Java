package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsCouponProductCategoryRelation;

/**
 * <p>
 * 优惠券和产品分类关系表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsCouponProductCategoryRelationMapper extends BaseMapper<SmsCouponProductCategoryRelation> {

}
