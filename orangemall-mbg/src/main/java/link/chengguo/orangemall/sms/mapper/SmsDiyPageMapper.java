package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsDiyPage;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-11-19
 */
public interface SmsDiyPageMapper extends BaseMapper<SmsDiyPage> {

}
