package link.chengguo.orangemall.sms.vo;

import io.swagger.annotations.Api;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendCatetory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * @Author: mxchen
 * @Date: 2020/8/28 21:39
 */
@Data
@Slf4j
@Api("产品专栏与商品关联")
@AllArgsConstructor
public class HomeProductCatetory {
    private SmsHomeRecommendCatetory smsHomeRecommendCatetory;
    private List<PmsProduct> waitRecommendProductList;
    private Map<String,List<PmsProduct>> allProductsMap;
}
