package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsExperience;

/**
 * <p>
 * 预约表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsExperienceMapper extends BaseMapper<SmsExperience> {

}
