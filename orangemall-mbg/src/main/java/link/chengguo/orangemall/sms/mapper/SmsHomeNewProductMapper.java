package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsHomeNewProduct;
import link.chengguo.orangemall.sms.vo.HomeProductAttr;

import java.util.List;

/**
 * <p>
 * 新鲜好物表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsHomeNewProductMapper extends BaseMapper<SmsHomeNewProduct> {
    List<HomeProductAttr> queryList();
}
