package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsSignRecord;

/**
 * <p>
 * 签到记录 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsSignRecordMapper extends BaseMapper<SmsSignRecord> {

}
