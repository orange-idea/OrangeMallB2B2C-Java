package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsRedPacket;

/**
 * <p>
 * 红包 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsRedPacketMapper extends BaseMapper<SmsRedPacket> {

}
