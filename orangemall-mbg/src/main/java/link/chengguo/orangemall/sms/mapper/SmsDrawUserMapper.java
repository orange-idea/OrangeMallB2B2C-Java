package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsDrawUser;

/**
 * <p>
 * 抽奖与用户关联表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsDrawUserMapper extends BaseMapper<SmsDrawUser> {

}
