package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsConfigure;

/**
 * <p>
 * 商品配置表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-18
 */
public interface SmsConfigureMapper extends BaseMapper<SmsConfigure> {

}
