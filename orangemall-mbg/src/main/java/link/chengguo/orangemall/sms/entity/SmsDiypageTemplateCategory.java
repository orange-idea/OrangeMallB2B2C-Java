package link.chengguo.orangemall.sms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author orangemall
 * @date 2019-12-04
 * 页面模版
 */
@Data
@TableName("sms_diypage_template_category")
public class SmsDiypageTemplateCategory implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("name")
    private String name;

    @TableField("store_id")
    private Integer storeId;

    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

}
