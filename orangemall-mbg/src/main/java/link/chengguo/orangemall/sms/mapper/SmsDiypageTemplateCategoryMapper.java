package link.chengguo.orangemall.sms.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsDiypageTemplateCategory;

/**
 * @author mallplus
 * @date 2019-12-04
 */
public interface SmsDiypageTemplateCategoryMapper extends BaseMapper<SmsDiypageTemplateCategory> {
}
