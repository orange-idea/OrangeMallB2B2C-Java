package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsPaimaiLog;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-15
 */
public interface SmsPaimaiLogMapper extends BaseMapper<SmsPaimaiLog> {

}
