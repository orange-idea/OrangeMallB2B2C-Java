package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsBasicMarking;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface SmsBasicMarkingMapper extends BaseMapper<SmsBasicMarking> {

}
