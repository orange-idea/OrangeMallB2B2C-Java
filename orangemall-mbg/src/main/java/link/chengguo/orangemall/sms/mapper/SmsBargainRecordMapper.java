package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsBargainRecord;

/**
 * <p>
 * 砍价免单记录表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsBargainRecordMapper extends BaseMapper<SmsBargainRecord> {

}
