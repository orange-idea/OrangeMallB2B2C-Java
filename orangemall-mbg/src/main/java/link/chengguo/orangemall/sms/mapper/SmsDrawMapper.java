package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsDraw;

/**
 * <p>
 * 一分钱抽奖 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsDrawMapper extends BaseMapper<SmsDraw> {

}
