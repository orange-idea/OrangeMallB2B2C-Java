package link.chengguo.orangemall.sms.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsContent;

/**
 * @author mallplus
 * @date 2019-12-07
 */
public interface SmsContentMapper extends BaseMapper<SmsContent> {
}
