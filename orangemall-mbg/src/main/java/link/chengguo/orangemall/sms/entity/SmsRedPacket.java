package link.chengguo.orangemall.sms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 红包
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
@TableName("sms_red_packet")
public class SmsRedPacket extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 店铺名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 红包编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 发红包的用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 红包金额
     */
    private BigDecimal amount;

    /**
     * 发红包日期
     */
    @TableField("send_date")
    private Date sendDate;

    /**
     * 红包总数
     */
    private Integer total;

    /**
     * 单个红包的金额
     */
    @TableField("unit_amount")
    private BigDecimal unitAmount;

    /**
     * 红包剩余个数
     */
    private Integer stock;

    /**
     * 红包类型
     */
    private Integer type;

    /**
     * 备注
     */
    private String note;

    @TableField(exist = false)
    private Integer status; // 1 已领取 2 未领取
    @TableField(exist = false)
    private BigDecimal reciveAmount;

}
