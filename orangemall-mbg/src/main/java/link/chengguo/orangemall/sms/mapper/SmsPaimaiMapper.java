package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsPaimai;

/**
 * <p>
 * 一分钱抽奖 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-19
 */
public interface SmsPaimaiMapper extends BaseMapper<SmsPaimai> {

}
