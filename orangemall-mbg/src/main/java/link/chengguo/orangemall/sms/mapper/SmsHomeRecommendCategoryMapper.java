package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendCatetory;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * 首页推荐专题表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsHomeRecommendCategoryMapper extends BaseMapper<SmsHomeRecommendCatetory > {

    Page<SmsHomeRecommendCatetory> customPageList(@Param("page") Page page1, @Param("entity") SmsHomeRecommendCatetory entity);
}
