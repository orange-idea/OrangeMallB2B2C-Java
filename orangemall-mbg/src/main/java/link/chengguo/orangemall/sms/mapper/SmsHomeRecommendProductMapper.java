package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendProduct;
import link.chengguo.orangemall.sms.vo.HomeProductAttr;

import java.util.List;

/**
 * <p>
 * 人气推荐商品表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface SmsHomeRecommendProductMapper extends BaseMapper<SmsHomeRecommendProduct> {
    List<HomeProductAttr> queryList();
}
