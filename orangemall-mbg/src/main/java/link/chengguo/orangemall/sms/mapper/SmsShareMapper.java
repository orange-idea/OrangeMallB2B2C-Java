package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsShare;

/**
 * <p>
 * 分享列表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface SmsShareMapper extends BaseMapper<SmsShare> {

}
