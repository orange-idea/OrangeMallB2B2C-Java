package link.chengguo.orangemall.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.sms.entity.SmsGroupActivity;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-12
 */
public interface SmsGroupActivityMapper extends BaseMapper<SmsGroupActivity> {

}
