package link.chengguo.orangemall.oms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;

/**
 * <p>
 * 订单中所包含的商品 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface OmsOrderItemMapper extends BaseMapper<OmsOrderItem> {

}
