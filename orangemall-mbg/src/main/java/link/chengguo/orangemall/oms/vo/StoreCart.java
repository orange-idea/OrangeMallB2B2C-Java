package link.chengguo.orangemall.oms.vo;


import link.chengguo.orangemall.oms.entity.OmsCartItem;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 购物车中选择规格的商品信息
 * https://github.com/orangemall on 2018/8/2.
 */
@Setter
@Getter
public class StoreCart {
    private List<OmsCartItem> list;
    private String storeName;
}
