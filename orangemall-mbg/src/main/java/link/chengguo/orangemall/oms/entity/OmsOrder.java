package link.chengguo.orangemall.oms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.utils.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Data
@TableName("oms_order")
public class OmsOrder extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private String payCode;
    @TableField(exist = false)
    private BigDecimal blance;
    @TableField(exist = false)
    private List<OmsOrderItem> orderItemList;
    @TableField(exist = false)
    private List<OmsOrderOperateHistory> historyList;

    @TableField(exist =false)
    private ShmsShopInfo shmsShopInfo;
    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 店铺名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 配送模式：详见后台参数设置下的配送模式
     */
    @TableField("send_type")
    private Integer sendType;

    /**
     * 购物类型：0-配送，需要地址，1-到店-不需要地址
     */
    @TableField("shopping_type")
    private Integer shoppingType;

    /**
     * 订单id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long pid;

    @TableField("member_id")
    private Long memberId;

    @TableField("coupon_id")
    private Long couponId;

    /**
     * 订单编号
     */
    @TableField("order_sn")
    private String orderSn;

    /**
     * 提交时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 用户帐号
     */
    @TableField("member_username")
    private String memberUsername;

    /**
     * 订单总金额
     */
    @TableField("total_amount")
    private BigDecimal totalAmount;

    /**
     * 应付金额（实际支付金额）
     */
    @TableField("pay_amount")
    private BigDecimal payAmount;

    /**
     * 运费金额
     */
    @TableField("freight_amount")
    private BigDecimal freightAmount;

    /**
     * 包装费用
     */
    @TableField("package_fee")
    private BigDecimal packageFee;

    /**
     * 期望送达时间
     */
    @TableField("expect_time")
    private String expectTime;

    /**
     * 促销优化金额（促销价、满减、阶梯价）
     */
    @TableField("promotion_amount")
    private BigDecimal promotionAmount;

    /**
     * 积分抵扣金额
     */
    @TableField("integration_amount")
    private BigDecimal integrationAmount;

    /**
     * 优惠券抵扣金额
     */
    @TableField("coupon_amount")
    private BigDecimal couponAmount;

    /**
     * 管理员后台调整订单使用的折扣金额
     */
    @TableField("discount_amount")
    private BigDecimal discountAmount;

    /**
     * 支付方式：0->未支付；1->支付宝；2->微信 3-余额支付
     */
    @TableField("pay_type")
    private Integer payType;
    @TableField(exist = false)
    private String payTypeName;

    /**
     * 订单来源：；1->小程序订单2 h5订单3PC订单  4 app
     */
    @TableField("source_type")
    private Integer sourceType;
    @TableField(exist = false)
    private String sourceTypeName;

    /**
     * 订单状态：0->待付款；1->待确认；2->已确认；3->已取消；4>已完成；5->已关闭；6->售后中
     *
     */
    private Integer status;

    /**
     * 订单状态名
     */
    @TableField(exist = false)
    private String statusName;
    /**
     * 支付状态：0->待付款；1->付款中；2->已付款（此时同步切换订单状态以及其他相应状态）；
     */
    @TableField("pay_status")
    private Integer payStatus;
    @TableField(exist = false)
    private String payStatusName;
    /**
     * 物流状态：0->未知；1->待发货，2->待收货，3->已收货
     */
    @TableField("express_status")
    private Integer expressStatus;
    @TableField(exist = false)
    private String expressStatusName;
    /**
     * 配送状态：0->未知；1->待抢单，2->待取货，3->已到店/取货中，4->配送中，5->已收货
     */
    @TableField("shipping_status")
    private Integer shippingStatus;
    @TableField(exist = false)
    private String shippingStatusName;

    /**
     * 拣货状态：1->待抢单；2->拣货中；3->拣货完成；
     */
    @TableField("picking_status")
    private Integer pickingStatus;

    @TableField(exist = false)
    private String pickingStatusName;
    /**
     * 核销状态：1->待核销；2->已核销
     */
    @TableField("verify_status")
    private Integer verifyStatus;
    @TableField(exist = false)
    private String verifyStatusName;

    /**
     * 售后状态：1->退款申请，2->同意退款，3->拒绝退款，4->取消申请，5->退款退货申请，6->同意退款退货，7->拒绝退款退货，8->确认收货
     * 1 -> 处理中
     */
    @TableField("after_sale_status")
    private Integer afterSaleStatus;
    @TableField(exist = false)
    private String afterSaleStatusName;

    /**
     * 订单类型：0->正常订单；1->秒杀订单 AllEnum
     */
    @TableField("order_type")
    private Integer orderType;
    /**
     * 物流公司(配送方式)
     */
    @TableField("delivery_company")
    private String deliveryCompany;

    /**
     * 物流单号
     */
    @TableField("delivery_sn")
    private String deliverySn;

    /**
     * 自动确认时间（天）
     */
    @TableField("auto_confirm_day")
    private Integer autoConfirmDay;

    /**
     * 可以获得的积分
     */
    private Integer integration;

    /**
     * 可以活动的成长值
     */
    private Integer growth;

    /**
     * 活动信息
     */
    @TableField("promotion_info")
    private String promotionInfo;

    /**
     * 发票类型：0->不开发票；1->电子发票；2->纸质发票
     */
    @TableField("bill_type")
    private Integer billType;

    /**
     * 发票抬头
     */
    @TableField("bill_header")
    private String billHeader;

    /**
     * 发票内容
     */
    @TableField("bill_content")
    private String billContent;

    /**
     * 收票人电话
     */
    @TableField("bill_receiver_phone")
    private String billReceiverPhone;

    /**
     * 收票人邮箱
     */
    @TableField("bill_receiver_email")
    private String billReceiverEmail;

    /**
     * 收货人姓名
     */
    @TableField("receiver_name")
    private String receiverName;

    /**
     * 收货人电话
     */
    @TableField("receiver_phone")
    private String receiverPhone;

    @TableField("receiver_id")
    private Long receiverId;


    /**
     * 收货人邮编
     */
    @TableField("receiver_post_code")
    private String receiverPostCode;

    /**
     * 省份/直辖市
     */
    @TableField("receiver_province")
    private String receiverProvince;

    /**
     * 城市
     */
    @TableField("receiver_city")
    private String receiverCity;

    /**
     * 区
     */
    @TableField("receiver_region")
    private String receiverRegion;

    /**
     * 详细地址
     */
    @TableField("receiver_detail_address")
    private String receiverDetailAddress;

    /**
     * 收货地址纬度
     */
    @TableField("receiver_lat")
    private String receiverLat;

    /**
     * 收货地址经度
     */
    @TableField("receiver_lng")
    private String receiverLng;

    /**
     * 订单备注
     */
    @TableField("note")
    private String note;

    /**
     * 确认收货状态：0->未确认；1->已确认
     */
    @TableField("confirm_status")
    private Integer confirmStatus;

    /**
     * 是否评论，1 未评论 2 已评论
     */
    @TableField("is_comment")
    private Integer isComment;
    @TableField(exist = false)
    private String isCommentName;

    /**
     * 删除状态：0->未删除；1->已删除
     */
    @TableField("delete_status")
    private Integer deleteStatus;
    /**
     * 下单时使用的积分
     */
    @TableField("use_integration")
    private Integer useIntegration;

    /**
     * 支付时间
     */
    @TableField("payment_time")
    private Date paymentTime;

    /**
     * 发货时间
     */
    @TableField("delivery_time")
    private Date deliveryTime;

    /**
     * 确认收货时间
     */
    @TableField("receive_time")
    private Date receiveTime;

    /**
     * 评价时间
     */
    @TableField("comment_time")
    private Date commentTime;

    /**
     * 修改时间
     */
    @TableField("modify_time")
    private Date modifyTime;

    /**
     * 核销时间
     */
    @TableField("verify_time")
    private Date verifyTime;

    /**
     * 商家接单时间
     */
    @TableField("take_time")
    private Date takeTime;

    @TableField("prepay_id")
    private String prepayId;

    @TableField("supply_id")
    private Long supplyId;

    @TableField("goods_id")
    private Long goodsId;

    @TableField("goods_name")
    private String goodsName;

    @TableField("school_id")
    private Long schoolId;
    @TableField("group_id")
    private Long groupId;
    @TableField("tax_type")
    private Integer taxType;    //	是否开发票 1=不发票 2=个人发票 3=公司发票
    @TableField("tax_content")
    private String taxContent;    //	0	发票内容
    @TableField("tax_code")
    private String taxCode;    //	税号
    @TableField("tax_title")
    private String taxTitle;    //	发票抬头
    @TableField("store_name")
    private String storeName;    //	税号

    @TableField(exist = false)
    private String startTime;    //搜索-开始时间
    @TableField(exist = false)
    private String endTime;     //搜索-结束时间


    //骑手相关信息
    @ApiModelProperty(name = "riderId",value = "骑手编号")
    @TableField("rider_id")
    private Long riderId;

    @ApiModelProperty(name = "riderName",value = "骑手名称")
    @TableField("rider_name")
    private String riderName;

    @ApiModelProperty(name = "riderPhone",value = "骑手电话")
    @TableField("rider_phone")
    private String riderPhone;

    @ApiModelProperty(name = "interfaceCode",value = "取消原因类型")
    @TableField("cancel_reason_id")
    private Integer cancelReasonId;

    @ApiModelProperty(name = "interfaceCode",value = "取消原因描述")
    @TableField("cancel_reason")
    private String cancelReason;
}
