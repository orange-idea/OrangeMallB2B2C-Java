package link.chengguo.orangemall.oms.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.oms.entity.OmsCompanyAddress;

/**
 * @author orangemall
 * @date 2019-12-07
 */
public interface OmsCompanyAddressMapper extends BaseMapper<OmsCompanyAddress> {
}
