package link.chengguo.orangemall.oms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.oms.entity.OmsPayments;

/**
 * <p>
 * 支付方式表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-14
 */
public interface OmsPaymentsMapper extends BaseMapper<OmsPayments> {

}
