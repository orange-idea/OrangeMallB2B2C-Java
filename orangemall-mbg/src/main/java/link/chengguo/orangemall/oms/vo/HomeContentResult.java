package link.chengguo.orangemall.oms.vo;


import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.pms.entity.PmsBrand;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeCategory;
import link.chengguo.orangemall.pms.entity.PmsSmallNaviconCategory;
import link.chengguo.orangemall.sms.entity.SmsCoupon;
import link.chengguo.orangemall.sms.entity.SmsHomeAdvertise;
import link.chengguo.orangemall.sms.vo.HomeFlashPromotion;
import link.chengguo.orangemall.sys.entity.SysStore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 首页内容返回信息封装
 * https://github.com/orangemall on 2019/1/28.
 */
@Getter
@Setter
public class HomeContentResult {
    List<PmsSmallNaviconCategory> navList;
    List<ActivityVo> activityList;
    //轮播广告
    private List<SmsHomeAdvertise> advertiseList;
    //推荐品牌
    private List<PmsBrand> brandList;
    //推荐品牌
    private List<SysStore> storeList;
    //当前秒杀场次
    //当前秒杀场次
    private HomeFlashPromotion homeFlashPromotion;
    //新品推荐
    private List<PmsProduct> newProductList;
    //人气推荐
    private List<PmsProduct> hotProductList;
    private List<PmsProduct> saleProductList;
    //推荐专题
    private List<CmsSubject> subjectList;
    private List<PmsProductAttributeCategory> cat_list;
    private List<SmsCoupon> couponList;

}
