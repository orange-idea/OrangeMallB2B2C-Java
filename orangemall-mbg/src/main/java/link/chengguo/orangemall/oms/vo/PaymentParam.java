package link.chengguo.orangemall.oms.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 生成订单时传入的参数
 * https://github.com/orangemall on 2018/8/30.
 */
@Data
public class PaymentParam {

    private  String url;
    private String trade_type;
    private String return_url;

    private BigDecimal money;
    private Long orderId;


}
