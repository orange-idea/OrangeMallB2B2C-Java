package link.chengguo.orangemall.oms.vo;

import link.chengguo.orangemall.oms.entity.OmsCartItem;
import lombok.Data;

import java.util.List;

/**
 * @Auther: yzb
 * @Date: 2019/12/26 14:51
 * @Description:购物车，所有商户的
 */
@Data
public class OmsCartResult{
    private Long shopId;
    private String shopName;
    private List<OmsCartItem> list;
}
