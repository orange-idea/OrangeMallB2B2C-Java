package link.chengguo.orangemall.oms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.oms.entity.OmsOrderSetting;

/**
 * <p>
 * 订单设置表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface OmsOrderSettingMapper extends BaseMapper<OmsOrderSetting> {

}
