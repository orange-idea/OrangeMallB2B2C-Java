package link.chengguo.orangemall.oms.mapper;


import link.chengguo.orangemall.oms.entity.OmsVerifyLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-01-07
*/
public interface OmsVerifyLogMapper extends BaseMapper<OmsVerifyLog> {
}
