package link.chengguo.orangemall.oms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import link.chengguo.orangemall.utils.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单退货申请
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Data
@NoArgsConstructor
@TableName("oms_order_return_apply")
public class OmsOrderReturnApply extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     *  城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 订单id
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 收货地址表id
     */
    @TableField("company_address_id")
    private Long companyAddressId;

    /**
     * 退货商品id
     */
    @TableField("order_item_id")
    private String orderItemId;

    /**
     * 订单编号
     */
    @TableField("order_sn")
    private String orderSn;

    /**
     * 申请时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 会员用户名
     */
    @TableField("member_username")
    private String memberUsername;

    /**
     * 退款金额
     */
    @TableField("return_amount")
    private BigDecimal returnAmount;

    /**
     * 退货人姓名
     */
    @TableField("return_name")
    private String returnName;

    /**
     * 退货人电话
     */
    @TableField("return_phone")
    private String returnPhone;

    /**
     * 申请状态：0->待处理；1->退货中；2->已完成；3->已拒绝
     */
    private Integer status;

    /**
     * 原因
     */
    private String reason;

    /**
     * 描述
     */
    private String description;

    /**
     * 凭证图片，以逗号隔开
     */
    @TableField("proof_pics")
    private String proofPics;

    /**
     * 处理时间
     */
    @TableField("handle_time")
    private Date handleTime;
    /**
     * 处理备注
     */
    @TableField("handle_note")
    private String handleNote;

    /**
     * 处理人员
     */
    @TableField("handle_man")
    private String handleMan;

    /**
     * 收货人
     */
    @TableField("receive_man")
    private String receiveMan;

    /**
     * 收货时间
     */
    @TableField("receive_time")
    private Date receiveTime;

    /**
     * 收货备注
     */
    @TableField("receive_note")
    private String receiveNote;

    public OmsOrderReturnApply(OmsOrder omsOrder) {
        this.cityCode = omsOrder.getCityCode();
        this.shopId = omsOrder.getShopId();
        this.orderId = omsOrder.getId();
        this.orderSn = omsOrder.getOrderSn();
        this.createTime = new Date();
        this.memberUsername = omsOrder.getMemberUsername();
        this.returnAmount = omsOrder.getPayAmount();
        this.returnName = omsOrder.getReceiverName();
        this.returnPhone = omsOrder.getReceiverPhone();
        this.status = omsOrder.getAfterSaleStatus();
    }

    @Override
    public String toString() {
        return "OmsOrderReturnApply{" +
                "id=" + id +
                ", cityCode='" + cityCode + '\'' +
                ", shopId=" + shopId +
                ", orderId=" + orderId +
                ", companyAddressId=" + companyAddressId +
                ", orderItemId='" + orderItemId + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", createTime=" + createTime +
                ", memberUsername='" + memberUsername + '\'' +
                ", returnAmount=" + returnAmount +
                ", returnName='" + returnName + '\'' +
                ", returnPhone='" + returnPhone + '\'' +
                ", status=" + status +
                ", reason='" + reason + '\'' +
                ", description='" + description + '\'' +
                ", proofPics='" + proofPics + '\'' +
                ", handleTime=" + handleTime +
                ", handleNote='" + handleNote + '\'' +
                ", handleMan='" + handleMan + '\'' +
                ", receiveMan='" + receiveMan + '\'' +
                ", receiveTime=" + receiveTime +
                ", receiveNote='" + receiveNote + '\'' +
                '}';
    }
}
