package link.chengguo.orangemall.oms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-01-07
 * 订单核销记录
 */
@Data
@TableName("oms_verify_log")
public class OmsVerifyLog implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("order_id")
    private Long orderId;

    @TableField("code")
    private String code;

    @TableField("money")
    private BigDecimal money;

    @TableField("type")
    private Integer type;


    @TableField("status")
    private Integer status;


    @TableField("verify_time")
    private Date verifyTime;


    @TableField("store_id")
    private Integer storeId;


    @TableField("city_code")
    private String cityCode;


    @TableField("shop_id")
    private Long shopId;


    @TableField("user_id")
    private Long userId;


    @TableField("user_name")
    private String userName;

    @TableField("content")
    private String content;

}
