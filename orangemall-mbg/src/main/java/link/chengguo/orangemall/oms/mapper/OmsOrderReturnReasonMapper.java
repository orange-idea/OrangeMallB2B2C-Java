package link.chengguo.orangemall.oms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.oms.entity.OmsOrderReturnReason;

/**
 * <p>
 * 退货原因表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface OmsOrderReturnReasonMapper extends BaseMapper<OmsOrderReturnReason> {

}
