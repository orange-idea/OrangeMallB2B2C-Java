package link.chengguo.orangemall.oms.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 生成订单时传入的参数
 * https://github.com/orangemall on 2018/8/30.
 */
@Data
public class PayParam {

    private String payCode;
    private BigDecimal payAmount;
    private BigDecimal balance;
    private Integer payment_type;
    private Long orderId;
    private PaymentParam params;

}
