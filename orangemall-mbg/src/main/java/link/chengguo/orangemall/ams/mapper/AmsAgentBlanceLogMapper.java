package link.chengguo.orangemall.ams.mapper;


import link.chengguo.orangemall.ams.entity.AmsAgentBlanceLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-20
*/
public interface AmsAgentBlanceLogMapper extends BaseMapper<AmsAgentBlanceLog> {
}
