package link.chengguo.orangemall.ams.model.result;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 代理抽佣等级表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class AmsAgentBrokerageLevelResult implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    /**
     * 等级名称
     */
    private String name;

    /**
     * 抽佣比例
     */
    private BigDecimal percent;

    /**
     * 说明信息
     */
    private String content;

    /**
     * 状态（0-停用，1-启用）
     */
    private Boolean status;

    /**
     * 排序
     */
    private Integer sort;

    private Date createTime;

    private Date updateTime;

    /**
     * 租户编号
     */
    private Integer storeId;

}
