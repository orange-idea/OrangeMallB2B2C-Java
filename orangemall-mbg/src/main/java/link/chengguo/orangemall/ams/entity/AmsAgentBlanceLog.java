package link.chengguo.orangemall.ams.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-20
 * 代理账户日志
 */
@Data
@TableName("ams_agent_blance_log")
public class AmsAgentBlanceLog implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("agent_id")
    private Long agentId;


    @TableField("city_code")
    private String cityCode;


    @TableField("price")
    private BigDecimal price;

    /**
     * 日志类型：0-订单结算，1-提现申请，2-申请通过，2-申请失败
     */
    @TableField("type")
    private Integer type;


    @TableField("note")
    private String note;


    @TableField("create_time")
    private Date createTime;


    @TableField("store_id")
    private Integer storeId;


}
