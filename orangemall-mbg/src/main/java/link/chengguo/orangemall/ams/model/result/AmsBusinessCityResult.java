package link.chengguo.orangemall.ams.model.result;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 店铺经营城市表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class AmsBusinessCityResult implements Serializable {

    private static final long serialVersionUID = 1L;


    private Integer id;

    /**
     * 开通服务城市地区码
     */
    private String businessCity;

    /**
     * 城市名
     */
    private String businessCityName;

    /**
     * 开通服务城市别名
     */
    private String businessCityAlias;

    /**
     * 城市拼音首字母
     */
    private String indexLetter;

    /**
     * 当前开通服务城市已开通的地区，地区码以字符串形式存放如：CN330502,CN220681
     */
    private String businessDistrict;

    /**
     * 租户编号
     */
    private Integer storeId;

}
