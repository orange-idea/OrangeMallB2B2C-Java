package link.chengguo.orangemall.ams.mapper;


import link.chengguo.orangemall.ams.entity.AmsSettings;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-12
*/
public interface AmsSettingsMapper extends BaseMapper<AmsSettings> {
}
