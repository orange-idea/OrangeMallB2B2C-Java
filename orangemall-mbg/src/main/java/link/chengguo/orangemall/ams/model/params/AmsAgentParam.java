package link.chengguo.orangemall.ams.model.params;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class AmsAgentParam implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    /**
     * 代理账号
     */
    private String account;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 代理名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 代理城市编号
     */
    private String cityCode;

    /**
     * 代理城市名称
     */
    private String cityName;

    /**
     * 代理地址
     */
    private String address;

    /**
     * 抽佣等级
     */
    private Integer brokerageLevel;

    /**
     * 抽佣比例：5000表示抽佣50%
     */
    private Integer brokeragePercent;

    private Date createTime;

    private Date updateTime;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态：1-启用，0-停用
     */
    private Integer status;

    /**
     *  搜索关键字
     */
    private String keyword;

    public String checkParam() {
        return null;
    }

}
