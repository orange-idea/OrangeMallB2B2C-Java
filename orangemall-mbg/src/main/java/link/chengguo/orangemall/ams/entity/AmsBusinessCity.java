package link.chengguo.orangemall.ams.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺经营城市表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
@TableName("ams_business_city")
public class AmsBusinessCity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 开通服务城市地区码
     */
    @TableField("business_city")
    private String businessCity;

    /**
     * 城市名
     */
    @TableField("business_city_name")
    private String businessCityName;

    /**
     * 开通服务城市别名
     */
    @TableField("business_city_alias")
    private String businessCityAlias;

    /**
     * 城市拼音首字母
     */
    @TableField("index_letter")
    private String indexLetter;

    /**
     * 当前开通服务城市已开通的地区，地区码以字符串形式存放如：CN330502,CN220681
     */
    @TableField("business_district")
    private String businessDistrict;

    /**
     * 租户编号
     */
    @TableField("store_id")
    private Integer storeId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBusinessCity() {
        return businessCity;
    }

    public void setBusinessCity(String businessCity) {
        this.businessCity = businessCity;
    }

    public String getBusinessCityName() {
        return businessCityName;
    }

    public void setBusinessCityName(String businessCityName) {
        this.businessCityName = businessCityName;
    }

    public String getBusinessCityAlias() {
        return businessCityAlias;
    }

    public void setBusinessCityAlias(String businessCityAlias) {
        this.businessCityAlias = businessCityAlias;
    }

    public String getIndexLetter() {
        return indexLetter;
    }

    public void setIndexLetter(String indexLetter) {
        this.indexLetter = indexLetter;
    }

    public String getBusinessDistrict() {
        return businessDistrict;
    }

    public void setBusinessDistrict(String businessDistrict) {
        this.businessDistrict = businessDistrict;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    @Override
    public String toString() {
        return "AmsBusinessCity{" +
        "id=" + id +
        ", businessCity=" + businessCity +
        ", businessCityName=" + businessCityName +
        ", businessCityAlias=" + businessCityAlias +
        ", indexLetter=" + indexLetter +
        ", businessDistrict=" + businessDistrict +
        ", storeId=" + storeId +
        "}";
    }
}
