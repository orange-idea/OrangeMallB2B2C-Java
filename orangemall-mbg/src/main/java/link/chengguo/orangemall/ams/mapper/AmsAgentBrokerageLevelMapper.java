package link.chengguo.orangemall.ams.mapper;

import link.chengguo.orangemall.ams.entity.AmsAgentBrokerageLevel;
import link.chengguo.orangemall.ams.model.params.AmsAgentBrokerageLevelParam;
import link.chengguo.orangemall.ams.model.result.AmsAgentBrokerageLevelResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代理抽佣等级表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface AmsAgentBrokerageLevelMapper extends BaseMapper<AmsAgentBrokerageLevel> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<AmsAgentBrokerageLevelResult> customList(@Param("paramCondition") AmsAgentBrokerageLevelParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") AmsAgentBrokerageLevelParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<AmsAgentBrokerageLevelResult> customPageList(@Param("page") Page page, @Param("paramCondition") AmsAgentBrokerageLevelParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") AmsAgentBrokerageLevelParam paramCondition);

}
