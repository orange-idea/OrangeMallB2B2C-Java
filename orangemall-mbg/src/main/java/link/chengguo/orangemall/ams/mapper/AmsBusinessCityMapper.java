package link.chengguo.orangemall.ams.mapper;

import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.model.params.AmsBusinessCityParam;
import link.chengguo.orangemall.ams.model.result.AmsBusinessCityResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 店铺经营城市表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface AmsBusinessCityMapper extends BaseMapper<AmsBusinessCity> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<AmsBusinessCityResult> customList(@Param("paramCondition") AmsBusinessCityParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") AmsBusinessCityParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<AmsBusinessCityResult> customPageList(@Param("page") Page page, @Param("paramCondition") AmsBusinessCityParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") AmsBusinessCityParam paramCondition);

}
