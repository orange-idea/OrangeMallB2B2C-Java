package link.chengguo.orangemall.ams.mapper;

import link.chengguo.orangemall.ams.entity.AmsAgent;
import link.chengguo.orangemall.ams.model.params.AmsAgentParam;
import link.chengguo.orangemall.ams.model.result.AmsAgentResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface AmsAgentMapper extends BaseMapper<AmsAgent> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<AmsAgentResult> customList(@Param("paramCondition") AmsAgentParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") AmsAgentParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<AmsAgentResult> customPageList(@Param("page") Page page, @Param("paramCondition") AmsAgentParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") AmsAgentParam paramCondition);

}
