package link.chengguo.orangemall.ams.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
@TableName("ams_agent")
public class AmsAgent implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 代理账号
     */
    @TableField("account")
    private String account;

    /**
     * 账号余额
     */
    @TableField("balance")
    private BigDecimal balance;

    /**
     * 账号余额-冻结
     */
    @TableField("balance_frozen")
    private BigDecimal balanceFrozen;

    /**
     * 账号余额-可提
     */
    @TableField("balance_withdraw")
    private BigDecimal balanceWithdraw;

    /**
     * 登录密码
     */
    @TableField("password")
    private String password;


    /**
     * 代理名称
     */
    @TableField("name")
    private String name;

    /**
     * 联系电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 代理城市编号
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 代理城市名称
     */
    @TableField("city_name")
    private String cityName;

    /**
     * 代理地址
     */
    @TableField("address")
    private String address;

    /**
     * 抽佣等级
     */
    @TableField("brokerage_level")
    private Integer brokerageLevel;

    /**
     * 抽佣比例：50表示抽佣50%
     */
    @TableField("brokerage_percent")
    private BigDecimal brokeragePercent;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 状态：1-启用，0-停用
     */
    @TableField("status")
    private Integer status;


    /**
     * 登录密码,旧的
     */
    @TableField(exist = false)
    private String oldPassword;


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getBrokerageLevel() {
        return brokerageLevel;
    }

    public void setBrokerageLevel(Integer brokerageLevel) {
        this.brokerageLevel = brokerageLevel;
    }

    public BigDecimal getBrokeragePercent() {
        return brokeragePercent;
    }

    public void setBrokeragePercent(BigDecimal brokeragePercent) {
        this.brokeragePercent = brokeragePercent;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AmsAgent{" +
        "id=" + id +
        ", account=" + account +
        ", password=" + password +
        ", name=" + name +
        ", phone=" + phone +
        ", cityCode=" + cityCode +
        ", cityName=" + cityName +
        ", address=" + address +
        ", brokerageLevel=" + brokerageLevel +
        ", brokeragePercent=" + brokeragePercent +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", sort=" + sort +
        ", status=" + status +
        "}";
    }
}
