package link.chengguo.orangemall.ams.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-12
 * 代理配置
 */
@Data
@TableName("ams_settings")
public class AmsSettings implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 配送费用
     */
    @TableField("express_fee")
    private BigDecimal expressFee;

    /**
     * 订单起送价格
     */
    @TableField("starting_price")
    private BigDecimal startingPrice;

    /**
     * 订单最低免配送金额
     */
    @TableField("free_express_price")
    private BigDecimal freeExpressPrice;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;


}
