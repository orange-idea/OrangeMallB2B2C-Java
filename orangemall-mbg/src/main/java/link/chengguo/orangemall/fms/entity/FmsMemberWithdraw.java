package link.chengguo.orangemall.fms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员提现
 */
@Data
@TableName("fms_member_withdraw")
public class FmsMemberWithdraw implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("member_id")
    private Long memberId;

    @TableField("member_name")
    private String memberName;

    @TableField("member_phone")
    private String memberPhone;


    @TableField("money")
    private BigDecimal money;


    /**
     * 转账方式 0-线上，1-线下
     */
    @TableField("transfer_method")
    private Integer transferMethod;

    /**
     * 转账结果 0-待处理，1-转账成功，2-转账失败（拒绝）
     */
    @TableField("transfer_result")
    private Integer transferResult;


    @TableField("tranfer_desc")
    private String tranferDesc;

    /**
     *  提现方式  0->支付宝；1->微信；2->银联,  3-线下
     */
    @TableField("account_type")
    private Integer accountType;


    /**
     * 账号, 银行卡号等
     */
    @TableField("withdraw_account")
    private String withdrawAccount;

    /**
     * 名称，
     * 支付宝-实名
     * 微信-昵称
     * 银联-实名，持卡人
     */
    @TableField("withdraw_name")
    private String withdrawName;


    @TableField("bank_name")
    private String bankName;


    @TableField("bank_branch_name")
    private String bankBranchName;


    @TableField("create_time")
    private Date createTime;


    @TableField("update_time")
    private Date updateTime;


    @TableField("store_id")
    private Integer storeId;


    /**
     *  查询-开始时间
     */
    @TableField(exist = false)
    private String startTime;

    /**
     * 查询-结束时间
     */
    @TableField(exist = false)
    private String endTime;

    /**
     * 查询-关键字
     */
    @TableField(exist = false)
    private String keyword;


}
