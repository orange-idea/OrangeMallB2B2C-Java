package link.chengguo.orangemall.fms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
    import java.math.BigDecimal;
import java.io.Serializable;

/**
* @author yzb
* @date 2020-02-10
会员充值日统计
*/
@Data
@TableName("fms_member_recharge_day")
public class FmsMemberRechargeDay implements Serializable {


            @TableId(value = "id", type = IdType.AUTO)
        private Long id;


            @TableField( "wait_pay_money")
        private BigDecimal waitPayMoney;


            @TableField( "wx_applet_money")
        private BigDecimal wxAppletMoney;


            @TableField( "wx_app_money")
        private BigDecimal wxAppMoney;


            @TableField( "ali_money")
        private BigDecimal aliMoney;


            @TableField( "code_money")
        private BigDecimal codeMoney;


            @TableField( "platform_money")
        private BigDecimal platformMoney;


            @TableField( "total_money")
        private BigDecimal totalMoney;


            @TableField( "create_time")
        private Date createTime;


            @TableField( "store_id")
        private Integer storeId;


}
