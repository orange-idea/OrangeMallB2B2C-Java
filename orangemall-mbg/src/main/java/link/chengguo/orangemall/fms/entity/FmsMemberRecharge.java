package link.chengguo.orangemall.fms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员充值
 */
@Data
@TableName("fms_member_recharge")
public class FmsMemberRecharge implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("member_id")
    private Long memberId;

    @TableField("member_name")
    private String memberName;

    @TableField("member_phone")
    private String memberPhone;

    @TableField("order_sn")
    private String orderSn;

    @TableField("money")
    private BigDecimal money;

    /**
     * 充值方式  0-未支付；1-微信小程序；2-微信App,  3-支付宝，4-扫码充值，5-后台划拨充值
     *
     */
    @TableField("recharge_type")
    private Integer rechargeType;

    /**
     * 充值状态  0-未支付，1-充值成功
     */
    @TableField("recharge_status")
    private Integer rechargeStatus;

    @TableField("content")
    private String content;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("store_id")
    private Integer storeId;

    /**
     *  查询-开始时间
     */
    @TableField(exist = false)
    private String startTime;

    /**
     * 查询-结束时间
     */
    @TableField(exist = false)
    private String endTime;

    /**
     * 查询-关键字
     */
    @TableField(exist = false)
    private String keyword;

}
