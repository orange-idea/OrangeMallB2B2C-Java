package link.chengguo.orangemall.fms.mapper;


import link.chengguo.orangemall.fms.entity.FmsShopWithdrawAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-10
*/
public interface FmsShopWithdrawAccountMapper extends BaseMapper<FmsShopWithdrawAccount> {
}
