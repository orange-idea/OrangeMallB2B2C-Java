package link.chengguo.orangemall.fms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-10
 * 商家提现账号
 */
@Data
@TableName("fms_shop_withdraw_account")
public class FmsShopWithdrawAccount implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("shop_id")
    private Long shopId;


    @TableField("account_type")
    private Integer accountType;


    @TableField("withdraw_account")
    private String withdrawAccount;


    @TableField("withdraw_name")
    private String withdrawName;


    @TableField("bank_name")
    private String bankName;


    @TableField("bank_branch_name")
    private String bankBranchName;


    @TableField("create_time")
    private Date createTime;


    @TableField("update_time")
    private Date updateTime;


    @TableField("store_id")
    private Integer storeId;


}
