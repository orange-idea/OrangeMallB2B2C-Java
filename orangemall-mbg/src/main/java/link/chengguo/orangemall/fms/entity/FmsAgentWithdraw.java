package link.chengguo.orangemall.fms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-10
 * 代理提现
 */
@Data
@TableName("fms_agent_withdraw")
public class FmsAgentWithdraw implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    @TableField("agent_id")
    private Long agentId;

    @TableField("city_code")
    private String cityCode;

    @TableField("city_name")
    private String cityName;

    @TableField("agent_name")
    private String agentName;

    @TableField("agent_phone")
    private String agentPhone;

    @TableField("money")
    private BigDecimal money;

    @TableField("transfer_method")
    private Integer transferMethod;

    @TableField("transfer_result")
    private Integer transferResult;

    @TableField("tranfer_desc")
    private String tranferDesc;

    @TableField("account_type")
    private Integer accountType;

    @TableField("withdraw_account")
    private String withdrawAccount;

    @TableField("withdraw_name")
    private String withdrawName;

    @TableField("bank_name")
    private String bankName;

    @TableField("bank_branch_name")
    private String bankBranchName;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("store_id")
    private Integer storeId;

    /**
     *  查询-开始时间
     */
    @TableField(exist = false)
    private String startTime;

    /**
     * 查询-结束时间
     */
    @TableField(exist = false)
    private String endTime;

    /**
     * 查询-关键字
     */
    @TableField(exist = false)
    private String keyword;


}
