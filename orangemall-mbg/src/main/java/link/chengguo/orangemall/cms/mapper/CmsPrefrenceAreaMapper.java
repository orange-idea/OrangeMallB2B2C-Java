package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsPrefrenceArea;

/**
 * <p>
 * 优选专区 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsPrefrenceAreaMapper extends BaseMapper<CmsPrefrenceArea> {

}
