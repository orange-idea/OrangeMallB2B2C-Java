package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsFavorite;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-06-15
 */
public interface CmsFavoriteMapper extends BaseMapper<CmsFavorite> {

}
