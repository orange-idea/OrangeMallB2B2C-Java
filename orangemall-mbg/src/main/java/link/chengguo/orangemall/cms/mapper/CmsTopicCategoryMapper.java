package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsTopicCategory;

/**
 * <p>
 * 话题分类表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsTopicCategoryMapper extends BaseMapper<CmsTopicCategory> {

}
