package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsPlatformInfoCategory;
import link.chengguo.orangemall.cms.model.params.CmsPlatformInfoCategoryParam;
import link.chengguo.orangemall.cms.model.result.CmsPlatformInfoCategoryResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 平台说明类型表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsPlatformInfoCategoryMapper extends BaseMapper<CmsPlatformInfoCategory> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsPlatformInfoCategoryResult> customList(@Param("paramCondition") CmsPlatformInfoCategoryParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsPlatformInfoCategoryParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsPlatformInfoCategoryResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsPlatformInfoCategoryParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsPlatformInfoCategoryParam paramCondition);

}
