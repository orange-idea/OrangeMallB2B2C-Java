package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsAboutUs;
import link.chengguo.orangemall.cms.model.params.CmsAboutUsParam;
import link.chengguo.orangemall.cms.model.result.CmsAboutUsResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 关于我们表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsAboutUsMapper extends BaseMapper<CmsAboutUs> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsAboutUsResult> customList(@Param("paramCondition") CmsAboutUsParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsAboutUsParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsAboutUsResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsAboutUsParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsAboutUsParam paramCondition);

}
