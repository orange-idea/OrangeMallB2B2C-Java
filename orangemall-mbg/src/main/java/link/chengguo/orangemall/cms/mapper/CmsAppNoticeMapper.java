package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsAppNotice;
import link.chengguo.orangemall.cms.model.params.CmsAppNoticeParam;
import link.chengguo.orangemall.cms.model.result.CmsAppNoticeResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 公告表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsAppNoticeMapper extends BaseMapper<CmsAppNotice> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsAppNoticeResult> customList(@Param("paramCondition") CmsAppNoticeParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsAppNoticeParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsAppNoticeResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsAppNoticeParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsAppNoticeParam paramCondition);

}
