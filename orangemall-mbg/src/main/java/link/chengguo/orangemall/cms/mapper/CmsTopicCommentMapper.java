package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsTopicComment;

/**
 * <p>
 * 专题评论表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsTopicCommentMapper extends BaseMapper<CmsTopicComment> {

}
