package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsFeedback;
import link.chengguo.orangemall.cms.model.params.CmsFeedbackParam;
import link.chengguo.orangemall.cms.model.result.CmsFeedbackResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 意见反馈内容表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsFeedbackMapper extends BaseMapper<CmsFeedback> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsFeedbackResult> customList(@Param("paramCondition") CmsFeedbackParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsFeedbackParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsFeedbackResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsFeedbackParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsFeedbackParam paramCondition);

}
