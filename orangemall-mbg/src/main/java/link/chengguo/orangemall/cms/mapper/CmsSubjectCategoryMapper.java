package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsSubjectCategory;

/**
 * <p>
 * 专题分类表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsSubjectCategoryMapper extends BaseMapper<CmsSubjectCategory> {

}
