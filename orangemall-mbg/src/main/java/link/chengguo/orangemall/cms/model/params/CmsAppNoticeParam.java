package link.chengguo.orangemall.cms.model.params;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 公告表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsAppNoticeParam implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 公告id
     */
    private Integer noticeId;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 封面
     */
    private String pic;

    /**
     * 摘要
     */
    private String summary;

    /**
     * 详情
     */
    private String content;

    /**
     * 发布者
     */
    private String createUser;

    /**
     * 是否阅读
     */
    private Integer ifReaded;

    /**
     * 状态
     */
    private String status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否删除
     */
    private Integer ifDel;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 删除时间
     */
    private Date deleteTime;

    public String checkParam() {
        return null;
    }

}
