package link.chengguo.orangemall.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.cms.entity.CmsSubject;
import link.chengguo.orangemall.vo.timeline.Timeline;
import link.chengguo.orangemall.vo.timeline.TimelinePost;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 专题表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface CmsSubjectMapper extends BaseMapper<CmsSubject> {

    int countByToday(Long memberId);

    List<TimelinePost> listTimelinePost(@Param("year") Integer year, @Param("month") Integer month);

    List<Timeline> listTimeline();
}
