package link.chengguo.orangemall.cms.model.result;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 平台说明类型表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsPlatformInfoCategoryResult implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 类型id
     */
    private Integer id;

    /**
     * 类型名称
     */
    private String name;

    /**
     * 类型类别
     */
    private Integer helpType;

    /**
     * 上级id
     */
    private Integer parentId;

    /**
     * 状态
     */
    private String status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 是否删除
     */
    private Integer ifDel;

    /**
     * 删除时间
     */
    private Date deleteTime;

}
