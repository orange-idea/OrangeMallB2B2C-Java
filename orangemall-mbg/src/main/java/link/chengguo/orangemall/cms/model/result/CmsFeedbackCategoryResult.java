package link.chengguo.orangemall.cms.model.result;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 意见反馈类型表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsFeedbackCategoryResult implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 类型id
     */
    private Integer id;

    /**
     * 类型名称
     */
    private String name;

    /**
     * 状态
     */
    private String status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否删除
     */
    private Integer ifDel;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 删除时间
     */
    private Date deleteTime;

}
