package link.chengguo.orangemall.cms.model.params;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 平台说明内容表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsPlatformInfoParam implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 内容id
     */
    private Integer id;

    /**
     * 类型ID
     */
    private Integer categoryId;

    /**
     * 标题
     */
    private String title;

    /**
     * 图片信息
     */
    private String pic;

    /**
     * 摘要说明
     */
    private String summary;

    /**
     * 详情
     */
    private String content;

    /**
     * 版本信息
     */
    private String version;

    /**
     * 状态
     */
    private String status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否删除
     */
    private Integer ifDel;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 删除时间
     */
    private Date deleteTime;

    public String checkParam() {
        return null;
    }

}
