package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsContactUs;
import link.chengguo.orangemall.cms.model.params.CmsContactUsParam;
import link.chengguo.orangemall.cms.model.result.CmsContactUsResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 联系我们表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsContactUsMapper extends BaseMapper<CmsContactUs> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsContactUsResult> customList(@Param("paramCondition") CmsContactUsParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsContactUsParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsContactUsResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsContactUsParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsContactUsParam paramCondition);

}
