package link.chengguo.orangemall.cms.model.result;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 意见反馈内容表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsFeedbackResult implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 内容id
     */
    private Integer id;

    /**
     * 类型id
     */
    private Integer categoryId;

    /**
     * 标题
     */
    private String title;

    /**
     * 图片
     */
    private String pic;

    /**
     * 反馈内容
     */
    private String content;

    /**
     * 用户id
     */
    private Integer memberId;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 是否删除
     */
    private Integer ifDel;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 删除时间
     */
    private Date deleteTime;

}
