package link.chengguo.orangemall.cms.model.params;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 联系我们表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsContactUsParam implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id,主键
     */
    private Integer contactId;

    /**
     * 公司名称
     */
    private String name;

    /**
     * 公司地址
     */
    private String address;

    /**
     * LOGO
     */
    private String logo;

    /**
     * 官方网址
     */
    private String website;

    /**
     * 客服电话
     */
    private String servicePhone;

    /**
     * 微信号码
     */
    private String wechatNumber;

    /**
     * QQ号码
     */
    private String qqNumber;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 启用状态
     */
    private String status;

    public String checkParam() {
        return null;
    }

}
