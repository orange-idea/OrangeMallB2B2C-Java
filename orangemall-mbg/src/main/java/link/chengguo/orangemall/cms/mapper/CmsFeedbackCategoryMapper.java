package link.chengguo.orangemall.cms.mapper;

import link.chengguo.orangemall.cms.entity.CmsFeedbackCategory;
import link.chengguo.orangemall.cms.model.params.CmsFeedbackCategoryParam;
import link.chengguo.orangemall.cms.model.result.CmsFeedbackCategoryResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 意见反馈类型表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
public interface CmsFeedbackCategoryMapper extends BaseMapper<CmsFeedbackCategory> {

    /**
     * 获取列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<CmsFeedbackCategoryResult> customList(@Param("paramCondition") CmsFeedbackCategoryParam paramCondition);

    /**
     * 获取map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    List<Map<String, Object>> customMapList(@Param("paramCondition") CmsFeedbackCategoryParam paramCondition);

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<CmsFeedbackCategoryResult> customPageList(@Param("page") Page page, @Param("paramCondition") CmsFeedbackCategoryParam paramCondition);

    /**
     * 获取分页map列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<Map<String, Object>> customPageMapList(@Param("page") Page page, @Param("paramCondition") CmsFeedbackCategoryParam paramCondition);

}
