package link.chengguo.orangemall.cms.model.params;

import lombok.Data;
import java.util.Date;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 关于我们表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
public class CmsAboutUsParam implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 关于我们id
     */
    private Integer aboutId;

    /**
     * 标题
     */
    private String title;

    /**
     * LOGO
     */
    private String logo;

    /**
     * 内容
     */
    private String content;

    /**
     * 客服电话
     */
    private String servicePhone;

    /**
     * 版权信息
     */
    private String copyright;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 启用状态
     */
    private String status;

    public String checkParam() {
        return null;
    }

}
