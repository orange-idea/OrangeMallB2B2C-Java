package link.chengguo.orangemall.cms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 关于我们表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
@TableName("cms_about_us")
public class CmsAboutUs implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 关于我们id
     */
    @TableId(value = "about_id", type = IdType.AUTO)
    private Integer aboutId;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * LOGO
     */
    @TableField("logo")
    private String logo;

    /**
     * 内容
     */
    @TableField("content")
    private String content;

    /**
     * 客服电话
     */
    @TableField("service_phone")
    private String servicePhone;

    /**
     * 版权信息
     */
    @TableField("copyright")
    private String copyright;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 添加时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 启用状态
     */
    @TableField("status")
    private String status;


    public Integer getAboutId() {
        return aboutId;
    }

    public void setAboutId(Integer aboutId) {
        this.aboutId = aboutId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CmsAboutUs{" +
        "aboutId=" + aboutId +
        ", title=" + title +
        ", logo=" + logo +
        ", content=" + content +
        ", servicePhone=" + servicePhone +
        ", copyright=" + copyright +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", status=" + status +
        "}";
    }
}
