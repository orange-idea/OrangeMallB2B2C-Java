package link.chengguo.orangemall.cms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 意见反馈内容表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
@TableName("cms_feedback")
public class CmsFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 内容id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 类型id
     */
    @TableField("category_id")
    private Integer categoryId;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 图片
     */
    @TableField("pic")
    private String pic;

    /**
     * 反馈内容
     */
    @TableField("content")
    private String content;

    /**
     * 用户id
     */
    @TableField("member_id")
    private Long memberId;

    /**
     * 联系方式
     */
    @TableField("phone")
    private String phone;

    /**
     * 是否删除
     */
    @TableField("if_del")
    private Integer ifDel;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private Date deleteTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getIfDel() {
        return ifDel;
    }

    public void setIfDel(Integer ifDel) {
        this.ifDel = ifDel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    @Override
    public String toString() {
        return "CmsFeedback{" +
        "id=" + id +
        ", categoryId=" + categoryId +
        ", title=" + title +
        ", pic=" + pic +
        ", content=" + content +
        ", memberId=" + memberId +
        ", phone=" + phone +
        ", ifDel=" + ifDel +
        ", createTime=" + createTime +
        ", deleteTime=" + deleteTime +
        "}";
    }
}
