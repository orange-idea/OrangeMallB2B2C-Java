package link.chengguo.orangemall.cms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 联系我们表
 * </p>
 *
 * @author yzb
 * @since 2019-12-21
 */
@Data
@TableName("cms_contact_us")
public class CmsContactUs implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id,主键
     */
    @TableId(value = "contact_id", type = IdType.AUTO)
    private Integer contactId;

    /**
     * 公司名称
     */
    @TableField("name")
    private String name;

    /**
     * 公司地址
     */
    @TableField("address")
    private String address;

    /**
     * LOGO
     */
    @TableField("logo")
    private String logo;

    /**
     * 官方网址
     */
    @TableField("website")
    private String website;

    /**
     * 客服电话
     */
    @TableField("service_phone")
    private String servicePhone;

    /**
     * 微信号码
     */
    @TableField("wechat_number")
    private String wechatNumber;

    /**
     * QQ号码
     */
    @TableField("qq_number")
    private String qqNumber;

    /**
     * 修改时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
     * 添加时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 启用状态
     */
    @TableField("status")
    private String status;


    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public String getWechatNumber() {
        return wechatNumber;
    }

    public void setWechatNumber(String wechatNumber) {
        this.wechatNumber = wechatNumber;
    }

    public String getQqNumber() {
        return qqNumber;
    }

    public void setQqNumber(String qqNumber) {
        this.qqNumber = qqNumber;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CmsContactUs{" +
        "contactId=" + contactId +
        ", name=" + name +
        ", address=" + address +
        ", logo=" + logo +
        ", website=" + website +
        ", servicePhone=" + servicePhone +
        ", wechatNumber=" + wechatNumber +
        ", qqNumber=" + qqNumber +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", status=" + status +
        "}";
    }
}
