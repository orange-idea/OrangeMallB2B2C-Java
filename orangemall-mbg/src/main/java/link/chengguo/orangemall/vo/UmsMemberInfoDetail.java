package link.chengguo.orangemall.vo;

import link.chengguo.orangemall.sms.entity.SmsCouponHistory;
import link.chengguo.orangemall.ums.entity.UmsMember;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Data
public class UmsMemberInfoDetail implements Serializable {

    private UmsMember member;
    private List<SmsCouponHistory> histories;
}
