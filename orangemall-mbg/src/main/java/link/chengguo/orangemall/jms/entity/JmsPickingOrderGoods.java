package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货商品
 */
@Data
@TableName("jms_picking_order_goods")
public class JmsPickingOrderGoods implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("order_id")
    private Long orderId;

    @TableField("order_detail_id")
    private Long orderDetailId;

    @TableField("picking_order_id")
    private Long pickingOrderId;

    @TableField("goods_type_id")
    private Integer goodsTypeId;

    @TableField("goods_type_name")
    private String goodsTypeName;

    @TableField("goods_id")
    private Long goodsId;

    @TableField("goods_name")
    private String goodsName;

    @TableField("goods_img")
    private String goodsImg;

    @TableField("goods_number")
    private Integer goodsNumber;

    @TableField("goods_sku")
    private String goodsSku;

    @TableField("goods_price")
    private String goodsPrice;

    @TableField("barcode")
    private String barcode;

    @TableField("status")
    private Integer status;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("finish_time")
    private Date finishTime;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;

    /**
     * 合并后相同商品合并显示订单ID
     */
    @TableField(exist = false)
    private String pickingOrderIds;

}
