package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import java.io.Serializable;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货订单跟踪
 */
@Data
@TableName("jms_order_track")
public class JmsOrderTrack implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 外部订单编号
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 外部订单编号
     */
    @TableField("expect_time")
    private String expectTime;

    /**
     * 商品分类数
     */
    @TableField("goods_type_number")
    private Integer goodsTypeNumber;

    /**
     * 已拣完分类数
     */
    @TableField("finish_goods_type_number")
    private Integer finishGoodsTypeNumber;

    /**
     * 商品总数量
     */
    @TableField("goods_number")
    private Integer goodsNumber;

    /**
     * 已完成数量
     */
    @TableField("finish_goods_number")
    private Integer finishGoodsNumber;

    /**
     * 拣货用户数量
     */
    @TableField("user_number")
    private Integer userNumber;

    /**
     * 完成拣货用户数量
     */
    @TableField("finish_user_number")
    private Integer finishUserNumber;

    /**
     * 拣货状态：0-待抢单，1-拣货中，2-完成
     */
    @TableField("status")
    private Integer status;


    /**
     * 商品分类名称集合
     */
    @TableField("goods_type_names")
    private String goodsTypeNames;

    /**
     * 商品分类ID集合
     */
    @TableField("goods_type_ids")
    private String goodsTypeIds;

    /**
     * 花费时间
     */
    @TableField("take_time")
    private Integer takeTime;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("finish_time")
    private Date finishTime;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;

    /**
     * 搜索-关键字
     */
    @TableField(exist = false)
    private String keyword;

    /**
     * 订单包含的类型
     */
    @TableField(exist = false)
    private List<JmsGoodsTypeTrack> goodsTypeTracks;

    /**
     * 商品详情ID集合
     */
    @TableField(exist = false)
    private String orderItemIds;

}
