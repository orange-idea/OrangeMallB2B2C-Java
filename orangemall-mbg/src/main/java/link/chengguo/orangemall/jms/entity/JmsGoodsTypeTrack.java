package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-27
 * 拣货商品分类跟踪
 */
@Data
@TableName("jms_goods_type_track")
public class JmsGoodsTypeTrack implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("order_id")
    private Long orderId;

    @TableField("order_item_ids")
    private String orderItemIds;

    @TableField("goods_type_id")
    private Long goodsTypeId;

    @TableField("goods_type_name")
    private String goodsTypeName;

    @TableField("goods_number")
    private Integer goodsNumber;

    @TableField("picking_order_id")
    private Long pickingOrderId;

    @TableField("user_id")
    private Long userId;

    @TableField("status")
    private Integer status;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;


}
