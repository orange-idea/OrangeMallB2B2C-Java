package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
    import java.math.BigDecimal;
import java.io.Serializable;

/**
* @author yzb
* @date 2020-02-26
拣货员账户信息
*/
@Data
@TableName("jms_picking_account")
public class JmsPickingAccount implements Serializable {


            @TableId(value = "id", type = IdType.AUTO)
        private Long id;


            @TableField( "user_id")
        private Long userId;


            @TableField( "balance")
        private BigDecimal balance;


            @TableField( "balance_use")
        private BigDecimal balanceUse;


            @TableField( "balance_frozen")
        private BigDecimal balanceFrozen;


            @TableField( "total_income")
        private BigDecimal totalIncome;


            @TableField( "total_withdraw")
        private BigDecimal totalWithdraw;


            @TableField( "create_time")
        private Date createTime;


            @TableField( "update_time")
        private Date updateTime;


            @TableField( "shop_id")
        private Long shopId;


            @TableField( "city_code")
        private String cityCode;


            @TableField( "store_id")
        private Integer storeId;


}
