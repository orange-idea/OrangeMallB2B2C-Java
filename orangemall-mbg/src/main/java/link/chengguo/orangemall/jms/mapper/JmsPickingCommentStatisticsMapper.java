package link.chengguo.orangemall.jms.mapper;


import link.chengguo.orangemall.jms.entity.JmsPickingCommentStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-26
*/
public interface JmsPickingCommentStatisticsMapper extends BaseMapper<JmsPickingCommentStatistics> {
}
