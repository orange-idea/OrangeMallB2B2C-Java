package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
    import java.math.BigDecimal;
import java.io.Serializable;

/**
* @author yzb
* @date 2020-02-26
拣货员评价统计
*/
@Data
@TableName("jms_picking_comment_statistics")
public class JmsPickingCommentStatistics implements Serializable {


            @TableId(value = "id", type = IdType.AUTO)
        private Long id;


            @TableField( "user_id")
        private Long userId;


            @TableField( "comment_good")
        private Integer commentGood;


            @TableField( "comment_general")
        private Integer commentGeneral;


            @TableField( "comment_low")
        private Integer commentLow;


            @TableField( "comment_picture")
        private Integer commentPicture;


            @TableField( "goods_rank")
        private BigDecimal goodsRank;


            @TableField( "shop_id")
        private Long shopId;


            @TableField( "city_code")
        private String cityCode;


            @TableField( "store_id")
        private Integer storeId;


            @TableField( "create_time")
        private Date createTime;


            @TableField( "update_time")
        private Date updateTime;


}
