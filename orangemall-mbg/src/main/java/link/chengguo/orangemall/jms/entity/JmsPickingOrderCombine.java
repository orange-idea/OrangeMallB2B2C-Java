package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-26
 * 合并拣货订单
 */
@Data
@TableName("jms_picking_order_combine")
public class JmsPickingOrderCombine implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("goods_number")
    private Integer goodsNumber;

    @TableField("finish_goods_number")
    private Integer finishGoodsNumber;

    /**
     * 拣货订单号
     */
    @TableField("picking_order_ids")
    private String pickingOrderIds;

    @TableField("status")
    private Integer status;

    @TableField("take_time")
    private Integer takeTime;

    @TableField("goods_type_ids")
    private String goodsTypeIds;

    @TableField("goods_type_names")
    private String goodsTypeNames;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("finish_time")
    private Date finishTime;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;

}
