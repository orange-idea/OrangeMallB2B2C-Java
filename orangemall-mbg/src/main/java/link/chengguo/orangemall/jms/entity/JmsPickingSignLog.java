package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货员签到记录
 */
@Data
@TableName("jms_picking_sign_log")
public class JmsPickingSignLog implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("name")
    private String name;

    @TableField("sign_time")
    private Date signTime;

    @TableField("address")
    private String address;

    @TableField("latitude")
    private String latitude;

    @TableField("longitude")
    private String longitude;

    @TableField("type")
    private Integer type;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;

}
