package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
    import java.math.BigDecimal;
import java.io.Serializable;

/**
* @author yzb
* @date 2020-02-26
拣货员账户日志
*/
@Data
@TableName("jms_pickinig_account_log")
public class JmsPickinigAccountLog implements Serializable {


            @TableId(value = "id", type = IdType.AUTO)
        private Long id;


            @TableField( "user_id")
        private Long userId;


            @TableField( "sign")
        private Integer sign;


            @TableField( "money")
        private BigDecimal money;


            @TableField( "type")
        private Integer type;


            @TableField( "content")
        private String content;


            @TableField( "data_id")
        private String dataId;


            @TableField( "create_time")
        private Date createTime;


            @TableField( "store_id")
        private Integer storeId;


}
