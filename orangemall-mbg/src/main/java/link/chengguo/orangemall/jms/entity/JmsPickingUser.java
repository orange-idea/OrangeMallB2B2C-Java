package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-26
 * 拣货员信息
 */
@Data
@TableName("jms_picking_user")
public class JmsPickingUser implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("name")
    private String name;

    @TableField("pic")
    private String pic;

    @TableField("phone")
    private String phone;

    @TableField("pwd")
    private String pwd;

    @TableField("role_id")
    private Integer roleId;

    /**
     * 上下班状态：0-下班，1-上班
     */
    @TableField("work_state")
    private Integer workState;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    @TableField("store_id")
    private Integer storeId;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    /**
     * 帐号启用状态:0->禁用；1->启用
     */
    @TableField("status")
    private Integer status;

    @TableField(exist = false)
    private String roleName;


}
