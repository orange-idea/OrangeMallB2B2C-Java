package link.chengguo.orangemall.jms.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
* @author yzb
* @date 2020-02-26
拣货员评价信息
*/
@Data
@TableName("jms_picking_comment")
public class JmsPickingComment implements Serializable {


            @TableId(value = "id", type = IdType.AUTO)
        private Long id;


            @TableField( "user_id")
        private Long userId;


            @TableField( "member_id")
        private Long memberId;


            @TableField( "member_phone")
        private String memberPhone;


            @TableField( "level")
        private Integer level;


            @TableField( "stars")
        private Integer stars;


            @TableField( "content")
        private String content;


            @TableField( "images")
        private String images;


            @TableField( "create_time")
        private Date createTime;


            @TableField( "shop_id")
        private Long shopId;


            @TableField( "city_code")
        private String cityCode;


            @TableField( "store_id")
        private Integer storeId;


}
