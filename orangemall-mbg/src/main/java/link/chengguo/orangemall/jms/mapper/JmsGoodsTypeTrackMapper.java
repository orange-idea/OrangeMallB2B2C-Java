package link.chengguo.orangemall.jms.mapper;


import link.chengguo.orangemall.jms.entity.JmsGoodsTypeTrack;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-27
*/
public interface JmsGoodsTypeTrackMapper extends BaseMapper<JmsGoodsTypeTrack> {
}
