package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UmsGrowthChangeHistory;

/**
 * <p>
 * 成长值变化历史记录表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface UmsGrowthChangeHistoryMapper extends BaseMapper<UmsGrowthChangeHistory> {

}
