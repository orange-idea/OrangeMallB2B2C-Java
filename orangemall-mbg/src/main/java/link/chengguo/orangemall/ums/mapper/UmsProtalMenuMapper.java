package link.chengguo.orangemall.ums.mapper;


import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author mxchen
* @date 2020-08-31
*/
@Mapper
public interface UmsProtalMenuMapper extends BaseMapper<UmsProtalMenu> {

}
