package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UmsMemberLevel;

/**
 * <p>
 * 会员等级表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface UmsMemberLevelMapper extends BaseMapper<UmsMemberLevel> {

}
