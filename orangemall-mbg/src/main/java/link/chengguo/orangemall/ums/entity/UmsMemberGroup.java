package link.chengguo.orangemall.ums.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * @Author: 钟业海
 * @Date: 2020/7/24 11:55
 */
@Data
public class UmsMemberGroup {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private Boolean status;
    private Integer sort;
    private Date creationTime;
}
