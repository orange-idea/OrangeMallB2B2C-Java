package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UserTocash;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface UserTocashMapper extends BaseMapper<UserTocash> {

}
