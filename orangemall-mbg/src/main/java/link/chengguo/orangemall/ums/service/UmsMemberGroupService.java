package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberGroup;

/**
 * @Author: 钟业海
 * @Date: 2020/7/24 11:58
 */
public interface UmsMemberGroupService extends IService<UmsMemberGroup> {
}
