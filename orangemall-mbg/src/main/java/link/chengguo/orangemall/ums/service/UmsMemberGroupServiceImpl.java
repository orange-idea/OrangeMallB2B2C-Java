package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberGroup;
import link.chengguo.orangemall.ums.mapper.UmsMemberGroupMapper;
import link.chengguo.orangemall.ums.service.UmsMemberGroupService;
import org.springframework.stereotype.Service;

/**
 * @Author: 钟业海
 * @Date: 2020/7/24 11:58
 */
@Service
public class UmsMemberGroupServiceImpl extends ServiceImpl<UmsMemberGroupMapper, UmsMemberGroup> implements UmsMemberGroupService {
}
