package link.chengguo.orangemall.ums.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @author chengguo
 * @date 2020-08-31
 */
@Data
@TableName("ums_protal_menu")
public class UmsProtalMenu implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 用户端的菜单名称
     */
    @TableField("name")
    private String name;
    /**
     * 用户端的菜单所属 【个人中心 ： 0 】【活动区域 ： 1  】【广告区域 ： 2】【  推荐区域：3】
     */
    @TableField("type")
    private String type;
    /**
     * 用户端的菜单图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 用户端的菜单跳转路径
     */
    @TableField("url")
    private String url;

    /**
     * 用户端的菜单排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 用户端的菜单状态（1：使用，0：停用）
     */
    @TableField("status")
    private String status;

    /**
     * 用户端的菜单描述信息
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
