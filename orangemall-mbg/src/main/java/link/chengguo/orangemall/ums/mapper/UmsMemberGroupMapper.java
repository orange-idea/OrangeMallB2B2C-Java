package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UmsMemberGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: 钟业海
 * @Date: 2020/7/24 11:57
 */
@Mapper
public interface UmsMemberGroupMapper extends BaseMapper<UmsMemberGroup> {
}
