package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UmsMemberRuleSetting;

/**
 * <p>
 * 会员积分成长规则表 Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface UmsMemberRuleSettingMapper extends BaseMapper<UmsMemberRuleSetting> {

}
