package link.chengguo.orangemall.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.ums.entity.UmsMemberLocation;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author chengguo
 * @since 2019-10-25
 */
public interface UmsMemberLocationMapper extends BaseMapper<UmsMemberLocation> {

}
