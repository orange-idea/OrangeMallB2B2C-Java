package link.chengguo.orangemall.mms.mapper;


import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author yzb
* @date 2020-02-22
*/
public interface MmsCardDetailMapper extends BaseMapper<MmsCardDetail> {
}
