package link.chengguo.orangemall.mms.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-22
 * 卡券操作记录
 */
@Data
@TableName("mms_card_operation_log")
public class MmsCardOperationLog implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 操作人员ID
     */
    @TableField("aid")
    private Long aid;

    /**
     * 操作人员名称
     */
    @TableField("name")
    private String name;

    /**
     * 卡券ID
     */
    @TableField("card_id")
    private Long cardId;

    /**
     * 卡券标题
     */
    @TableField("card_title")
    private String cardTitle;

    /**
     * 卡券标题ID
     */
    @TableField("title_id")
    private Integer titleId;

    /**
     * 操作类型：0-激活，1-作废
     */
    @TableField("type")
    private Integer type;

    /**
     *  操作数量
     */
    @TableField("number")
    private Integer number;

    /**
     * 卡券金额
     */
    @TableField("money")
    private String money;

    /**
     * 操作总金额
     */
    @TableField("total_money")
    private String totalMoney;

    /**
     * 激活结果：0-失败，1-成功
     */
    @TableField("status")
    private Integer status;

    /**
     * 说明内容
     */
    @TableField("content")
    private String content;

    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 搜索，关键字
     */
    @TableField(exist = false)
    private String keyword;

    /**
     * 搜索，时间
     */
    @TableField(exist = false)
    private String searchTime;
}
