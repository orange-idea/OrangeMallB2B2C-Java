package link.chengguo.orangemall.mms.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-22
 * 充值卡券标题
 */
@Data
@TableName("mms_card_title")
public class MmsCardTitle implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 卡券类型
     */
    @TableField("title")
    private String title;

    /**
     * 卡券金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 卡券数量
     */
    @TableField("number")
    private Integer number;

    /**
     * 添加时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 搜索，关键字
     */
    @TableField(exist = false)
    private String keyword;
    @TableField("bg_image")
    private String bgImage;

    @TableField("des_comment")
    private String desComment;
}
