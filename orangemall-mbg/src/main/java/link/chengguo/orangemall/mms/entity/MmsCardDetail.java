package link.chengguo.orangemall.mms.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import java.math.BigDecimal;
import java.io.Serializable;

/**
 * @author yzb
 * @date 2020-02-22
 * 充值卡券详情
 */
@Data
@TableName("mms_card_detail")
public class MmsCardDetail implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 卡券标题ID
     */
    @TableField("title_id")
    private Integer titleId;

    /**
     * 卡券唯一识别码
     */
    @TableField("uuid")
    private String uuid;

    /**
     * 卡券金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     *  使用会员ID
     */
    @TableField("member_id")
    private Long memberId;

    /**
     * 使用会员名称
     */
    @TableField("name")
    private String name;

    /**
     * 使用者电话
     */
    @TableField("phone")
    private String phone;

    @TableField("create_time")
    private Date createTime;

    /**
     *  是否使用
     */
    @TableField("if_use")
    private Integer ifUse;

    /**
     * 使用时间
     */
    @TableField("recharge_time")
    private Date rechargeTime;

    /**
     * 是否作废 0-否，1-是
     */
    @TableField("if_invalid")
    private Integer ifInvalid;

    /**
     * 作废时间
     */
    @TableField("invalid_time")
    private Date invalidTime;

    /**
     * 是否激活：0-否，1-是
     */
    @TableField("if_active")
    private Integer ifActive;

    /**
     * 激活时间
     */
    @TableField("active_time")
    private Date activeTime;

    /**
     * 操作员ID
     */
    @TableField("aid")
    private Long aid;

    /**
     * 搜索，关键字
     */
    @TableField(exist = false)
    private String keyword;

}
