package link.chengguo.orangemall.shms.vo;


import link.chengguo.orangemall.shms.entity.ShmsShopCategory;

import java.util.List;

/**
 */
public class ShmsShopCategoryWithChildrenItem extends ShmsShopCategory {
    private List<ShmsShopCategory> children;

    public List<ShmsShopCategory> getChildren() {
        return children;
    }

    public void setChildren(List<ShmsShopCategory> children) {
        this.children = children;
    }
}
