package link.chengguo.orangemall.shms.vo;

import link.chengguo.orangemall.shms.entity.ShmsShopIndustry;

import java.util.List;

public class ShmsShopIndustryWithChildrenItem extends ShmsShopIndustry {
    private List<ShmsShopIndustry> children;

    public List<ShmsShopIndustry> getChildren() {
        return children;
    }

    public void setChildren(List<ShmsShopIndustry> children) {
        this.children = children;
    }
}
