package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopBrokerageLevel;

/**
 * <p>
 * 店铺抽佣等级表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopBrokerageLevelMapper extends BaseMapper<ShmsShopBrokerageLevel> {


}
