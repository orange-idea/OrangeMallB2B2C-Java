package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopBusinessCity;

/**
 * <p>
 * 店铺经营城市表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopBusinessCityMapper extends BaseMapper<ShmsShopBusinessCity> {


}
