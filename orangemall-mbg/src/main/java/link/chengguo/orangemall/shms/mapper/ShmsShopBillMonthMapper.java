package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 店铺账单月统计表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopBillMonthMapper extends BaseMapper<ShmsShopBillMonth> {

    /**
     * 查询月的订单统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    Page<ShmsShopBillMonth> getMonthBillByMonth(@Param("page") Page page,@Param("paramCondition")ShmsShopBillMonth paramCondition);

}
