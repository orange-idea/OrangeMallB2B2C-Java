package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopConfig;

/**
 * <p>
 * 店铺参数配置表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopConfigMapper extends BaseMapper<ShmsShopConfig> {


}
