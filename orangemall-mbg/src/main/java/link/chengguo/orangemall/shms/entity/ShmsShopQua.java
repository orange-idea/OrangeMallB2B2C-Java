package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺资质表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_qua")
public class ShmsShopQua implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 关联店铺ID
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 检查结果
     */
    @TableField("result")
    private String result;

    /**
     * 单位名称
     */
    @TableField("name")
    private String name;

    /**
     * 经营地址
     */
    @TableField("address")
    private String address;

    /**
     * 法人代表
     */
    @TableField("legal")
    private String legal;

    /**
     * 许可证
     */
    @TableField("licence")
    private String licence;

    /**
     * 经营范围
     */
    @TableField("scope")
    private String scope;

    /**
     * 有效期
     */
    @TableField("validity")
    private String validity;

    /**
     * 其他资质照片
     */
    @TableField("images")
    private String images;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLegal() {
        return legal;
    }

    public void setLegal(String legal) {
        this.legal = legal;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ShmsShopQua{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", result=" + result +
        ", name=" + name +
        ", address=" + address +
        ", legal=" + legal +
        ", licence=" + licence +
        ", scope=" + scope +
        ", validity=" + validity +
        ", images=" + images +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
