package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 店铺账单日统计表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopBillDayMapper extends BaseMapper<ShmsShopBillDay> {


    /**
     * 查询统计信息，日统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    List<ShmsShopBillMonth> statisticsMonthBill(@Param("paramCondition") ShmsShopBillDay paramCondition);


    /**
     * 查询每天的订单统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    Page<ShmsShopBillDay> getDayBillByDay(@Param("page") Page page, @Param("paramCondition")ShmsShopBillDay paramCondition);

}
