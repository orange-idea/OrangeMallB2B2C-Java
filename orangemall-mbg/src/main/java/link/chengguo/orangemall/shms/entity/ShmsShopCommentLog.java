package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺评价记录表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_comment_log")
public class ShmsShopCommentLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 评价用户编号
     */
    @TableField("member_id")
    private Long memberId;

    /**
     * 好中差，如果按照星星，则进行转换。0-好，1-中，2-查
     */
    @TableField("level")
    private Integer level;

    /**
     * 星星数量(1-2：差评,3-4中评，5-好评)
     */
    @TableField("stars")
    private Integer stars;

    /**
     * 评价内容
     */
    @TableField("content")
    private String content;

    /**
     * 评价图片
     */
    @TableField("images")
    private String images;

    /**
     * 评价时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ShmsShopCommentLog{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", memberId=" + memberId +
        ", level=" + level +
        ", stars=" + stars +
        ", content=" + content +
        ", images=" + images +
        ", createTime=" + createTime +
        "}";
    }
}
