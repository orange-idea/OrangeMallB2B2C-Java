package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopIndustry;
import link.chengguo.orangemall.shms.vo.ShmsShopIndustryWithChildrenItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 店铺行业表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopIndustryMapper extends BaseMapper<ShmsShopIndustry> {

    /**
     * 查询带子分类的分类
     * @return
     */
    List<ShmsShopIndustryWithChildrenItem> listWithChildren(@Param("paramCondition") ShmsShopIndustry paramCondition);
}
