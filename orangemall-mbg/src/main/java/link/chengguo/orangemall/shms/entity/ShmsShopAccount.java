package link.chengguo.orangemall.shms.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺账户表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_account")
public class ShmsShopAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 店铺ID
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 冻结余额
     */
    @TableField("balance_frozen")
    private BigDecimal balanceFrozen;

    /**
     * 可提余额
     */
    @TableField("balance_withdraw")
    private BigDecimal balanceWithdraw;

    /**
     * 账户余额
     */
    @TableField("balance")
    private BigDecimal balance;

    /**
     * 提现总额
     */
    @TableField("total_withdraw")
    private BigDecimal totalWithdraw;

    /**
     * 收入总额
     */
    @TableField("total_income")
    private BigDecimal totalIncome;

    /**
     * 退款总额
     */
    @TableField("total_refund")
    private BigDecimal totalRefund;

    /**
     * 佣金总额
     */
    @TableField("total_brokerage")
    private BigDecimal totalBrokerage;

    /**
     * 物流费用
     */
    @TableField("total_express_fee")
    private BigDecimal totalExpressFee;

    /**
     * 订单总额
     */
    @TableField("total_order_amount")
    private BigDecimal totalOrderAmount;

    /**
     * 平台红包
     */
    @TableField("total_red")
    private BigDecimal totalRed;

    /**
     * 店铺优惠券
     */
    @TableField("total_coupon")
    private BigDecimal totalCoupon;

    /**
     * 店铺折扣
     */
    @TableField("total_discount")
    private BigDecimal totalDiscount;

    /**
     * 积分总数
     */
    @TableField("total_integral")
    private Integer totalIntegral;

    /**
     * 积分金额
     */
    @TableField("total_integral_money")
    private BigDecimal totalIntegralMoney;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

    @TableField("city_code")
    private String cityCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public BigDecimal getBalanceFrozen() {
        return balanceFrozen;
    }

    public void setBalanceFrozen(BigDecimal balanceFrozen) {
        this.balanceFrozen = balanceFrozen;
    }

    public BigDecimal getBalanceWithdraw() {
        return balanceWithdraw;
    }

    public void setBalanceWithdraw(BigDecimal balanceWithdraw) {
        this.balanceWithdraw = balanceWithdraw;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(BigDecimal totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getTotalRefund() {
        return totalRefund;
    }

    public void setTotalRefund(BigDecimal totalRefund) {
        this.totalRefund = totalRefund;
    }

    public BigDecimal getTotalBrokerage() {
        return totalBrokerage;
    }

    public void setTotalBrokerage(BigDecimal totalBrokerage) {
        this.totalBrokerage = totalBrokerage;
    }

    public BigDecimal getTotalExpressFee() {
        return totalExpressFee;
    }

    public void setTotalExpressFee(BigDecimal totalExpressFee) {
        this.totalExpressFee = totalExpressFee;
    }

    public BigDecimal getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public void setTotalOrderAmount(BigDecimal totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public BigDecimal getTotalRed() {
        return totalRed;
    }

    public void setTotalRed(BigDecimal totalRed) {
        this.totalRed = totalRed;
    }

    public BigDecimal getTotalCoupon() {
        return totalCoupon;
    }

    public void setTotalCoupon(BigDecimal totalCoupon) {
        this.totalCoupon = totalCoupon;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getTotalIntegral() {
        return totalIntegral;
    }

    public void setTotalIntegral(Integer totalIntegral) {
        this.totalIntegral = totalIntegral;
    }

    public BigDecimal getTotalIntegralMoney() {
        return totalIntegralMoney;
    }

    public void setTotalIntegralMoney(BigDecimal totalIntegralMoney) {
        this.totalIntegralMoney = totalIntegralMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ShmsShopAccount{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", balanceFrozen=" + balanceFrozen +
        ", balanceWithdraw=" + balanceWithdraw +
        ", balance=" + balance +
        ", totalWithdraw=" + totalWithdraw +
        ", totalIncome=" + totalIncome +
        ", totalRefund=" + totalRefund +
        ", totalBrokerage=" + totalBrokerage +
        ", totalExpressFee=" + totalExpressFee +
        ", totalOrderAmount=" + totalOrderAmount +
        ", totalRed=" + totalRed +
        ", totalCoupon=" + totalCoupon +
        ", totalDiscount=" + totalDiscount +
        ", totalIntegral=" + totalIntegral +
        ", totalIntegralMoney=" + totalIntegralMoney +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
