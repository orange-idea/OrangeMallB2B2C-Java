package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopCommentSum;

/**
 * <p>
 * 店铺评价统计表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopCommentSumMapper extends BaseMapper<ShmsShopCommentSum> {

}
