package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺审核记录表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_check_log")
public class ShmsShopCheckLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺ID
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 操作员编号
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 操作员名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 审核结果(0-待审核，1-审核中，2-审核通过，3-拒绝通过)
     */
    @TableField("check_result")
    private Integer checkResult;

    /**
     * 拒绝原因
     */
    @TableField("refuse_reason")
    private String refuseReason;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(Integer checkResult) {
        this.checkResult = checkResult;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    @Override
    public String toString() {
        return "ShmsShopCheckLog{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", userId=" + userId +
        ", userName=" + userName +
        ", checkResult=" + checkResult +
        ", refuseReason=" + refuseReason +
        "}";
    }
}
