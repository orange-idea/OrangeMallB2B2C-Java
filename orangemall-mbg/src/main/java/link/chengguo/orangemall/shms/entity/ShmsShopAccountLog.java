package link.chengguo.orangemall.shms.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺账户日志表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_account_log")
public class ShmsShopAccountLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shop_id")
    private Long shopId;

    @TableField("shop_account_id")
    private Long shopAccountId;

    /**
     * 冻结余额
     */
    @TableField("balance_frozen")
    private BigDecimal balanceFrozen;

    /**
     * 可提余额
     */
    @TableField("balance_withdraw")
    private BigDecimal balanceWithdraw;

    /**
     * 账户余额
     */
    @TableField("balance")
    private BigDecimal balance;

    /**
     * 操作金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 操作前冻结余额
     */
    @TableField("last_balance_frozen")
    private BigDecimal lastBalanceFrozen;

    /**
     * 操作前可提余额
     */
    @TableField("last_balance_withdraw")
    private BigDecimal lastBalanceWithdraw;

    /**
     * 操作前账户余额
     */
    @TableField("last_balance")
    private BigDecimal lastBalance;

    /**
     * 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     */
    @TableField("change_type")
    private Integer changeType;

    /**
     * 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
     */
    @TableField("change_type_name")
    private String changeTypeName;

    /**
     * 加减符号：+或-分表表示增加或减少
     */
    @TableField("symbol")
    private String symbol;

    /**
     * 变动描述
     */
    @TableField("change_desc")
    private String changeDesc;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;


    @TableField("city_code")
    private String cityCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopAccountId() {
        return shopAccountId;
    }

    public void setShopAccountId(Long shopAccountId) {
        this.shopAccountId = shopAccountId;
    }

    public BigDecimal getBalanceFrozen() {
        return balanceFrozen;
    }

    public void setBalanceFrozen(BigDecimal balanceFrozen) {
        this.balanceFrozen = balanceFrozen;
    }

    public BigDecimal getBalanceWithdraw() {
        return balanceWithdraw;
    }

    public void setBalanceWithdraw(BigDecimal balanceWithdraw) {
        this.balanceWithdraw = balanceWithdraw;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getLastBalanceFrozen() {
        return lastBalanceFrozen;
    }

    public void setLastBalanceFrozen(BigDecimal lastBalanceFrozen) {
        this.lastBalanceFrozen = lastBalanceFrozen;
    }

    public BigDecimal getLastBalanceWithdraw() {
        return lastBalanceWithdraw;
    }

    public void setLastBalanceWithdraw(BigDecimal lastBalanceWithdraw) {
        this.lastBalanceWithdraw = lastBalanceWithdraw;
    }

    public BigDecimal getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(BigDecimal lastBalance) {
        this.lastBalance = lastBalance;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }

    public String getChangeDesc() {
        return changeDesc;
    }

    public void setChangeDesc(String changeDesc) {
        this.changeDesc = changeDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ShmsShopAccountLog{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", shopAccountId=" + shopAccountId +
        ", balanceFrozen=" + balanceFrozen +
        ", balanceWithdraw=" + balanceWithdraw +
        ", balance=" + balance +
        ", money=" + money +
        ", lastBalanceFrozen=" + lastBalanceFrozen +
        ", lastBalanceWithdraw=" + lastBalanceWithdraw +
        ", lastBalance=" + lastBalance +
        ", changeType=" + changeType +
        ", changeDesc=" + changeDesc +
        ", createTime=" + createTime +
        "}";
    }
}
