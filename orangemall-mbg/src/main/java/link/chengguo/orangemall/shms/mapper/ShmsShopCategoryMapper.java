package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.shms.vo.ShmsShopCategoryWithChildrenItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 店铺分类表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopCategoryMapper extends BaseMapper<ShmsShopCategory> {

    /**
     * 查询带子分类的分类
     * @return
     */
    List<ShmsShopCategoryWithChildrenItem> listWithChildren(@Param("paramCondition")  ShmsShopCategory paramCondition);
}
