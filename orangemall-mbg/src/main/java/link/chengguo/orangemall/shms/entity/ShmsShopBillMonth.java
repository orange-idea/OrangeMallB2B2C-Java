package link.chengguo.orangemall.shms.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺账单月统计表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_bill_month")
public class ShmsShopBillMonth implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 日期
     */
    @TableField("month")
    private String month;

    /**
     * 订单数
     */
    @TableField("order_count")
    private Integer orderCount;

    /**
     * 收入总额
     */
    @TableField("total_income")
    private BigDecimal totalIncome;

    /**
     * 退款订单
     */
    @TableField("refund_count")
    private Integer refundCount;

    /**
     * 退款金额
     */
    @TableField("refund_amount")
    private BigDecimal refundAmount;

    /**
     * 佣金金额
     */
    @TableField("percent_amount")
    private BigDecimal percentAmount;

    /**
     * 平台抽佣
     */
    @TableField("plat_amount")
    private BigDecimal platAmount;

    /**
     * 代理抽佣
     */
    @TableField("agent_amount")
    private BigDecimal agentAmount;


    /**
     * 物流费用
     */
    @TableField("express_fee")
    private BigDecimal expressFee;

    /**
     * 订单总额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;

    /**
     * 订单实收金额
     */
    @TableField("paid_amount")
    private BigDecimal paidAmount;

    /**
     * 平台红包
     */
    @TableField("red_amount")
    private BigDecimal redAmount;

    /**
     * 店铺优惠券
     */
    @TableField("coupon_amount")
    private BigDecimal couponAmount;

    /**
     * 店铺折扣
     */
    @TableField("discount_amount")
    private BigDecimal discountAmount;

    /**
     * 是否打款( 未打款，1-已打款)
     */
    @TableField("pay_status")
    private Integer payStatus;

    /**
     * 打款时间
     */
    @TableField("pay_time")
    private Date payTime;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 开始时间
     */
    @TableField(exist = false)
    private String startTime;

    /**
     * 结束时间
     */
    @TableField(exist = false)
    private String endTime;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public BigDecimal getPlatAmount() {
        return platAmount;
    }

    public void setPlatAmount(BigDecimal platAmount) {
        this.platAmount = platAmount;
    }

    public BigDecimal getAgentAmount() {
        return agentAmount;
    }

    public void setAgentAmount(BigDecimal agentAmount) {
        this.agentAmount = agentAmount;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public BigDecimal getPercentAmount() {
        return percentAmount;
    }

    public void setPercentAmount(BigDecimal percentAmount) {
        this.percentAmount = percentAmount;
    }

    public BigDecimal getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(BigDecimal expressFee) {
        this.expressFee = expressFee;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getRedAmount() {
        return redAmount;
    }

    public void setRedAmount(BigDecimal redAmount) {
        this.redAmount = redAmount;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ShmsShopBillMonth{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", month=" + month +
        ", orderCount=" + orderCount +
        ", totalIncome=" + totalIncome +
        ", refundCount=" + refundCount +
        ", refundAmount=" + refundAmount +
        ", percentAmount=" + percentAmount +
        ", expressFee=" + expressFee +
        ", orderAmount=" + orderAmount +
        ", redAmount=" + redAmount +
        ", couponAmount=" + couponAmount +
        ", discountAmount=" + discountAmount +
        ", payStatus=" + payStatus +
        ", payTime=" + payTime +
        ", createTime=" + createTime +
        "}";
    }
}
