package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;

/**
 * <p>
 * 店铺账户日志表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopAccountLogMapper extends BaseMapper<ShmsShopAccountLog> {


}
