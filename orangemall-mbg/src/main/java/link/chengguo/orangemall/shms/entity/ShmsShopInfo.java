package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.*;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 店铺信息表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_info")
public class ShmsShopInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(exist =  false)
    private String sid;

    /**
     * 店铺分类
     */
    @TableField("category_id")
    private String categoryId;

    /**
     * 店铺分类名称
     */
    @TableField(exist = false)
    private String categoryName;

    /**
     * 自营分类（自营店铺添加）
     */
    @TableField("self_category_id")
    private String selfCategoryId;


    /**
     * 店铺分类名称
     */
    @TableField(exist = false)
    private String selfCategoryName;

    /**
     * 行业分类
     */
    @TableField("industry_id")
    private String industryId;

    /**
     * 店铺分类名称
     */
    @TableField(exist = false)
    private String industryName;

    /**
     * 加入方式（0-申请入驻，1-后台添加）
     */
    @TableField("add_type")
    private Integer addType;

    /**
     * 店铺类型（0-入驻，1-自营）
     */
    @TableField("manage_mode")
    private Integer manageMode;

    /**
     * 店铺名称
     */
    @TableField("name")
    private String name;

    /**
     * 店铺logo
     */
    @TableField("logo")
    private String logo;

    /**
     * 店铺背景图
     */
    @TableField("bg_image")
    private String bgImage;

    /**
     * 店铺关键字
     */
    @TableField("keyword")
    private String keyword;

    /**
     * 营业时间，格式如：09:00-12:00,14:00-22:00
     */
    @TableField("business_hours")
    private String businessHours;

    /**
     * 客服电话
     */
    @TableField("waiter_phone")
    private String waiterPhone;

    /**
     * 店铺简介
     */
    @TableField("introduction")
    private String introduction;

    /**
     * 店铺公告
     */
    @TableField("notice")
    private String notice;

    /**
     * 独立配置运费：0-否，1-是
     */
    @TableField("special_case")
    private Integer specialCase;

    /**
     * 配送费用
     */
    @TableField("express_fee")
    private BigDecimal expressFee;

    /**
     * 起送价格
     */
    @TableField("starting_price")
    private BigDecimal startingPrice;

    /**
     * 免运费价格
     */
    @TableField("free_express_price")
    private BigDecimal freeExpressPrice;

    /**
     * 抽佣等级
     */
    @TableField("commision_level")
    private Integer commisionLevel;

    /**
     * 抽佣比例
     */
    @TableField(exist = false)
    private BigDecimal percent;

    /**
     * 配送模式，支持多个逗号拼接，1-到店扫码购，2-平台配送，3-商家配送，4-到店自提，5-预约到店，6-上门服务，7-物流配送
     */
    @TableField("delivery_mode")
    private String deliveryMode;

    /**
     * 是否歇业（0-营业，1-歇业）
     */
    @TableField("is_close")
    private Integer isClose;

    /**
     * 责任人
     */
    @TableField("responsible_person")
    private String responsiblePerson;

    /**
     * 公司名称
     */
    @TableField("company_name")
    private String companyName;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 联系方式
     */
    @TableField("contact_mobile")
    private String contactMobile;

    /**
     * 登录密码
     */
    @TableField("password")
    private String password;

    /**
     * 申请时间
     */
    @TableField("apply_date")
    private Date applyDate;


    /**
     * 审核通过时间
     */
    @TableField("confirm_date")
    private Date confirmDate;

    /**
     * 过期时间
     */
    @TableField("expired_date")
    private Date expiredDate;

    /**
     * 入驻身份信息认证状态0-待审核,1-审核通过，2-拒绝通过
     */
    @TableField("account_status")
    private Integer accountStatus;

    /**
     * 拒绝原因
     */
    @TableField("refuse_resson")
    private String refuseResson;

    /**
     * 证件类型(0-个人,1-企业)
     */
    @TableField("identity_type")
    private Integer identityType;

    /**
     * 证件号码
     */
    @TableField("identity_number")
    private String identityNumber;

    /**
     * 证件正面
     */
    @TableField("identity_pic_front")
    private String identityPicFront;

    /**
     * 证件反面
     */
    @TableField("identity_pic_back")
    private String identityPicBack;

    /**
     * 手持证件拍
     */
    @TableField("personhand_identity_pic")
    private String personhandIdentityPic;

    /**
     * 营业执照
     */
    @TableField("business_licence_pic")
    private String businessLicencePic;

    /**
     * 营业执照注册号
     */
    @TableField("business_licence")
    private String businessLicence;

    /**
     * 其他资质
     */
    @TableField("other_qualification")
    private String otherQualification;

    /**
     * 主营项目
     */
    @TableField("main_project")
    private String mainProject;

    /**
     * 省份
     */
    @TableField("province")
    private String province;
    @TableField(exist = false)
    private String provinceName;
    /**
     * 市区
     */
    @TableField("city")
    private String city;
    @TableField(exist = false)
    private String cityName;
    /**
     * 地区
     */
    @TableField("district")
    private String district;
    @TableField(exist = false)
    private String districtName;
    /**
     * 街道
     */
    @TableField("street")
    private String street;

    /**
     * 详细地址
     */
    @TableField("address")
    private String address;

    /**
     * 经度
     */
    @TableField("longitude")
    private String longitude;

    /**
     * 纬度
     */
    @TableField("latitude")
    private String latitude;

    /**
     * 区域编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 是否联动(1-联动，0-不联动)
     */
    @TableField("linkage")
    private Integer linkage;

    /**
     * 是否推荐:
     */
    @TableField("is_recommend")
    private Integer isRecommend;

    /**
     * 配送时间
     */
    @TableField("delivery_time")
    private Integer deliveryTime;

    /**
     * 0-禁用，1-启用
     */
    @TableField("status")
    private Integer status;

    /**
     * 排序
     */
    @TableField("sort")
    private Long sort;

    /**
     * 添加时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 是否删除（1-已删除）
     */
    @TableField("is_delete")
    private Integer isDelete;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private Date deleteTime;

    /**
     * 是否开通扫码购：0-否，1-是
     */
    @TableField("scan_open")
    private Integer scanOpen;

    /**
     * 是否开通ERP：0-否，1-是
     */
    @TableField("erp_open")
    private Integer erpOpen;

    /**
     * 是否开通拣货：0-否，1-是
     */
    @TableField("picking_open")
    private Integer pickingOpen;

    /**
     * 配送系统匹配的Token
     */
    @TableField("token")
    private String token;

    /**
     * 店铺下的商品
     */
    @TableField(exist = false)
    private List<PmsProduct> goodsList;

    /**
     * 距离
     */
    @TableField(exist = false)
    private Double distance;


    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSelfCategoryName() {
        return selfCategoryName;
    }

    public void setSelfCategoryName(String selfCategoryName) {
        this.selfCategoryName = selfCategoryName;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public Integer getSpecialCase() {
        return specialCase;
    }

    public void setSpecialCase(Integer specialCase) {
        this.specialCase = specialCase;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public Integer getIsClose() {
        return isClose;
    }

    public void setIsClose(Integer isClose) {
        this.isClose = isClose;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getScanOpen() {
        return scanOpen;
    }

    public void setScanOpen(Integer scanOpen) {
        this.scanOpen = scanOpen;
    }

    public Integer getErpOpen() {
        return erpOpen;
    }

    public void setErpOpen(Integer erpOpen) {
        this.erpOpen = erpOpen;
    }

    public Integer getPickingOpen() {
        return pickingOpen;
    }

    public void setPickingOpen(Integer pickingOpen) {
        this.pickingOpen = pickingOpen;
    }

    public List<PmsProduct> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<PmsProduct> goodsList) {
        this.goodsList = goodsList;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSelfCategoryId() {
        return selfCategoryId;
    }

    public void setSelfCategoryId(String selfCategoryId) {
        this.selfCategoryId = selfCategoryId;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public Integer getManageMode() {
        return manageMode;
    }

    public void setManageMode(Integer manageMode) {
        this.manageMode = manageMode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBgImage() {
        return bgImage;
    }

    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getWaiterPhone() {
        return waiterPhone;
    }

    public void setWaiterPhone(String waiterPhone) {
        this.waiterPhone = waiterPhone;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public BigDecimal getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(BigDecimal expressFee) {
        this.expressFee = expressFee;
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }

    public BigDecimal getFreeExpressPrice() {
        return freeExpressPrice;
    }

    public void setFreeExpressPrice(BigDecimal freeExpressPrice) {
        this.freeExpressPrice = freeExpressPrice;
    }

    public Integer getCommisionLevel() {
        return commisionLevel;
    }

    public void setCommisionLevel(Integer commisionLevel) {
        this.commisionLevel = commisionLevel;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Integer getClose() {
        return isClose;
    }

    public void setClose(Integer isClose) {
        this.isClose = isClose;
    }

    public String getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(String responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getRefuseResson() {
        return refuseResson;
    }

    public void setRefuseResson(String refuseResson) {
        this.refuseResson = refuseResson;
    }

    public Integer getIdentityType() {
        return identityType;
    }

    public void setIdentityType(Integer identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getIdentityPicFront() {
        return identityPicFront;
    }

    public void setIdentityPicFront(String identityPicFront) {
        this.identityPicFront = identityPicFront;
    }

    public String getIdentityPicBack() {
        return identityPicBack;
    }

    public void setIdentityPicBack(String identityPicBack) {
        this.identityPicBack = identityPicBack;
    }

    public String getPersonhandIdentityPic() {
        return personhandIdentityPic;
    }

    public void setPersonhandIdentityPic(String personhandIdentityPic) {
        this.personhandIdentityPic = personhandIdentityPic;
    }

    public String getBusinessLicencePic() {
        return businessLicencePic;
    }

    public void setBusinessLicencePic(String businessLicencePic) {
        this.businessLicencePic = businessLicencePic;
    }

    public String getBusinessLicence() {
        return businessLicence;
    }

    public void setBusinessLicence(String businessLicence) {
        this.businessLicence = businessLicence;
    }

    public String getOtherQualification() {
        return otherQualification;
    }

    public void setOtherQualification(String otherQualification) {
        this.otherQualification = otherQualification;
    }

    public String getMainProject() {
        return mainProject;
    }

    public void setMainProject(String mainProject) {
        this.mainProject = mainProject;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getLinkage() {
        return linkage;
    }

    public void setLinkage(Integer linkage) {
        this.linkage = linkage;
    }

    public Integer getRecommend() {
        return isRecommend;
    }

    public void setRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDelete() {
        return isDelete;
    }

    public void setDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    @Override
    public String toString() {
        return "ShmsShopInfo{" +
        "id=" + id +
        ", categoryId=" + categoryId +
        ", selfCategoryId=" + selfCategoryId +
        ", industryId=" + industryId +
        ", addType=" + addType +
        ", manageMode=" + manageMode +
        ", name=" + name +
        ", logo=" + logo +
        ", bgImage=" + bgImage +
        ", keyword=" + keyword +
        ", businessHours=" + businessHours +
        ", waiterPhone=" + waiterPhone +
        ", introduction=" + introduction +
        ", notice=" + notice +
        ", expressFee=" + expressFee +
        ", startingPrice=" + startingPrice +
        ", freeExpressPrice=" + freeExpressPrice +
        ", commisionLevel=" + commisionLevel +
        ", deliveryMode=" + deliveryMode +
        ", isClose=" + isClose +
        ", responsiblePerson=" + responsiblePerson +
        ", companyName=" + companyName +
        ", email=" + email +
        ", contactMobile=" + contactMobile +
        ", applyDate=" + applyDate +
        ", confirmDate=" + confirmDate +
        ", expiredDate=" + expiredDate +
        ", accountStatus=" + accountStatus +
        ", refuseResson=" + refuseResson +
        ", identityType=" + identityType +
        ", identityNumber=" + identityNumber +
        ", identityPicFront=" + identityPicFront +
        ", identityPicBack=" + identityPicBack +
        ", personhandIdentityPic=" + personhandIdentityPic +
        ", businessLicencePic=" + businessLicencePic +
        ", businessLicence=" + businessLicence +
        ", otherQualification=" + otherQualification +
        ", mainProject=" + mainProject +
        ", province=" + province +
        ", city=" + city +
        ", district=" + district +
        ", street=" + street +
        ", address=" + address +
        ", longitude=" + longitude +
        ", latitude=" + latitude +
        ", cityCode=" + cityCode +
        ", linkage=" + linkage +
        ", isRecommend=" + isRecommend +
        ", deliveryTime=" + deliveryTime +
        ", status=" + status +
        ", sort=" + sort +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", isDelete=" + isDelete +
        ", deleteTime=" + deleteTime +
        "}";
    }
}
