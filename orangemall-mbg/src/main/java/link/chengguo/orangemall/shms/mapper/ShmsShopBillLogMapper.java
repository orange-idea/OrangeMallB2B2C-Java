package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 店铺账单明细表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopBillLogMapper extends BaseMapper<ShmsShopBillLog> {

    /**
     * 查询统计信息，日统计
     * @param  paramCondition
     * @author yzb
     * @Date 2020-02-13
     */
    List<ShmsShopBillDay> statisticsDayBill(@Param("paramCondition") ShmsShopBillLog paramCondition);


    /**
     * 根据店铺ID查询账单统计
     * @param paramCondition
     * @return
     */
    ShmsShopBillDay statisticsBillByShopId(@Param("paramCondition") ShmsShopBillLog paramCondition);

}
