package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺评价统计表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_comment_sum")
public class ShmsShopCommentSum implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shop_id")
    private Long shopId;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;


    /**
     * 好评数
     */
    @TableField("comment_good")
    private Integer commentGood;

    /**
     * 中评数
     */
    @TableField("comment_general")
    private Integer commentGeneral;

    /**
     * 差评数
     */
    @TableField("comment_low")
    private Integer commentLow;

    /**
     * 晒图评价数
     */
    @TableField("content_picture")
    private Integer contentPicture;

    /**
     * 好评率10000=100.00%
     */
    @TableField("goods_rank")
    private Integer goodsRank;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getCommentGood() {
        return commentGood;
    }

    public void setCommentGood(Integer commentGood) {
        this.commentGood = commentGood;
    }

    public Integer getCommentGeneral() {
        return commentGeneral;
    }

    public void setCommentGeneral(Integer commentGeneral) {
        this.commentGeneral = commentGeneral;
    }

    public Integer getCommentLow() {
        return commentLow;
    }

    public void setCommentLow(Integer commentLow) {
        this.commentLow = commentLow;
    }

    public Integer getContentPicture() {
        return contentPicture;
    }

    public void setContentPicture(Integer contentPicture) {
        this.contentPicture = contentPicture;
    }

    public Integer getGoodsRank() {
        return goodsRank;
    }

    public void setGoodsRank(Integer goodsRank) {
        this.goodsRank = goodsRank;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ShmsShopCommentSum{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", commentGood=" + commentGood +
        ", commentGeneral=" + commentGeneral +
        ", commentLow=" + commentLow +
        ", contentPicture=" + contentPicture +
        ", goodsRank=" + goodsRank +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
