package link.chengguo.orangemall.shms.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺账单明细表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_bill_log")
public class ShmsShopBillLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shop_id")
    private Long shopId;

    @TableField("city_code")
    private String cityCode;

    /**
     * 订单类型：0-订单支付，1-退款，2-退货退款
     */
    @TableField("bill_order_type")
    private Integer billOrderType;

    /**
     * 订单编号
     */
    @TableField("order_id")
    private Long orderId;

    /**
     * 订单编码
     */
    @TableField("order_sn")
    private String orderSn;

    /**
     * 商品总额
     */
    @TableField("goods_amount")
    private BigDecimal goodsAmount;

    @TableField("express_fee")
    private BigDecimal expressFee;

    /**
     * 保险费用
     */
    @TableField("insure")
    private BigDecimal insure;

    /**
     * 支付手续费
     */
    @TableField("pay_fee")
    private BigDecimal payFee;

    /**
     * 包装费
     */
    @TableField("pack_fee")
    private BigDecimal packFee;

    /**
     * 积分抵扣
     */
    @TableField("integral")
    private Integer integral;

    /**
     * 积分金额
     */
    @TableField("integral_money")
    private BigDecimal integralMoney;

    /**
     * 红包金额
     */
    @TableField("red_money")
    private BigDecimal redMoney;

    /**
     * 折扣金额
     */
    @TableField("discount_money")
    private BigDecimal discountMoney;

    /**
     * 优惠券金额
     */
    @TableField("coupon_money")
    private BigDecimal couponMoney;

    /**
     * 发票费用
     */
    @TableField("inv_tax")
    private BigDecimal invTax;

    /**
     * 订单金额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;

    /**
     * 实付金额
     */
    @TableField("money_paid")
    private BigDecimal moneyPaid;

    /**
     * 佣金比例,50表示抽取50%
     */
    @TableField("brokerage_percent")
    private BigDecimal brokeragePercent;

    /**
     * 平台抽佣
     */
    @TableField("plat_amount")
    private BigDecimal platAmount;

    /**
     * 代理抽佣
     */
    @TableField("agent_amount")
    private BigDecimal agentAmount;

    /**
     * 佣金金额
     */
    @TableField("brokerage_amount")
    private BigDecimal brokerageAmount;

    /**
     * 收入金额
     */
    @TableField("shop_income")
    private BigDecimal shopIncome;

    /**
     * 结算状态(0-未达到结算条件，1-待结算，2-已结算，3-退款/退款退货完成)
     */
    @TableField("bill_status")
    private Integer billStatus;

    /**
     * 结算时间
     */
    @TableField("bill_time")
    private Date billTime;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    /**
     * 开始时间
     */
    @TableField(exist = false)
    private String startTime;

    /**
     * 结束时间
     */
    @TableField(exist = false)
    private String endTime;

    /**
     * 代理城市
     */
    @TableField(exist = false)
    private String cityName;

    /**
     * 店铺名称
     */
    @TableField(exist = false)
    private String shopName;


    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public BigDecimal getPlatAmount() {
        return platAmount;
    }

    public void setPlatAmount(BigDecimal platAmount) {
        this.platAmount = platAmount;
    }

    public BigDecimal getAgentAmount() {
        return agentAmount;
    }

    public void setAgentAmount(BigDecimal agentAmount) {
        this.agentAmount = agentAmount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Integer getBillOrderType() {
        return billOrderType;
    }

    public void setBillOrderType(Integer billOrderType) {
        this.billOrderType = billOrderType;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public BigDecimal getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(BigDecimal goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    public BigDecimal getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(BigDecimal expressFee) {
        this.expressFee = expressFee;
    }

    public BigDecimal getInsure() {
        return insure;
    }

    public void setInsure(BigDecimal insure) {
        this.insure = insure;
    }

    public BigDecimal getPayFee() {
        return payFee;
    }

    public void setPayFee(BigDecimal payFee) {
        this.payFee = payFee;
    }

    public BigDecimal getPackFee() {
        return packFee;
    }

    public void setPackFee(BigDecimal packFee) {
        this.packFee = packFee;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public BigDecimal getIntegralMoney() {
        return integralMoney;
    }

    public void setIntegralMoney(BigDecimal integralMoney) {
        this.integralMoney = integralMoney;
    }

    public BigDecimal getRedMoney() {
        return redMoney;
    }

    public void setRedMoney(BigDecimal redMoney) {
        this.redMoney = redMoney;
    }

    public BigDecimal getDiscountMoney() {
        return discountMoney;
    }

    public void setDiscountMoney(BigDecimal discountMoney) {
        this.discountMoney = discountMoney;
    }

    public BigDecimal getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(BigDecimal couponMoney) {
        this.couponMoney = couponMoney;
    }

    public BigDecimal getInvTax() {
        return invTax;
    }

    public void setInvTax(BigDecimal invTax) {
        this.invTax = invTax;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getMoneyPaid() {
        return moneyPaid;
    }

    public void setMoneyPaid(BigDecimal moneyPaid) {
        this.moneyPaid = moneyPaid;
    }

    public BigDecimal getBrokeragePercent() {
        return brokeragePercent;
    }

    public void setBrokeragePercent(BigDecimal brokeragePercent) {
        this.brokeragePercent = brokeragePercent;
    }

    public BigDecimal getBrokerageAmount() {
        return brokerageAmount;
    }

    public void setBrokerageAmount(BigDecimal brokerageAmount) {
        this.brokerageAmount = brokerageAmount;
    }

    public BigDecimal getShopIncome() {
        return shopIncome;
    }

    public void setShopIncome(BigDecimal shopIncome) {
        this.shopIncome = shopIncome;
    }

    public Integer getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(Integer billStatus) {
        this.billStatus = billStatus;
    }

    public Date getBillTime() {
        return billTime;
    }

    public void setBillTime(Date billTime) {
        this.billTime = billTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "ShmsShopBillLog{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", billOrderType=" + billOrderType +
        ", orderId=" + orderId +
        ", orderSn=" + orderSn +
        ", goodsAmount=" + goodsAmount +
        ", expressFee=" + expressFee +
        ", insure=" + insure +
        ", payFee=" + payFee +
        ", packFee=" + packFee +
        ", integral=" + integral +
        ", integralMoney=" + integralMoney +
        ", redMoney=" + redMoney +
        ", discountMoney=" + discountMoney +
        ", couponMoney=" + couponMoney +
        ", invTax=" + invTax +
        ", orderAmount=" + orderAmount +
        ", moneyPaid=" + moneyPaid +
        ", brokeragePercent=" + brokeragePercent +
        ", brokerageAmount=" + brokerageAmount +
        ", shopIncome=" + shopIncome +
        ", billStatus=" + billStatus +
        ", billTime=" + billTime +
        ", createTime=" + createTime +
        "}";
    }
}
