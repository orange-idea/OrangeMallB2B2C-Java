package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import link.chengguo.orangemall.shms.entity.ShmsShopSearchKeywords;

/**
 * <p>
 * 店铺搜索关键字表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopSearchKeywordsMapper extends BaseMapper<ShmsShopSearchKeywords> {


}
