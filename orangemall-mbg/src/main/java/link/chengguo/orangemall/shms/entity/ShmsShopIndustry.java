package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺行业表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_industry")
public class ShmsShopIndustry implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 行业名称
     */
    @TableField("name")
    private String name;

    /**
     * 行业图片
     */
    @TableField("image")
    private String image;

    /**
     * 状态（0-停用，1-启用）
     */
    @TableField("status")
    private Integer status;

    /**
     * 分类描述
     */
    @TableField("content")
    private String content;

    /**
     * 父类编号
     */
    @TableField("parent_id")
    private Integer parentId;

    /**
     * 是否自营（1-自营，0-非自营）
     */
    @TableField("is_self")
    private Integer isSelf;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getSelf() {
        return isSelf;
    }

    public void setSelf(Integer isSelf) {
        this.isSelf = isSelf;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ShmsShopIndustry{" +
        "id=" + id +
        ", name=" + name +
        ", image=" + image +
        ", status=" + status +
        ", content=" + content +
        ", parentId=" + parentId +
        ", isSelf=" + isSelf +
        ", sort=" + sort +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
