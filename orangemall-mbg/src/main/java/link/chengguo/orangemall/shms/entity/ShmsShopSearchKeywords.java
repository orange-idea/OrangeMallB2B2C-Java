package link.chengguo.orangemall.shms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 店铺搜索关键字表
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
@Data
@TableName("shms_shop_search_keywords")
public class ShmsShopSearchKeywords implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;


    /**
     * 店铺编号
     */
    @TableField("shop_id")
    private Long shopId;

    /**
     * 关键字信息
     */
    @TableField("keywords")
    private String keywords;

    /**
     * 搜索次数
     */
    @TableField("search_count")
    private Integer searchCount;

    /**
     * 最近搜索时间
     */
    @TableField("last_time")
    private Date lastTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Integer searchCount) {
        this.searchCount = searchCount;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    @Override
    public String toString() {
        return "ShmsShopSearchKeywords{" +
        "id=" + id +
        ", shopId=" + shopId +
        ", keywords=" + keywords +
        ", searchCount=" + searchCount +
        ", lastTime=" + lastTime +
        "}";
    }
}
