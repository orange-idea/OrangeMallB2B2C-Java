package link.chengguo.orangemall.shms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 店铺信息表 Mapper 接口
 * </p>
 *
 * @author yzb
 * @since 2019-12-19
 */
public interface ShmsShopInfoMapper extends BaseMapper<ShmsShopInfo> {


    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<ShmsShopInfo> customPageList(@Param("page") Page page, @Param("paramCondition") ShmsShopInfo paramCondition);


    /**
     * 获取分页实体列表
     *
     * @param order 排序：0-降序，1-升序
     * @author yzb
     * @Date 2019-12-21
     */
    Page<ShmsShopInfo> customPageListByDistance(@Param("page") Page page,@Param("paramCondition") ShmsShopInfo paramCondition, @Param("order")Integer order);

}
