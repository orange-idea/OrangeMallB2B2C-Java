package link.chengguo.orangemall.enums;

/**
 * @Auther: Tiger
 * @Date: 2019-04-26 16:04
 * @Description:
 */
public enum OrderStatus {
    //0->待付款；1->待确认；2->已确认；3->已取消；4>已完成；5->已关闭；6->售后中
    Default(0),//默认状态，此时查看支付状态
    WaitEnsure(1),//待确认
    Ensured(2),//已确认
    Canceled(3),//已取消
    Finished(4),//已完成
    Closed(5),//已关闭/拒绝接单
    AfterSale(6),//售后中

    //以下为原来的
    //   订单状态：12->待付款；2->待发货；3->已发货；4->已完成；5->售后订单 6->已关闭；
    INIT(12),//待付款
    PayNotNotice(1),//支付成功，没有回调
    TO_DELIVER(2),//待发货
    DELIVERED(3),  // 待收货
    TO_COMMENT(4),//待评价
    TRADE_SUCCESS(5), // 已完成

    RIGHT_APPLY(6), //售后中
    RIGHT_APPLYF_SUCCESS(7), // 售后已完成
    TO_SHARE(8),  // 待分享
    REFUNDING(13),  // 申请退款
    REFUND(14),  // 已退款
    //    CANCELED(7),
    CLOSED(15), // 已关闭 // 已取消 统一
    INVALID(16),//无效订单
    DELETED(17);//已删除


    private int value;

    private OrderStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
