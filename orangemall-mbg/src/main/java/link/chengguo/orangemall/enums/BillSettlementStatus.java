package link.chengguo.orangemall.enums;

/**
 *
 * 账单结算状态
 * @Auther: yizb
 * @Date: 2020-02-12
 * @Description:
 */
public enum BillSettlementStatus {
    // 0-未达到结算条件，1-待结算，2-已结算，3-已退款
    CannotSettlement(0),//未达到结算条件
    ReadySettlement(1),//待结算
    Settlemented(2),//已结算
    Refunded(3);//已退款
    private int value;

    BillSettlementStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
