package link.chengguo.orangemall.enums;

/**
 *
 * 提现方式
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum WithdrawType {
    // 0->支付宝；1->微信；2->银联,  3-线下
    Ali(0),//支付宝
    Wx(1),//微信
    Union(2),//银联
    Offline(3);//线下

    private int value;

    WithdrawType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
