package link.chengguo.orangemall.enums;

/**
 *
 * 账户变化原因
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum AccountChangeType {
    // 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单结算，5-提现申请,6-申请通过,7-申请拒绝
    OrderPay(0),//订单支付
    Refund(1),//退款申请
    RefundSuccess(2),//退款通过
    RefundFailure(3),//退款拒绝
    OrderSettlement(4),//订单结算
    WithdrawApply(5),//提现申请
    WithdrawSuccess(6),//申请通过
    WithdrawFailure(7);//申请拒绝
    private int value;

    AccountChangeType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
