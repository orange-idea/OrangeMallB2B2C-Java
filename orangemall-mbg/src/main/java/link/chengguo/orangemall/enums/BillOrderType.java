package link.chengguo.orangemall.enums;

/**
 *
 * 账单订单状态
 * @Auther: yizb
 * @Date: 2020-02-12
 * @Description:
 */
public enum BillOrderType {
    // 0-订单支付，1-退款申请，2-退款通过，3-退款拒绝，4-订单退款退货，5-订单退款退货通过,6-订单退款拒绝
    OrderPay(0),//订单支付
    Refund(1),//退款申请
    RefundSuccess(2),//退款通过
    RefundFailure(3),//退款拒绝
    ReturnRefundApply(4),//提现申请
    ReturnRefundSuccess(5),//申请通过
    ReturnRefundFailure(6);//申请拒绝
    private int value;

    BillOrderType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
