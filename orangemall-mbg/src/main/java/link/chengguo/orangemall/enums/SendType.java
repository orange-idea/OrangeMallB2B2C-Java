package link.chengguo.orangemall.enums;

/**
 *
 * 配送模式
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum SendType {
    // 1-到店扫码购，2-平台配送，3-商家配送，4-到店自提，5-预约到店，6-上门服务，7-物流配送
    CodeBuy(1),//到店扫码购
    PlatformDelivery(2),//平台配送
    ShopDelivery(3),//商家配送
    ShopPickup(4),//到店自提
    BookingConsume(5),//预约到店
    HourseService(6),//上门服务
    Express(7);//快递物流

    private int value;

    private SendType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
