package link.chengguo.orangemall.enums;

/**
 *
 * 支付方式
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum PayType {
    // 0->未支付；1->微信小程序；2->微信App,  3-支付宝，4-余额支付，5-积分，6-银联，7-划拨
    Default(0),//未支付
    WxApplet(1),//微信小程序
    WxApp(2),//微信App
    Ali(3),//支付宝
    Balance(4),//余额
    Integral(5),//积分
    Union(6),//银联
    Hand(7);//后台划拨

    private int value;

    PayType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
