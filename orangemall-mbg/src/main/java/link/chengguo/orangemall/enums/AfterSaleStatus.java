package link.chengguo.orangemall.enums;

/**
 *
 * 售后状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum AfterSaleStatus {

    // 售后状态：0->默认状态，1->退款申请，2->同意退款，3->拒绝退款，4->取消申请，5->退款退货申请，6->同意退款退货，7->拒绝退款退货，8->确认收货
    Default(0),//默认状态
    RefundApply(1),//退款申请
    AgreeRefund(2),//同意退款，此时订单结束，退款完成
    RefuseRefund(3),//拒绝退款,此时继续走正常流程。
    CancelRefund(4),//取消申请
    RefundGoodsApply(5),//退款退货申请
    AgreeRefundGoods(6),//同意退款退货
    RefuseRefundGoods(7),//拒绝退款退货，此时走正常流程
    EnsureRefundGoods(8);//确认收货，此时订单结束，退款退货完成

    private int value;

    private AfterSaleStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
