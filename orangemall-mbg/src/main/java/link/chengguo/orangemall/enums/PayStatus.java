package link.chengguo.orangemall.enums;

/**
 *
 * 支付状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum PayStatus {


    // 0->待付款；1->付款中；2->已付款
    WaitPay(0),//待付款
    Paing(1),//付款中
    Paid(2),//已付款，已经回调
    Failure(3);//支付失败，已经回调
    private int value;

    private PayStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
