package link.chengguo.orangemall.enums;

/**
 *
 * 物流状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum ExpressStatus {

    // 物流状态：0->未知；1->待发货，2->待收货，3->已收货
    Default(0),//未知，此时处于默认状态
    ToDeliver(1),//待发货
    Delivered(2),  // 待收货/已发货
    Finish(3);//已收货
    private int value;

    private ExpressStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
