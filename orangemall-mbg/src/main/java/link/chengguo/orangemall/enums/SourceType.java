package link.chengguo.orangemall.enums;

/**
 *
 *  来源方式
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum SourceType {
    // 0->PC订单；1->小程序订单；2->App订单 3-H5订单
    Pc(0),//PC订单
    Xcx(1),//小程序订单
    App(2),//App订单
    H5(3);//H5订单
    private int value;

    SourceType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
