package link.chengguo.orangemall.enums;

/**
 *
 * 配送状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum ShippingStatus {

    //0->未知,1-商家已接单/待抢单/待分配，2-已派单/已抢单/待取货，3-已到店/取货中，4-已取货/配送中，5-已送达，6-已取消，7-其他异常
    Default(0),//未知，此时处于默认状态
    WaitGrab(1),//商家已接单/待抢单/待分配
    WaitPickUp(2),//已派单/已抢单/待取货
    ArrivedShop(3),//已到店/取货中/备货中
    WaitFinish(4),//已取货/配送中
    Finished(5),//已收货/完成
    Canceled(6),//已取消
    Others(7);//其他异常

    // 0->未知；1->待抢单，2->待取货，3->已到店/取货中，4->配送中，5->已收货
//    Default(0),//未知，此时处于默认状态
//    WaitGrab(1),//待抢单
//    WaitPickUp(2),//待取货
//    ArrivedShop(3),//已到店、取货中、备货中
//    WaitFinish(4),//配送中、待上门取货
//    Finished(5);//已收货

    private int value;

    private ShippingStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
