package link.chengguo.orangemall.enums;

/**
 *
 * 拣货状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum PickingStatus {


    //拣货状态： 0->未知；1->待抢单；2->待拣货；3->拣货中；4->拣货完成；
    Default(0),//未知，此时处于默认状态
    WaitGrab(1),//待抢单
    WaitPicking(2),//待拣货
    Picking(3),//拣货中
    Picked(4);//拣货完成

    private int value;

    PickingStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
