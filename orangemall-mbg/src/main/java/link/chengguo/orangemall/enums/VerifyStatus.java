package link.chengguo.orangemall.enums;

/**
 *
 * 核销状态
 * @Auther: yizb
 * @Date: 2020-01-04
 * @Description:
 */
public enum VerifyStatus {

    // 0->未知；1->待核销；2->已核销
    Default(0),//未知，此时处于默认状态
    WaitVerify(1),//待核销
    Verifyed(2);//已核销


    private int value;

    private VerifyStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

}
