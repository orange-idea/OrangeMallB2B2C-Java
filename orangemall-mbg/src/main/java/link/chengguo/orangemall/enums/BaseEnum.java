package link.chengguo.orangemall.enums;

public interface BaseEnum<K> {
    K code();

    String desc();
}
