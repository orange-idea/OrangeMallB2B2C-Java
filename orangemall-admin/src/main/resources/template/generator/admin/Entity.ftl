package ${package}.entity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
<#if hasTimestamp>
    import java.sql.Timestamp;
</#if>
<#if hasBigDecimal>
    import java.math.BigDecimal;
</#if>
import java.io.Serializable;

/**
* @author ${author}
* @date ${date}
*/
@Data
@TableName("${tableName}")
public class ${className} implements Serializable {
<#if columns??>
    <#list columns as column>


        <#if column.changeColumnName = 'id'>
            @TableId(value = "id", type = IdType.AUTO)
        </#if>
        <#if column.changeColumnName = 'create_time'>
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @TableField(value = "create_time", fill = FieldFill.INSERT)
        </#if>
        <#if column.changeColumnName = 'update_time'>
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
            @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
        </#if>
        <#if column.changeColumnName != 'id'>
            @TableField( "${column.columnName}")
        </#if>
        private ${column.columnType} ${column.changeColumnName};
    </#list>
</#if>


}
