package link.chengguo.orangemall.ums.service;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import link.chengguo.orangemall.ums.entity.Sms;

/**
 * @author 作者 chengguo E-mail: 1264395832@qq.com
 * @version 创建时间：2017年11月12日 上午22:57:51
 * 短信service
 */
public interface ISmsService {

    /**
     * 保存短信
     *
     * @param sms
     * @param params
     */
    //void save(Sms sms, Map<String, String> params);

    void save(Sms sms, String params);

    /**
     * 修改短信
     *
     * @param sms
     */
    void update(Sms sms);

    /**
     * 查询短信
     *
     * @param id
     * @return
     */
    Sms findById(Long id);


    /**
     * 发送短信
     */
    SendSmsResponse sendSmsMsg(Sms sms);
}
