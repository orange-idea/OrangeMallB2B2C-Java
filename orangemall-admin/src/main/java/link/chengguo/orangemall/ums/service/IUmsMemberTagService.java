package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberTag;

/**
 * <p>
 * 用户标签表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberTagService extends IService<UmsMemberTag> {

}
