package link.chengguo.orangemall.ums.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.ums.entity.UmsMemberGroup;
import link.chengguo.orangemall.ums.service.UmsMemberGroupService;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Slf4j
@RestController
@Api(tags = "UmsMemberGroupController", description = "用户分组")
@RequestMapping("/ums/UmsMemberGroup")
public class UmsMemberGroupController {
    @Resource
    private UmsMemberGroupService umsMemberGroupService;

    @SysLog(MODULE = "ums", REMARK = "根据条件查询所有")
    @ApiOperation("根据条件查询所有列表")
    @GetMapping(value = "/all")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:read')")
    public Object all(UmsMemberGroup entity
    ) {
        try {
            return new CommonResult().success(umsMemberGroupService.list(new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "根据条件查询所有列表")
    @ApiOperation("根据条件查询所有列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:read')")
    public Object list(Integer pageNum,Integer pageSize) {
        try {
            QueryWrapper<UmsMemberGroup> queryWrapper = new QueryWrapper();
            return new CommonResult().success(umsMemberGroupService.page(new Page<UmsMemberGroup>(pageNum, pageSize),queryWrapper.orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "保存")
    @ApiOperation("保存")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:create')")
    public Object create(@RequestBody UmsMemberGroup entity) {
        try {
            entity.setCreationTime(new Date());
            if (umsMemberGroupService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "更新")
    @ApiOperation("更新")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:update')")
    public Object update(@RequestBody UmsMemberGroup entity) {
        try {
            if (umsMemberGroupService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "删除")
    @ApiOperation("删除")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:delete')")
    public Object delete(@ApiParam("id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("id");
            }
            if (umsMemberGroupService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "给分配")
    @ApiOperation("查询明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:read')")
    public Object getById(@ApiParam("id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("id");
            }
            UmsMemberGroup coupon = umsMemberGroupService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @ResponseBody
    @SysLog(MODULE = "pms", REMARK = "批量删除")
    @PreAuthorize("hasAuthority('ums:UmsMemberGroup:delete')")
    public Object deleteBatch(@RequestParam("ids") List<Integer> ids) {
        boolean count = umsMemberGroupService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

}
