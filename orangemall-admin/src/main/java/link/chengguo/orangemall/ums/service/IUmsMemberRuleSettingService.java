package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMemberRuleSetting;

/**
 * <p>
 * 会员积分成长规则表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberRuleSettingService extends IService<UmsMemberRuleSetting> {

}
