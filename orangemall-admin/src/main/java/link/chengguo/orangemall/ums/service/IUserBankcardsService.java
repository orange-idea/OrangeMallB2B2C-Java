package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UserBankcards;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IUserBankcardsService extends IService<UserBankcards> {

}
