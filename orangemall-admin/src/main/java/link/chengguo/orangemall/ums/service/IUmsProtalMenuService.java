package link.chengguo.orangemall.ums.service;

import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author chengguo
* @date 2020-08-31
*/

public interface IUmsProtalMenuService extends IService<UmsProtalMenu> {

}
