package link.chengguo.orangemall.ums.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import link.chengguo.orangemall.ums.service.IUmsProtalMenuService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author chengguo
 * @date 2020-08-31
 * umsProtalMenu
 */
@Slf4j
@RestController
@RequestMapping("/ums/umsProtalMenu")
public class UmsProtalMenuController {

    @Resource
    private IUmsProtalMenuService IUmsProtalMenuService;

    @SysLog(MODULE = "ums", REMARK = "根据条件查询所有umsProtalMenu列表")
    @ApiOperation("根据条件查询所有umsProtalMenu列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:read')")
    public Object getUmsProtalMenuByPage(UmsProtalMenu entity,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IUmsProtalMenuService.page(new Page<UmsProtalMenu>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有umsProtalMenu列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "保存umsProtalMenu")
    @ApiOperation("保存umsProtalMenu")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:create')")
    public Object saveUmsProtalMenu(@RequestBody UmsProtalMenu entity) {
        try {

            if (IUmsProtalMenuService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存umsProtalMenu：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "更新umsProtalMenu")
    @ApiOperation("更新umsProtalMenu")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:update')")
    public Object updateUmsProtalMenu(@RequestBody UmsProtalMenu entity) {
        try {
            if (IUmsProtalMenuService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新umsProtalMenu：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "删除umsProtalMenu")
    @ApiOperation("删除umsProtalMenu")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:delete')")
    public Object deleteUmsProtalMenu(@ApiParam("umsProtalMenuid") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("umsProtalMenuid");
            }
            if (IUmsProtalMenuService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除umsProtalMenu：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ums", REMARK = "给umsProtalMenu分配umsProtalMenu")
    @ApiOperation("查询umsProtalMenu明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:read')")
    public Object getUmsProtalMenuById(@ApiParam("umsProtalMenuid") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("umsProtalMenuid");
            }
            UmsProtalMenu coupon = IUmsProtalMenuService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询umsProtalMenu明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除umsProtalMenu")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "ums", REMARK = "批量删除umsProtalMenu")
    @PreAuthorize("hasAuthority('ums:umsProtalMenu:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IUmsProtalMenuService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "ums", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, UmsProtalMenu entity) {
        // 模拟从数据库获取需要导出的数据
        List<UmsProtalMenu> personList = IUmsProtalMenuService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", UmsProtalMenu.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "ums", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<UmsProtalMenu> personList = EasyPoiUtils.importExcel(file, UmsProtalMenu.class);
        IUmsProtalMenuService.saveBatch(personList);
    }
}


