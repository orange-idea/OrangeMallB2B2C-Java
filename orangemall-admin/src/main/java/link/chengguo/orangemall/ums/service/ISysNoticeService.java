package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.SysNotice;

/**
 * <p>
 * 公告表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface ISysNoticeService extends IService<SysNotice> {

}
