package link.chengguo.orangemall.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ums.entity.UmsMember;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IUmsMemberService extends IService<UmsMember> {

    void updataMemberOrderInfo();

    void refreshMember(Long id);

    boolean saveInfo(UmsMember entity);
}
