package link.chengguo.orangemall.ums.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 配送方式表 前端控制器
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@RestController
@RequestMapping("/ums/omsShip")
public class OmsShipController {

}

