package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsDraw;

/**
 * <p>
 * 一分钱抽奖 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsDrawService extends IService<SmsDraw> {

}
