package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsBasicGifts;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-07-07
 */
public interface ISmsBasicGiftsService extends IService<SmsBasicGifts> {

    int updateStatus(Long id, Integer status);
}
