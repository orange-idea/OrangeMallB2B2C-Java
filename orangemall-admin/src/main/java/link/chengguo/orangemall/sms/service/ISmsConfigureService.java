package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsConfigure;

/**
 * <p>
 * 商品配置表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-18
 */
public interface ISmsConfigureService extends IService<SmsConfigure> {

}
