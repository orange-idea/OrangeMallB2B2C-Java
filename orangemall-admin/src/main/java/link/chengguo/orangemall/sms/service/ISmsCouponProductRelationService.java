package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsCouponProductRelation;

/**
 * <p>
 * 优惠券和产品的关系表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsCouponProductRelationService extends IService<SmsCouponProductRelation> {

}
