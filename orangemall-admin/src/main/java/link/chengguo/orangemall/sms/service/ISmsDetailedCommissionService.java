package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsDetailedCommission;

/**
 * <p>
 * 分销佣金明细表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsDetailedCommissionService extends IService<SmsDetailedCommission> {

}
