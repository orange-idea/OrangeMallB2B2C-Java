package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsSignConfig;

/**
 * <p>
 * 签到配置表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsSignConfigService extends IService<SmsSignConfig> {

}
