package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsDrawUser;

/**
 * <p>
 * 抽奖与用户关联表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
public interface ISmsDrawUserService extends IService<SmsDrawUser> {

}
