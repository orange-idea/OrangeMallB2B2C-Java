package link.chengguo.orangemall.sms.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import link.chengguo.orangemall.ams.service.IAmsAgentService;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.service.IPmsProductCategoryService;
import link.chengguo.orangemall.pms.service.IPmsProductService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendCatetory;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendSubject;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendCategoryService;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendSubjectService;
import link.chengguo.orangemall.sms.vo.HomeProductCatetory;
import link.chengguo.orangemall.ums.entity.Sms;
import link.chengguo.orangemall.util.StringUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页推荐专题表
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Slf4j
@RestController
@Api(tags = "SmsHomeRecommendCatetoryController", description = "首页产品推荐表管理")
@RequestMapping("/sms/SmsHomeRecommendCatetory")
public class SmsHomeRecommendCategoryController {

    @Autowired
    private ISmsHomeRecommendCategoryService ISmsHomeRecommendCategoryService;
    @Autowired
    private IPmsProductCategoryService IPmsProductCategoryService;
    @Autowired
    private IPmsProductService IPmsProductService;


    @SysLog(MODULE = "sms", REMARK = "根据条件查询所有首页产品推荐表管理列表")
    @ApiOperation("根据条件查询所有首页推荐专题表列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:read')")
    public Object getSmsHomeRecommendCatetoryByPage(SmsHomeRecommendCatetory entity,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ISmsHomeRecommendCategoryService.page(new Page<SmsHomeRecommendCatetory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有首页推荐专题表列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }
    @SysLog(MODULE = "sms", REMARK = "根据条件查询所有首页产品推荐表管理列表")
    @ApiOperation("根据条件查询所有首页推荐专题表列表")
    @GetMapping(value = "/get/{id}")
    //@PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:read')")
    public Object getSmsHomeRecommendCatetoryById(@PathVariable("id") Long id) {
        //根据id查询所有相关联的数据
        try {
            HomeProductCatetory detail = ISmsHomeRecommendCategoryService.getDetailById(id);
            return new CommonResult().success(detail);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存首页产品推荐表：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
    }

    @SysLog(MODULE = "sms", REMARK = "保存首页推荐专题表")
    @ApiOperation("保存首页产品推荐表")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:create')")
    public Object saveSmsHomeRecommendSubject(@RequestBody SmsHomeRecommendCatetory entity) {
        try {
            if (ISmsHomeRecommendCategoryService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存首页产品推荐表：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sms", REMARK = "更新首页推荐专题表")
    @ApiOperation("更新首首页产品推荐表")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:update')")
    public Object updateSmsHomeRecommendSubject(@RequestBody SmsHomeRecommendCatetory entity) {
        try {
            if (ISmsHomeRecommendCategoryService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新首页推荐专题表：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sms", REMARK = "删除首页产品推荐表")
    @ApiOperation("删除首页产品推荐表")
    @PostMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:delete')")
    public Object deleteSmsHomeRecommendSubject(@ApiParam("首页推荐专题表id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("首页推荐专题表id");
            }
            if (ISmsHomeRecommendCategoryService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除首页产品推荐：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sms", REMARK = "给首页产品推荐表分配商品")
    @ApiOperation("查询首页产品专栏详细")
    @GetMapping(value = "/{id}")
    //  @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:read')")
    public Object getSmsHomeRecommendSubjectById(@ApiParam("首页产品专栏id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("缺少首页产品专栏id");
            }
            SmsHomeRecommendCatetory coupon = ISmsHomeRecommendCategoryService.getById(id);

            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询首页推荐专题表明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除首页推荐专题表")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.POST)
    @ResponseBody
    @SysLog(MODULE = "pms", REMARK = "批量删除首页推荐专题表")
    @PreAuthorize("hasAuthority('sms:SmsHomeRecommendCatetory:delete')")
    public Object deleteBatch(@RequestParam("ids") List<Long> ids) {
        boolean count = ISmsHomeRecommendCategoryService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @ApiOperation("修改推荐排序")
    @RequestMapping(value = "/update/sort/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object updateSort(@PathVariable Long id, Integer sort) {
        int count = ISmsHomeRecommendCategoryService.updateSort(id, sort);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("批量修改推荐状态")
    @RequestMapping(value = "/update/recommendStatus", method = RequestMethod.POST)
    @ResponseBody
    public Object updateRecommendStatus(@RequestParam("ids") List<Long> ids, @RequestParam Integer recommendStatus) {
        int count = ISmsHomeRecommendCategoryService.updateRecommendStatus(ids, recommendStatus);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }
}
