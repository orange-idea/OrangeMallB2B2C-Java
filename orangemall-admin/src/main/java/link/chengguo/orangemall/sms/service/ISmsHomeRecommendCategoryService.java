package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendCatetory;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendSubject;
import link.chengguo.orangemall.sms.vo.HomeProductCatetory;

import java.util.List;

/**
 * <p>
 * 首页推荐专题表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface ISmsHomeRecommendCategoryService extends IService<SmsHomeRecommendCatetory> {
    /**
     * 更新推荐状态
     */
    int updateRecommendStatus(List<Long> ids, Integer recommendStatus);

    /**
     * 修改推荐排序
     */
    int updateSort(Long id, Integer sort);

    int create(List<SmsHomeRecommendCatetory> homeBrandList);

    Page<SmsHomeRecommendCatetory> customPageList(Integer pageNum, Integer pageSize, SmsHomeRecommendCatetory entity);

    HomeProductCatetory getDetailById(Long id);
}
