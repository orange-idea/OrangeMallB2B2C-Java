package link.chengguo.orangemall.sms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sms.entity.SmsContent;

/**
 * @author orangemall
 * @date 2019-12-07
 */

public interface ISmsContentService extends IService<SmsContent> {

}
