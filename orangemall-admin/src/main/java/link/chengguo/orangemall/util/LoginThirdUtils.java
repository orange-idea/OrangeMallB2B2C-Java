package link.chengguo.orangemall.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import link.chengguo.orangemall.utils.CommonResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录子平台，如租户平台登录代理平台等
 */
public class LoginThirdUtils {

    private static final Logger logger = LogManager.getLogger(HttpUtils.class);
    //请求地址
    private static String URLByAdmin="http://localhost:8085/sys/sysUser/loginByAdmin";
    private static String URLByAgent="http://localhost:8087/sys/sysUser/loginByAgent";

    public static CommonResult loginByAdmin(String params,String baseUrl) throws Exception{
        URLByAdmin=baseUrl+":8085/sys/sysUser/loginByAdmin";
        logger.info("请求参数为:" + params);
        try {
            String result= HttpUtils.post(URLByAdmin, params);
            logger.info("返回参数为:" + result);
            JSONObject jsonObject =  JSON.parseObject(result);
            String data = jsonObject.get("data").toString();
            JSONObject dataObject =  JSON.parseObject(data);
            String token=dataObject.get("token").toString();
            String tokenHead=dataObject.get("tokenHead").toString();
            Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", token);
            tokenMap.put("tokenHead", tokenHead);
            String code = jsonObject.get("code").toString();
            //发送成功
            if("200".equals(code)){
                return  new CommonResult().success(tokenMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
            logger.error("请求异常：" + e);
            return  new CommonResult().failed("请求异常"+e.getMessage());
        }
        return  new CommonResult().failed("登录失败");
    }
    public static CommonResult loginByAgent(String params,String baseUrl) throws Exception{
        URLByAgent=baseUrl+":8087/sys/sysUser/loginByAgent";
        logger.info("请求参数为:" + params);
        try {
            String result= HttpUtils.post(URLByAgent, params);
            logger.info("返回参数为:" + result);
            JSONObject jsonObject =  JSON.parseObject(result);
            String data = jsonObject.get("data").toString();
            JSONObject dataObject =  JSON.parseObject(data);
            String token=dataObject.get("token").toString();
            String tokenHead=dataObject.get("tokenHead").toString();
            Map<String, Object> tokenMap = new HashMap<>();
            tokenMap.put("token", token);
            tokenMap.put("tokenHead", tokenHead);
            String code = jsonObject.get("code").toString();
            //发送成功
            if("200".equals(code)){
                return  new CommonResult().success(tokenMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("请求异常：" + e);
            return  new CommonResult().failed("请求异常"+e.getMessage());
        }
        return  new CommonResult().failed("登录失败");
    }

    public static boolean hasAuthority(String token, String baseUrl){
        try {
            String result= HttpUtils.postAuthority(token,baseUrl);
            logger.info("返回参数为:" + result);
            boolean data = JSON.parseObject(result,Boolean.class);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("请求异常：" + e);
            return  false;
        }
    }

}
