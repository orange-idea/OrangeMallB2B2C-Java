package link.chengguo.orangemall.mms.service;

import link.chengguo.orangemall.mms.entity.MmsCardTitle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-22
*/

public interface IMmsCardTitleService extends IService<MmsCardTitle> {

    /**
     * 添加卡券
     * @param title
     * @return
     */
    boolean addCardTitle(MmsCardTitle title);

    /**
     * 删除卡券
     * @param id
     * @return
     */
    boolean deleteCardTitle(Integer id);


}
