package link.chengguo.orangemall.mms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.mms.entity.MmsCardOperationLog;
import link.chengguo.orangemall.mms.service.IMmsCardOperationLogService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-22
 * 卡券操作记录
 */
@Slf4j
@RestController
@Api(tags = "充值卡券操作记录", description = "MmsCardOperationLogController")
@RequestMapping("/mms/mmsCardOperationLog")
public class MmsCardOperationLogController {

    @Resource
    private IMmsCardOperationLogService IMmsCardOperationLogService;

    @SysLog(MODULE = "mms", REMARK = "根据条件查询所有卡券操作记录列表")
    @ApiOperation("根据条件查询所有卡券操作记录列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:read')")
    public Object getMmsCardOperationLogByPage(MmsCardOperationLog entity,
                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper(entity);
            if (ValidatorUtils.notEmpty(entity.getKeyword())){
                queryWrapper.apply("( card_title like '%"+entity.getKeyword()+"%'  or name like '%"+entity.getKeyword()+"%')");
            }
            if (ValidatorUtils.notEmpty(entity.getSearchTime())){
                queryWrapper.like("create_time",entity.getSearchTime());
            }
            queryWrapper.orderByDesc("id");
            return new CommonResult().success(IMmsCardOperationLogService.page(new Page<>(pageNum, pageSize), queryWrapper));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有卡券操作记录列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "保存卡券操作记录")
    @ApiOperation("保存卡券操作记录")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:create')")
    public Object saveMmsCardOperationLog(@RequestBody MmsCardOperationLog entity) {
        try {

            if (IMmsCardOperationLogService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存卡券操作记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "更新卡券操作记录")
    @ApiOperation("更新卡券操作记录")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:update')")
    public Object updateMmsCardOperationLog(@RequestBody MmsCardOperationLog entity) {
        try {
            if (IMmsCardOperationLogService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新卡券操作记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "删除卡券操作记录")
    @ApiOperation("删除卡券操作记录")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:delete')")
    public Object deleteMmsCardOperationLog(@ApiParam("卡券操作记录id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("卡券操作记录id");
            }
            if (IMmsCardOperationLogService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除卡券操作记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "给卡券操作记录分配卡券操作记录")
    @ApiOperation("查询卡券操作记录明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:read')")
    public Object getMmsCardOperationLogById(@ApiParam("卡券操作记录id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("卡券操作记录id");
            }
            MmsCardOperationLog coupon = IMmsCardOperationLogService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询卡券操作记录明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除卡券操作记录")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "mms", REMARK = "批量删除卡券操作记录")
    @PreAuthorize("hasAuthority('mms:mmsCardOperationLog:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IMmsCardOperationLogService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "mms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, MmsCardOperationLog entity) {
        // 模拟从数据库获取需要导出的数据
        List<MmsCardOperationLog> personList = IMmsCardOperationLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", MmsCardOperationLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "mms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<MmsCardOperationLog> personList = EasyPoiUtils.importExcel(file, MmsCardOperationLog.class);
        IMmsCardOperationLogService.saveBatch(personList);
    }
}


