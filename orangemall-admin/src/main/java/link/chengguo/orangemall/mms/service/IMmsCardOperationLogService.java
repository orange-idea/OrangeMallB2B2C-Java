package link.chengguo.orangemall.mms.service;

import link.chengguo.orangemall.mms.entity.MmsCardOperationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-22
*/

public interface IMmsCardOperationLogService extends IService<MmsCardOperationLog> {

    /**
     * 添加激活记录
     * @param log
     * @return
     */
    boolean addActiveLog(MmsCardOperationLog log);

    /**
     * 添加作废记录
     * @param log
     * @return
     */
    boolean addInvalidLog(MmsCardOperationLog log);

}
