package link.chengguo.orangemall.mms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.mms.entity.MmsCardTitle;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import link.chengguo.orangemall.mms.service.IMmsCardTitleService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-22
 * 充值卡券标题
 */
@Slf4j
@RestController
@Api(tags = "充值卡券标题", description = "MmsCardTitleController")
@RequestMapping("/mms/mmsCardTitle")
public class MmsCardTitleController {

    @Resource
    private IMmsCardTitleService IMmsCardTitleService;
    @Resource
    private IMmsCardDetailService cardDetailService;

    @SysLog(MODULE = "mms", REMARK = "根据条件查询所有充值卡券列表")
    @ApiOperation("根据条件查询所有充值卡券列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:read')")
    public Object getMmsCardTitleByPage(MmsCardTitle entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper(entity);
            if (ValidatorUtils.notEmpty(entity.getKeyword())){
                queryWrapper.like("title",entity.getKeyword());
            }
            queryWrapper.orderByDesc("id");
            return new CommonResult().success(IMmsCardTitleService.page(new Page<MmsCardTitle>(pageNum, pageSize), queryWrapper));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有充值卡券列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "保存充值卡券")
    @ApiOperation("保存充值卡券")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:create')")
    public Object saveMmsCardTitle(@RequestBody MmsCardTitle entity) {
        try {

            if (IMmsCardTitleService.addCardTitle(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存充值卡券：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "激活某个标题下的全部充值卡券")
    @ApiOperation("激活某个标题下的全部充值卡券")
    @PostMapping(value = "/acitveAllById")
    public Object acitveAllById(@RequestBody MmsCardTitle entity) {
        try {
            if (cardDetailService.activeAllByTitleId(entity.getId())) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存充值卡券：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "mms", REMARK = "更新充值卡券")
    @ApiOperation("更新充值卡券标题")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:update')")
    public Object updateMmsCardTitle(@RequestBody MmsCardTitle entity) {
        try {
            if (IMmsCardTitleService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新充值卡券标题：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "删除充值卡券标题")
    @ApiOperation("删除充值卡券标题")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:delete')")
    public Object deleteMmsCardTitle(@ApiParam("充值卡券标题id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("充值卡券标题id");
            }
            if (IMmsCardTitleService.deleteCardTitle(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除充值卡券标题：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "给充值卡券标题分配充值卡券标题")
    @ApiOperation("查询充值卡券标题明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:read')")
    public Object getMmsCardTitleById(@ApiParam("充值卡券标题id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("充值卡券标题id");
            }
            MmsCardTitle coupon = IMmsCardTitleService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询充值卡券标题明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除充值卡券标题")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "mms", REMARK = "批量删除充值卡券标题")
    @PreAuthorize("hasAuthority('mms:mmsCardTitle:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Integer> ids) {
        boolean count = IMmsCardTitleService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "mms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, MmsCardTitle entity) {
        // 模拟从数据库获取需要导出的数据
        List<MmsCardTitle> personList = IMmsCardTitleService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", MmsCardTitle.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "mms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<MmsCardTitle> personList = EasyPoiUtils.importExcel(file, MmsCardTitle.class);
        IMmsCardTitleService.saveBatch(personList);
    }
}


