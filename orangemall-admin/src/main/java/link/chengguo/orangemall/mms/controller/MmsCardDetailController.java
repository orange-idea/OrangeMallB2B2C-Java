package link.chengguo.orangemall.mms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-22
 * 充值卡券详情
 */
@Slf4j
@RestController
@Api(tags = "充值卡券详情", description = "MmsCardDetailController")
@RequestMapping("/mms/mmsCardDetail")
public class MmsCardDetailController {

    @Resource
    private IMmsCardDetailService IMmsCardDetailService;

    @SysLog(MODULE = "mms", REMARK = "根据条件查询所有充值卡券详情列表")
    @ApiOperation("根据条件查询所有充值卡券详情列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:read')")
    public Object getMmsCardDetailByPage(MmsCardDetail entity,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper(entity);
            if (ValidatorUtils.notEmpty(entity.getKeyword())){
                queryWrapper.like("id",entity.getKeyword());
            }
            queryWrapper.orderByDesc("id");
            return new CommonResult().success(IMmsCardDetailService.page(new Page<>(pageNum, pageSize), queryWrapper));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有充值卡券详情列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "保存充值卡券详情")
    @ApiOperation("保存充值卡券详情")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:create')")
    public Object saveMmsCardDetail(@RequestBody MmsCardDetail entity) {
        try {
            if (IMmsCardDetailService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存充值卡券详情：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "激活充值卡券")
    @ApiOperation("激活充值卡券")
    @PostMapping(value = "/activeById")
    public Object activeById(@RequestBody MmsCardDetail entity) {
        try {
            if (IMmsCardDetailService.activeById(entity.getId())) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("激活充值卡券：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "批量激活充值卡券")
    @ApiOperation("批量激活充值卡券")
    @PostMapping(value = "/batchActiveByIds")
    public Object batchActiveByIds(@RequestParam("titleId") Integer titleId,@RequestParam("ids") String ids) {
        try {
            if (IMmsCardDetailService.activeByIds(titleId,ids)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("激活充值卡券：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "mms", REMARK = "作废充值卡券")
    @ApiOperation("作废充值卡券")
    @PostMapping(value = "/invalidById")
    public Object invalidById(@RequestBody MmsCardDetail entity) {
        try {
            if (IMmsCardDetailService.invalidById(entity.getId())) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("作废充值卡券：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "更新充值卡券详情")
    @ApiOperation("更新充值卡券详情")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:update')")
    public Object updateMmsCardDetail(@RequestBody MmsCardDetail entity) {
        try {
            if (IMmsCardDetailService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新充值卡券详情：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "删除充值卡券详情")
    @ApiOperation("删除充值卡券详情")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:delete')")
    public Object deleteMmsCardDetail(@ApiParam("充值卡券详情id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("充值卡券详情id");
            }
            if (IMmsCardDetailService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除充值卡券详情：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "mms", REMARK = "给充值卡券详情分配充值卡券详情")
    @ApiOperation("查询充值卡券详情明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:read')")
    public Object getMmsCardDetailById(@ApiParam("充值卡券详情id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("充值卡券详情id");
            }
            MmsCardDetail coupon = IMmsCardDetailService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询充值卡券详情明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除充值卡券详情")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "mms", REMARK = "批量删除充值卡券详情")
    @PreAuthorize("hasAuthority('mms:mmsCardDetail:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IMmsCardDetailService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "mms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, MmsCardDetail entity) {
        // 模拟从数据库获取需要导出的数据
        List<MmsCardDetail> personList = IMmsCardDetailService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", MmsCardDetail.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "mms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<MmsCardDetail> personList = EasyPoiUtils.importExcel(file, MmsCardDetail.class);
        IMmsCardDetailService.saveBatch(personList);
    }
}


