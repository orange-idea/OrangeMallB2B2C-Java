package link.chengguo.orangemall.mms.service;

import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.mms.entity.MmsCardTitle;

/**
* @author yzb
* @date 2020-02-22
*/

public interface IMmsCardDetailService extends IService<MmsCardDetail> {

    /**
     *  添加卡券详情
     * @param detail
     * @return
     */
    boolean addCardDetail(MmsCardDetail detail);

    /**
     *  根据卡券标题批量添加卡券详情
     * @param title
     * @return
     */
    boolean addBatchCardDetail(MmsCardTitle title);

    /**
     *  根据卡券标题ID激活下面的全部卡券
     * @param titleId
     * @return
     */
    boolean activeAllByTitleId(Integer titleId);

    /**
     *  批量激活卡券
     * @param ids
     * @param titleId 所属标题ID
     * @return
     */
    boolean activeByIds(Integer titleId,String ids);

    /**
     *  根据卡券ID激活卡券
     * @param id
     * @return
     */
    boolean activeById(Long id);


    /**
     *  根据卡券ID作废卡券
     * @param id
     * @return
     */
    boolean invalidById(Long id);

}
