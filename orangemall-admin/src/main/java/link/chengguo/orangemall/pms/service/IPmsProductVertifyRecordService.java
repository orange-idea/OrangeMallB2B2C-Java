package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsProductVertifyRecord;

/**
 * <p>
 * 商品审核记录 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsProductVertifyRecordService extends IService<PmsProductVertifyRecord> {

}
