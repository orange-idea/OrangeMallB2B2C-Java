package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsMemberPrice;

/**
 * <p>
 * 商品会员价格表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsMemberPriceService extends IService<PmsMemberPrice> {

}
