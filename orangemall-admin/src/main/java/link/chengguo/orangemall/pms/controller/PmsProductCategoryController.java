package link.chengguo.orangemall.pms.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.pms.entity.PmsProductCategory;
import link.chengguo.orangemall.pms.vo.PmsProductCategoryWithChildrenItem;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 产品分类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Slf4j
@RestController
@Api(tags = "PmsProductCategoryController", description = "产品分类管理")
@RequestMapping("/pms/PmsProductCategory")
public class PmsProductCategoryController {
    @Resource
    private link.chengguo.orangemall.pms.service.IPmsProductCategoryService IPmsProductCategoryService;

    @SysLog(MODULE = "pms", REMARK = "根据条件查询所有产品分类列表")
    @ApiOperation("根据条件查询所有产品分类列表")
    @GetMapping(value = "/list")
    public Object getPmsProductCategoryByPage(PmsProductCategory entity,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                entity.setCityCode(UserUtils.getCurrentMember().getCityCode());
            }
            entity.setType(AllEnum.CategoryType.Agent.code());//筛选平台分类
            return new CommonResult().success(IPmsProductCategoryService.page(new Page<PmsProductCategory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有产品分类列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @ApiOperation("分页查询商品分类")
    @RequestMapping(value = "/list/{parentId}", method = RequestMethod.GET)
    @ResponseBody
    public Object getList(@PathVariable Long parentId,
                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        PmsProductCategory entity = new PmsProductCategory();
        if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
            entity.setCityCode(UserUtils.getCurrentMember().getCityCode());
        }
        entity.setType(AllEnum.CategoryType.Agent.code());//筛选平台分类
        entity.setParentId(parentId);
        return new CommonResult().success(IPmsProductCategoryService.page(new Page<PmsProductCategory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
    }

    @SysLog(MODULE = "pms", REMARK = "保存产品分类")
    @ApiOperation("保存产品分类")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:create')")
    public Object savePmsProductCategory(@RequestBody PmsProductCategory entity) {
        try {
            if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                entity.setCityCode(UserUtils.getCurrentMember().getCityCode());
            }
            entity.setType(AllEnum.CategoryType.Agent.code());//筛选平台分类
            if (IPmsProductCategoryService.saveAnd(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存产品分类：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "pms", REMARK = "更新产品分类")
    @ApiOperation("更新产品分类")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:update')")
    public Object updatePmsProductCategory(@RequestBody PmsProductCategory entity) {
        try {
            if (IPmsProductCategoryService.updateAnd(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新产品分类：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "pms", REMARK = "删除产品分类")
    @ApiOperation("删除产品分类")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:delete')")
    public Object deletePmsProductCategory(@ApiParam("产品分类id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("产品分类id");
            }
            if (IPmsProductCategoryService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除产品分类：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "pms", REMARK = "给产品分类分配产品分类")
    @ApiOperation("查询产品分类明细")
    @GetMapping(value = "/{id}")
    public Object getPmsProductCategoryById(@ApiParam("产品分类id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("产品分类id");
            }
            PmsProductCategory coupon = IPmsProductCategoryService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询产品分类明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除产品分类")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @ResponseBody
    @SysLog(MODULE = "pms", REMARK = "批量删除产品分类")
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:delete')")
    public Object deleteBatch(@RequestParam("ids") List<Long> ids) {
        boolean count = IPmsProductCategoryService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation("查询所有一级分类及子分类")
    @RequestMapping(value = "/list/withChildren", method = RequestMethod.GET)
    @ResponseBody
    public Object listWithChildren(PmsProductCategory category) {
        List<PmsProductCategoryWithChildrenItem> list = IPmsProductCategoryService.listWithChildren(category);
        return new CommonResult().success(list);
    }

    @ApiOperation("修改导航栏显示状态")
    @RequestMapping(value = "/update/navStatus", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:update')")
    public Object updateNavStatus(@RequestParam("ids") Long ids, @RequestParam("navStatus") Integer navStatus) {
        PmsProductCategory entity = new PmsProductCategory();
        entity.setId(ids);
        entity.setNavStatus(navStatus);

        if (IPmsProductCategoryService.updateById(entity)) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed();
        }
    }
//    public Object updateNavStatus(@RequestParam("ids") List<Long> ids, @RequestParam("navStatus") Integer navStatus) {
//        int count = IPmsProductCategoryService.updateNavStatus(ids, navStatus);
//        if (count > 0) {
//            return new CommonResult().success(count);
//        } else {
//            return new CommonResult().failed();
//        }
//    }

    @ApiOperation("修改显示状态")
    @RequestMapping(value = "/update/showStatus", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:update')")
    public Object updateShowStatus(@RequestParam("ids") Long ids, @RequestParam("showStatus") Integer showStatus) {
        PmsProductCategory entity = new PmsProductCategory();
        entity.setId(ids);
        entity.setShowStatus(showStatus);
        if (IPmsProductCategoryService.updateById(entity)) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed();
        }
    }

    @ApiOperation("修改首页显示状态")
    @RequestMapping(value = "/update/indexStatus", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('pms:PmsProductCategory:update')")
    public Object updateIndexStatus(@RequestParam("ids") Long ids, @RequestParam("indexStatus") Integer indexStatus) {
        PmsProductCategory entity = new PmsProductCategory();
        entity.setId(ids);
        entity.setIndexStatus(indexStatus);
        if (IPmsProductCategoryService.updateById(entity)) {
            return new CommonResult().success();
        } else {
            return new CommonResult().failed();
        }
    }

}
