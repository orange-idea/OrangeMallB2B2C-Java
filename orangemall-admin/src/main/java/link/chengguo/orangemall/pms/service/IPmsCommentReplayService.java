package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsCommentReplay;

/**
 * <p>
 * 产品评价回复表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsCommentReplayService extends IService<PmsCommentReplay> {

}
