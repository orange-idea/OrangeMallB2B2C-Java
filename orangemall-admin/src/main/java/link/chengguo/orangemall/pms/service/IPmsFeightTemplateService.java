package link.chengguo.orangemall.pms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.pms.entity.PmsFeightTemplate;

/**
 * <p>
 * 运费模版 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
public interface IPmsFeightTemplateService extends IService<PmsFeightTemplate> {

}
