package link.chengguo.orangemall.ams.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.service.IAmsBusinessCityService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 经营城市
 */
@Slf4j
@RestController
@RequestMapping("/ams/amsBusinessCity")
public class AmsBusinessCityController {

    @Resource
    private IAmsBusinessCityService IAmsBusinessCityService;

    @SysLog(MODULE = "ams", REMARK = "根据条件查询所有经营城市列表")
    @ApiOperation("根据条件查询所有经营城市列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:read')")
    public Object getAmsBusinessCityByPage(AmsBusinessCity entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IAmsBusinessCityService.page(new Page<AmsBusinessCity>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有经营城市列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "ams", REMARK = "根据条件查询所有经营城市列表")
    @ApiOperation("根据条件查询所有经营城市列表")
    @GetMapping(value = "/all")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:read')")
    public Object getAmsBusinessCityByPage(AmsBusinessCity entity) {
        try {
            return new CommonResult().success(IAmsBusinessCityService.list(new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有经营城市列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "保存经营城市")
    @ApiOperation("保存经营城市")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:create')")
    public Object saveAmsBusinessCity(@RequestBody AmsBusinessCity entity) {
        try {

            if (IAmsBusinessCityService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "更新经营城市")
    @ApiOperation("更新经营城市")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:update')")
    public Object updateAmsBusinessCity(@RequestBody AmsBusinessCity entity) {
        try {
            if (IAmsBusinessCityService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "删除经营城市")
    @ApiOperation("删除经营城市")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:delete')")
    public Object deleteAmsBusinessCity(@ApiParam("经营城市id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("经营城市id");
            }
            if (IAmsBusinessCityService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除经营城市：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "给经营城市分配经营城市")
    @ApiOperation("查询经营城市明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:read')")
    public Object getAmsBusinessCityById(@ApiParam("经营城市id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("经营城市id");
            }
            AmsBusinessCity coupon = IAmsBusinessCityService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询经营城市明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除经营城市")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "ams", REMARK = "批量删除经营城市")
    @PreAuthorize("hasAuthority('ams:amsBusinessCity:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IAmsBusinessCityService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "ams", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, AmsBusinessCity entity) {
        // 模拟从数据库获取需要导出的数据
        List<AmsBusinessCity> personList = IAmsBusinessCityService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", AmsBusinessCity.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "ams", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<AmsBusinessCity> personList = EasyPoiUtils.importExcel(file, AmsBusinessCity.class);
        IAmsBusinessCityService.saveBatch(personList);
    }
}


