package link.chengguo.orangemall.ams.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.entity.AmsSettings;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-12
 * 代理配置
 */
@Slf4j
@RestController
@RequestMapping("/ams/amsSettings")
public class AmsSettingsController {

    @Resource
    private link.chengguo.orangemall.ams.service.IAmsSettingsService IAmsSettingsService;

    @SysLog(MODULE = "ams", REMARK = "根据条件查询所有代理配置列表")
    @ApiOperation("根据条件查询所有代理配置列表")
    @GetMapping(value = "/list")
    public Object getAmsSettingsByPage(AmsSettings entity,
                                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IAmsSettingsService.page(new Page<AmsSettings>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有代理配置列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "保存代理配置")
    @ApiOperation("保存代理配置")
    @PostMapping(value = "/create")
    public Object saveAmsSettings(@RequestBody AmsSettings entity) {
        try {
            if (IAmsSettingsService.addAmsSettings(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存代理配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "更新代理配置")
    @ApiOperation("更新代理配置")
    @PostMapping(value = "/update/{id}")
    public Object updateAmsSettings(@RequestBody AmsSettings entity) {
        try {
            if (IAmsSettingsService.updateAmsSettings(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新代理配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "删除代理配置")
    @ApiOperation("删除代理配置")
    @GetMapping(value = "/delete/{id}")
    public Object deleteAmsSettings(@ApiParam("代理配置id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理配置id");
            }
            if (IAmsSettingsService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除代理配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "给代理配置分配代理配置")
    @ApiOperation("查询代理配置明细")
    @GetMapping(value = "/{id}")
    public Object getAmsSettingsById(@ApiParam("代理配置id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理配置id");
            }
            AmsSettings coupon = IAmsSettingsService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询代理配置明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除代理配置")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "ams", REMARK = "批量删除代理配置")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IAmsSettingsService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "ams", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, AmsSettings entity) {
        // 模拟从数据库获取需要导出的数据
        List<AmsSettings> personList = IAmsSettingsService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", AmsSettings.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "ams", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<AmsSettings> personList = EasyPoiUtils.importExcel(file, AmsSettings.class);
        IAmsSettingsService.saveBatch(personList);
    }
}


