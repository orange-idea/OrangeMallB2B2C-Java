package link.chengguo.orangemall.ams.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.entity.AmsAgentBlanceLog;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-20
 * 代理账户日志
 */
@Slf4j
@RestController
@RequestMapping("/ams/amsAgentBlanceLog")
public class AmsAgentBlanceLogController {

    @Resource
    private link.chengguo.orangemall.ams.service.IAmsAgentBlanceLogService IAmsAgentBlanceLogService;

    @SysLog(MODULE = "ams", REMARK = "根据条件查询所有代理账户日志列表")
    @ApiOperation("根据条件查询所有代理账户日志列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:read')")
    public Object getAmsAgentBlanceLogByPage(AmsAgentBlanceLog entity,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IAmsAgentBlanceLogService.page(new Page<AmsAgentBlanceLog>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有代理账户日志列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "保存代理账户日志")
    @ApiOperation("保存代理账户日志")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:create')")
    public Object saveAmsAgentBlanceLog(@RequestBody AmsAgentBlanceLog entity) {
        try {

            if (IAmsAgentBlanceLogService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存代理账户日志：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "更新代理账户日志")
    @ApiOperation("更新代理账户日志")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:update')")
    public Object updateAmsAgentBlanceLog(@RequestBody AmsAgentBlanceLog entity) {
        try {
            if (IAmsAgentBlanceLogService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新代理账户日志：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "删除代理账户日志")
    @ApiOperation("删除代理账户日志")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:delete')")
    public Object deleteAmsAgentBlanceLog(@ApiParam("代理账户日志id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理账户日志id");
            }
            if (IAmsAgentBlanceLogService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除代理账户日志：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "给代理账户日志分配代理账户日志")
    @ApiOperation("查询代理账户日志明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:read')")
    public Object getAmsAgentBlanceLogById(@ApiParam("代理账户日志id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理账户日志id");
            }
            AmsAgentBlanceLog coupon = IAmsAgentBlanceLogService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询代理账户日志明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除代理账户日志")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "ams", REMARK = "批量删除代理账户日志")
    @PreAuthorize("hasAuthority('ams:amsAgentBlanceLog:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IAmsAgentBlanceLogService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "ams", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, AmsAgentBlanceLog entity) {
        // 模拟从数据库获取需要导出的数据
        List<AmsAgentBlanceLog> personList = IAmsAgentBlanceLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", AmsAgentBlanceLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "ams", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<AmsAgentBlanceLog> personList = EasyPoiUtils.importExcel(file, AmsAgentBlanceLog.class);
        IAmsAgentBlanceLogService.saveBatch(personList);
    }
}


