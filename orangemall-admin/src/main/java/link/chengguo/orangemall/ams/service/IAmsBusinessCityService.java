package link.chengguo.orangemall.ams.service;

import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-21
*/

public interface IAmsBusinessCityService extends IService<AmsBusinessCity> {

    AmsBusinessCity getCityByCityCode(String cityCode);
}
