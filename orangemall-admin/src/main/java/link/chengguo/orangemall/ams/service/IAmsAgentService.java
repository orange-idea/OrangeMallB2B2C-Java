package link.chengguo.orangemall.ams.service;

import link.chengguo.orangemall.ams.entity.AmsAgent;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.model.params.AmsAgentParam;
import link.chengguo.orangemall.ams.model.result.AmsAgentResult;

/**
* @author yzb
* @date 2019-12-21
*/

public interface IAmsAgentService extends IService<AmsAgent> {



    /**
     * 更新管理员登录密码
     * @param agentId
     * @return
     */
    boolean resetAgentPwd(Long agentId) throws Exception;

    /**
     * 创建代理
     * @param amsAgent
     * @return
     */
    boolean createAgent(AmsAgent amsAgent) throws Exception;

    /**
     * 创建代理
     * @param amsAgent
     * @return
     */
    boolean updateAgent(AmsAgent amsAgent) throws Exception;

    /**
     * 根据城市编码查询代理
     * @param cityCode
     * @return
     */
    AmsAgent getAgentByCityCode(String cityCode);




    /**
     * 修改代理登录密码
     * @return
     */
    boolean updateAgentPwd(Long userId, String oldPwd, String newPwd) throws Exception;


    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<AmsAgentResult> customPageList(Integer pageNum, Integer pageSize, AmsAgentParam paramCondition);
}
