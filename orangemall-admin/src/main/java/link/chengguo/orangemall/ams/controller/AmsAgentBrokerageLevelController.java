package link.chengguo.orangemall.ams.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.ams.entity.AmsAgentBrokerageLevel;
import link.chengguo.orangemall.ams.service.IAmsAgentBrokerageLevelService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 抽佣管理
 */
@Slf4j
@RestController
@RequestMapping("/ams/amsAgentBrokerageLevel")
public class AmsAgentBrokerageLevelController {

    @Resource
    private IAmsAgentBrokerageLevelService IAmsAgentBrokerageLevelService;

    @SysLog(MODULE = "ams", REMARK = "根据条件查询所有抽佣管理列表")
    @ApiOperation("根据条件查询所有抽佣管理列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:read')")
    public Object getAmsAgentBrokerageLevelByPage(AmsAgentBrokerageLevel entity,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IAmsAgentBrokerageLevelService.page(new Page<AmsAgentBrokerageLevel>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有抽佣管理列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "保存抽佣管理")
    @ApiOperation("保存抽佣管理")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:create')")
    public Object saveAmsAgentBrokerageLevel(@RequestBody AmsAgentBrokerageLevel entity) {
        try {

            if (IAmsAgentBrokerageLevelService.add(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存抽佣管理：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "更新抽佣管理")
    @ApiOperation("更新抽佣管理")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:update')")
    public Object updateAmsAgentBrokerageLevel(@RequestBody AmsAgentBrokerageLevel entity) {
        try {
            if (IAmsAgentBrokerageLevelService.update(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新抽佣管理：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "删除抽佣管理")
    @ApiOperation("删除抽佣管理")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:delete')")
    public Object deleteAmsAgentBrokerageLevel(@ApiParam("抽佣管理id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("抽佣管理id");
            }
            if (IAmsAgentBrokerageLevelService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除抽佣管理：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "ams", REMARK = "给抽佣管理分配抽佣管理")
    @ApiOperation("查询抽佣管理明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:read')")
    public Object getAmsAgentBrokerageLevelById(@ApiParam("抽佣管理id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("抽佣管理id");
            }
            AmsAgentBrokerageLevel coupon = IAmsAgentBrokerageLevelService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询抽佣管理明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除抽佣管理")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "ams", REMARK = "批量删除抽佣管理")
    @PreAuthorize("hasAuthority('ams:amsAgentBrokerageLevel:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IAmsAgentBrokerageLevelService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "ams", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, AmsAgentBrokerageLevel entity) {
        // 模拟从数据库获取需要导出的数据
        List<AmsAgentBrokerageLevel> personList = IAmsAgentBrokerageLevelService.list(new QueryWrapper<>(entity).orderByDesc("sort"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", AmsAgentBrokerageLevel.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "ams", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<AmsAgentBrokerageLevel> personList = EasyPoiUtils.importExcel(file, AmsAgentBrokerageLevel.class);
        IAmsAgentBrokerageLevelService.saveBatch(personList);
    }
}


