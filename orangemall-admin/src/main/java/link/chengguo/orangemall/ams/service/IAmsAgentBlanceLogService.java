package link.chengguo.orangemall.ams.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ams.entity.AmsAgentBlanceLog;

/**
* @author yzb
* @date 2020-02-20
*/

public interface IAmsAgentBlanceLogService extends IService<AmsAgentBlanceLog> {

}
