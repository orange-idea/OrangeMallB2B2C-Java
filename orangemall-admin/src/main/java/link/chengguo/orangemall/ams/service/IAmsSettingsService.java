package link.chengguo.orangemall.ams.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.ams.entity.AmsSettings;

/**
* @author yzb
* @date 2020-02-12
*/

public interface IAmsSettingsService extends IService<AmsSettings> {

    /**
     * 添加配置信息
     * @param settings
     * @return
     */
    boolean addAmsSettings(AmsSettings settings);


    /**
     * 根据ID修改配置信息
     * @param settings
     * @return
     */
    boolean updateAmsSettings(AmsSettings settings);


    /**
     * 根据代理城市ID查询配置信息
     * @param cityCode
     * @return
     */
    AmsSettings getAmsSettingsByCityCode(String cityCode);

}
