package link.chengguo.orangemall.ams.service;

import link.chengguo.orangemall.ams.entity.AmsAgentBrokerageLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-21
*/

public interface IAmsAgentBrokerageLevelService extends IService<AmsAgentBrokerageLevel> {


    boolean add(AmsAgentBrokerageLevel amsAgentBrokerageLevel);

    boolean update(AmsAgentBrokerageLevel amsAgentBrokerageLevel);
}
