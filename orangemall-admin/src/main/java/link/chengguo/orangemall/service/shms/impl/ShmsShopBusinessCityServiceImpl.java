package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopBusinessCity;
import link.chengguo.orangemall.shms.mapper.ShmsShopBusinessCityMapper;
import link.chengguo.orangemall.shms.service.IShmsShopBusinessCityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2019-12-19
 */
@Service
public class ShmsShopBusinessCityServiceImpl extends ServiceImpl
        <ShmsShopBusinessCityMapper, ShmsShopBusinessCity> implements IShmsShopBusinessCityService {

    @Resource
    private ShmsShopBusinessCityMapper shmsShopBusinessCityMapper;


}
