package link.chengguo.orangemall.service.ams.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ams.entity.AmsAgentBlanceLog;
import link.chengguo.orangemall.ams.mapper.AmsAgentBlanceLogMapper;
import link.chengguo.orangemall.ams.service.IAmsAgentBlanceLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author yzb
* @date 2020-02-20
*/
@Service
public class AmsAgentBlanceLogServiceImpl extends ServiceImpl
<AmsAgentBlanceLogMapper, AmsAgentBlanceLog> implements IAmsAgentBlanceLogService {

@Resource
private  AmsAgentBlanceLogMapper amsAgentBlanceLogMapper;


}
