package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsGroup;
import link.chengguo.orangemall.sms.mapper.SmsGroupMapper;
import link.chengguo.orangemall.sms.service.ISmsGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsGroupServiceImpl extends ServiceImpl<SmsGroupMapper, SmsGroup> implements ISmsGroupService {

}
