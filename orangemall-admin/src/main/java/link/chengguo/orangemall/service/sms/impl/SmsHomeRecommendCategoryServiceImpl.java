package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProduct;
import link.chengguo.orangemall.pms.service.IPmsProductService;
import link.chengguo.orangemall.sms.entity.SmsHomeRecommendCatetory;
import link.chengguo.orangemall.sms.mapper.SmsHomeRecommendCategoryMapper;
import link.chengguo.orangemall.sms.service.ISmsHomeRecommendCategoryService;
import link.chengguo.orangemall.sms.vo.HomeProductCatetory;
import link.chengguo.orangemall.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 首页推荐专题表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsHomeRecommendCategoryServiceImpl extends ServiceImpl<SmsHomeRecommendCategoryMapper, SmsHomeRecommendCatetory> implements ISmsHomeRecommendCategoryService  {
    @Resource
    private SmsHomeRecommendCategoryMapper  recommendCatetoryMapper;


    @Autowired
    private IPmsProductService pmsProductService;
    @Override
    public int updateSort(Long id, Integer sort) {
        SmsHomeRecommendCatetory  recommendProduct = new SmsHomeRecommendCatetory ();
        recommendProduct.setId(id);
        recommendProduct.setSort(sort);
        return recommendCatetoryMapper.updateById(recommendProduct);
    }



    @Override
    public int updateRecommendStatus(List<Long> ids, Integer recommendStatus) {

        SmsHomeRecommendCatetory  record = new SmsHomeRecommendCatetory ();
        record.setRecommendStatus(recommendStatus);
        return recommendCatetoryMapper.update(record, new QueryWrapper<SmsHomeRecommendCatetory >().in("id", ids));
    }

    @Override
    public int create(List<SmsHomeRecommendCatetory > recommendCatetoryList) {
        for (SmsHomeRecommendCatetory  recommendCatetory : recommendCatetoryList) {
            recommendCatetory.setRecommendStatus(1);
            recommendCatetory.setSort(0);
            recommendCatetoryMapper.insert(recommendCatetory);
        }
        return recommendCatetoryList.size();
    }

    @Override
    public Page<SmsHomeRecommendCatetory> customPageList(Integer pageNum, Integer pageSize, SmsHomeRecommendCatetory entity) {
        Page page1= new Page<>();
        page1.setSize(pageSize);
        page1.setCurrent(pageNum);
        return recommendCatetoryMapper.customPageList(page1,entity);
    }

    @Override
    public HomeProductCatetory getDetailById(Long id) {
        Map<String ,List<PmsProduct>> allProductsMap = new HashMap<> ();
        List<PmsProduct> waitRecommendProductList = new ArrayList<>();
        //获取SmsHomeRecommendCatetory实体 分析不明确字段：categoryIds，projectIds
        SmsHomeRecommendCatetory catetory = recommendCatetoryMapper.selectById(id);
        //约定了categoryIds中的id用逗号隔开
        String[] catetoryIds = catetory.getCategoryIds().split(",");
        if(!StringUtils.isEmpty(catetory.getProjectIds())){
            String[] projectIds = catetory.getProjectIds().split(",");
            for(String idStr2 : projectIds){
                waitRecommendProductList.add(pmsProductService.getById(Integer.parseInt(idStr2)));
            }
        }
        for(String idStr : catetoryIds){
            Integer categoryId = Integer.parseInt(idStr);
            allProductsMap.put(idStr,pmsProductService.list(new QueryWrapper<PmsProduct>().eq("category_id",categoryId)));
        }
        return new HomeProductCatetory(catetory,waitRecommendProductList,allProductsMap);
    }
}
