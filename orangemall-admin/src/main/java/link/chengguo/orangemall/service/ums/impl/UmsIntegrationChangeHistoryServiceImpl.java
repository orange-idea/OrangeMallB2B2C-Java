package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsIntegrationChangeHistory;
import link.chengguo.orangemall.ums.mapper.UmsIntegrationChangeHistoryMapper;
import link.chengguo.orangemall.ums.service.IUmsIntegrationChangeHistoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分变化历史记录表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsIntegrationChangeHistoryServiceImpl extends ServiceImpl<UmsIntegrationChangeHistoryMapper, UmsIntegrationChangeHistory> implements IUmsIntegrationChangeHistoryService {

}
