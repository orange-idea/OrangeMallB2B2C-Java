package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsCollect;
import link.chengguo.orangemall.ums.mapper.UmsCollectMapper;
import link.chengguo.orangemall.ums.service.IUmsCollectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsCollectServiceImpl extends ServiceImpl<UmsCollectMapper, UmsCollect> implements IUmsCollectService {

}
