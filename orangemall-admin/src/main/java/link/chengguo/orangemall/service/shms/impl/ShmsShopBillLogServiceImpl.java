package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.ams.service.IAmsAgentService;
import link.chengguo.orangemall.enums.BillSettlementStatus;
import link.chengguo.orangemall.oms.service.IOmsOrderSettingService;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillLogMapper;
import link.chengguo.orangemall.shms.service.IShmsShopAccountService;
import link.chengguo.orangemall.shms.service.IShmsShopBillLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillLogServiceImpl extends ServiceImpl
        <ShmsShopBillLogMapper, ShmsShopBillLog> implements IShmsShopBillLogService {

    @Resource
    private ShmsShopBillLogMapper shmsShopBillLogMapper;
    @Resource
    private IOmsOrderSettingService omsOrderSettingService;
    @Resource
    private IShmsShopAccountService shopAccountService;
    @Resource
    private IAmsAgentService agentService;



    /**
     * 账单结算
     *
     * @return
     */
    @Transactional
    @Override
    public boolean shopBillSettement() {
        return true;
    }

    /**
     * 查询统计信息，日统计等
     * @param  shopBillLog,startTime和endTime必填
     * @author yzb
     * @Date 2020-02-13
     */
    @Override
    public List<ShmsShopBillDay> statisticsDayBill(ShmsShopBillLog shopBillLog) {
        List<ShmsShopBillDay>  shopBillDay= shmsShopBillLogMapper.statisticsDayBill(shopBillLog);
        return shopBillDay;
    }

    @Override
    public ShmsShopBillDay statisticsBillByShopId(ShmsShopBillLog paramCondition) {
        return shmsShopBillLogMapper.statisticsBillByShopId(paramCondition);
    }

    /**
     * 查询待结算订单列表
     * @param  date
     * @return
     */
    @Override
    public List<ShmsShopBillLog> getWaitSettlementList(Date date) {
        ShmsShopBillLog parm=new ShmsShopBillLog();
        parm.setBillStatus(BillSettlementStatus.ReadySettlement.getValue());//待结算
        QueryWrapper queryWrapper=new QueryWrapper(parm);
        queryWrapper.le("update_time",date);
        return shmsShopBillLogMapper.selectList(queryWrapper);
    }
}
