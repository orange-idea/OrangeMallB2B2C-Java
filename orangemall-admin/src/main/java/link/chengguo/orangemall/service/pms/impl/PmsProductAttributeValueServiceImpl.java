package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsProductAttributeValue;
import link.chengguo.orangemall.pms.mapper.PmsProductAttributeValueMapper;
import link.chengguo.orangemall.pms.service.IPmsProductAttributeValueService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 存储产品参数信息的表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsProductAttributeValueServiceImpl extends ServiceImpl<PmsProductAttributeValueMapper, PmsProductAttributeValue> implements IPmsProductAttributeValueService {

}
