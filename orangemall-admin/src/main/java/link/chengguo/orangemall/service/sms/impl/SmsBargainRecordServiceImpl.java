package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsBargainRecord;
import link.chengguo.orangemall.sms.mapper.SmsBargainRecordMapper;
import link.chengguo.orangemall.sms.service.ISmsBargainRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 砍价免单记录表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsBargainRecordServiceImpl extends ServiceImpl<SmsBargainRecordMapper, SmsBargainRecord> implements ISmsBargainRecordService {

}
