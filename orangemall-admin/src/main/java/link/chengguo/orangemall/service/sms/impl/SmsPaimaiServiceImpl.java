package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsPaimai;
import link.chengguo.orangemall.sms.mapper.SmsPaimaiMapper;
import link.chengguo.orangemall.sms.service.ISmsPaimaiService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 一分钱抽奖 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-19
 */
@Service
public class SmsPaimaiServiceImpl extends ServiceImpl<SmsPaimaiMapper, SmsPaimai> implements ISmsPaimaiService {

}
