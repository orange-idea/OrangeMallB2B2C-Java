package link.chengguo.orangemall.service.mms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import link.chengguo.orangemall.mms.entity.MmsCardTitle;
import link.chengguo.orangemall.mms.mapper.MmsCardTitleMapper;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import link.chengguo.orangemall.mms.service.IMmsCardTitleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author yzb
 * @date 2020-02-22
 */
@Service
public class MmsCardTitleServiceImpl extends ServiceImpl
        <MmsCardTitleMapper, MmsCardTitle> implements IMmsCardTitleService {

    @Resource
    private MmsCardTitleMapper mmsCardTitleMapper;
    @Resource
    private IMmsCardDetailService cardDetailService;

    @Transactional
    @Override
    public boolean addCardTitle(MmsCardTitle title) {
        //添加卡券标题
        mmsCardTitleMapper.insert(title);
        //添加卡券详情
        cardDetailService.addBatchCardDetail(title);
        return true;
    }

    @Transactional
    @Override
    public boolean deleteCardTitle(Integer id) {
        MmsCardDetail detail=new MmsCardDetail();
        detail.setTitleId(id);
        cardDetailService.remove(new QueryWrapper<>(detail));
        mmsCardTitleMapper.deleteById(id);
        return true;
    }
}
