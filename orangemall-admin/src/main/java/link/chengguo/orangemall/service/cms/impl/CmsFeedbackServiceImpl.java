package link.chengguo.orangemall.service.cms.impl;

import link.chengguo.orangemall.cms.entity.CmsFeedback;
import link.chengguo.orangemall.cms.mapper.CmsFeedbackMapper;
import link.chengguo.orangemall.cms.service.ICmsFeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsFeedbackServiceImpl extends ServiceImpl
<CmsFeedbackMapper, CmsFeedback> implements ICmsFeedbackService {

@Resource
private  CmsFeedbackMapper cmsFeedbackMapper;


}
