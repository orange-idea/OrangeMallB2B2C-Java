package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberReceiveAddress;
import link.chengguo.orangemall.ums.mapper.UmsMemberReceiveAddressMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberReceiveAddressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员收货地址表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberReceiveAddressServiceImpl extends ServiceImpl<UmsMemberReceiveAddressMapper, UmsMemberReceiveAddress> implements IUmsMemberReceiveAddressService {

}
