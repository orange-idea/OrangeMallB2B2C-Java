package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BakGoods;
import link.chengguo.orangemall.bill.mapper.BakGoodsMapper;
import link.chengguo.orangemall.bill.service.IBakGoodsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品基本信息表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-17
 */
@Service
public class BakGoodsServiceImpl extends ServiceImpl<BakGoodsMapper, BakGoods> implements IBakGoodsService {

}
