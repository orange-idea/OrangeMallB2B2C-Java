package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;
import link.chengguo.orangemall.shms.mapper.ShmsShopAccountLogMapper;
import link.chengguo.orangemall.shms.service.IShmsShopAccountLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopAccountLogServiceImpl extends ServiceImpl
<ShmsShopAccountLogMapper, ShmsShopAccountLog> implements IShmsShopAccountLogService {

@Resource
private  ShmsShopAccountLogMapper shmsShopAccountLogMapper;


}
