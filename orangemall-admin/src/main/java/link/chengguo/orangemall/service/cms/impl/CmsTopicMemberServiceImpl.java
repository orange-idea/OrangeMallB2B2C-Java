package link.chengguo.orangemall.service.cms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.cms.entity.CmsTopicMember;
import link.chengguo.orangemall.cms.mapper.CmsTopicMemberMapper;
import link.chengguo.orangemall.cms.service.ICmsTopicMemberService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
@Service
public class CmsTopicMemberServiceImpl extends ServiceImpl<CmsTopicMemberMapper, CmsTopicMember> implements ICmsTopicMemberService {

}
