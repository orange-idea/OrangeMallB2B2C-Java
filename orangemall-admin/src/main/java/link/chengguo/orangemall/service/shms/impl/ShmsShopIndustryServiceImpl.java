package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopIndustry;
import link.chengguo.orangemall.shms.mapper.ShmsShopIndustryMapper;
import link.chengguo.orangemall.shms.service.IShmsShopIndustryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.vo.ShmsShopIndustryWithChildrenItem;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopIndustryServiceImpl extends ServiceImpl
<ShmsShopIndustryMapper, ShmsShopIndustry> implements IShmsShopIndustryService {

@Resource
private  ShmsShopIndustryMapper shmsShopIndustryMapper;


    @Override
    public List<ShmsShopIndustryWithChildrenItem> listWithChildren(ShmsShopIndustry param) {
        return shmsShopIndustryMapper.listWithChildren(param);
    }
}
