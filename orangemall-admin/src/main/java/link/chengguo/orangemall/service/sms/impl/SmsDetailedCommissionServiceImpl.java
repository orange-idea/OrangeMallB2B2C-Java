package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsDetailedCommission;
import link.chengguo.orangemall.sms.mapper.SmsDetailedCommissionMapper;
import link.chengguo.orangemall.sms.service.ISmsDetailedCommissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分销佣金明细表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsDetailedCommissionServiceImpl extends ServiceImpl<SmsDetailedCommissionMapper, SmsDetailedCommission> implements ISmsDetailedCommissionService {

}
