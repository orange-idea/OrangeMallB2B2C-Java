package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsDraw;
import link.chengguo.orangemall.sms.mapper.SmsDrawMapper;
import link.chengguo.orangemall.sms.service.ISmsDrawService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 一分钱抽奖 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsDrawServiceImpl extends ServiceImpl<SmsDrawMapper, SmsDraw> implements ISmsDrawService {

}
