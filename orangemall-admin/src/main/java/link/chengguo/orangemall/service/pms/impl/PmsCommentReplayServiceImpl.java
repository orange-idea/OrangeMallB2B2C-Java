package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsCommentReplay;
import link.chengguo.orangemall.pms.mapper.PmsCommentReplayMapper;
import link.chengguo.orangemall.pms.service.IPmsCommentReplayService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 产品评价回复表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsCommentReplayServiceImpl extends ServiceImpl<PmsCommentReplayMapper, PmsCommentReplay> implements IPmsCommentReplayService {

}
