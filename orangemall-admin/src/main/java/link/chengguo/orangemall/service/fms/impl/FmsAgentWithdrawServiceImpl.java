package link.chengguo.orangemall.service.fms.impl;

import link.chengguo.orangemall.enums.WithdrawType;
import link.chengguo.orangemall.exception.BusinessMallException;
import link.chengguo.orangemall.fms.entity.FmsAgentWithdraw;
import link.chengguo.orangemall.fms.mapper.FmsAgentWithdrawMapper;
import link.chengguo.orangemall.fms.service.IFmsAgentWithdrawService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pay.service.PayService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.sys.service.ISysUserService;
import link.chengguo.orangemall.util.UserUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;

/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsAgentWithdrawServiceImpl extends ServiceImpl
<FmsAgentWithdrawMapper, FmsAgentWithdraw> implements IFmsAgentWithdrawService {

    @Resource
    private FmsAgentWithdrawMapper fmsAgentWithdrawMapper;
    @Resource
    private PayService payService;
    @Resource
    private BCryptPasswordEncoder passwordEncoder;
    @Resource
    private ISysUserService userService;

    @Override
    public boolean withdrawVerify(Integer result, Long id,String pwd,String note) {
        //判断密码是否正确
        SysUser user= UserUtils.getCurrentMember();
        SysUser user2=userService.getById(user.getId());
        if (!passwordEncoder.matches(pwd, user2.getPassword())) {
            throw new BusinessMallException("密码错误");
        }
        //修改审核结果
        FmsAgentWithdraw agentWithdraw = fmsAgentWithdrawMapper.selectById(id);
        if (agentWithdraw.getTransferResult()!=0){
            throw new RuntimeException("该申请已处理过，不能重复操作！");
        }
        agentWithdraw.setTransferResult(result);
        agentWithdraw.setUpdateTime(new Date());
        agentWithdraw.setTranferDesc(note);
        fmsAgentWithdrawMapper.updateById(agentWithdraw);
        if (result==2){
            //拒绝，则结束
            return true;
        }
        //如果同意，则需要进行转账操作
        if (agentWithdraw.getTransferMethod() == WithdrawType.Ali.getValue()) {
            //支付宝转账,调试使用最低转账金额
            boolean rt = payService.transferByAli("0.1", agentWithdraw.getWithdrawAccount(),
                    agentWithdraw.getWithdrawName(), "华盛配送平台代理提现转账");
//            boolean rt=payService.transferByAli(shopWithdraw.getMoney().toString(),shopWithdraw.getWithdrawAccount(),
//                    shopWithdraw.getWithdrawName(),"华盛配送平台代理提现转账");
            if (rt == false) {
                return false;
            }
        }
        return true;
    }
}
