package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BakCategory;
import link.chengguo.orangemall.bill.mapper.BakCategoryMapper;
import link.chengguo.orangemall.bill.service.IBakCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 类目表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-17
 */
@Service
public class BakCategoryServiceImpl extends ServiceImpl<BakCategoryMapper, BakCategory> implements IBakCategoryService {

}
