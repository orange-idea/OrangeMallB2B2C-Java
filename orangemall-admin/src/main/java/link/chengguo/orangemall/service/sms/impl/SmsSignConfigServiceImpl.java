package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsSignConfig;
import link.chengguo.orangemall.sms.mapper.SmsSignConfigMapper;
import link.chengguo.orangemall.sms.service.ISmsSignConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到配置表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsSignConfigServiceImpl extends ServiceImpl<SmsSignConfigMapper, SmsSignConfig> implements ISmsSignConfigService {

}
