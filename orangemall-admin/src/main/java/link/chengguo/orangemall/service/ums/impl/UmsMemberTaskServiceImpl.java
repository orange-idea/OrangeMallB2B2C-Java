package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberTask;
import link.chengguo.orangemall.ums.mapper.UmsMemberTaskMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberTaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员任务表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberTaskServiceImpl extends ServiceImpl<UmsMemberTaskMapper, UmsMemberTask> implements IUmsMemberTaskService {

}
