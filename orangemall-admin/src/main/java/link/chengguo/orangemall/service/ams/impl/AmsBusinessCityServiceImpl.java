package link.chengguo.orangemall.service.ams.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.mapper.AmsBusinessCityMapper;
import link.chengguo.orangemall.ams.service.IAmsBusinessCityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class AmsBusinessCityServiceImpl extends ServiceImpl
<AmsBusinessCityMapper, AmsBusinessCity> implements IAmsBusinessCityService {

@Resource
private  AmsBusinessCityMapper amsBusinessCityMapper;


    @Override
    public AmsBusinessCity getCityByCityCode(String cityCode) {
        AmsBusinessCity city=new AmsBusinessCity();
        city.setBusinessDistrict(cityCode);
        return amsBusinessCityMapper.selectOne(new QueryWrapper<>(city));
    }
}
