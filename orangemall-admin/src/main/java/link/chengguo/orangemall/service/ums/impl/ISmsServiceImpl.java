package link.chengguo.orangemall.service.ums.impl;


import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.sys.entity.SysSmsConfig;
import link.chengguo.orangemall.sys.service.SysSmsConfigService;
import link.chengguo.orangemall.ums.entity.Sms;
import link.chengguo.orangemall.ums.mapper.SmsDao;
import link.chengguo.orangemall.ums.service.ISmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;


/**
 * * 程序名 : AliyunSmsConfig
 * 建立日期: 2018-07-09
 * 作者 : someday
 * 模块 : 短信中心
 * 描述 : 调用阿里短信接口
 * 备注 : version20180709001
 * <p>
 * 修改历史
 * 序号 	       日期 		        修改人 		         修改原因
 */
@Slf4j
@Service
public class ISmsServiceImpl implements ISmsService {

    @Resource
    private IAcsClient acsClient;

    @Resource
    private SmsDao smsDao;

    @Autowired
    private SysSmsConfigService smsConfigService;


    @Override
    public SendSmsResponse sendSmsMsg(Sms sms) {
        SysSmsConfig one = smsConfigService.getOne(new QueryWrapper<>());

        if (sms.getSignName() == null) {
            sms.setSignName(one.getSignName());
        }

        if (sms.getTemplateCode() == null) {
            sms.setTemplateCode(one.getTemplateCode());
        }

        // 阿里云短信官网demo代码
        SendSmsRequest request = new SendSmsRequest();
        request.setMethod(MethodType.POST);
        request.setPhoneNumbers(sms.getPhone());
        request.setSignName(sms.getSignName());
        request.setTemplateCode(sms.getTemplateCode());
        request.setTemplateParam(sms.getParams());
        request.setOutId(sms.getId().toString());

        SendSmsResponse response = null;
//		测试时不需要开此 add by someday begin
        try {
            response = acsClient.getAcsResponse(request);
            if (response != null) {
                log.info("发送短信结果：code:{}，message:{}，requestId:{}，bizId:{}", response.getCode(), response.getMessage(),
                        response.getRequestId(), response.getBizId());

                sms.setCode(response.getCode());
                sms.setMessage(response.getMessage());
                sms.setBizId(response.getBizId());
            }
        } catch (ClientException e) {
            e.printStackTrace();
        }
//		测试时不需要开此 add by someday end
        update(sms);

        return response;
    }

    @Transactional
    @Override
    public void save(Sms sms, String params) {
        if (params!=null&&params!="") {
            sms.setParams(params);
        }

        sms.setCreateTime(new Date());
        sms.setUpdateTime(sms.getCreateTime());
        sms.setDay(sms.getCreateTime());

        smsDao.save(sms);
    }

    @Transactional
    @Override
    public void update(Sms sms) {
        sms.setUpdateTime(new Date());
        smsDao.update(sms);
    }

    @Override
    public Sms findById(Long id) {
        return smsDao.findById(id);
    }


}
