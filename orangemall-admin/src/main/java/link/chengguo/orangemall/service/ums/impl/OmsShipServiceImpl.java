package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.OmsShip;
import link.chengguo.orangemall.ums.mapper.OmsShipMapper;
import link.chengguo.orangemall.ums.service.IOmsShipService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配送方式表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class OmsShipServiceImpl extends ServiceImpl<OmsShipMapper, OmsShip> implements IOmsShipService {

}
