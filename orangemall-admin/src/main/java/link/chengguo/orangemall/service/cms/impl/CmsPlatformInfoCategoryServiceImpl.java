package link.chengguo.orangemall.service.cms.impl;

import link.chengguo.orangemall.cms.entity.CmsPlatformInfoCategory;
import link.chengguo.orangemall.cms.mapper.CmsPlatformInfoCategoryMapper;
import link.chengguo.orangemall.cms.service.ICmsPlatformInfoCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-21
*/
@Service
public class CmsPlatformInfoCategoryServiceImpl extends ServiceImpl
<CmsPlatformInfoCategoryMapper, CmsPlatformInfoCategory> implements ICmsPlatformInfoCategoryService {

@Resource
private  CmsPlatformInfoCategoryMapper cmsPlatformInfoCategoryMapper;


}
