package link.chengguo.orangemall.service.ams.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ams.entity.AmsSettings;
import link.chengguo.orangemall.ams.mapper.AmsSettingsMapper;
import link.chengguo.orangemall.ams.service.IAmsSettingsService;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.mapper.ShmsShopInfoMapper;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.UserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-12
 */
@Service
public class AmsSettingsServiceImpl extends ServiceImpl
        <AmsSettingsMapper, AmsSettings> implements IAmsSettingsService {

    @Resource
    private AmsSettingsMapper amsSettingsMapper;
    @Resource
    private ShmsShopInfoMapper shmsShopInfoMapper;


    @Transactional
    @Override
    public boolean addAmsSettings(AmsSettings settings) {
        //查询当前代理编号
        SysUser user = UserUtils.getCurrentMember();
        //查询当前代理是否已存在配置
        if (getAmsSettingsByCityCode(user.getCityCode()) != null) {
            //代理存在
            System.err.println("同一个区域不能添加多个配置！");
            throw new RuntimeException("同一个区域不能添加多个配置");
        }
        settings.setCityCode(user.getCityCode());
        settings.setCreateTime(new Date());
        amsSettingsMapper.insert(settings);

        //这个区域所有店铺进行修改
        ShmsShopInfo shopInfo=new ShmsShopInfo();
        shopInfo.setExpressFee(settings.getExpressFee());
        shopInfo.setFreeExpressPrice(settings.getFreeExpressPrice());
        shopInfo.setStartingPrice(settings.getStartingPrice());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("city_code",user.getCityCode());
        shmsShopInfoMapper.update(shopInfo,queryWrapper);
        return true;
    }

    @Override
    public boolean updateAmsSettings(AmsSettings settings) {
        settings.setUpdateTime(new Date());
        amsSettingsMapper.updateById(settings);

        //这个区域所有店铺进行修改
        ShmsShopInfo shopInfo=new ShmsShopInfo();
        shopInfo.setExpressFee(settings.getExpressFee());
        shopInfo.setFreeExpressPrice(settings.getFreeExpressPrice());
        shopInfo.setStartingPrice(settings.getStartingPrice());

        ShmsShopInfo parm=new ShmsShopInfo();
        parm.setCityCode(settings.getCityCode());
        parm.setSpecialCase(0);//非独立配置运费等
        QueryWrapper queryWrapper=new QueryWrapper(parm);
        shmsShopInfoMapper.update(shopInfo,queryWrapper);
        return true;
    }

    @Override
    public AmsSettings getAmsSettingsByCityCode(String cityCode) {
        AmsSettings settings = new AmsSettings();
        settings.setCityCode(cityCode);
        return amsSettingsMapper.selectOne(new QueryWrapper<>(settings));
    }
}
