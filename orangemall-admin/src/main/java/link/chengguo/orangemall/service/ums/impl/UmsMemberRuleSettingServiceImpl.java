package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UmsMemberRuleSetting;
import link.chengguo.orangemall.ums.mapper.UmsMemberRuleSettingMapper;
import link.chengguo.orangemall.ums.service.IUmsMemberRuleSettingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员积分成长规则表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class UmsMemberRuleSettingServiceImpl extends ServiceImpl<UmsMemberRuleSettingMapper, UmsMemberRuleSetting> implements IUmsMemberRuleSettingService {

}
