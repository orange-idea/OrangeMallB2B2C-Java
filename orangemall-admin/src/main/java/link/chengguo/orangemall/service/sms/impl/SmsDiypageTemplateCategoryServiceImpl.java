package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsDiypageTemplateCategory;
import link.chengguo.orangemall.sms.mapper.SmsDiypageTemplateCategoryMapper;
import link.chengguo.orangemall.sms.service.ISmsDiypageTemplateCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author orangemall
 * @date 2019-12-04
 */
@Service
public class SmsDiypageTemplateCategoryServiceImpl extends ServiceImpl<SmsDiypageTemplateCategoryMapper, SmsDiypageTemplateCategory> implements ISmsDiypageTemplateCategoryService {

    @Resource
    private SmsDiypageTemplateCategoryMapper smsDiypageTemplateCategoryMapper;


    @Override
    public Object selTemplateCategory() {
        return smsDiypageTemplateCategoryMapper.selectList(new QueryWrapper<>());
    }
}
