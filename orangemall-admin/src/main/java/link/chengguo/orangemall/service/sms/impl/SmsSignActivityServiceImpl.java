package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsSignActivity;
import link.chengguo.orangemall.sms.mapper.SmsSignActivityMapper;
import link.chengguo.orangemall.sms.service.ISmsSignActivityService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 签到活动 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsSignActivityServiceImpl extends ServiceImpl<SmsSignActivityMapper, SmsSignActivity> implements ISmsSignActivityService {

}
