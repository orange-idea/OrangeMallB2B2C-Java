package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopAccount;
import link.chengguo.orangemall.shms.mapper.ShmsShopAccountMapper;
import link.chengguo.orangemall.shms.service.IShmsShopAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopAccountServiceImpl extends ServiceImpl
<ShmsShopAccountMapper, ShmsShopAccount> implements IShmsShopAccountService {

@Resource
private  ShmsShopAccountMapper shmsShopAccountMapper;


}
