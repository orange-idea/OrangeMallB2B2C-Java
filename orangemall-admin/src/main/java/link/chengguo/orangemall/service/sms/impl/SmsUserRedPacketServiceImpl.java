package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsUserRedPacket;
import link.chengguo.orangemall.sms.mapper.SmsUserRedPacketMapper;
import link.chengguo.orangemall.sms.service.ISmsUserRedPacketService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户红包 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsUserRedPacketServiceImpl extends ServiceImpl<SmsUserRedPacketMapper, SmsUserRedPacket> implements ISmsUserRedPacketService {

}
