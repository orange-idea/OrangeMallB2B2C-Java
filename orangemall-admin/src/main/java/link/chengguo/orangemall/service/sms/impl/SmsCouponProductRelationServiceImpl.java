package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsCouponProductRelation;
import link.chengguo.orangemall.sms.mapper.SmsCouponProductRelationMapper;
import link.chengguo.orangemall.sms.service.ISmsCouponProductRelationService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券和产品的关系表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class SmsCouponProductRelationServiceImpl extends ServiceImpl<SmsCouponProductRelationMapper, SmsCouponProductRelation> implements ISmsCouponProductRelationService {

}
