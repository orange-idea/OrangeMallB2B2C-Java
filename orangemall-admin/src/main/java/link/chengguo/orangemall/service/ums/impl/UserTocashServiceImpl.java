package link.chengguo.orangemall.service.ums.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.ums.entity.UserTocash;
import link.chengguo.orangemall.ums.mapper.UserTocashMapper;
import link.chengguo.orangemall.ums.service.IUserTocashService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class UserTocashServiceImpl extends ServiceImpl<UserTocashMapper, UserTocash> implements IUserTocashService {

}
