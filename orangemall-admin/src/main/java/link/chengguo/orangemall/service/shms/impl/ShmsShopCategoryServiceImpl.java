package link.chengguo.orangemall.service.shms.impl;

import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import link.chengguo.orangemall.shms.mapper.ShmsShopCategoryMapper;
import link.chengguo.orangemall.shms.service.IShmsShopCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.shms.vo.ShmsShopCategoryWithChildrenItem;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/
@Service
public class ShmsShopCategoryServiceImpl extends ServiceImpl
<ShmsShopCategoryMapper, ShmsShopCategory> implements IShmsShopCategoryService {

@Resource
private  ShmsShopCategoryMapper shmsShopCategoryMapper;


    @Override
    public List<ShmsShopCategoryWithChildrenItem> listWithChildren(ShmsShopCategory param) {
        return shmsShopCategoryMapper.listWithChildren(param);
    }
}
