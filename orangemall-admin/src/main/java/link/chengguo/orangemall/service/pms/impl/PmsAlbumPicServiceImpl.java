package link.chengguo.orangemall.service.pms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.pms.entity.PmsAlbumPic;
import link.chengguo.orangemall.pms.mapper.PmsAlbumPicMapper;
import link.chengguo.orangemall.pms.service.IPmsAlbumPicService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 画册图片表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Service
public class PmsAlbumPicServiceImpl extends ServiceImpl<PmsAlbumPicMapper, PmsAlbumPic> implements IPmsAlbumPicService {

}
