package link.chengguo.orangemall.service.sms.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sms.entity.SmsExperience;
import link.chengguo.orangemall.sms.mapper.SmsExperienceMapper;
import link.chengguo.orangemall.sms.service.ISmsExperienceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预约表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-10-17
 */
@Service
public class SmsExperienceServiceImpl extends ServiceImpl<SmsExperienceMapper, SmsExperience> implements ISmsExperienceService {

}
