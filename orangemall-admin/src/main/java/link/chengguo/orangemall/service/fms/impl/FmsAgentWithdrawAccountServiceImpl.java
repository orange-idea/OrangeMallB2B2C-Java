package link.chengguo.orangemall.service.fms.impl;

import link.chengguo.orangemall.fms.entity.FmsAgentWithdrawAccount;
import link.chengguo.orangemall.fms.mapper.FmsAgentWithdrawAccountMapper;
import link.chengguo.orangemall.fms.service.IFmsAgentWithdrawAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsAgentWithdrawAccountServiceImpl extends ServiceImpl
<FmsAgentWithdrawAccountMapper, FmsAgentWithdrawAccount> implements IFmsAgentWithdrawAccountService {

@Resource
private  FmsAgentWithdrawAccountMapper fmsAgentWithdrawAccountMapper;


}
