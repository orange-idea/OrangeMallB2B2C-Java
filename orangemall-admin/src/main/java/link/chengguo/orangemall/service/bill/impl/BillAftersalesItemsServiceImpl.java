package link.chengguo.orangemall.service.bill.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.bill.entity.BillAftersalesItems;
import link.chengguo.orangemall.bill.mapper.BillAftersalesItemsMapper;
import link.chengguo.orangemall.bill.service.IBillAftersalesItemsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 售后单明细表 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
@Service
public class BillAftersalesItemsServiceImpl extends ServiceImpl<BillAftersalesItemsMapper, BillAftersalesItems> implements IBillAftersalesItemsService {

}
