package link.chengguo.orangemall.service.mms.impl;

import link.chengguo.orangemall.mms.entity.MmsCardOperationLog;
import link.chengguo.orangemall.mms.entity.MmsCardTitle;
import link.chengguo.orangemall.mms.mapper.MmsCardOperationLogMapper;
import link.chengguo.orangemall.mms.mapper.MmsCardTitleMapper;
import link.chengguo.orangemall.mms.service.IMmsCardOperationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.UserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2020-02-22
 */
@Service
public class MmsCardOperationLogServiceImpl extends ServiceImpl
        <MmsCardOperationLogMapper, MmsCardOperationLog> implements IMmsCardOperationLogService {

    @Resource
    private MmsCardOperationLogMapper mmsCardOperationLogMapper;
    @Resource
    private MmsCardTitleMapper mmsCardTitleMapper;

    @Transactional
    @Override
    public boolean addActiveLog(MmsCardOperationLog log) {
        SysUser sysUser = UserUtils.getCurrentMember();
        log.setCreateTime(new Date());
        log.setAid(sysUser.getId());
        log.setType(0);
        log.setName(sysUser.getNickName());
        MmsCardTitle title=mmsCardTitleMapper.selectById(log.getTitleId());
        log.setCardTitle(title.getTitle());
        mmsCardOperationLogMapper.insert(log);
        return true;
    }

    @Override
    public boolean addInvalidLog(MmsCardOperationLog log) {
        SysUser sysUser = UserUtils.getCurrentMember();
        log.setCreateTime(new Date());
        log.setAid(sysUser.getId());
        log.setType(1);
        log.setName(sysUser.getNickName());
        MmsCardTitle title=mmsCardTitleMapper.selectById(log.getTitleId());
        log.setCardTitle(title.getTitle());
        mmsCardOperationLogMapper.insert(log);
        return true;
    }
}
