package link.chengguo.orangemall.service.shms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import link.chengguo.orangemall.shms.mapper.ShmsShopBillMonthMapper;
import link.chengguo.orangemall.shms.service.IShmsShopBillDayService;
import link.chengguo.orangemall.shms.service.IShmsShopBillMonthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2019-12-18
 */
@Service
public class ShmsShopBillMonthServiceImpl extends ServiceImpl
        <ShmsShopBillMonthMapper, ShmsShopBillMonth> implements IShmsShopBillMonthService {

    @Resource
    private ShmsShopBillMonthMapper shmsShopBillMonthMapper;
    @Resource
    private IShmsShopBillDayService shopBillDayService;


    @Override
    public boolean addShopBillMonthForTask() {
        return false;
    }

    @Override
    public boolean addShopBillMonth(Date date) {

        return false;
    }

    @Override
    public boolean addOrUpdateShopBillDay(ShmsShopBillMonth shmsShopBillMonth) {
        ShmsShopBillMonth tmp=getShmsShopBillMonth(shmsShopBillMonth.getShopId(),shmsShopBillMonth.getMonth());
        if (tmp!=null){
            shmsShopBillMonth.setId(tmp.getId());
            shmsShopBillMonthMapper.updateById(shmsShopBillMonth);
        }else{
            shmsShopBillMonthMapper.insert(shmsShopBillMonth);
        }
        return true;
    }

    @Override
    public ShmsShopBillMonth getShmsShopBillMonth(Long shopId, String month) {
        ShmsShopBillMonth shopBillDay=new ShmsShopBillMonth();
        shopBillDay.setShopId(shopId);
        shopBillDay.setMonth(month);
        return shmsShopBillMonthMapper.selectOne(new QueryWrapper<>(shopBillDay));
    }

    @Override
    public Page<ShmsShopBillMonth> getMonthBillByMonth(Integer pageIndex, Integer pageSize, ShmsShopBillMonth paramCondition) {
        Page page= new Page<>();
        page.setSize(pageSize);
        page.setCurrent(pageIndex);
        return shmsShopBillMonthMapper.getMonthBillByMonth(page,paramCondition);
    }
}
