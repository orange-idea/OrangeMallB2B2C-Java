package link.chengguo.orangemall.service.ams.impl;

import link.chengguo.orangemall.ams.entity.AmsAgentBrokerageLevel;
import link.chengguo.orangemall.ams.mapper.AmsAgentBrokerageLevelMapper;
import link.chengguo.orangemall.ams.service.IAmsAgentBrokerageLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzb
 * @date 2019-12-21
 */
@Service
public class AmsAgentBrokerageLevelServiceImpl extends ServiceImpl
        <AmsAgentBrokerageLevelMapper, AmsAgentBrokerageLevel> implements IAmsAgentBrokerageLevelService {

    @Resource
    private AmsAgentBrokerageLevelMapper amsAgentBrokerageLevelMapper;


    @Override
    public boolean add(AmsAgentBrokerageLevel amsAgentBrokerageLevel) {
        amsAgentBrokerageLevel.setCreateTime(new Date());
        return amsAgentBrokerageLevelMapper.insert(amsAgentBrokerageLevel)>0?true:false;
    }

    @Override
    public boolean update(AmsAgentBrokerageLevel amsAgentBrokerageLevel) {
        amsAgentBrokerageLevel.setUpdateTime(new Date());
        return amsAgentBrokerageLevelMapper.updateById(amsAgentBrokerageLevel)>0?true:false;
    }
}
