package link.chengguo.orangemall.service.mms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.mms.entity.MmsCardDetail;
import link.chengguo.orangemall.mms.entity.MmsCardOperationLog;
import link.chengguo.orangemall.mms.entity.MmsCardTitle;
import link.chengguo.orangemall.mms.mapper.MmsCardDetailMapper;
import link.chengguo.orangemall.mms.mapper.MmsCardTitleMapper;
import link.chengguo.orangemall.mms.service.IMmsCardDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.mms.service.IMmsCardOperationLogService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.StringUtils;
import link.chengguo.orangemall.util.UserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-22
 */
@Service
public class MmsCardDetailServiceImpl extends ServiceImpl
        <MmsCardDetailMapper, MmsCardDetail> implements IMmsCardDetailService {

    @Resource
    private MmsCardDetailMapper mmsCardDetailMapper;
    @Resource
    private IMmsCardOperationLogService cardOperationLogService;
    @Resource
    private MmsCardTitleMapper cardTitleMapper;

    @Override
    public boolean addCardDetail(MmsCardDetail detail) {
        SysUser user= UserUtils.getCurrentMember();
        detail.setAid(user.getId());
        detail.setCreateTime(new Date());
        detail.setUuid(StringUtils.getUUID());
        mmsCardDetailMapper.insert(detail);
        return true;
    }

    /**
     * 批量添加充值券
     * @param title
     * @return
     */
    @Transactional
    @Override
    public boolean addBatchCardDetail(MmsCardTitle title) {
        int number=title.getNumber();
        MmsCardDetail detail=new MmsCardDetail();
        detail.setMoney(title.getMoney());
        detail.setTitleId(title.getId());
        for (int i=0;i<number;i++){
            addCardDetail(detail);
        }
        return true;
    }

    /**
     * 批量激活充值券，根据卡券标题ID
     * @param titleId
     * @return
     */
    @Override
    public boolean activeAllByTitleId(Integer titleId) {
        MmsCardTitle title= cardTitleMapper.selectById(titleId);
        //查询标题ID下的所有未激活的卡券。
        MmsCardDetail parm=new MmsCardDetail();
        parm.setIfActive(0);//未激活的
        parm.setTitleId(titleId);
        List<MmsCardDetail> cardDetails=this.list(new QueryWrapper<>(parm));
        for (MmsCardDetail detail:cardDetails){
            detail.setIfActive(1);
            detail.setActiveTime(new Date());
            mmsCardDetailMapper.updateById(detail);
        }
        //添加激活日志
        MmsCardOperationLog log=new MmsCardOperationLog();
        log.setTitleId(title.getId());
        log.setCardTitle(title.getTitle());
        log.setMoney(title.getMoney().toString());
        log.setNumber(cardDetails.size());
        BigDecimal totalmoney=title.getMoney().multiply(new BigDecimal(cardDetails.size()));
        log.setTotalMoney(totalmoney.toString());
        cardOperationLogService.addActiveLog(log);
        return true;
    }

    /**
     * 批量激活
     * @param ids
     * @return
     */
    @Transactional
    @Override
    public boolean activeByIds(Integer titleId,String ids) {
        String[] sid=ids.split(",");
        //查询ID的金额等信息
        if (sid==null || sid.length==0){
            throw new RuntimeException("传入参数异常");
        }
        MmsCardTitle title= cardTitleMapper.selectById(titleId);
        //查询标题ID下的所有未激活的卡券。
        MmsCardDetail detail=null;
        for (String id:sid){
            detail=mmsCardDetailMapper.selectById(id);
            detail.setIfActive(1);
            detail.setActiveTime(new Date());
            mmsCardDetailMapper.updateById(detail);
        }
        //添加激活日志
        MmsCardOperationLog log=new MmsCardOperationLog();
        log.setTitleId(title.getId());
        log.setCardTitle(title.getTitle());
        log.setMoney(title.getMoney().toString());
        log.setNumber(sid.length);
        BigDecimal totalmoney=title.getMoney().multiply(new BigDecimal(sid.length));
        log.setTotalMoney(totalmoney.toString());
        cardOperationLogService.addActiveLog(log);
        return true;
    }

    /**
     *  根据ID激活
     * @param id
     * @return
     */
    @Transactional
    @Override
    public boolean activeById(Long id) {
        MmsCardDetail detail=mmsCardDetailMapper.selectById(id);
        if (detail.getIfActive()==1){
            throw new RuntimeException("卡券已激活，无需重复激活");
        }
        MmsCardOperationLog log=new MmsCardOperationLog();
        log.setMoney(detail.getMoney().toString());
        log.setTotalMoney(detail.getMoney().toString());
        log.setNumber(1);
        log.setTitleId(detail.getTitleId());
        cardOperationLogService.addActiveLog(log);
        detail.setIfActive(1);
        detail.setActiveTime(new Date());
        mmsCardDetailMapper.updateById(detail);
        return true;
    }

    @Override
    public boolean invalidById(Long id) {
        MmsCardDetail detail=mmsCardDetailMapper.selectById(id);
        if (detail.getIfInvalid()==1){
            throw new RuntimeException("卡券已作废，无需重复作废");
        }
        if (detail.getIfActive()==0){
            throw new RuntimeException("卡券未激活，无需作废");
        }
        MmsCardOperationLog log=new MmsCardOperationLog();
        log.setMoney(detail.getMoney().toString());
        log.setTotalMoney(detail.getMoney().toString());
        log.setTitleId(detail.getTitleId());
        log.setNumber(1);
        cardOperationLogService.addInvalidLog(log);
        detail.setIfInvalid(1);
        detail.setInvalidTime(new Date());
        mmsCardDetailMapper.updateById(detail);
        return true;
    }
}
