package link.chengguo.orangemall.service.ums.impl;

import link.chengguo.orangemall.ums.entity.UmsProtalMenu;
import link.chengguo.orangemall.ums.mapper.UmsProtalMenuMapper;
import link.chengguo.orangemall.ums.service.IUmsProtalMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author chengguo
 * @date 2020-08-31
 */
@Service
public class UmsProtalMenuServiceImpl extends ServiceImpl
        <UmsProtalMenuMapper, UmsProtalMenu> implements IUmsProtalMenuService {

    @Resource
    private UmsProtalMenuMapper umsProtalMenuMapper;


}
