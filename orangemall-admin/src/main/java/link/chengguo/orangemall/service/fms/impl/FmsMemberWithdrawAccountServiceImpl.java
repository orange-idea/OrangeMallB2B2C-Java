package link.chengguo.orangemall.service.fms.impl;

import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;
import link.chengguo.orangemall.fms.mapper.FmsMemberWithdrawAccountMapper;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2020-02-10
*/
@Service
public class FmsMemberWithdrawAccountServiceImpl extends ServiceImpl
<FmsMemberWithdrawAccountMapper, FmsMemberWithdrawAccount> implements IFmsMemberWithdrawAccountService {

@Resource
private  FmsMemberWithdrawAccountMapper fmsMemberWithdrawAccountMapper;


}
