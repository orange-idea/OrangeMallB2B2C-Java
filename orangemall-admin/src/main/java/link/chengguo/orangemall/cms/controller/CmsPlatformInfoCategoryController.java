package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsPlatformInfoCategory;
import link.chengguo.orangemall.cms.service.ICmsPlatformInfoCategoryService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 平台说明类型
 */
@Slf4j
@RestController
@RequestMapping("/cms/cmsPlatformInfoCategory")
public class CmsPlatformInfoCategoryController {

    @Resource
    private ICmsPlatformInfoCategoryService ICmsPlatformInfoCategoryService;

    @SysLog(MODULE = "cms", REMARK = "根据条件查询所有平台说明类型列表")
    @ApiOperation("根据条件查询所有平台说明类型列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('cms:cmsPlatformInfoCategory:read')")
    public Object getCmsPlatformInfoCategoryByPage(CmsPlatformInfoCategory entity,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ICmsPlatformInfoCategoryService.page(new Page<CmsPlatformInfoCategory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有平台说明类型列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "保存平台说明类型")
    @ApiOperation("保存平台说明类型")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('cms:cmsPlatformInfoCategory:create')")
    public Object saveCmsPlatformInfoCategory(@RequestBody CmsPlatformInfoCategory entity) {
        try {

            if (ICmsPlatformInfoCategoryService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存平台说明类型：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "更新平台说明类型")
    @ApiOperation("更新平台说明类型")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('cms:cmsPlatformInfoCategory:update')")
    public Object updateCmsPlatformInfoCategory(@RequestBody CmsPlatformInfoCategory entity) {
        try {
            if (ICmsPlatformInfoCategoryService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新平台说明类型：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "删除平台说明类型")
    @ApiOperation("删除平台说明类型")
    @GetMapping(value = "/delete/{id}")
    public Object deleteCmsPlatformInfoCategory(@ApiParam("平台说明类型id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("平台说明类型id");
            }
            if (ICmsPlatformInfoCategoryService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除平台说明类型：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "给平台说明类型分配平台说明类型")
    @ApiOperation("查询平台说明类型明细")
    @GetMapping(value = "/{id}")
    public Object getCmsPlatformInfoCategoryById(@ApiParam("平台说明类型id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("平台说明类型id");
            }
            CmsPlatformInfoCategory coupon = ICmsPlatformInfoCategoryService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询平台说明类型明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除平台说明类型")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "cms", REMARK = "批量删除平台说明类型")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = ICmsPlatformInfoCategoryService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsPlatformInfoCategory entity) {
        // 模拟从数据库获取需要导出的数据
        List<CmsPlatformInfoCategory> personList = ICmsPlatformInfoCategoryService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsPlatformInfoCategory.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<CmsPlatformInfoCategory> personList = EasyPoiUtils.importExcel(file, CmsPlatformInfoCategory.class);
        ICmsPlatformInfoCategoryService.saveBatch(personList);
    }
}


