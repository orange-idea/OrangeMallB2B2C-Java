package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsContactUs;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 联系我们
 */
@Slf4j
@RestController
@RequestMapping("/cms/cmsContactUs")
public class CmsContactUsController {

    @Resource
    private link.chengguo.orangemall.cms.service.ICmsContactUsService ICmsContactUsService;

    @SysLog(MODULE = "cms", REMARK = "根据条件查询所有联系我们列表")
    @ApiOperation("根据条件查询所有联系我们列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:read')")
    public Object getCmsContactUsByPage(CmsContactUs entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ICmsContactUsService.page(new Page<CmsContactUs>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有联系我们列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "保存联系我们")
    @ApiOperation("保存联系我们")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:create')")
    public Object saveCmsContactUs(@RequestBody CmsContactUs entity) {
        try {

            if (ICmsContactUsService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存联系我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "更新联系我们")
    @ApiOperation("更新联系我们")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:update')")
    public Object updateCmsContactUs(@RequestBody CmsContactUs entity) {
        try {
            if (ICmsContactUsService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新联系我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "删除联系我们")
    @ApiOperation("删除联系我们")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:delete')")
    public Object deleteCmsContactUs(@ApiParam("联系我们id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("联系我们id");
            }
            if (ICmsContactUsService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除联系我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "给联系我们分配联系我们")
    @ApiOperation("查询联系我们明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:read')")
    public Object getCmsContactUsById(@ApiParam("联系我们id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("联系我们id");
            }
            CmsContactUs coupon = ICmsContactUsService.getById(id);
            if(coupon!=null){
                return new CommonResult().success(coupon);

            }else{
                return new CommonResult().failed();
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询联系我们明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除联系我们")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "cms", REMARK = "批量删除联系我们")
    @PreAuthorize("hasAuthority('cms:cmsContactUs:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = ICmsContactUsService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsContactUs entity) {
        // 模拟从数据库获取需要导出的数据
        List<CmsContactUs> personList = ICmsContactUsService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsContactUs.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<CmsContactUs> personList = EasyPoiUtils.importExcel(file, CmsContactUs.class);
        ICmsContactUsService.saveBatch(personList);
    }
}


