package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsAboutUs;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 关于我们
 */
@Slf4j
@RestController
@RequestMapping("/cms/cmsAboutUs")
public class CmsAboutUsController {

    @Resource
    private link.chengguo.orangemall.cms.service.ICmsAboutUsService ICmsAboutUsService;

    @SysLog(MODULE = "cms", REMARK = "根据条件查询所有关于我们列表")
    @ApiOperation("根据条件查询所有关于我们列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:read')")
    public Object getCmsAboutUsByPage(CmsAboutUs entity,
                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ICmsAboutUsService.page(new Page<CmsAboutUs>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有关于我们列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "保存关于我们")
    @ApiOperation("保存关于我们")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:create')")
    public Object saveCmsAboutUs(@RequestBody CmsAboutUs entity) {
        try {

            if (ICmsAboutUsService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存关于我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "更新关于我们")
    @ApiOperation("更新关于我们")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:update')")
    public Object updateCmsAboutUs(@RequestBody CmsAboutUs entity) {
        try {
            if (ICmsAboutUsService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新关于我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "删除关于我们")
    @ApiOperation("删除关于我们")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:delete')")
    public Object deleteCmsAboutUs(@ApiParam("关于我们id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("关于我们id");
            }
            if (ICmsAboutUsService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除关于我们：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "给关于我们分配关于我们")
    @ApiOperation("查询关于我们明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:read')")
    public Object getCmsAboutUsById(@ApiParam("关于我们id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("关于我们id");
            }
            CmsAboutUs coupon = ICmsAboutUsService.getById(id);
            if(coupon!=null){
                return new CommonResult().success(coupon);
            }else{
                return new CommonResult().failed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询关于我们明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除关于我们")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "cms", REMARK = "批量删除关于我们")
    @PreAuthorize("hasAuthority('cms:cmsAboutUs:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = ICmsAboutUsService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsAboutUs entity) {
        // 模拟从数据库获取需要导出的数据
        List<CmsAboutUs> personList = ICmsAboutUsService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsAboutUs.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<CmsAboutUs> personList = EasyPoiUtils.importExcel(file, CmsAboutUs.class);
        ICmsAboutUsService.saveBatch(personList);
    }
}


