package link.chengguo.orangemall.cms.service;

import link.chengguo.orangemall.cms.entity.CmsFeedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-21
*/

public interface ICmsFeedbackService extends IService<CmsFeedback> {

}
