package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsFeedback;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2019-12-21
*  意见反馈
*/
@Slf4j
@RestController
@RequestMapping("/cms/cmsFeedback")
public class CmsFeedbackController {

@Resource
private link.chengguo.orangemall.cms.service.ICmsFeedbackService ICmsFeedbackService;

@SysLog(MODULE = "cms", REMARK = "根据条件查询所有意见反馈列表")
@ApiOperation("根据条件查询所有意见反馈列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('cms:cmsFeedback:read')")
public Object getCmsFeedbackByPage(CmsFeedback entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(ICmsFeedbackService.page(new Page<CmsFeedback>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有意见反馈列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "保存意见反馈")
@ApiOperation("保存意见反馈")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('cms:cmsFeedback:create')")
public Object saveCmsFeedback(@RequestBody CmsFeedback entity) {
try {

if (ICmsFeedbackService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存意见反馈：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "更新意见反馈")
@ApiOperation("更新意见反馈")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedback:update')")
public Object updateCmsFeedback(@RequestBody CmsFeedback entity) {
try {
if (ICmsFeedbackService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新意见反馈：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "删除意见反馈")
@ApiOperation("删除意见反馈")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedback:delete')")
public Object deleteCmsFeedback(@ApiParam("意见反馈id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("意见反馈id");
}
if (ICmsFeedbackService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除意见反馈：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "给意见反馈分配意见反馈")
@ApiOperation("查询意见反馈明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedback:read')")
public Object getCmsFeedbackById(@ApiParam("意见反馈id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("意见反馈id");
}
CmsFeedback coupon = ICmsFeedbackService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询意见反馈明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除意见反馈")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "cms", REMARK = "批量删除意见反馈")
@PreAuthorize("hasAuthority('cms:cmsFeedback:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = ICmsFeedbackService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsFeedback entity) {
    // 模拟从数据库获取需要导出的数据
    List<CmsFeedback> personList = ICmsFeedbackService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsFeedback.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<CmsFeedback> personList = EasyPoiUtils.importExcel(file, CmsFeedback.class);
    ICmsFeedbackService.saveBatch(personList);
    }
    }


