package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsFeedbackCategory;
import link.chengguo.orangemall.cms.service.ICmsFeedbackCategoryService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* @author yzb
* @date 2019-12-21
*  意见反馈类型
*/
@Slf4j
@RestController
@RequestMapping("/cms/cmsFeedbackCategory")
public class CmsFeedbackCategoryController {

@Resource
private ICmsFeedbackCategoryService ICmsFeedbackCategoryService;

@SysLog(MODULE = "cms", REMARK = "根据条件查询所有意见反馈类型列表")
@ApiOperation("根据条件查询所有意见反馈类型列表")
@GetMapping(value = "/list")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:read')")
public Object getCmsFeedbackCategoryByPage(CmsFeedbackCategory entity,
@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
) {
try {
return new CommonResult().success(ICmsFeedbackCategoryService.page(new Page<CmsFeedbackCategory>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
} catch (Exception e) {
e.printStackTrace();
            log.error("根据条件查询所有意见反馈类型列表：%s", e.getMessage(), e);
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "保存意见反馈类型")
@ApiOperation("保存意见反馈类型")
@PostMapping(value = "/create")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:create')")
public Object saveCmsFeedbackCategory(@RequestBody CmsFeedbackCategory entity) {
try {

if (ICmsFeedbackCategoryService.save(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("保存意见反馈类型：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "更新意见反馈类型")
@ApiOperation("更新意见反馈类型")
@PostMapping(value = "/update/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:update')")
public Object updateCmsFeedbackCategory(@RequestBody CmsFeedbackCategory entity) {
try {
if (ICmsFeedbackCategoryService.updateById(entity)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("更新意见反馈类型：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "删除意见反馈类型")
@ApiOperation("删除意见反馈类型")
@GetMapping(value = "/delete/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:delete')")
public Object deleteCmsFeedbackCategory(@ApiParam("意见反馈类型id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("意见反馈类型id");
}
if (ICmsFeedbackCategoryService.removeById(id)) {
return new CommonResult().success();
}
} catch (Exception e) {
e.printStackTrace();
            log.error("删除意见反馈类型：%s", e.getMessage(), e);
return new CommonResult().failed(e.getMessage());
}
return new CommonResult().failed();
}

@SysLog(MODULE = "cms", REMARK = "给意见反馈类型分配意见反馈类型")
@ApiOperation("查询意见反馈类型明细")
@GetMapping(value = "/{id}")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:read')")
public Object getCmsFeedbackCategoryById(@ApiParam("意见反馈类型id") @PathVariable Long id) {
try {
if (ValidatorUtils.empty(id)) {
return new CommonResult().paramFailed("意见反馈类型id");
}
CmsFeedbackCategory coupon = ICmsFeedbackCategoryService.getById(id);
return new CommonResult().success(coupon);
} catch (Exception e) {
e.printStackTrace();
            log.error("查询意见反馈类型明细：%s", e.getMessage(), e);
return new CommonResult().failed();
}

}

@ApiOperation(value = "批量删除意见反馈类型")
@RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
@SysLog(MODULE = "cms", REMARK = "批量删除意见反馈类型")
@PreAuthorize("hasAuthority('cms:cmsFeedbackCategory:delete')")
public Object deleteBatch(@RequestParam("ids") List
<Long> ids) {
    boolean count = ICmsFeedbackCategoryService.removeByIds(ids);
    if (count) {
    return new CommonResult().success(count);
    } else {
    return new CommonResult().failed();
    }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsFeedbackCategory entity) {
    // 模拟从数据库获取需要导出的数据
    List<CmsFeedbackCategory> personList = ICmsFeedbackCategoryService.list(new QueryWrapper<>(entity).orderByDesc("id"));
    // 导出操作
    EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsFeedbackCategory.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
    List<CmsFeedbackCategory> personList = EasyPoiUtils.importExcel(file, CmsFeedbackCategory.class);
    ICmsFeedbackCategoryService.saveBatch(personList);
    }
    }


