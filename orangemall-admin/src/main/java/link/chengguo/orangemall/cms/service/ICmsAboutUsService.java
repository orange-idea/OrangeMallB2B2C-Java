package link.chengguo.orangemall.cms.service;

import link.chengguo.orangemall.cms.entity.CmsAboutUs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-21
*/

public interface ICmsAboutUsService extends IService<CmsAboutUs> {

}
