package link.chengguo.orangemall.cms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.cms.entity.CmsSubject;

/**
 * <p>
 * 专题表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface ICmsSubjectService extends IService<CmsSubject> {

    int updateRecommendStatus(Long ids, Integer recommendStatus);

    int updateShowStatus(Long ids, Integer showStatus);

    boolean saves(CmsSubject entity);
}
