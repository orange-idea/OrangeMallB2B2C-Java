package link.chengguo.orangemall.cms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.cms.entity.CmsAppNotice;
import link.chengguo.orangemall.cms.service.ICmsAppNoticeService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-21
 * 公告信息
 */
@Slf4j
@RestController
@RequestMapping("/cms/cmsAppNotice")
public class CmsAppNoticeController {

    @Resource
    private ICmsAppNoticeService ICmsAppNoticeService;

    @SysLog(MODULE = "cms", REMARK = "根据条件查询所有公告信息列表")
    @ApiOperation("根据条件查询所有公告信息列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:read')")
    public Object getCmsAppNoticeByPage(CmsAppNotice entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ICmsAppNoticeService.page(new Page<CmsAppNotice>(pageNum, pageSize), new QueryWrapper<>(entity)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有公告信息列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "保存公告信息")
    @ApiOperation("保存公告信息")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:create')")
    public Object saveCmsAppNotice(@RequestBody CmsAppNotice entity) {
        try {

            if (ICmsAppNoticeService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存公告信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "更新公告信息")
    @ApiOperation("更新公告信息")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:update')")
    public Object updateCmsAppNotice(@RequestBody CmsAppNotice entity) {
        try {
            if (ICmsAppNoticeService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新公告信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "删除公告信息")
    @ApiOperation("删除公告信息")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:delete')")
    public Object deleteCmsAppNotice(@ApiParam("公告信息id") @PathVariable Integer id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("公告信息id");
            }
            if (ICmsAppNoticeService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除公告信息：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "给公告信息分配公告信息")
    @ApiOperation("查询公告信息明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:read')")
    public Object getCmsAppNoticeById(@ApiParam("公告信息id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("公告信息id");
            }
            CmsAppNotice coupon = ICmsAppNoticeService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询公告信息明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除公告信息")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "cms", REMARK = "批量删除公告信息")
    @PreAuthorize("hasAuthority('cms:cmsAppNotice:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = ICmsAppNoticeService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, CmsAppNotice entity) {
        // 模拟从数据库获取需要导出的数据
        List<CmsAppNotice> personList = ICmsAppNoticeService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", CmsAppNotice.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<CmsAppNotice> personList = EasyPoiUtils.importExcel(file, CmsAppNotice.class);
        ICmsAppNoticeService.saveBatch(personList);
    }
}


