package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsShopWithdrawAccount;
import link.chengguo.orangemall.fms.service.IFmsShopWithdrawAccountService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 商家提现账号
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsShopWithdrawAccount")
public class FmsShopWithdrawAccountController {

    @Resource
    private IFmsShopWithdrawAccountService IFmsShopWithdrawAccountService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有商家提现账号列表")
    @ApiOperation("根据条件查询所有商家提现账号列表")
    @GetMapping(value = "/list")
    public Object getFmsShopWithdrawAccountByPage(FmsShopWithdrawAccount entity,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IFmsShopWithdrawAccountService.page(new Page<FmsShopWithdrawAccount>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有商家提现账号列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存商家提现账号")
    @ApiOperation("保存商家提现账号")
    @PostMapping(value = "/create")
    public Object saveFmsShopWithdrawAccount(@RequestBody FmsShopWithdrawAccount entity) {
        try {

            if (IFmsShopWithdrawAccountService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存商家提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新商家提现账号")
    @ApiOperation("更新商家提现账号")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsShopWithdrawAccount(@RequestBody FmsShopWithdrawAccount entity) {
        try {
            if (IFmsShopWithdrawAccountService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新商家提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除商家提现账号")
    @ApiOperation("删除商家提现账号")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsShopWithdrawAccount(@ApiParam("商家提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("商家提现账号id");
            }
            if (IFmsShopWithdrawAccountService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除商家提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给商家提现账号分配商家提现账号")
    @ApiOperation("查询商家提现账号明细")
    @GetMapping(value = "/{id}")
    public Object getFmsShopWithdrawAccountById(@ApiParam("商家提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("商家提现账号id");
            }
            FmsShopWithdrawAccount coupon = IFmsShopWithdrawAccountService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询商家提现账号明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除商家提现账号")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除商家提现账号")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsShopWithdrawAccountService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsShopWithdrawAccount entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsShopWithdrawAccount> personList = IFmsShopWithdrawAccountService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsShopWithdrawAccount.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsShopWithdrawAccount> personList = EasyPoiUtils.importExcel(file, FmsShopWithdrawAccount.class);
        IFmsShopWithdrawAccountService.saveBatch(personList);
    }
}


