package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsMemberRechargeDay;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员充值日统计
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsMemberRechargeDay")
public class FmsMemberRechargeDayController {

    @Resource
    private link.chengguo.orangemall.fms.service.IFmsMemberRechargeDayService IFmsMemberRechargeDayService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有会员充值日统计列表")
    @ApiOperation("根据条件查询所有会员充值日统计列表")
    @GetMapping(value = "/list")
    public Object getFmsMemberRechargeDayByPage(FmsMemberRechargeDay entity,
                                                @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IFmsMemberRechargeDayService.page(new Page<FmsMemberRechargeDay>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有会员充值日统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存会员充值日统计")
    @ApiOperation("保存会员充值日统计")
    @PostMapping(value = "/create")
    public Object saveFmsMemberRechargeDay(@RequestBody FmsMemberRechargeDay entity) {
        try {

            if (IFmsMemberRechargeDayService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存会员充值日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新会员充值日统计")
    @ApiOperation("更新会员充值日统计")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsMemberRechargeDay(@RequestBody FmsMemberRechargeDay entity) {
        try {
            if (IFmsMemberRechargeDayService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新会员充值日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除会员充值日统计")
    @ApiOperation("删除会员充值日统计")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsMemberRechargeDay(@ApiParam("会员充值日统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员充值日统计id");
            }
            if (IFmsMemberRechargeDayService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除会员充值日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给会员充值日统计分配会员充值日统计")
    @ApiOperation("查询会员充值日统计明细")
    @GetMapping(value = "/{id}")
    public Object getFmsMemberRechargeDayById(@ApiParam("会员充值日统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员充值日统计id");
            }
            FmsMemberRechargeDay coupon = IFmsMemberRechargeDayService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询会员充值日统计明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除会员充值日统计")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除会员充值日统计")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsMemberRechargeDayService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsMemberRechargeDay entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsMemberRechargeDay> personList = IFmsMemberRechargeDayService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsMemberRechargeDay.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsMemberRechargeDay> personList = EasyPoiUtils.importExcel(file, FmsMemberRechargeDay.class);
        IFmsMemberRechargeDayService.saveBatch(personList);
    }
}


