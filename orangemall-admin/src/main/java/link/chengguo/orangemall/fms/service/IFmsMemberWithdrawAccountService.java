package link.chengguo.orangemall.fms.service;

import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsMemberWithdrawAccountService extends IService<FmsMemberWithdrawAccount> {

}
