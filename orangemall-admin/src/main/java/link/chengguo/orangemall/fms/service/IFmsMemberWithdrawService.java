package link.chengguo.orangemall.fms.service;

import link.chengguo.orangemall.fms.entity.FmsMemberWithdraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsMemberWithdrawService extends IService<FmsMemberWithdraw> {

    /**
     * 提现审核
     * @param result 1-转账成功，2-转账失败（拒绝）
     * @param id 申请记录编号
     * @param pwd  审核密码
     * @param  note  备注
     * @return
     */
    boolean withdrawVerify(Integer result,Long id,String pwd,String note);

}
