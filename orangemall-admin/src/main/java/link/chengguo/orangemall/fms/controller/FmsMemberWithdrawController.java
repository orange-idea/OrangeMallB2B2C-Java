package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdraw;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员提现
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsMemberWithdraw")
public class FmsMemberWithdrawController {

    @Resource
    private IFmsMemberWithdrawService IFmsMemberWithdrawService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有会员提现列表")
    @ApiOperation("根据条件查询所有会员提现列表")
    @GetMapping(value = "/list")
    public Object getFmsMemberWithdrawByPage(FmsMemberWithdraw entity,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            QueryWrapper queryWrapper=new QueryWrapper<>();
            if (entity.getKeyword()!=null && !"".equals(entity.getKeyword())){
                queryWrapper.apply("( member_phone like '%"+entity.getKeyword()+"%'  or member_name like '%"+entity.getKeyword()+"%')");
            }
            if (entity.getStartTime()!=null && entity.getEndTime()!=null){
                queryWrapper.ge("create_time",entity.getStartTime());
                queryWrapper.le("create_time",entity.getEndTime()+" 23:59:59");
            }
            if (entity.getTransferResult()!=null){
                queryWrapper.eq("transfer_result",entity.getTransferResult());
            }
            return new CommonResult().success(IFmsMemberWithdrawService.page(new Page<FmsMemberWithdraw>(pageNum, pageSize), queryWrapper));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有会员提现列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    /**
     * 会员提现审核接口
     * @return
     */
    @SysLog(MODULE = "fms", REMARK = "提现审核操作")
    @ApiOperation("提现审核操作")
    @PostMapping(value = "/withdrawVerify")
    public Object withdrawVerify(@RequestBody FmsMemberWithdraw agentWithdraw) {
        try {
            if (IFmsMemberWithdrawService.withdrawVerify(agentWithdraw.getTransferResult(),agentWithdraw.getId(),
                    agentWithdraw.getKeyword(),agentWithdraw.getTranferDesc())) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("提现审核操作：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }



    @SysLog(MODULE = "fms", REMARK = "保存会员提现")
    @ApiOperation("保存会员提现")
    @PostMapping(value = "/create")
    public Object saveFmsMemberWithdraw(@RequestBody FmsMemberWithdraw entity) {
        try {

            if (IFmsMemberWithdrawService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存会员提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新会员提现")
    @ApiOperation("更新会员提现")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsMemberWithdraw(@RequestBody FmsMemberWithdraw entity) {
        try {
            if (IFmsMemberWithdrawService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新会员提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除会员提现")
    @ApiOperation("删除会员提现")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsMemberWithdraw(@ApiParam("会员提现id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员提现id");
            }
            if (IFmsMemberWithdrawService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除会员提现：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给会员提现分配会员提现")
    @ApiOperation("查询会员提现明细")
    @GetMapping(value = "/{id}")
    public Object getFmsMemberWithdrawById(@ApiParam("会员提现id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员提现id");
            }
            FmsMemberWithdraw coupon = IFmsMemberWithdrawService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询会员提现明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除会员提现")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除会员提现")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsMemberWithdrawService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsMemberWithdraw entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsMemberWithdraw> personList = IFmsMemberWithdrawService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsMemberWithdraw.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsMemberWithdraw> personList = EasyPoiUtils.importExcel(file, FmsMemberWithdraw.class);
        IFmsMemberWithdrawService.saveBatch(personList);
    }
}


