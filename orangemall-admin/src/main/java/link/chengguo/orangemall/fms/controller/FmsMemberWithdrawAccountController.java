package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsMemberWithdrawAccount;
import link.chengguo.orangemall.fms.service.IFmsMemberWithdrawAccountService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员提现账号
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsMemberWithdrawAccount")
public class FmsMemberWithdrawAccountController {

    @Resource
    private IFmsMemberWithdrawAccountService IFmsMemberWithdrawAccountService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有会员提现账号列表")
    @ApiOperation("根据条件查询所有会员提现账号列表")
    @GetMapping(value = "/list")
    public Object getFmsMemberWithdrawAccountByPage(FmsMemberWithdrawAccount entity,
                                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IFmsMemberWithdrawAccountService.page(new Page<FmsMemberWithdrawAccount>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有会员提现账号列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存会员提现账号")
    @ApiOperation("保存会员提现账号")
    @PostMapping(value = "/create")
    public Object saveFmsMemberWithdrawAccount(@RequestBody FmsMemberWithdrawAccount entity) {
        try {

            if (IFmsMemberWithdrawAccountService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存会员提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新会员提现账号")
    @ApiOperation("更新会员提现账号")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsMemberWithdrawAccount(@RequestBody FmsMemberWithdrawAccount entity) {
        try {
            if (IFmsMemberWithdrawAccountService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新会员提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除会员提现账号")
    @ApiOperation("删除会员提现账号")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsMemberWithdrawAccount(@ApiParam("会员提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员提现账号id");
            }
            if (IFmsMemberWithdrawAccountService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除会员提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给会员提现账号分配会员提现账号")
    @ApiOperation("查询会员提现账号明细")
    @GetMapping(value = "/{id}")
    public Object getFmsMemberWithdrawAccountById(@ApiParam("会员提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员提现账号id");
            }
            FmsMemberWithdrawAccount coupon = IFmsMemberWithdrawAccountService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询会员提现账号明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除会员提现账号")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除会员提现账号")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsMemberWithdrawAccountService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsMemberWithdrawAccount entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsMemberWithdrawAccount> personList = IFmsMemberWithdrawAccountService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsMemberWithdrawAccount.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsMemberWithdrawAccount> personList = EasyPoiUtils.importExcel(file, FmsMemberWithdrawAccount.class);
        IFmsMemberWithdrawAccountService.saveBatch(personList);
    }
}


