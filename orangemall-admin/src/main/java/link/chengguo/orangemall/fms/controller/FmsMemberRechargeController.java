package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsMemberRecharge;
import link.chengguo.orangemall.fms.service.IFmsMemberRechargeService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 会员充值
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsMemberRecharge")
public class FmsMemberRechargeController {

    @Resource
    private IFmsMemberRechargeService IFmsMemberRechargeService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有会员充值列表")
    @ApiOperation("根据条件查询所有会员充值列表")
    @GetMapping(value = "/list")
    public Object getFmsMemberRechargeByPage(FmsMemberRecharge entity,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            //名称模糊查询
            QueryWrapper queryWrapper=new QueryWrapper<>();
            if (entity.getKeyword()!=null && !"".equals(entity.getKeyword())){
                queryWrapper.apply("( member_phone like '%"+entity.getKeyword()+"%'  or member_name like '%"+entity.getKeyword()+"%')");
            }
            if (entity.getStartTime()!=null && entity.getEndTime()!=null){
                queryWrapper.ge("create_time",entity.getStartTime());
                queryWrapper.le("create_time",entity.getEndTime()+" 23:59:59");
            }
            return new CommonResult().success(IFmsMemberRechargeService.page(new Page<>(pageNum, pageSize), queryWrapper));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有会员充值列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存会员充值")
    @ApiOperation("保存会员充值")
    @PostMapping(value = "/create")
    public Object saveFmsMemberRecharge(@RequestBody FmsMemberRecharge entity) {
        try {

            if (IFmsMemberRechargeService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存会员充值：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新会员充值")
    @ApiOperation("更新会员充值")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsMemberRecharge(@RequestBody FmsMemberRecharge entity) {
        try {
            if (IFmsMemberRechargeService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新会员充值：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除会员充值")
    @ApiOperation("删除会员充值")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsMemberRecharge(@ApiParam("会员充值id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员充值id");
            }
            if (IFmsMemberRechargeService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除会员充值：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给会员充值分配会员充值")
    @ApiOperation("查询会员充值明细")
    @GetMapping(value = "/{id}")
    public Object getFmsMemberRechargeById(@ApiParam("会员充值id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("会员充值id");
            }
            FmsMemberRecharge coupon = IFmsMemberRechargeService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询会员充值明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除会员充值")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除会员充值")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsMemberRechargeService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsMemberRecharge entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsMemberRecharge> personList = IFmsMemberRechargeService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsMemberRecharge.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsMemberRecharge> personList = EasyPoiUtils.importExcel(file, FmsMemberRecharge.class);
        IFmsMemberRechargeService.saveBatch(personList);
    }
}


