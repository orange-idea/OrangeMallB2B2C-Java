package link.chengguo.orangemall.fms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsAgentWithdrawAccount;
import link.chengguo.orangemall.fms.service.IFmsAgentWithdrawAccountService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2020-02-10
 * 代理提现账号
 */
@Slf4j
@RestController
@RequestMapping("/fms/fmsAgentWithdrawAccount")
public class FmsAgentWithdrawAccountController {
    @Resource
    private IFmsAgentWithdrawAccountService IFmsAgentWithdrawAccountService;

    @SysLog(MODULE = "fms", REMARK = "根据条件查询所有代理提现账号列表")
    @ApiOperation("根据条件查询所有代理提现账号列表")
    @GetMapping(value = "/list")
    public Object getFmsAgentWithdrawAccountByPage(FmsAgentWithdrawAccount entity,
                                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IFmsAgentWithdrawAccountService.page(new Page<FmsAgentWithdrawAccount>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有代理提现账号列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "保存代理提现账号")
    @ApiOperation("保存代理提现账号")
    @PostMapping(value = "/create")
    public Object saveFmsAgentWithdrawAccount(@RequestBody FmsAgentWithdrawAccount entity) {
        try {

            if (IFmsAgentWithdrawAccountService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存代理提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "更新代理提现账号")
    @ApiOperation("更新代理提现账号")
    @PostMapping(value = "/update/{id}")
    public Object updateFmsAgentWithdrawAccount(@RequestBody FmsAgentWithdrawAccount entity) {
        try {
            if (IFmsAgentWithdrawAccountService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新代理提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "删除代理提现账号")
    @ApiOperation("删除代理提现账号")
    @GetMapping(value = "/delete/{id}")
    public Object deleteFmsAgentWithdrawAccount(@ApiParam("代理提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理提现账号id");
            }
            if (IFmsAgentWithdrawAccountService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除代理提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "fms", REMARK = "给代理提现账号分配代理提现账号")
    @ApiOperation("查询代理提现账号明细")
    @GetMapping(value = "/{id}")
    public Object getFmsAgentWithdrawAccountById(@ApiParam("代理提现账号id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("代理提现账号id");
            }
            FmsAgentWithdrawAccount coupon = IFmsAgentWithdrawAccountService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询代理提现账号明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除代理提现账号")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "fms", REMARK = "批量删除代理提现账号")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IFmsAgentWithdrawAccountService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "fms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, FmsAgentWithdrawAccount entity) {
        // 模拟从数据库获取需要导出的数据
        List<FmsAgentWithdrawAccount> personList = IFmsAgentWithdrawAccountService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", FmsAgentWithdrawAccount.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "fms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<FmsAgentWithdrawAccount> personList = EasyPoiUtils.importExcel(file, FmsAgentWithdrawAccount.class);
        IFmsAgentWithdrawAccountService.saveBatch(personList);
    }

    @SysLog(MODULE = "fms", REMARK = "更新代理提现账号")
    @ApiOperation("更新代理提现账号")
    @PostMapping(value = "/addOrupdateByType")
    public Object addOrupdateByType(@RequestBody FmsAgentWithdrawAccount entity) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            if (user==null){
                return new CommonResult().failed("账号信息异常");
            }
            FmsAgentWithdrawAccount parm=new FmsAgentWithdrawAccount();
            parm.setCityCode(user.getCityCode());
            parm.setAccountType(entity.getAccountType());
            entity.setCityCode(user.getCityCode());
            FmsAgentWithdrawAccount result=IFmsAgentWithdrawAccountService.getOne(new QueryWrapper<>(parm));
            if (result!=null){
                //存在,则进行更新
                if (IFmsAgentWithdrawAccountService.update(entity,new QueryWrapper<>(parm))) {
                    return new CommonResult().success();
                }
            }else{
                if (IFmsAgentWithdrawAccountService.save(entity)) {
                    return new CommonResult().success();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新代理提现账号：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "fms", REMARK = "查询代理提现账号明细")
    @ApiOperation("查询代理提现账号明细(账号类型：0->支付宝；1->微信；2->银联)")
    @GetMapping(value = "/getAccountByType")
    public Object getFmsAgentWithdrawAccountById(@RequestParam(value = "type", defaultValue = "0") Integer type) {
        try {
            SysUser user= UserUtils.getCurrentMember();
            if (user==null){
                return new CommonResult().failed("账号信息异常");
            }
            FmsAgentWithdrawAccount account=new FmsAgentWithdrawAccount();
            account.setAccountType(type);
            account.setCityCode(user.getCityCode());
            FmsAgentWithdrawAccount coupon = IFmsAgentWithdrawAccountService.getOne(new QueryWrapper<>(account));
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询代理提现账号明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

}


