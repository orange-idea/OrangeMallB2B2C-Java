package link.chengguo.orangemall.fms.service;

import link.chengguo.orangemall.fms.entity.FmsShopWithdraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2020-02-10
*/

public interface IFmsShopWithdrawService extends IService<FmsShopWithdraw> {

    /**
     * 提现审核
     * @param result 1-转账成功，2-转账失败（拒绝）
     * @param id 申请记录编号
     * @return
     */
    boolean withdrawVerify(Integer result,Long id,String pwd,String note);

}
