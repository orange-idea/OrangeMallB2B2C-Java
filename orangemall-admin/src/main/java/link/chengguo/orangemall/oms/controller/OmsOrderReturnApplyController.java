package link.chengguo.orangemall.oms.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.IgnoreAuth;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.entity.OmsOrderItem;
import link.chengguo.orangemall.oms.entity.OmsOrderReturnApply;
import link.chengguo.orangemall.oms.service.IOmsOrderItemService;
import link.chengguo.orangemall.oms.service.IOmsOrderService;
import link.chengguo.orangemall.oms.vo.OmsUpdateStatusParam;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * 订单退货申请
 * </p>
 *
 * @author chengguo
 * @since 2019-04-19
 */
@Slf4j
@RestController
@Api(tags = "oms", description = "订单退货申请管理")
@RequestMapping("/oms/OmsOrderReturnApply")
public class OmsOrderReturnApplyController {
    @Resource
    private link.chengguo.orangemall.oms.service.IOmsOrderReturnApplyService IOmsOrderReturnApplyService;

    @Resource
    private IOmsOrderService omsOrderService;
    @Resource
    private IOmsOrderItemService omsOrderItemService;

    @SysLog(MODULE = "oms", REMARK = "根据条件查询所有订单退货申请列表")
    @ApiOperation("根据条件查询所有订单退货申请列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('oms:OmsOrderReturnApply:read')")
    public Object getOmsOrderReturnApplyByPage(OmsOrderReturnApply entity,
                                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IOmsOrderReturnApplyService.page(new Page<OmsOrderReturnApply>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有订单退货申请列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }
//    @IgnoreAuth
//    @GetMapping(value = "/saveAll")
    public Object saveAll(){
        //1.查询订单中所有处在售后状态的订单
        try{
            List<OmsOrder> list = omsOrderService.list(new QueryWrapper<OmsOrder>().eq("status", 6));

            for(OmsOrder omsOrder : list){
                OmsOrderReturnApply omsOrderReturnApply = new OmsOrderReturnApply(omsOrder);

                int order_sn = IOmsOrderReturnApplyService.count(new QueryWrapper<OmsOrderReturnApply>().eq("order_sn", omsOrderReturnApply.getOrderSn()));
                if(order_sn>0){
                    log.info("即将跳出："+omsOrderReturnApply.getOrderId());
                    continue;
                }
                log.info("执行着："+omsOrderReturnApply.getId());
                List<OmsOrderItem> orderItems = omsOrderItemService.list(new QueryWrapper<OmsOrderItem>().eq("order_id", omsOrderReturnApply.getOrderId()));
                StringBuffer str = new StringBuffer();
                Iterator<OmsOrderItem> iterator = orderItems.iterator();
                while (iterator.hasNext()){
                    if(str.length()>0){
                        str.append(",");
                    }
                    OmsOrderItem omsOrderItem = iterator.next();
                    str.append(omsOrderItem.getId());

                }
                omsOrderReturnApply.setOrderItemId(str.toString());
                IOmsOrderReturnApplyService.save(omsOrderReturnApply);

            }
            return new CommonResult().success(IOmsOrderReturnApplyService.list(new QueryWrapper<>()));
        }catch (Exception e){
            e.printStackTrace();
        }

        return new CommonResult().success("成功");
    }
    @SysLog(MODULE = "oms", REMARK = "保存订单退货申请")
    @ApiOperation("保存订单退货申请")
    @PostMapping(value = "/create")
    public Object saveOmsOrderReturnApply(@RequestBody OmsOrderReturnApply entity) {
        try {
            if (IOmsOrderReturnApplyService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存订单退货申请：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "oms", REMARK = "更新订单退货申请")
    @ApiOperation("更新订单退货申请")
    @PostMapping(value = "/update/{id}")
    public Object updateOmsOrderReturnApply(@RequestBody OmsOrderReturnApply entity) {
        try {
            if (IOmsOrderReturnApplyService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新订单退货申请：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "oms", REMARK = "删除订单退货申请")
    @ApiOperation("删除订单退货申请")
    @GetMapping(value = "/delete/{id}")
    public Object deleteOmsOrderReturnApply(@ApiParam("订单退货申请id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("订单退货申请id");
            }
            if (IOmsOrderReturnApplyService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除订单退货申请：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "oms", REMARK = "给订单退货申请分配订单退货申请")
    @ApiOperation("查询订单退货申请明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('oms:OmsOrderReturnApply:read')")
    public Object getOmsOrderReturnApplyById(@ApiParam("订单退货申请id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("订单退货申请id");
            }
            OmsOrderReturnApply coupon = IOmsOrderReturnApplyService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单退货申请明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除订单退货申请")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @ResponseBody
    @SysLog(MODULE = "pms", REMARK = "批量删除订单退货申请")
    @PreAuthorize("hasAuthority('oms:OmsOrderReturnApply:delete')")
    public Object deleteBatch(@RequestParam("ids") List<Long> ids) {
        boolean count = IOmsOrderReturnApplyService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }

    @SysLog(MODULE = "oms", REMARK = "获取所有收货地址")
    @ApiOperation("修改申请状态")
    @RequestMapping(value = "/update/status/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object updateStatus(@PathVariable Long id, @RequestBody OmsUpdateStatusParam statusParam) {
        int count = IOmsOrderReturnApplyService.updateStatus(id, statusParam);
        if (count > 0) {
            return new CommonResult().success(count);
        }
        return new CommonResult().failed();
    }
}
