package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsPayments;

/**
 * @author orangemall
 * @date 2019-12-07
 */

public interface IOmsPaymentsService extends IService<OmsPayments> {

}
