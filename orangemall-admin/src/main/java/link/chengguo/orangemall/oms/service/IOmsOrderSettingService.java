package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrderSetting;

/**
 * <p>
 * 订单设置表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderSettingService extends IService<OmsOrderSetting> {

}
