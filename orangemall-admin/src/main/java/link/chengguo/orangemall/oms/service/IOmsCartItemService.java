package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsCartItem;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsCartItemService extends IService<OmsCartItem> {

}
