package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsCompanyAddress;

/**
 * @author orangemall
 * @date 2019-12-07
 */

public interface IOmsCompanyAddressService extends IService<OmsCompanyAddress> {

}
