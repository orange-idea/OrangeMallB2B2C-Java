package link.chengguo.orangemall.oms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrderOperateHistory;

/**
 * <p>
 * 订单操作历史记录 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-17
 */
public interface IOmsOrderOperateHistoryService extends IService<OmsOrderOperateHistory> {

}
