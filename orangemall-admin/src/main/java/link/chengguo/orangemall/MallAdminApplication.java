package link.chengguo.orangemall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 应用启动入口
 * https://github.com/orangemall on 2018/4/26.
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan({"link.chengguo.orangemall.mapper","link.chengguo.orangemall.ums.mapper", "link.chengguo.orangemall.sms.mapper", "link.chengguo.orangemall.cms.mapper", "link.chengguo.orangemall.sys.mapper", "link.chengguo.orangemall.oms.mapper", "com.chengguo.orangemall.pms.mapper"})
@EnableTransactionManagement
public class MallAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallAdminApplication.class, args);
    }
}
