package link.chengguo.orangemall.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillReship;

/**
 * <p>
 * 退货单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillReshipService extends IService<BillReship> {

}
