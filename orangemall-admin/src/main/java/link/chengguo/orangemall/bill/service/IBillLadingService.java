package link.chengguo.orangemall.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillLading;

/**
 * <p>
 * 提货单表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillLadingService extends IService<BillLading> {

}
