package link.chengguo.orangemall.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.bill.entity.BillPaymentsRel;

/**
 * <p>
 * 支付单明细表 服务类
 * </p>
 *
 * @author chengguo
 * @since 2019-09-16
 */
public interface IBillPaymentsRelService extends IService<BillPaymentsRel> {

}
