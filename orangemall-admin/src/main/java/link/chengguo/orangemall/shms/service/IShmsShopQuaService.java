package link.chengguo.orangemall.shms.service;

import link.chengguo.orangemall.shms.entity.ShmsShopQua;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopQuaService extends IService<ShmsShopQua> {

}
