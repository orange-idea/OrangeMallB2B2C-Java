package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.service.IAmsBusinessCityService;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.shms.entity.ShmsShopBillDay;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import link.chengguo.orangemall.shms.service.IShmsShopBillDayService;
import link.chengguo.orangemall.shms.service.IShmsShopInfoService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.DateUtils;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺账单日统计
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopBillDayController ", description = "店铺账单日统计")
@RequestMapping("/shms/shmsShopBillDay")
public class ShmsShopBillDayController {

    @Resource
    private IShmsShopBillDayService IShmsShopBillDayService;
    @Resource
    private IShmsShopInfoService shopInfoService;
    @Resource
    private IAmsBusinessCityService businessCityService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺账单日统计列表")
    @ApiOperation("根据条件查询所有店铺账单日统计列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:read')")
    public Object getShmsShopBillDayByPage(ShmsShopBillDay entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                //查询自己店铺的
                SysUser user=UserUtils.getCurrentMember();
                entity.setCityCode(user.getCityCode());
            }
            // 查询当月
            if (!ValidatorUtils.isEmpty(entity.getStartTime())){
                //如2020-01  添加日期为2020-01-01
                String startTime=entity.getStartTime()+"-01";
                Date date=DateUtils.strToDate(startTime);
                Date date2=DateUtils.addDateMonths(date,1);
                String endTime=DateUtils.format(date2,DateUtils.DATE_PATTERN);
                entity.setStartTime(startTime);
                entity.setEndTime(endTime);
            }else{
                entity.setStartTime(null);
                entity.setEndTime(null);
            }
            return new CommonResult().success(IShmsShopBillDayService.getDayBillByDay(pageNum,pageSize,entity));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账单日统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "shms", REMARK = "根据条件查询每个店铺账单日统计列表")
    @ApiOperation("根据条件查询每个店铺账单日统计列表")
    @GetMapping(value = "/getShopBillList")
    public Object getShopBillList(ShmsShopBillDay entity,
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            //查询自己店铺的
            SysUser user=UserUtils.getCurrentMember();
            QueryWrapper queryWrapper=new QueryWrapper();
            if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                queryWrapper.eq("city_code",user.getCityCode());
            }
            if (!ValidatorUtils.isEmpty(entity.getStartTime())){
                //如2020-01  添加日期为2020-01-01
                String startTime=entity.getStartTime()+"-01";
                Date date=DateUtils.strToDate(startTime);
                Date date2=DateUtils.addDateMonths(date,1);
                String endTime=DateUtils.format(date2,DateUtils.DATE_PATTERN);
                queryWrapper.ge("day",startTime);
                queryWrapper.le("day",endTime);
            }else{
                entity.setStartTime(null);
                entity.setEndTime(null);
            }
            IPage<ShmsShopBillDay> page=IShmsShopBillDayService.page(new Page<>(pageNum, pageSize),queryWrapper);
            List<ShmsShopBillDay> list=page.getRecords();
            for (int i=0;i<list.size();i++){
                if(list.get(i).getShopId() == null){
                    continue;
                }
                ShmsShopInfo shopInfo = shopInfoService.getById(list.get(i).getShopId());
                if(shopInfo == null){
                    continue;
                }
                list.get(i).setShopName(shopInfoService.getById(list.get(i).getShopId()).getName());
                if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Tenant.code())){
                    list.get(i).setCityName(businessCityService.getCityByCityCode(list.get(i).getCityCode()).getBusinessCityName());
                }
            }
            page.setRecords(list);
            return new CommonResult().success(page);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询每个店铺账单日统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "根据条件查询每个代理账单日统计列表")
    @ApiOperation("根据条件查询每个代理账单日统计列表")
    @GetMapping(value = "/getAgentBillList")
    public Object getAgentBillList(ShmsShopBillDay entity,
                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            // 查询当月
            if (!ValidatorUtils.isEmpty(entity.getStartTime())){
                //如2020-01  添加日期为2020-01-01
                String startTime=entity.getStartTime()+"-01";
                Date date=DateUtils.strToDate(startTime);
                Date date2=DateUtils.addDateMonths(date,1);
                String endTime=DateUtils.format(date2,DateUtils.DATE_PATTERN);
                entity.setStartTime(startTime);
                entity.setEndTime(endTime);
            }else{
                entity.setStartTime(null);
                entity.setEndTime(null);
            }
            entity.setAgentAmount(new BigDecimal(100));
            IPage<ShmsShopBillDay> page=IShmsShopBillDayService.getDayBillByDay(pageNum,pageSize,entity);
            List<ShmsShopBillDay> list=page.getRecords();
            for (int i=0;i<list.size();i++){
                if(list.get(i)!=null){
                    list.get(i).setShopName(shopInfoService.getById(list.get(i).getShopId()).getName());
                    list.get(i).setCityName(businessCityService.getCityByCityCode(list.get(i).getCityCode()).getBusinessCityName());
                }

            }
            page.setRecords(list);
            return new CommonResult().success(page);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账单日统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }


    @SysLog(MODULE = "shms", REMARK = "保存店铺账单日统计")
    @ApiOperation("保存店铺账单日统计")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:create')")
    public Object saveShmsShopBillDay(@RequestBody ShmsShopBillDay entity) {
        try {

            if (IShmsShopBillDayService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺账单日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺账单日统计")
    @ApiOperation("更新店铺账单日统计")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:update')")
    public Object updateShmsShopBillDay(@RequestBody ShmsShopBillDay entity) {
        try {
            if (IShmsShopBillDayService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺账单日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺账单日统计")
    @ApiOperation("删除店铺账单日统计")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:delete')")
    public Object deleteShmsShopBillDay(@ApiParam("店铺账单日统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单日统计id");
            }
            if (IShmsShopBillDayService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺账单日统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺账单日统计分配店铺账单日统计")
    @ApiOperation("查询店铺账单日统计明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:read')")
    public Object getShmsShopBillDayById(@ApiParam("店铺账单日统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单日统计id");
            }
            ShmsShopBillDay coupon = IShmsShopBillDayService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺账单日统计明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺账单日统计")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺账单日统计")
    @PreAuthorize("hasAuthority('shms:shmsShopBillDay:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopBillDayService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopBillDay entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopBillDay> personList = IShmsShopBillDayService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopBillDay.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopBillDay> personList = EasyPoiUtils.importExcel(file, ShmsShopBillDay.class);
        IShmsShopBillDayService.saveBatch(personList);
    }
}


