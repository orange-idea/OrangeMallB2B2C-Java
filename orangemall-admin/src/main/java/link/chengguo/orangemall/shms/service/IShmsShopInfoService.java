package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.shms.entity.ShmsShopInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopInfoService extends IService<ShmsShopInfo> {

    /**
     * 店铺审核
     * @param shmsShopInfo
     * @return
     */
    boolean shopVerify(ShmsShopInfo shmsShopInfo) throws Exception;

    /**
     * 创建店铺
     * @param shmsShopInfo
     * @return
     */
    boolean createShopInfo(ShmsShopInfo shmsShopInfo) throws Exception;

    /**
     * 更新店铺管理员登录密码
     * @param shopId
     * @return
     */
    boolean resetShopPwd(Long shopId) throws Exception;

    /**
     * 更新店铺
     * @param shmsShopInfo
     * @return
     */
    boolean updateShopInfo(ShmsShopInfo shmsShopInfo) throws Exception;

    /**
     * 获取分页实体列表
     *
     * @author yzb
     * @Date 2019-12-21
     */
    Page<ShmsShopInfo> customPageList(Integer page,Integer pageSize, ShmsShopInfo paramCondition);


    void saveSmsAndSendCode(ShmsShopInfo entity);
}
