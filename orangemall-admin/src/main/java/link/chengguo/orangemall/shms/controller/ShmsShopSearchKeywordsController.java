package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopSearchKeywords;
import link.chengguo.orangemall.shms.service.IShmsShopSearchKeywordsService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺关键字查询
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopSearchKeywordsController ", description = "店铺关键字查询")
@RequestMapping("/shms/shmsShopSearchKeywords")
public class ShmsShopSearchKeywordsController {

    @Resource
    private IShmsShopSearchKeywordsService IShmsShopSearchKeywordsService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺关键字查询列表")
    @ApiOperation("根据条件查询所有店铺关键字查询列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:read')")
    public Object getShmsShopSearchKeywordsByPage(ShmsShopSearchKeywords entity,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopSearchKeywordsService.page(new Page<ShmsShopSearchKeywords>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺关键字查询列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺关键字查询")
    @ApiOperation("保存店铺关键字查询")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:create')")
    public Object saveShmsShopSearchKeywords(@RequestBody ShmsShopSearchKeywords entity) {
        try {

            if (IShmsShopSearchKeywordsService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺关键字查询：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺关键字查询")
    @ApiOperation("更新店铺关键字查询")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:update')")
    public Object updateShmsShopSearchKeywords(@RequestBody ShmsShopSearchKeywords entity) {
        try {
            if (IShmsShopSearchKeywordsService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺关键字查询：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺关键字查询")
    @ApiOperation("删除店铺关键字查询")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:delete')")
    public Object deleteShmsShopSearchKeywords(@ApiParam("店铺关键字查询id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺关键字查询id");
            }
            if (IShmsShopSearchKeywordsService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺关键字查询：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺关键字查询分配店铺关键字查询")
    @ApiOperation("查询店铺关键字查询明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:read')")
    public Object getShmsShopSearchKeywordsById(@ApiParam("店铺关键字查询id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺关键字查询id");
            }
            ShmsShopSearchKeywords coupon = IShmsShopSearchKeywordsService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺关键字查询明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺关键字查询")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺关键字查询")
    @PreAuthorize("hasAuthority('shms:shmsShopSearchKeywords:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopSearchKeywordsService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopSearchKeywords entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopSearchKeywords> personList = IShmsShopSearchKeywordsService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopSearchKeywords.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopSearchKeywords> personList = EasyPoiUtils.importExcel(file, ShmsShopSearchKeywords.class);
        IShmsShopSearchKeywordsService.saveBatch(personList);
    }
}


