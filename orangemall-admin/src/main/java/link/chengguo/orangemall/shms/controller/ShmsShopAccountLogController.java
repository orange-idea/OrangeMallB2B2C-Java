package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopAccountLog;
import link.chengguo.orangemall.shms.service.IShmsShopAccountLogService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺账户记录
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopAccountLogController ", description = "店铺账户记录")
@RequestMapping("/shms/shmsShopAccountLog")
public class ShmsShopAccountLogController {

    @Resource
    private IShmsShopAccountLogService IShmsShopAccountLogService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺账户记录列表")
    @ApiOperation("根据条件查询所有店铺账户记录列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:read')")
    public Object getShmsShopAccountLogByPage(ShmsShopAccountLog entity,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopAccountLogService.page(new Page<ShmsShopAccountLog>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账户记录列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺账户记录")
    @ApiOperation("保存店铺账户记录")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:create')")
    public Object saveShmsShopAccountLog(@RequestBody ShmsShopAccountLog entity) {
        try {

            if (IShmsShopAccountLogService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺账户记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺账户记录")
    @ApiOperation("更新店铺账户记录")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:update')")
    public Object updateShmsShopAccountLog(@RequestBody ShmsShopAccountLog entity) {
        try {
            if (IShmsShopAccountLogService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺账户记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺账户记录")
    @ApiOperation("删除店铺账户记录")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:delete')")
    public Object deleteShmsShopAccountLog(@ApiParam("店铺账户记录id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账户记录id");
            }
            if (IShmsShopAccountLogService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺账户记录：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺账户记录分配店铺账户记录")
    @ApiOperation("查询店铺账户记录明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:read')")
    public Object getShmsShopAccountLogById(@ApiParam("店铺账户记录id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账户记录id");
            }
            ShmsShopAccountLog coupon = IShmsShopAccountLogService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺账户记录明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺账户记录")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺账户记录")
    @PreAuthorize("hasAuthority('shms:shmsShopAccountLog:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopAccountLogService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopAccountLog entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopAccountLog> personList = IShmsShopAccountLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopAccountLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopAccountLog> personList = EasyPoiUtils.importExcel(file, ShmsShopAccountLog.class);
        IShmsShopAccountLogService.saveBatch(personList);
    }
}


