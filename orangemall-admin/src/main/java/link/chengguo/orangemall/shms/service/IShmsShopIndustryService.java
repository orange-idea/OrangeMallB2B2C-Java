package link.chengguo.orangemall.shms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.entity.ShmsShopIndustry;
import link.chengguo.orangemall.shms.vo.ShmsShopIndustryWithChildrenItem;

import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopIndustryService extends IService<ShmsShopIndustry> {

    /**
     * 查询一级带子分类的
     * @return
     */
    List<ShmsShopIndustryWithChildrenItem> listWithChildren(ShmsShopIndustry param);


}
