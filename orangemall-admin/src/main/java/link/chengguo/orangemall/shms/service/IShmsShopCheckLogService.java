package link.chengguo.orangemall.shms.service;

import link.chengguo.orangemall.shms.entity.ShmsShopCheckLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopCheckLogService extends IService<ShmsShopCheckLog> {

}
