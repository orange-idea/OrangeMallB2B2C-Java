package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopConfig;
import link.chengguo.orangemall.shms.service.IShmsShopConfigService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺配置
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopConfigController ", description = "店铺配置")
@RequestMapping("/shms/shmsShopConfig")
public class ShmsShopConfigController {

    @Resource
    private IShmsShopConfigService IShmsShopConfigService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺配置列表")
    @ApiOperation("根据条件查询所有店铺配置列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:read')")
    public Object getShmsShopConfigByPage(ShmsShopConfig entity,
                                          @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopConfigService.page(new Page<ShmsShopConfig>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺配置列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺配置")
    @ApiOperation("保存店铺配置")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:create')")
    public Object saveShmsShopConfig(@RequestBody ShmsShopConfig entity) {
        try {

            if (IShmsShopConfigService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺配置")
    @ApiOperation("更新店铺配置")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:update')")
    public Object updateShmsShopConfig(@RequestBody ShmsShopConfig entity) {
        try {
            if (IShmsShopConfigService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺配置")
    @ApiOperation("删除店铺配置")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:delete')")
    public Object deleteShmsShopConfig(@ApiParam("店铺配置id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺配置id");
            }
            if (IShmsShopConfigService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺配置：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺配置分配店铺配置")
    @ApiOperation("查询店铺配置明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:read')")
    public Object getShmsShopConfigById(@ApiParam("店铺配置id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺配置id");
            }
            ShmsShopConfig coupon = IShmsShopConfigService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺配置明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺配置")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺配置")
    @PreAuthorize("hasAuthority('shms:shmsShopConfig:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopConfigService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopConfig entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopConfig> personList = IShmsShopConfigService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopConfig.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopConfig> personList = EasyPoiUtils.importExcel(file, ShmsShopConfig.class);
        IShmsShopConfigService.saveBatch(personList);
    }
}


