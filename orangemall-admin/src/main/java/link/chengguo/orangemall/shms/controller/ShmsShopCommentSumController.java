package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopCommentSum;
import link.chengguo.orangemall.shms.service.IShmsShopCommentSumService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺评价统计
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopCommentSumController ", description = "店铺评价统计")
@RequestMapping("/shms/shmsShopCommentSum")
public class ShmsShopCommentSumController {

    @Resource
    private IShmsShopCommentSumService IShmsShopCommentSumService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺评价统计列表")
    @ApiOperation("根据条件查询所有店铺评价统计列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:read')")
    public Object getShmsShopCommentSumByPage(ShmsShopCommentSum entity,
                                              @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopCommentSumService.page(new Page<ShmsShopCommentSum>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺评价统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺评价统计")
    @ApiOperation("保存店铺评价统计")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:create')")
    public Object saveShmsShopCommentSum(@RequestBody ShmsShopCommentSum entity) {
        try {

            if (IShmsShopCommentSumService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺评价统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺评价统计")
    @ApiOperation("更新店铺评价统计")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:update')")
    public Object updateShmsShopCommentSum(@RequestBody ShmsShopCommentSum entity) {
        try {
            if (IShmsShopCommentSumService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺评价统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺评价统计")
    @ApiOperation("删除店铺评价统计")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:delete')")
    public Object deleteShmsShopCommentSum(@ApiParam("店铺评价统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺评价统计id");
            }
            if (IShmsShopCommentSumService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺评价统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺评价统计分配店铺评价统计")
    @ApiOperation("查询店铺评价统计明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:read')")
    public Object getShmsShopCommentSumById(@ApiParam("店铺评价统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺评价统计id");
            }
            ShmsShopCommentSum coupon = IShmsShopCommentSumService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺评价统计明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺评价统计")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺评价统计")
    @PreAuthorize("hasAuthority('shms:shmsShopCommentSum:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopCommentSumService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopCommentSum entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopCommentSum> personList = IShmsShopCommentSumService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopCommentSum.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopCommentSum> personList = EasyPoiUtils.importExcel(file, ShmsShopCommentSum.class);
        IShmsShopCommentSumService.saveBatch(personList);
    }
}


