package link.chengguo.orangemall.shms.service;

import link.chengguo.orangemall.shms.entity.ShmsShopBusinessCity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-19
*/

public interface IShmsShopBusinessCityService extends IService<ShmsShopBusinessCity> {

}
