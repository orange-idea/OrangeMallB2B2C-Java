package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.ams.entity.AmsBusinessCity;
import link.chengguo.orangemall.ams.mapper.AmsBusinessCityMapper;
import link.chengguo.orangemall.ams.service.IAmsBusinessCityService;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.enums.BillSettlementStatus;
import link.chengguo.orangemall.shms.entity.ShmsShopBillLog;
import link.chengguo.orangemall.shms.service.IShmsShopBillLogService;
import link.chengguo.orangemall.shms.service.IShmsShopInfoService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺账单明细
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopBillLogController ", description = "店铺账单明细")
@RequestMapping("/shms/shmsShopBillLog")
public class ShmsShopBillLogController {

    @Resource
    private IShmsShopBillLogService IShmsShopBillLogService;
    @Resource
    private IShmsShopInfoService shopInfoService;
    @Resource
    private AmsBusinessCityMapper businessCityMapper;
    @Resource
    private IAmsBusinessCityService businessCityService;


    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺账单明细列表")
    @ApiOperation("根据条件查询所有店铺账单明细列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:read')")
    public Object getShmsShopBillLogByPage(ShmsShopBillLog entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {

            if (UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Tenant.code())){
                //已结算的
                entity.setBillStatus(BillSettlementStatus.Settlemented.getValue());
            }else if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                //查询自己店铺的
                SysUser user = UserUtils.getCurrentMember();
                entity.setCityCode(user.getCityCode());
            }
            QueryWrapper queryWrapper = new QueryWrapper(entity);
            queryWrapper.orderByDesc("id");
            // 查询时间
            if (!ValidatorUtils.isEmpty(entity.getStartTime())){
                queryWrapper.between("create_time",entity.getStartTime(),entity.getEndTime());
            }
            IPage<ShmsShopBillLog> page=IShmsShopBillLogService.page(new Page<ShmsShopBillLog>(pageNum, pageSize), queryWrapper);
            List<ShmsShopBillLog> list=page.getRecords();
            for (int i=0;i<list.size();i++){
                list.get(i).setShopName(shopInfoService.getById(list.get(i).getShopId()).getName());
                if (UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Tenant.code())){
                    list.get(i).setCityName(businessCityService.getCityByCityCode(list.get(i).getCityCode()).getBusinessCityName());
                }else if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                    list.get(i).getShopId();
                    AmsBusinessCity city=new AmsBusinessCity();
                    city.setBusinessDistrict(list.get(i).getCityCode());
                    list.get(i).setCityName(businessCityMapper.selectOne(new QueryWrapper<>(city)).getBusinessCityName());
                }
            }
            page.setRecords(list);
            return new CommonResult().success(page);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账单明细列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺账单明细")
    @ApiOperation("保存店铺账单明细")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:create')")
    public Object saveShmsShopBillLog(@RequestBody ShmsShopBillLog entity) {
        try {

            if (IShmsShopBillLogService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺账单明细：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺账单明细")
    @ApiOperation("更新店铺账单明细")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:update')")
    public Object updateShmsShopBillLog(@RequestBody ShmsShopBillLog entity) {
        try {
            if (IShmsShopBillLogService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺账单明细：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺账单明细")
    @ApiOperation("删除店铺账单明细")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:delete')")
    public Object deleteShmsShopBillLog(@ApiParam("店铺账单明细id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单明细id");
            }
            if (IShmsShopBillLogService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺账单明细：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺账单明细分配店铺账单明细")
    @ApiOperation("查询店铺账单明细明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:read')")
    public Object getShmsShopBillLogById(@ApiParam("店铺账单明细id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单明细id");
            }
            ShmsShopBillLog coupon = IShmsShopBillLogService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺账单明细明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺账单明细")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺账单明细")
    @PreAuthorize("hasAuthority('shms:shmsShopBillLog:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopBillLogService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopBillLog entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopBillLog> personList = IShmsShopBillLogService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopBillLog.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopBillLog> personList = EasyPoiUtils.importExcel(file, ShmsShopBillLog.class);
        IShmsShopBillLogService.saveBatch(personList);
    }
}


