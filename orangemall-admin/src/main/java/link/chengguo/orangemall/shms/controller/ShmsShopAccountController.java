package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.shms.entity.ShmsShopAccount;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺账户
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopAccountController", description = "店铺账户管理")
@RequestMapping("/shms/shmsShopAccount")
public class ShmsShopAccountController {

    @Resource
    private link.chengguo.orangemall.shms.service.IShmsShopAccountService IShmsShopAccountService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺账户列表")
    @ApiOperation("根据条件查询所有店铺账户列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:read')")
    public Object getShmsShopAccountByPage(ShmsShopAccount entity,
                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(IShmsShopAccountService.page(new Page<ShmsShopAccount>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账户列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺账户")
    @ApiOperation("保存店铺账户")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:create')")
    public Object saveShmsShopAccount(@RequestBody ShmsShopAccount entity) {
        try {
            entity.setCreateTime(new Date());
            if (IShmsShopAccountService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺账户：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺账户")
    @ApiOperation("更新店铺账户")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:update')")
    public Object updateShmsShopAccount(@RequestBody ShmsShopAccount entity) {
        try {
            if (IShmsShopAccountService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺账户：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺账户")
    @ApiOperation("删除店铺账户")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:delete')")
    public Object deleteShmsShopAccount(@ApiParam("店铺账户id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账户id");
            }
            if (IShmsShopAccountService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺账户：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺账户分配店铺账户")
    @ApiOperation("查询店铺账户明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:read')")
    public Object getShmsShopAccountById(@ApiParam("店铺账户id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账户id");
            }
            ShmsShopAccount coupon = IShmsShopAccountService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺账户明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺账户")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺账户")
    @PreAuthorize("hasAuthority('shms:shmsShopAccount:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopAccountService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopAccount entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopAccount> personList = IShmsShopAccountService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopAccount.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopAccount> personList = EasyPoiUtils.importExcel(file, ShmsShopAccount.class);
        IShmsShopAccountService.saveBatch(personList);
    }
}


