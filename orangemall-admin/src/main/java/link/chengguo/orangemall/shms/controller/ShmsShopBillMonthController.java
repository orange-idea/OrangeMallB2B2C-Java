package link.chengguo.orangemall.shms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.AllEnum;
import link.chengguo.orangemall.shms.entity.ShmsShopBillMonth;
import link.chengguo.orangemall.shms.service.IShmsShopBillMonthService;
import link.chengguo.orangemall.sys.entity.SysUser;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.util.UserUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-18
 * 店铺账单月统计
 */
@Slf4j
@RestController
@Api(tags = "ShmsShopBillMonthController ", description = "店铺账单月统计")
@RequestMapping("/shms/shmsShopBillMonth")
public class ShmsShopBillMonthController {

    @Resource
    private IShmsShopBillMonthService IShmsShopBillMonthService;

    @SysLog(MODULE = "shms", REMARK = "根据条件查询所有店铺账单月统计列表")
    @ApiOperation("根据条件查询所有店铺账单月统计列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:read')")
    public Object getShmsShopBillMonthByPage(ShmsShopBillMonth entity,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            if(UserUtils.getCurrentMember().getPlatform().equals(AllEnum.PlatformType.Agent.code())){
                SysUser user= UserUtils.getCurrentMember();
                entity.setCityCode(user.getCityCode());
            }
            //查询自己区域的
            SysUser user= UserUtils.getCurrentMember();
            entity.setCityCode(user.getCityCode());
            // 查询当月
            if (!ValidatorUtils.isEmpty(entity.getStartTime())){
                //如2020  添加日期为2020-01
                String startTime=entity.getStartTime()+"-01";
                int next=Integer.parseInt(entity.getStartTime())+1;
                String endTime=next+"-01";
                entity.setStartTime(startTime);
                entity.setEndTime(endTime);
            }else{
                entity.setStartTime(null);
                entity.setEndTime(null);
            }
            return new CommonResult().success(IShmsShopBillMonthService.getMonthBillByMonth(pageNum,pageSize,entity));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有店铺账单月统计列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "保存店铺账单月统计")
    @ApiOperation("保存店铺账单月统计")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:create')")
    public Object saveShmsShopBillMonth(@RequestBody ShmsShopBillMonth entity) {
        try {

            if (IShmsShopBillMonthService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存店铺账单月统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "更新店铺账单月统计")
    @ApiOperation("更新店铺账单月统计")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:update')")
    public Object updateShmsShopBillMonth(@RequestBody ShmsShopBillMonth entity) {
        try {
            if (IShmsShopBillMonthService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新店铺账单月统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "删除店铺账单月统计")
    @ApiOperation("删除店铺账单月统计")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:delete')")
    public Object deleteShmsShopBillMonth(@ApiParam("店铺账单月统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单月统计id");
            }
            if (IShmsShopBillMonthService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除店铺账单月统计：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "shms", REMARK = "给店铺账单月统计分配店铺账单月统计")
    @ApiOperation("查询店铺账单月统计明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:read')")
    public Object getShmsShopBillMonthById(@ApiParam("店铺账单月统计id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("店铺账单月统计id");
            }
            ShmsShopBillMonth coupon = IShmsShopBillMonthService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询店铺账单月统计明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除店铺账单月统计")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "shms", REMARK = "批量删除店铺账单月统计")
    @PreAuthorize("hasAuthority('shms:shmsShopBillMonth:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = IShmsShopBillMonthService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "shms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, ShmsShopBillMonth entity) {
        // 模拟从数据库获取需要导出的数据
        List<ShmsShopBillMonth> personList = IShmsShopBillMonthService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", ShmsShopBillMonth.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "shms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<ShmsShopBillMonth> personList = EasyPoiUtils.importExcel(file, ShmsShopBillMonth.class);
        IShmsShopBillMonthService.saveBatch(personList);
    }
}


