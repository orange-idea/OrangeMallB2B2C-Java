package link.chengguo.orangemall.shms.service;

import link.chengguo.orangemall.shms.entity.ShmsShopCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.shms.vo.ShmsShopCategoryWithChildrenItem;

import java.util.List;

/**
* @author yzb
* @date 2019-12-18
*/

public interface IShmsShopCategoryService extends IService<ShmsShopCategory> {

    /**
     * 查询一级带子分类的
     * @return
     */
    List<ShmsShopCategoryWithChildrenItem> listWithChildren(ShmsShopCategory param);

}
