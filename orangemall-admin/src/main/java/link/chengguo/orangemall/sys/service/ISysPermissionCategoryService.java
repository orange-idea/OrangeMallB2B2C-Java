package link.chengguo.orangemall.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.sys.entity.SysPermissionCategory;

/**
 * 权限类别表
 *
 * @author chengguo
 * @email 1264395832@qq.com
 * @date 2019-04-27 18:52:51
 */

public interface ISysPermissionCategoryService extends IService<SysPermissionCategory> {

}
