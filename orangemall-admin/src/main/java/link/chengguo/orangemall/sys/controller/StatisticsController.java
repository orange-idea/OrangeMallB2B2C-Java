package link.chengguo.orangemall.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.fms.entity.FmsMemberRecharge;
import link.chengguo.orangemall.fms.service.IFmsMemberRechargeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 钟业海
 * @Date: 2020/8/22 11:46
 */
@Slf4j
@RestController
@RequestMapping("/sys/statistics")
public class StatisticsController {

    @Autowired
    private IFmsMemberRechargeService rechargeService;

    @SysLog(MODULE = "sys", REMARK = "充值统计")
    @ApiOperation("充值统计数据接口")
    @PreAuthorize("hasAuthority('sys:statistics:rechargeStatistics')")
    @GetMapping(value = "/rechargeStatistics")
    public Object rechargeStatistics(LocalDate startDate,LocalDate endDate){
        List<Map<String, Object>> maps = rechargeService.listMaps(new QueryWrapper<FmsMemberRecharge>().select("recharge_type,count(*) as count").groupBy("recharge_type").eq("recharge_status",1).between("create_time",startDate,endDate));
        List<Map<String, Object>> dateMaps = rechargeService.listMaps(new QueryWrapper<FmsMemberRecharge>().select("ifnull(sum(money),0) as sum,DATE_FORMAT(create_time,'%Y-%m-%d') as time").groupBy("recharge_type").eq("recharge_status",1).between("create_time",startDate,endDate));
        BigDecimal sum = (BigDecimal) rechargeService.getMap(new QueryWrapper<FmsMemberRecharge>().select("ifnull(sum(money),0) as sum").eq("recharge_status",1).between("create_time",startDate,endDate)).get("sum");
        Map map = new LinkedHashMap(3);
        map.put("maps",maps);
        map.put("dateMaps",dateMaps);
        map.put("sum",sum);
        return map;
    }


}
