package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysMemberArea;
import link.chengguo.orangemall.sys.mapper.SysMemberAreaMapper;
import link.chengguo.orangemall.sys.service.ISysMemberAreaService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-04-14
 */
@Service
public class SysMemberAreaServiceImpl extends ServiceImpl<SysMemberAreaMapper, SysMemberArea> implements ISysMemberAreaService {

}
