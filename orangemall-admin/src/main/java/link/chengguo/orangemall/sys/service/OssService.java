package link.chengguo.orangemall.sys.service;


import link.chengguo.orangemall.sys.vo.OssCallbackResult;
import link.chengguo.orangemall.sys.vo.OssPolicyResult;

import javax.servlet.http.HttpServletRequest;

/**
 * oss上传管理Service
 * https://github.com/orangemall on 2018/5/17.
 */
public interface OssService {
    OssPolicyResult policy();

    OssCallbackResult callback(HttpServletRequest request);
}
