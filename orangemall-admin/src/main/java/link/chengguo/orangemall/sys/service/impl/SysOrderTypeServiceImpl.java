package link.chengguo.orangemall.sys.service.impl;

import link.chengguo.orangemall.sys.entity.SysOrderType;
import link.chengguo.orangemall.sys.mapper.SysOrderTypeMapper;
import link.chengguo.orangemall.sys.service.ISysOrderTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
* @author yzb
* @date 2019-12-26
*/
@Service
public class SysOrderTypeServiceImpl extends ServiceImpl
<SysOrderTypeMapper, SysOrderType> implements ISysOrderTypeService {

@Resource
private  SysOrderTypeMapper sysOrderTypeMapper;


}
