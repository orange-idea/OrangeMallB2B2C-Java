package link.chengguo.orangemall.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.sys.entity.SysSmsConfig;
import link.chengguo.orangemall.sys.service.SysSmsConfigService;
import link.chengguo.orangemall.utils.CommonResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:09
 */
@RestController
@RequestMapping("/sys/sysSmsConfig")
public class SysSmsConfigController {
    @Resource
    private SysSmsConfigService configService;


    @GetMapping
    @PreAuthorize("hasAuthority('sys:sysSmsConfig:read')")
    public Object get() {
        SysSmsConfig one = configService.getOne(new QueryWrapper<>());
        return new CommonResult().success(one);
    }


    @PostMapping(value = "/update")
    @PreAuthorize("hasAuthority('sys:sysSmsConfig:update')")
    @ApiOperation("修改配置")
    public Object updateConfig(@RequestBody SysSmsConfig config) {
        return new CommonResult().success(configService.saveOrUpdate(config));
    }
}
