package link.chengguo.orangemall.sys.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.sys.entity.SysOrderType;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yzb
 * @date 2019-12-26
 * 配送模式
 */
@Slf4j
@RestController
@RequestMapping("/cms/sysOrderType")
public class SysOrderTypeController {

    @Resource
    private link.chengguo.orangemall.sys.service.ISysOrderTypeService ISysOrderTypeService;

    @SysLog(MODULE = "cms", REMARK = "根据条件查询所有 配送模式列表")
    @ApiOperation("根据条件查询所有 配送模式列表")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('cms:sysOrderType:read')")
    public Object getSysOrderTypeByPage(SysOrderType entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(ISysOrderTypeService.page(new Page<SysOrderType>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("id")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有 配送模式列表：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "保存 配送模式")
    @ApiOperation("保存 配送模式")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('cms:sysOrderType:create')")
    public Object saveSysOrderType(@RequestBody SysOrderType entity) {
        try {

            if (ISysOrderTypeService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存 配送模式：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "更新 配送模式")
    @ApiOperation("更新 配送模式")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('cms:sysOrderType:update')")
    public Object updateSysOrderType(@RequestBody SysOrderType entity) {
        try {
            if (ISysOrderTypeService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新 配送模式：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "删除 配送模式")
    @ApiOperation("删除 配送模式")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('cms:sysOrderType:delete')")
    public Object deleteSysOrderType(@ApiParam(" 配送模式id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed(" 配送模式id");
            }
            if (ISysOrderTypeService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除 配送模式：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "cms", REMARK = "给 配送模式分配 配送模式")
    @ApiOperation("查询 配送模式明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('cms:sysOrderType:read')")
    public Object getSysOrderTypeById(@ApiParam(" 配送模式id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed(" 配送模式id");
            }
            SysOrderType coupon = ISysOrderTypeService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询 配送模式明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除 配送模式")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "cms", REMARK = "批量删除 配送模式")
    @PreAuthorize("hasAuthority('cms:sysOrderType:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = ISysOrderTypeService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "cms", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, SysOrderType entity) {
        // 模拟从数据库获取需要导出的数据
        List<SysOrderType> personList = ISysOrderTypeService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", SysOrderType.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "cms", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<SysOrderType> personList = EasyPoiUtils.importExcel(file, SysOrderType.class);
        ISysOrderTypeService.saveBatch(personList);
    }
}


