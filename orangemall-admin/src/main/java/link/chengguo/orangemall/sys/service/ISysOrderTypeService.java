package link.chengguo.orangemall.sys.service;

import link.chengguo.orangemall.sys.entity.SysOrderType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author yzb
* @date 2019-12-26
*/

public interface ISysOrderTypeService extends IService<SysOrderType> {

}
