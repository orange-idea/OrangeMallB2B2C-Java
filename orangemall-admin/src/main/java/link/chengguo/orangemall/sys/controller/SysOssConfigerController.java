package link.chengguo.orangemall.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.sys.entity.SysOssConfig;
import link.chengguo.orangemall.sys.service.SysOssConfigService;
import link.chengguo.orangemall.utils.CommonResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:22
 */
@RestController
@RequestMapping("/sys/sysOssConfig")
public class SysOssConfigerController {
    @Resource
    private SysOssConfigService ossConfigService;


    @GetMapping
    @PreAuthorize("hasAuthority('sys:sysOssConfig:read')")
    public Object get() {
        return new CommonResult().success(ossConfigService.getOne(new QueryWrapper<>()));
    }


    @PostMapping(value = "/update")
    @PreAuthorize("hasAuthority('sys:sysOssConfig:update')")
    @ApiOperation("修改配置")
    public Object updateConfig(@RequestBody SysOssConfig ossConfig) {
        return new CommonResult().success(ossConfigService.saveOrUpdate(ossConfig));
    }
}
