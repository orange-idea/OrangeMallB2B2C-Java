package link.chengguo.orangemall.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.sys.entity.SysHomeMenu;
import link.chengguo.orangemall.sys.service.SysHomeMenuService;
import link.chengguo.orangemall.util.EasyPoiUtils;
import link.chengguo.orangemall.utils.CommonResult;
import link.chengguo.orangemall.utils.ValidatorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Author: 钟业海
 * @Date: 2020/7/18 9:22
 */
@Slf4j
@RestController
@RequestMapping("/sys/SysHomeMenu")
public class SysHomeMenuController {
    @Resource
    private SysHomeMenuService homeMenuService;


    @SysLog(MODULE = "sys", REMARK = "根据条件查询所有首页菜单")
    @ApiOperation("根据条件查询所有首页菜单")
    @GetMapping(value = "/list")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:read')")
    public Object getSysOrderTypeByPage(SysHomeMenu entity,
                                        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        try {
            return new CommonResult().success(homeMenuService.page(new Page<SysHomeMenu>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("sort")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据条件查询所有首页菜单：%s", e.getMessage(), e);
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "保存首页菜单")
    @ApiOperation("保存首页菜单")
    @PostMapping(value = "/create")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:create')")
    public Object saveSysHomeMenu(@RequestBody SysHomeMenu entity) {
        try {

            if (homeMenuService.save(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存首页菜单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "更新首页菜单")
    @ApiOperation("更新首页菜单")
    @PostMapping(value = "/update/{id}")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:update')")
    public Object updateSysHomeMenu(@RequestBody SysHomeMenu entity) {
        try {
            if (homeMenuService.updateById(entity)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("更新首页菜单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "删除首页菜单")
    @ApiOperation("删除首页菜单")
    @GetMapping(value = "/delete/{id}")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:delete')")
    public Object deleteSysHomeMenu(@ApiParam("首页菜单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("首页菜单id");
            }
            if (homeMenuService.removeById(id)) {
                return new CommonResult().success();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除首页菜单：%s", e.getMessage(), e);
            return new CommonResult().failed(e.getMessage());
        }
        return new CommonResult().failed();
    }

    @SysLog(MODULE = "sys", REMARK = "查询首页菜单明细")
    @ApiOperation("查询首页菜单明细")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:read')")
    public Object getSysHomeMenuById(@ApiParam("首页菜单id") @PathVariable Long id) {
        try {
            if (ValidatorUtils.empty(id)) {
                return new CommonResult().paramFailed("首页菜单id");
            }
            SysHomeMenu coupon = homeMenuService.getById(id);
            return new CommonResult().success(coupon);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询首页菜单明细：%s", e.getMessage(), e);
            return new CommonResult().failed();
        }

    }

    @ApiOperation(value = "批量删除首页菜单")
    @RequestMapping(value = "/delete/batch", method = RequestMethod.GET)
    @SysLog(MODULE = "sys", REMARK = "批量删除首页菜单")
    @PreAuthorize("hasAuthority('sys:SysHomeMenu:delete')")
    public Object deleteBatch(@RequestParam("ids") List
            <Long> ids) {
        boolean count = homeMenuService.removeByIds(ids);
        if (count) {
            return new CommonResult().success(count);
        } else {
            return new CommonResult().failed();
        }
    }


    @SysLog(MODULE = "sys", REMARK = "导出社区数据")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response, SysHomeMenu entity) {
        // 模拟从数据库获取需要导出的数据
        List<SysHomeMenu> personList = homeMenuService.list(new QueryWrapper<>(entity).orderByDesc("id"));
        // 导出操作
        EasyPoiUtils.exportExcel(personList, "导出社区数据", "社区数据", SysHomeMenu.class, "导出社区数据.xls", response);

    }

    @SysLog(MODULE = "sys", REMARK = "导入社区数据")
    @PostMapping("/importExcel")
    public void importUsers(@RequestParam MultipartFile file) {
        List<SysHomeMenu> personList = EasyPoiUtils.importExcel(file, SysHomeMenu.class);
        homeMenuService.saveBatch(personList);
    }
}
