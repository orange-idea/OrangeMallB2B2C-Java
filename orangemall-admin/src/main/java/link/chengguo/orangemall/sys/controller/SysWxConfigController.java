package link.chengguo.orangemall.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import link.chengguo.orangemall.sys.entity.SysWxConfig;
import link.chengguo.orangemall.sys.service.SysWxConfigService;
import link.chengguo.orangemall.utils.CommonResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Author: 钟业海
 * @Date: 2020/7/21 10:09
 */
@ApiOperation("微信配置")
@RestController
@RequestMapping("/sys/sysWxConfig")
public class SysWxConfigController {
    @Resource
    private SysWxConfigService configService;


    @GetMapping
    @PreAuthorize("hasAuthority('sys:sysWxConfig:read')")
    public Object get() {
        SysWxConfig one = configService.getOne(new QueryWrapper<>());
        return new CommonResult().success(one);
    }


    @PostMapping(value = "/update")
    @PreAuthorize("hasAuthority('sys:sysWxConfig:update')")
    @ApiOperation("修改配置")
    public Object updateConfig(@RequestBody SysWxConfig config) {
        return new CommonResult().success(configService.saveOrUpdate(config));
    }
}
