package link.chengguo.orangemall.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.sys.entity.SysQiniuContent;
import link.chengguo.orangemall.sys.mapper.SysQiniuContentMapper;
import link.chengguo.orangemall.sys.service.ISysQiniuContentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author chengguo
 * @since 2019-11-30
 */
@Service
public class SysQiniuContentServiceImpl extends ServiceImpl<SysQiniuContentMapper, SysQiniuContent> implements ISysQiniuContentService {

}
