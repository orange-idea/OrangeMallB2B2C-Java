package link.chengguo.orangemall.pay.controller.alipay;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayFundCouponOrderAgreementPayModel;
import com.alipay.api.domain.AlipayFundTransToaccountTransferModel;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import link.chengguo.orangemall.alipay.AliPayApi;
import link.chengguo.orangemall.alipay.AliPayApiConfig;
import link.chengguo.orangemall.alipay.AliPayBean;
import link.chengguo.orangemall.annotation.SysLog;
import link.chengguo.orangemall.enums.PayStatus;
import link.chengguo.orangemall.fms.service.IFmsMemberRechargeService;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.entity.OmsPayments;
import link.chengguo.orangemall.oms.service.IOmsOrderItemService;
import link.chengguo.orangemall.oms.service.IOmsOrderService;
import link.chengguo.orangemall.oms.service.IOmsPaymentsService;
import link.chengguo.orangemall.pay.service.PayService;
import link.chengguo.orangemall.pay.utils.StringUtils;
import link.chengguo.orangemall.ums.service.IUmsMemberService;
import link.chengguo.orangemall.util.JsonUtils;
import link.chengguo.orangemall.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>IJPay 让支付触手可及，封装了微信支付、支付宝支付、银联支付常用的支付方式以及各种常用的接口。</p>
 *
 * <p>不依赖任何第三方 mvc 框架，仅仅作为工具使用简单快速完成支付模块的开发，可轻松嵌入到任何系统里。 </p>
 *
 * <p>IJPay 交流群: 723992875</p>
 *
 * <p>Node.js 版: https://gitee.com/javen205/TNW</p>
 *
 * <p>支付宝支付 Demo</p>
 *
 * @author Javen
 */
@RestController
@Api(tags = "支付宝支付相关接口", description = "AliPayController")
@RequestMapping("/api/aliPay")
public class AliPayController extends AbstractAliPayApiController {
    private static final Logger log = LoggerFactory.getLogger(AliPayController.class);
    String domain = "";
    String privateKey = "";
    String publicKey = "";
    String appId = "";
    @Resource
    private IOmsOrderService orderService;
    @Resource
    private IUmsMemberService memberService;
    @Resource
    private IOmsOrderItemService orderItemService;
    @Autowired
    private IOmsPaymentsService paymentsService;
    @Resource
    private PayService payService;
    @Resource
    private IFmsMemberRechargeService memberRechargeService;

    @Override
    public AliPayApiConfig getApiConfig() {
        AliPayApiConfig aliPayApiConfig = null;
        try {
            //aliPayApiConfig = AliPayApiConfigKit.getApiConfig(aliPayBean.getAppId());
            OmsPayments payments = paymentsService.getById(2);
            if (payments != null) {
                AliPayBean aliPayBean = JsonUtils.jsonToPojo(payments.getParams(), AliPayBean.class);
                aliPayApiConfig = AliPayApiConfig.builder()
                        .setAppId(aliPayBean.getAppId())
                        .setAliPayPublicKey(aliPayBean.getPublicKey())
                        .setCharset("UTF-8")
                        .setPrivateKey(aliPayBean.getPrivateKey())
                        .setServiceUrl(aliPayBean.getServerUrl())
                        .setSignType("RSA2")
                        .build();
                domain = aliPayBean.getDomain();
                privateKey = aliPayBean.getPrivateKey();
                publicKey = aliPayBean.getPublicKey();
                appId = aliPayBean.getAppId();
            }

        } catch (Exception e) {
             AliPayBean aliPayBean = new AliPayBean();
            aliPayBean.setAppId("2017010804923732");
            aliPayBean.setDomain("http://www.yjlive.cn/api/api");
            aliPayBean.setServerUrl("https://openapi.alipay.com/gateway.do");
            aliPayBean.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzQsSWsgXJO7HTWLd/8Y9de6kPBlGqvnBCdL6N8pbg9TZ5LDQOpPxef940nY/dHQiBw61bVZQULSps2mhOs7xjebhEJfhXiGV+aZBjacxr+qJ4EpM/pjH3MrfA8IB5MpB9OFEeOTGos3FMzeQHPiqeeDAIDEFs4fO112/3OWfCD6rLI88H0FoDqZ4oSsAkniFZAW1IjwW9Whgicgo4v3IjcWV+k4eFCSCORpnNKjLbsco0qJic1FaHqbkccnpW8/40j/Vo/ZZG/qCDyZ/Lt7+OKDgO8dzelFfG/IoAuEMGsgb26tCAZMVyjMxXUgLrqnTESAx6121pqy1fiwyMC6cRwIDAQAB\n");
            aliPayBean.setPrivateKey("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNCxJayBck7sdNYt3/xj117qQ8GUaq+cEJ0vo3yluD1NnksNA6k/F5/3jSdj90dCIHDrVtVlBQtKmzaaE6zvGN5uEQl+FeIZX5pkGNpzGv6ongSkz+mMfcyt8DwgHkykH04UR45MaizcUzN5Ac+Kp54MAgMQWzh87XXb/c5Z8IPqssjzwfQWgOpnihKwCSeIVkBbUiPBb1aGCJyCji/ciNxZX6Th4UJII5Gmc0qMtuxyjSomJzUVoepuRxyelbz/jSP9Wj9lkb+oIPJn8u3v44oOA7x3N6UV8b8igC4QwayBvbq0IBkxXKMzFdSAuuqdMRIDHrXbWmrLV+LDIwLpxHAgMBAAECggEACYIM7NbAc/76kPUXtEeeC/zv8rV9WGpScEEvRy0EB130aK1mSoEXvn+BO8kt8hl8hnVBJnvNJ6DpCZ/JUS/NdbYSE7HnSnUmPjhea9In9K9ci2EGpvuwsOVbaBI0Akb6vf9ALJb3Ow9tqI1YCm/hf9tTLWr4h7Wxer0nK3geYsRn6O92AKFYjxvImR/qj9sr2DNHg83lX/2YkdDuxhLWF8oTZzunhqvEWo17mEcCrFpx/vY3bME+ZMG/IAtp89PFXXsNHii4nI0buR8mx4z6CgIetgL0qFJUeMUir1ZKn+uAyy9Jv1V9Bu7R07LXsjlZlA/xnew5ie/42iyGcusYiQKBgQDvRiPWCX0eEe4rVcPHAZJ1d/jsOGwvhzmE4TUQdnjVU0bKdgY4hHS9BrvjCTAWmzEE0siS85inzQj26DGGNr5U+D0HYfTEymNQNBMLin/ApLvNPL7xzFdpA19sVvatVV+c5Vl9JaVGuBraK69Q7Cz/6OQwXU1NGQWohZhOyMX6MwKBgQDbYF2BM7npaXFQVbigGmXgdvLHeWdZag7M4dB0lHsqGWAdtQzIIn05q9rBWNMusEfal/eZKuvmoXaDquh/g0VDCmmxxIE4OS9j37g64/4QbWJxwM8rDzA6Z58peng7CUse9Pb3Q/F8JQ/EvniEa6JT1qXWGWhQcpGsACCZEYYpnQKBgGQPAsFo6md+vAhnLx2zbJmu9+tglO0zMTx+KQCfalxbHMlhnaxYx7Ccdkm09+UcNN19f97j+zyAo3UNGFi139YMkQjbT85TjEBn5mb3HgFjYh2rf3YCK7OAc5EMtM87WmZ0Cn4pFfqC1sfRaNkASrkhnPsUqVTKV/FnHJAlqZS9AoGBAJgAKCmajolEzwerrXX5dHdX05YU72AL1V9uY0IzkzczR96tkMKm6v9nrPXktsaVy+ORAjS1gahWXciTRe78JKRz9ZH/ps0vCj/4Ri0/xczaDajlwGWEa5U8MRLLUb0ODmfPscLX591tzIQ0uUp/TYUrp9I13opHJ9n2aJ/GfaAdAoGARUzil6jIO3mASNaH7MPR4MqvxMO0LuBwaVxR1mvM7GtDDDYWU3fTJ6lFpyr340cYgEmHAVnLezbLmP75Jqo42j7H5V3BplPITSSik9ti3WOHFlPRYsZBewL7cJb4VX5oRrbfOX8to9wfw2TvofHE82NDtQn9fQUoFpqKlkIraL4=\n");
            aliPayApiConfig = AliPayApiConfig.builder()
                    .setAppId(aliPayBean.getAppId())
                    .setAliPayPublicKey(aliPayBean.getPublicKey())
                    .setCharset("UTF-8")
                    .setPrivateKey(aliPayBean.getPrivateKey())
                    .setServiceUrl(aliPayBean.getServerUrl())
                    .setSignType("RSA2")
                    .build();
            domain = aliPayBean.getDomain();
            privateKey = aliPayBean.getPrivateKey();
            publicKey = aliPayBean.getPublicKey();
            appId = aliPayBean.getAppId();
        }
        return aliPayApiConfig;

    }


    /**
     * 单笔转账到支付宝账户
     */
    @SysLog(MODULE = "alipay", REMARK = "单笔转账到支付宝账户")
    @ApiOperation("单笔转账到支付宝账户")
    @PostMapping(value = "/transfer")
    @ResponseBody
    public boolean transfer( @RequestParam(value = "money") String money,
                             @RequestParam(value = "account") String account,
                             @RequestParam(value = "realName") String realName,
                             @RequestParam(value = "note") String note
                             ) {

        boolean isSuccess = false;
        AlipayFundTransToaccountTransferModel model = new AlipayFundTransToaccountTransferModel();
        model.setOutBizNo(StringUtils.getOutTradeNo());
        model.setPayeeType("ALIPAY_LOGONID");
        model.setAmount(money);
        model.setPayeeAccount(account);
        model.setPayeeRealName(realName);
        model.setPayerShowName("华盛配送平台");
        model.setRemark(note);
        try {
            isSuccess = AliPayApi.transfer(model,this.getApiConfig());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }



    /**
     * 红包协议支付接口
     * https://docs.open.alipay.com/301/106168/
     */
//    @RequestMapping(value = "/agreementPay")
//    @ResponseBody
    public Object agreementPay() {
        try {
            AlipayFundCouponOrderAgreementPayModel model = new AlipayFundCouponOrderAgreementPayModel();
            model.setOutOrderNo(StringUtils.getOutTradeNo());
            model.setOutRequestNo(StringUtils.getOutTradeNo());
            model.setOrderTitle("红包协议支付接口-By IJPay");
            model.setAmount("36");
            model.setPayerUserId("2088102180432465");

            return new CommonResult().success(AliPayApi.fundCouponOrderAgreementPayToResponse(model,this.getApiConfig()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 退款
     */
    @RequestMapping(value = "/tradeRefund", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Object tradeRefund() {
        try {
            AlipayTradeRefundModel model = new AlipayTradeRefundModel();
            model.setOutTradeNo("081014283315023");
            model.setTradeNo("2017081021001004200200273870");
            model.setRefundAmount("86.00");
            model.setRefundReason("正常退款");
            return new CommonResult().success(AliPayApi.tradeRefundToResponse(model,this.getApiConfig()).getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 交易查询
     */
    @RequestMapping(value = "/tradeQuery", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public boolean tradeQuery() {
        boolean isSuccess = false;
        try {
            AlipayTradeQueryModel model = new AlipayTradeQueryModel();
            model.setOutTradeNo("081014283315023");
            model.setTradeNo("2017081021001004200200273870");

            isSuccess = AliPayApi.tradeQueryToResponse(model,this.getApiConfig()).isSuccess();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    @RequestMapping(value = "/tradeQueryByStr", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Object tradeQueryByStr(@RequestParam("out_trade_no") String outTradeNo) {
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        model.setOutTradeNo(outTradeNo);

        try {
            return new CommonResult().success(AliPayApi.tradeQueryToResponse(model,this.getApiConfig()).getBody());
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }

        return null;
    }



    /**
     * 批量付款到支付宝账户有密接口
     */
//    @RequestMapping(value = "/batchTrans")
//    @ResponseBody
    public void batchTrans(HttpServletResponse response) {
        try {
            String signType = "MD5";
            String notifyUrl = domain + "/aliPay/notify_url";
            Map<String, String> params = new HashMap<>(15);
            params.put("partner", "PID");
            params.put("sign_type", signType);
            params.put("notify_url", notifyUrl);
            params.put("account_name", "xxx");
            params.put("detail_data", "流水号1^收款方账号1^收款账号姓名1^付款金额1^备注说明1|流水号2^收款方账号2^收款账号姓名2^付款金额2^备注说明2");
            params.put("batch_no", String.valueOf(System.currentTimeMillis()));
            params.put("batch_num", 1 + "");
            params.put("batch_fee", 10.00 + "");
            params.put("email", "xx@xxx.com");

            AliPayApi.batchTrans(params, privateKey, signType, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequestMapping(value = "/return_url")
    @ResponseBody
    public Object returnUrl(HttpServletRequest request) {
        try {
            // 获取支付宝GET过来反馈信息
            Map<String, String> map = AliPayApi.toMap(request);
            System.out.println("return_url");
            System.out.println(map.toString());
            for (Map.Entry<String, String> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }

            boolean verifyResult = AlipaySignature.rsaCheckV1(map, publicKey, "UTF-8",
                    "RSA2");

            if (verifyResult) {
                // TODO 请在这里加上商户的业务逻辑程序代码
                System.out.println("return_url 验证成功");

                return new CommonResult().success();
            } else {
                System.out.println("return_url 验证失败");
                // TODO
                return new CommonResult().failed();
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "failure";
        }
    }


    @RequestMapping(value = "/notify_url")
    @ResponseBody
    public Object notifyUrl(HttpServletRequest request) {
        try {
            // 获取支付宝POST过来反馈信息
            Map<String, String> params = AliPayApi.toMap(request);
            System.out.println("notify_url");
            System.out.println(params.toString());
            for (Map.Entry<String, String> entry : params.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }
            String out_trade_no = params.get("out_trade_no");
            OmsOrder param = new OmsOrder();
            param.setOrderSn(out_trade_no);
            OmsOrder omsOrder=orderService.getOne(new QueryWrapper<>(param));
            //订单存在且未支付
            if(omsOrder!=null && omsOrder.getPayStatus().intValue()!= PayStatus.Paid.getValue()){
                boolean verifyResult = AlipaySignature.rsaCheckV1(params, publicKey, "UTF-8", "RSA2");
                if (verifyResult) {
                    // TODO 请在这里加上商户的业务逻辑程序代码 异步通知可能出现订单重复通知 需要做去重处理
                    boolean result=true;
                    System.out.println("notify_url 验证成功succcess");
                    if (result){
                        return new CommonResult().success();
                    }
                } else {
                    log.error("订单" + out_trade_no + "支付失败");
                    // TODO
                    return new CommonResult().failed();
                }
            }

        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "failure";
        }
        return new CommonResult().failed();
    }

}
