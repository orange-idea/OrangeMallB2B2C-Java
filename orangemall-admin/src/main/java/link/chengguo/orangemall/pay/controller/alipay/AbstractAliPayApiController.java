package link.chengguo.orangemall.pay.controller.alipay;


import link.chengguo.orangemall.alipay.AliPayApiConfig;

/**
 * @author Javen
 */
public abstract class AbstractAliPayApiController {
    /**
     * 获取支付宝配置
     *
     * @return {@link AliPayApiConfig} 支付宝配置
     */
    public abstract AliPayApiConfig getApiConfig();
}
