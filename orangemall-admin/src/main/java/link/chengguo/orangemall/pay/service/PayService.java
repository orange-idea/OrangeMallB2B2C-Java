package link.chengguo.orangemall.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import link.chengguo.orangemall.oms.entity.OmsOrder;

public interface PayService extends IService<OmsOrder>{


    /**
     * 支付宝转账到个人账户
     * @param money
     * @param account
     * @param realName
     * @param note
     * @return
     */
    boolean transferByAli(String money,String account,String realName, String note);


    /**
     * 微信转账到个人账户
     * @param money
     * @param account
     * @param realName
     * @param note
     * @return
     */
    boolean transferByWx(String money,String account,String realName, String note);


}
