package link.chengguo.orangemall.pay.service.impl;

import com.alipay.api.domain.AlipayFundTransToaccountTransferModel;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import link.chengguo.orangemall.alipay.AliPayApi;
import link.chengguo.orangemall.alipay.AliPayApiConfig;
import link.chengguo.orangemall.alipay.AliPayBean;
import link.chengguo.orangemall.oms.entity.OmsOrder;
import link.chengguo.orangemall.oms.entity.OmsPayments;
import link.chengguo.orangemall.oms.mapper.OmsOrderMapper;
import link.chengguo.orangemall.oms.service.IOmsPaymentsService;
import link.chengguo.orangemall.pay.controller.alipay.AliPayController;
import link.chengguo.orangemall.pay.service.PayService;
import link.chengguo.orangemall.pay.utils.StringUtils;
import link.chengguo.orangemall.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayServiceImpl extends ServiceImpl<OmsOrderMapper, OmsOrder> implements PayService {

    private static final Logger log = LoggerFactory.getLogger(AliPayController.class);
    String domain = "";
    String privateKey = "";
    String publicKey = "";
    String appId = "";
    @Autowired
    private IOmsPaymentsService paymentsService;

    private AliPayApiConfig getApiConfig() {
        AliPayApiConfig aliPayApiConfig = null;
        try {
            //aliPayApiConfig = AliPayApiConfigKit.getApiConfig(aliPayBean.getAppId());
            OmsPayments payments = paymentsService.getById(2);
            if (payments != null) {
                AliPayBean aliPayBean = JsonUtils.jsonToPojo(payments.getParams(), AliPayBean.class);
                aliPayApiConfig = AliPayApiConfig.builder()
                        .setAppId(aliPayBean.getAppId())
                        .setAliPayPublicKey(aliPayBean.getPublicKey())
                        .setCharset("UTF-8")
                        .setPrivateKey(aliPayBean.getPrivateKey())
                        .setServiceUrl(aliPayBean.getServerUrl())
                        .setSignType("RSA2")
                        .build();
                domain = aliPayBean.getDomain();
                privateKey = aliPayBean.getPrivateKey();
                publicKey = aliPayBean.getPublicKey();
                appId = aliPayBean.getAppId();
            }

        } catch (Exception e) {
            AliPayBean aliPayBean = new AliPayBean();
            aliPayBean.setAppId("2017010804923732");
            aliPayBean.setDomain("http://www.yjlive.cn/api/api");
            aliPayBean.setServerUrl("https://openapi.alipay.com/gateway.do");
            aliPayBean.setPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzQsSWsgXJO7HTWLd/8Y9de6kPBlGqvnBCdL6N8pbg9TZ5LDQOpPxef940nY/dHQiBw61bVZQULSps2mhOs7xjebhEJfhXiGV+aZBjacxr+qJ4EpM/pjH3MrfA8IB5MpB9OFEeOTGos3FMzeQHPiqeeDAIDEFs4fO112/3OWfCD6rLI88H0FoDqZ4oSsAkniFZAW1IjwW9Whgicgo4v3IjcWV+k4eFCSCORpnNKjLbsco0qJic1FaHqbkccnpW8/40j/Vo/ZZG/qCDyZ/Lt7+OKDgO8dzelFfG/IoAuEMGsgb26tCAZMVyjMxXUgLrqnTESAx6121pqy1fiwyMC6cRwIDAQAB\n");
            aliPayBean.setPrivateKey("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNCxJayBck7sdNYt3/xj117qQ8GUaq+cEJ0vo3yluD1NnksNA6k/F5/3jSdj90dCIHDrVtVlBQtKmzaaE6zvGN5uEQl+FeIZX5pkGNpzGv6ongSkz+mMfcyt8DwgHkykH04UR45MaizcUzN5Ac+Kp54MAgMQWzh87XXb/c5Z8IPqssjzwfQWgOpnihKwCSeIVkBbUiPBb1aGCJyCji/ciNxZX6Th4UJII5Gmc0qMtuxyjSomJzUVoepuRxyelbz/jSP9Wj9lkb+oIPJn8u3v44oOA7x3N6UV8b8igC4QwayBvbq0IBkxXKMzFdSAuuqdMRIDHrXbWmrLV+LDIwLpxHAgMBAAECggEACYIM7NbAc/76kPUXtEeeC/zv8rV9WGpScEEvRy0EB130aK1mSoEXvn+BO8kt8hl8hnVBJnvNJ6DpCZ/JUS/NdbYSE7HnSnUmPjhea9In9K9ci2EGpvuwsOVbaBI0Akb6vf9ALJb3Ow9tqI1YCm/hf9tTLWr4h7Wxer0nK3geYsRn6O92AKFYjxvImR/qj9sr2DNHg83lX/2YkdDuxhLWF8oTZzunhqvEWo17mEcCrFpx/vY3bME+ZMG/IAtp89PFXXsNHii4nI0buR8mx4z6CgIetgL0qFJUeMUir1ZKn+uAyy9Jv1V9Bu7R07LXsjlZlA/xnew5ie/42iyGcusYiQKBgQDvRiPWCX0eEe4rVcPHAZJ1d/jsOGwvhzmE4TUQdnjVU0bKdgY4hHS9BrvjCTAWmzEE0siS85inzQj26DGGNr5U+D0HYfTEymNQNBMLin/ApLvNPL7xzFdpA19sVvatVV+c5Vl9JaVGuBraK69Q7Cz/6OQwXU1NGQWohZhOyMX6MwKBgQDbYF2BM7npaXFQVbigGmXgdvLHeWdZag7M4dB0lHsqGWAdtQzIIn05q9rBWNMusEfal/eZKuvmoXaDquh/g0VDCmmxxIE4OS9j37g64/4QbWJxwM8rDzA6Z58peng7CUse9Pb3Q/F8JQ/EvniEa6JT1qXWGWhQcpGsACCZEYYpnQKBgGQPAsFo6md+vAhnLx2zbJmu9+tglO0zMTx+KQCfalxbHMlhnaxYx7Ccdkm09+UcNN19f97j+zyAo3UNGFi139YMkQjbT85TjEBn5mb3HgFjYh2rf3YCK7OAc5EMtM87WmZ0Cn4pFfqC1sfRaNkASrkhnPsUqVTKV/FnHJAlqZS9AoGBAJgAKCmajolEzwerrXX5dHdX05YU72AL1V9uY0IzkzczR96tkMKm6v9nrPXktsaVy+ORAjS1gahWXciTRe78JKRz9ZH/ps0vCj/4Ri0/xczaDajlwGWEa5U8MRLLUb0ODmfPscLX591tzIQ0uUp/TYUrp9I13opHJ9n2aJ/GfaAdAoGARUzil6jIO3mASNaH7MPR4MqvxMO0LuBwaVxR1mvM7GtDDDYWU3fTJ6lFpyr340cYgEmHAVnLezbLmP75Jqo42j7H5V3BplPITSSik9ti3WOHFlPRYsZBewL7cJb4VX5oRrbfOX8to9wfw2TvofHE82NDtQn9fQUoFpqKlkIraL4=\n");
            aliPayApiConfig = AliPayApiConfig.builder()
                    .setAppId(aliPayBean.getAppId())
                    .setAliPayPublicKey(aliPayBean.getPublicKey())
                    .setCharset("UTF-8")
                    .setPrivateKey(aliPayBean.getPrivateKey())
                    .setServiceUrl(aliPayBean.getServerUrl())
                    .setSignType("RSA2")
                    .build();
            domain = aliPayBean.getDomain();
            privateKey = aliPayBean.getPrivateKey();
            publicKey = aliPayBean.getPublicKey();
            appId = aliPayBean.getAppId();
        }
        return aliPayApiConfig;

    }

    @Override
    public boolean transferByAli(String money, String account, String realName, String note) {
        boolean isSuccess = false;
        AlipayFundTransToaccountTransferModel model = new AlipayFundTransToaccountTransferModel();
        model.setOutBizNo(StringUtils.getOutTradeNo());
        model.setPayeeType("ALIPAY_LOGONID");
        model.setAmount(money);
        model.setPayeeAccount(account);
        model.setPayeeRealName(realName);
        model.setPayerShowName("华盛配送平台");
        model.setRemark(note);
        try {
            isSuccess = AliPayApi.transfer(model,this.getApiConfig());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    @Override
    public boolean transferByWx(String money, String account, String realName, String note) {
        return false;
    }
}
