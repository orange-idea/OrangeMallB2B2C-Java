package link.chengguo.orangemall.search.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatis配置类
 * Created by orangemall on 2019/4/8.
 */
@Configuration
@MapperScan({"link.chengguo.orangemall.search.mapper", "link.chengguo.orangemall.search.dao"})
public class MyBatisConfig {
}
